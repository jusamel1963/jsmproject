// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "JSMGameplayEffect.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UJSMGameplayEffect : public UGameplayEffect
{
	GENERATED_BODY()
	
};
