// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Data/DataAssets/ItemData/JSMWeaponDataAsset.h"
#include "JSMRangeWeaponDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UJSMRangeWeaponDataAsset : public UJSMWeaponDataAsset
{
	GENERATED_BODY()

public:
	UJSMRangeWeaponDataAsset()
	{
		FiringMode = EFiringMode::Single;
		WeaponRangeDistance = 700.f;
		WeaponBulletRadius = 5.f;
		RecoilRate = 0.5f;
		FireInterval = 0.2f;
		MagCapacity = 10;
		BagCapacity = 50;
		FireAmmoCount = 1;
		AmmoCategory = EWeaponAmmoType::None;
		FireSpread = 0;
	}

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RangeWeapon")
	EFiringMode FiringMode;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RangeWeapon")
	float WeaponRangeDistance;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RangeWeapon")
	float WeaponBulletRadius;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RangeWeapon")
	float RecoilRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RangeWeapon")
	float FireInterval;

	//źâ
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RangeWeapon")
	uint8 MagCapacity;

	//���� źâ
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RangeWeapon")
	uint8 BagCapacity;

	//�ѹ� �߻� �Ҷ� �Ҹ�Ǵ� �Ѿ� ��
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RangeWeapon")
	uint8 FireAmmoCount;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RangeWeapon")
	TSoftObjectPtr<class UAnimSequence> FireAnimSequence;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RangeWeapon")
	TSoftObjectPtr<class UAnimSequence> ReloadAnimSequence;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RangeWeapon")
	TSoftObjectPtr<UTexture2D> WeaponScope;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RangeWeapon")
	EWeaponAmmoType AmmoCategory;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "RangeWeapon")
	float FireSpread;

	// AsyncLoad
	virtual TArray<FSoftObjectPath> GetObjectPathsToLoad() override
	{
		TArray<FSoftObjectPath> Paths = Super::GetObjectPathsToLoad();

		if (FireAnimSequence.IsNull() == false)
		{
			if (FireAnimSequence.IsPending())
			{
				Paths.Add(FireAnimSequence.ToSoftObjectPath());
			}
		}

		if (ReloadAnimSequence.IsNull() == false)
		{
			if (ReloadAnimSequence.IsPending())
			{
				Paths.Add(ReloadAnimSequence.ToSoftObjectPath());
			}
		}

		if (WeaponScope.IsNull() == false)
		{
			if (WeaponScope.IsPending())
			{
				Paths.Add(WeaponScope.ToSoftObjectPath());
			}
		}
		return Paths;
	}

	virtual void LoadSynchronous() override
	{
		Super::LoadSynchronous();

		if (FireAnimSequence.IsNull() == false)
		{
			FireAnimSequence.LoadSynchronous();
		}

		if (ReloadAnimSequence.IsNull() == false)
		{
			ReloadAnimSequence.LoadSynchronous();
		}

		if (WeaponScope.IsNull() == false)
		{
			WeaponScope.LoadSynchronous();
		}
	}
};
