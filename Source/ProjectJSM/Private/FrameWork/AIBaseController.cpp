// Copyright JSM Studio, Inc. All Rights Reserved.


#include "FrameWork/AIBaseController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Data/DataAssets/CharacterData/JSMAIDataAsset.h"
#include "System/AI/Perception/AIPerceptionComponentBase.h"
#include "System/AI/BlackboardKeys.h"
#include "Data/Common/GameEnum.h"
#include "Core/Managers/JSMEventManager.h"

AAIBaseController::AAIBaseController(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	bSetControlRotationFromPawnOrientation = false;

	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponentBase>(TEXT("AIPerceptionComp")));
	BehaviorTreeComponent = ObjectInitializer.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorTreeComp"));
	Blackboard = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));

	SetGenericTeamId(FGenericTeamId(1));
}

void AAIBaseController::InitializeAIData(const UJSMAIDataAsset* aiDataAsset)
{
	if (!IsValid(aiDataAsset))
	{
		return;
	}

	UAIPerceptionComponentBase* PerceptionComponentBase = Cast<UAIPerceptionComponentBase>(PerceptionComponent);
	if (!IsValid(PerceptionComponentBase))
	{
		return;
	}

	PerceptionComponentBase->InitializeAIData(aiDataAsset);

	UBlackboardComponent* BlackboardComp = Blackboard;
	UseBlackboard(aiDataAsset->BlackBoard.Get(), BlackboardComp);
	Btree = aiDataAsset->BehaviorTree.Get();

	RunBehaviorTree(Btree);
	BehaviorTreeComponent->StartTree(*Btree);
	Blackboard->SetValueAsEnum(BlackboardKeys::AIStatus, static_cast<uint8>(EAIState::Patrol));
}


void AAIBaseController::BeginPlay()
{
	Super::BeginPlay();
}

void AAIBaseController::OnPossess(APawn* const ParamPawn)
{
	Super::OnPossess(ParamPawn);
}

void AAIBaseController::Tick(float DeltaTime)
{
	Super::Super::Tick(DeltaTime);
	UpdateControlRotation(DeltaTime, true);
}