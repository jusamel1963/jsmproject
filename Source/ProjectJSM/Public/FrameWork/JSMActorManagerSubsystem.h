// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "JSMActorManagerSubsystem.generated.h"


UCLASS()
class PROJECTJSM_API UJSMActorManagerSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
public:
	template<class T>
	T* SpawnActor(UWorld* InWorld, UClass* Class, FTransform const& Transform, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters())
	{
		check(InWorld);
		T* SpawnedActor = InWorld->SpawnActor<T>(Class, Transform, SpawnParameters);
		if (IsValid(SpawnedActor))
		{
			return SpawnedActor;
		}

		return nullptr;
	}
};
