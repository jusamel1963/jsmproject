// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Character/Component/JSMDamageCtrlComponent.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "AbilitySystem/AttributeSet/JSMAttributeSet.h"
#include "Objects/Character/CharacterBase.h"
#include "GameplayEffectExtension.h"

UJSMDamageCtrlComponent::UJSMDamageCtrlComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	SetIsReplicatedByDefault(true);
	bWantsInitializeComponent = true;
}

void UJSMDamageCtrlComponent::InitializeComponent()
{
	Super::InitializeComponent();
	OwnerCharacter = Cast<ACharacterBase>(GetOwner());
	check(IsValid(OwnerCharacter));

	AbilitySystemComponent = Cast<UJSMAbilitySystemComponent>(OwnerCharacter->GetAbilitySystemComponent());
	AttributeSetBase = OwnerCharacter->GetAttributeSet<UJSMAttributeSet>();
	if (ensure(AttributeSetBase.IsValid()))
	{
		AttributeSetBase->OnHealthDamaged().AddUObject(this, &UJSMDamageCtrlComponent::OnHealthDamaged);
	}
}

void UJSMDamageCtrlComponent::UninitializeComponent()
{
	Super::UninitializeComponent();

	if (AttributeSetBase.IsValid())
	{
		AttributeSetBase->OnHealthDamaged().RemoveAll(this);
		AttributeSetBase.Reset();
	}
	if (AbilitySystemComponent.IsValid())
	{
		AbilitySystemComponent.Reset();
	}

	OwnerCharacter = nullptr;
}

void UJSMDamageCtrlComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UJSMDamageCtrlComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UJSMDamageCtrlComponent::OnHealthDamaged(const FGameplayEffectModCallbackData& Data)
{
	check(AbilitySystemComponent.Get() == &Data.Target);

	ACharacterBase* InstigatorCharacter = Cast<ACharacterBase>(Data.EffectSpec.GetContext().GetInstigator());
	if (!IsValid(OwnerCharacter) || !IsValid(InstigatorCharacter))
	{
		return;
	}
	const float NewHealth = AttributeSetBase->GetCurrHealth();

	if (NewHealth <= 0.0f)
	{
		OwnerCharacter->DieCharacter(InstigatorCharacter);
	}
}
