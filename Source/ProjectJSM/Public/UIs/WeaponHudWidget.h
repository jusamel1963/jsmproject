// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UIs/BaseUserWidget.h"
#include "WeaponHudWidget.generated.h"


UCLASS()
class PROJECTJSM_API UWeaponHudWidget : public UBaseUserWidget
{
	GENERATED_BODY()
public:
	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UOverlay> Overlay_Root;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr <class UImage> PrimaryWeapon;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr <class UImage> HolsterWeapon;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr <class UTextBlock> MagAmmo;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr <class UTextBlock> MagCapacity;

public:
	void Update_WeaponHud(UTexture2D* InPrimaryWeapon, UTexture2D* InHolsterWeapon, const FText& InMagAmmo, const FText& InMagCapacity);
};
