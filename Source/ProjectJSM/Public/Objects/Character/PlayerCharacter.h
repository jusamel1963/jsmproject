// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Character/CharacterBase.h"
#include "PlayerCharacter.generated.h"


UCLASS()
class PROJECTJSM_API APlayerCharacter : public ACharacterBase
{
	GENERATED_BODY()

public:
	APlayerCharacter(const FObjectInitializer& ObjectInitializer);
	FORCEINLINE class UJSMInventoryComponent* GetInventoryComponent() const { return InventoryComponent; }

	UFUNCTION(BlueprintCallable, Category = "Input")
	void PickupWeaponAction();
	UFUNCTION(BlueprintCallable, Category = "Input")
	void InteractionAction();
	UFUNCTION(BlueprintCallable, Category = "Input")
	void TakeDownAction();

	void ActivePickupWeaponAbility(const FHitResult& OutHitResult);
	UFUNCTION(Server, Reliable)
	void CS_ActivePickupWeaponAbility(const FHitResult& OutHitResult);

	void SS_InteractionAction(const FHitResult& OutHitResult);
	UFUNCTION(Server, Reliable)
	void CS_InteractionAction(const FHitResult& OutHitResult);

	void SS_TakeDownAction(const FHitResult& OutHitResult);
	UFUNCTION(Server, Reliable)
	void CS_TakeDownAction(const FHitResult& OutHitResult);
	UFUNCTION(Client, Reliable)
	void SC_TakeDownAction(const FHitResult& OutHitResult);
	UFUNCTION(NetMulticast, Reliable)
	void SM_TakeDownAction(const FHitResult& OutHitResult);


	void SS_TakeDownAttack(const FHitResult& OutHitResult);

	virtual void TraceUnderCrosshairs(FHitResult& OutHitResult, FVector_NetQuantize& EndLoc, const float& LineDistance, const float& TraceRadius, const FVector_NetQuantize& Offset = FVector_NetQuantize::ZeroVector);
	virtual void Custom_AimAction(bool bValue) override;
	virtual void FireWeapon(FHitResult& OutHitResult, FVector_NetQuantize& EndLoc, const float& LineDistance, const float& TraceRadius, const FVector_NetQuantize& Offset, const FVector_NetQuantize& MuzzleLoc, const FVector_NetQuantize& MuzzleEndLoc = FVector_NetQuantize()) override;
	virtual void PredictProjectile(FVector_NetQuantize& LaunchVelocity) override;

	UFUNCTION(Server, Reliable)
	void CS_TakeDownAttackEnd();

	UFUNCTION(NetMulticast, Reliable)
	void SM_TakeDownAttackEnd();

protected:
	virtual void InitializeCharacter(const FCharacterDesc* charDesc);

private:
	UPROPERTY(VisibleAnywhere)
	TObjectPtr<class UJSMInventoryComponent> InventoryComponent;

	bool bPlayStealthAttack = false;
};
