// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponSpawnerActor.generated.h"

USTRUCT(Blueprintable)
struct FWeaponSpawnData
{
	GENERATED_BODY()
public:

	UPROPERTY(EditInstanceOnly)
	FName WeaponKeyName;
};

UCLASS()
class PROJECTJSM_API AWeaponSpawnerActor : public AActor
{
	GENERATED_BODY()

public:
	AWeaponSpawnerActor();
	void SetRespawnTimer();
	virtual void Execute();

protected:
	virtual void BeginPlay() override;

	void SpawnWeapon();
	UFUNCTION()
	void OnSpawnTimer();
	UFUNCTION()
	void OnDestroyActor(AActor* DestroyedActor);
public:
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(EditAnywhere, Category = "Spawn Config")
	TArray<FWeaponSpawnData> SpawnData;
	UPROPERTY(EditAnywhere, Category = "Spawn Config", meta = (EditCondition = "bRespawnable", ShortTooltip = " == 0 : Only once, > n : n time respawn "))
	float RespawnTimeInterval = 0.0f;
	UPROPERTY(EditAnywhere, Category = "Spawn Config")
	bool bRespawnable = false;

protected:
	UPROPERTY(Transient)
	TArray<TObjectPtr<AActor>>	SpawnedActors;
	FTimerHandle SpawnerTimerHandle;
};
