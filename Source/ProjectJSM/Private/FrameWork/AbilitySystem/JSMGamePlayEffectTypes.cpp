// Copyright JSM Studio, Inc. All Rights Reserved.

#include "FrameWork/AbilitySystem/JSMGamePlayEffectTypes.h"
#include "AbilitySystem/GameEffect/JSMGameplayEffect.h"


bool FJSMGameplayEffectArgument::NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
{
	Ar << ArgumentTag;
	Ar << ArgumentType;

	switch (ArgumentType)
	{
	case EJSMGameplayEffectArgumentType::Float:
		Ar << FloatValue;
		break;

	case EJSMGameplayEffectArgumentType::GameplayEffect:
		Ar << GameplayEffectValue;
		break;

	default:
		unimplemented();
	}

	bOutSuccess = true;
	return true;
}

bool FJSMGameplayEffectContext::NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
{
	const bool bRet = Super::NetSerialize(Ar, Map, bOutSuccess);

	uint8 RepBits = 0;
	if (Ar.IsSaving())
	{
		if (Arguments.Num() > 0)
		{
			RepBits |= 1 << 7;
		}
	}

	Ar.SerializeBits(&RepBits, 8);

	if (RepBits & (1 << 4))
	{
		SafeNetSerializeTArray_WithNetSerialize<31>(Ar, Arguments, Map);
	}

	return bRet;
}

void FJSMGameplayEffectContext::SetArguments(const TArray<FJSMGameplayEffectArgument>& InArguments)
{
	// TArray 의 할당연산자는 Shallow Copy 함
	Arguments = InArguments;
}

void FJSMGameplayEffectContext::AddArguments(const TArray<FJSMGameplayEffectArgument>& InArguments)
{
	Arguments.Append(InArguments);
}

void FJSMGameplayEffectContext::TransferFloatArgumentsToSetByCallerMagnitudes(TSharedPtr<FGameplayEffectSpec> Spec)
{
	check(Spec.IsValid());

	for (auto& Argument : Arguments)
	{
		if (Argument.ArgumentType == EJSMGameplayEffectArgumentType::Float)
		{
			Spec->SetSetByCallerMagnitude(Argument.ArgumentTag, Argument.FloatValue);
		}
	}
}

void FJSMGameplayEffectContext::SetArgument(FGameplayTag ArgumentTag, float Value)
{
	for (auto& Argument : Arguments)
	{
		if (Argument.ArgumentTag == ArgumentTag)
		{
			Argument.FloatValue = Value;
			return;
		}
	}
	AddArgument(ArgumentTag, Value);
}

float FJSMGameplayEffectContext::GetArgument(FGameplayTag ArgumentTag, float DefaultValue) const
{
	for (auto& Argument : Arguments)
	{
		if (Argument.ArgumentTag == ArgumentTag)
		{
			if (ensure(Argument.ArgumentType == EJSMGameplayEffectArgumentType::Float))
			{
				return Argument.FloatValue;
			}
		}
	}
	return DefaultValue;
}

void FJSMGameplayEffectContext::SetArgument(FGameplayTag ArgumentTag, TSubclassOf<UJSMGameplayEffect> Value)
{
	for (auto& Argument : Arguments)
	{
		if (Argument.ArgumentTag == ArgumentTag)
		{
			Argument.GameplayEffectValue = Value;
			return;
		}
	}
	AddArgument(ArgumentTag, Value);
}

TSubclassOf<UJSMGameplayEffect> FJSMGameplayEffectContext::GetArgument(FGameplayTag ArgumentTag, TSubclassOf<UJSMGameplayEffect> DefaultValue) const
{
	for (auto& Argument : Arguments)
	{
		if (Argument.ArgumentTag == ArgumentTag)
		{
			if (ensure(Argument.ArgumentType == EJSMGameplayEffectArgumentType::GameplayEffect))
			{
				return Argument.GameplayEffectValue;
			}
		}
	}
	return DefaultValue;
}
