// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Weapons/ThrowableWeapon.h"
#include "ThrowableKnifeWeapon.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API AThrowableKnifeWeapon : public AThrowableWeapon
{
	GENERATED_BODY()
public:
	AThrowableKnifeWeapon();

public:
	virtual void HitEvent(class AActor* InOtherActor, const FHitResult& Hit) override;
};
