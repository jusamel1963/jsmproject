// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Data/Common/GameEnum.h"
#include "CoverValues.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCoverValues : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class PROJECTJSM_API ICoverValues
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual ECoverType GetCoverType() PURE_VIRTUAL(GetCoverType, return ECoverType::FindCover;);
	virtual bool IsMoveLeft() PURE_VIRTUAL(IsMoveLeft, return false;);
	virtual bool IsMoveRight() PURE_VIRTUAL(IsMoveRight, return false;);
};
