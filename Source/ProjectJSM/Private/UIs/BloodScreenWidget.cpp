// Copyright JSM Studio, Inc. All Rights Reserved.


#include "UIs/BloodScreenWidget.h"
#include "Components/Image.h"
#include "Animation/WidgetAnimation.h"

void UBloodScreenWidget::NativeConstruct()
{
	Super::NativeConstruct();
	PlayBloodAnimation();
}

void UBloodScreenWidget::SetBloodScreenOpacity(const float& InOpacity)
{
	if (IsValid(Img_Blood))
	{
		Img_Blood->SetOpacity(InOpacity);
	}
}

void UBloodScreenWidget::PlayBloodAnimation()
{
	UE_LOG(LogTemp, Warning, TEXT("PlayBloodAnimation"));
	if (IsValid(BloodAnimation))
	{
		PlayAnimation(BloodAnimation, 0, 0, EUMGSequencePlayMode::PingPong);
	}
}