// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AISpawnerActor.generated.h"

USTRUCT(Blueprintable)
struct FAISpawnData
{
	GENERATED_BODY()
public:

	UPROPERTY(EditInstanceOnly)
	FName AIKeyName;

	UPROPERTY(EditInstanceOnly)
	bool bPatrol;

	UPROPERTY(EditInstanceOnly, meta = (EditCondition = "bPatrol"))
	TArray<TObjectPtr<class AActor>> PatrolPoints;
};

UCLASS()
class PROJECTJSM_API AAISpawnerActor : public AActor
{
	GENERATED_BODY()
	
public:
	AAISpawnerActor();
	void SetRespawnTimer();
	virtual void Execute();

protected:
	virtual void BeginPlay() override;

	void SpawnAI();
	UFUNCTION()
	void OnSpawnTimer();
	UFUNCTION()
	void OnDestroyActor(AActor* DestroyedActor);
public:
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(EditAnywhere, Category = "Spawn Config")
	TArray<FAISpawnData> SpawnData;
	UPROPERTY(EditAnywhere, Category = "Spawn Config", meta = (EditCondition = "bRespawnable", ShortTooltip = " == 0 : Only once, > n : n time respawn "))
	float RespawnTimeInterval = 0.0f;
	UPROPERTY(EditAnywhere, Category = "Spawn Config")
	bool bRespawnable = false;

protected:
	UPROPERTY(Transient)
	TArray<TObjectPtr<AActor>>	SpawnedActors;
	FTimerHandle SpawnerTimerHandle;
};
