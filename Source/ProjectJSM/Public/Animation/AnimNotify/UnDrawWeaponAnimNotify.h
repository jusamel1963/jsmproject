// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotify/AnimNotifyBase.h"
#include "UnDrawWeaponAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UUnDrawWeaponAnimNotify : public UAnimNotifyBase
{
	GENERATED_BODY()
public:
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation);
};
