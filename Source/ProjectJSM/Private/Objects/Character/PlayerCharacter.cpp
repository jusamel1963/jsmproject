// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Character/PlayerCharacter.h"
#include "Objects/Character/Component/JSMInventoryComponent.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "Statics/JSMClientStatics.h"
#include "Objects/Weapons/Weapon.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "Components/ALSDebugComponent.h"
#include "Data/DataAssets/ItemData/JSMWeaponDataAsset.h"
#include "AbilitySystem/JSMAbilitySystemImpl.h"
#include "Objects/Character/AICharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SplineComponent.h"
#include "Data/DataAssets/CharacterData/JSMCharacterDataAsset.h"
#include "Components/SplineMeshComponent.h"
#include "Data/DataAssets/ItemData/JSMThrowableWeaponDataAsset.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Objects/Vehicles/VehicleInterface.h"
#include "Objects/Vehicles/NoWheelVehicleBase.h"
#include "Objects/Interactives/InteractiveInterface.h"

APlayerCharacter::APlayerCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	InventoryComponent = CreateDefaultSubobject<UJSMInventoryComponent>(TEXT("JSMInventoryComponent"));
	InventoryComponent->SetIsReplicated(true);

	TeamId = FGenericTeamId(0);
}

void APlayerCharacter::InitializeCharacter(const FCharacterDesc* charDesc)
{
	Super::InitializeCharacter(charDesc);
}

void APlayerCharacter::PickupWeaponAction()
{
	if (!IsLocallyControlled())
	{
		return;
	}

	FHitResult OutHitResult; FVector_NetQuantize EndLoc;
	TraceUnderCrosshairs(OutHitResult, EndLoc, SearchTraceDistance, SearchTraceRadius);

	if (HasAuthority())
	{
		ActivePickupWeaponAbility(OutHitResult);
	}
	else
	{
		CS_ActivePickupWeaponAbility(OutHitResult);
	}
}

void APlayerCharacter::InteractionAction()
{
	if (!IsLocallyControlled())
	{
		return;
	}

	FHitResult OutHitResult; FVector_NetQuantize EndLoc;
	TraceUnderCrosshairs(OutHitResult, EndLoc, SearchTraceDistance, SearchTraceRadius);

	if (HasAuthority())
	{
		SS_InteractionAction(OutHitResult);
	}
	else
	{
		CS_InteractionAction(OutHitResult);
	}
}

void APlayerCharacter::ActivePickupWeaponAbility(const FHitResult& OutHitResult)
{
	AActor* HitActor = OutHitResult.GetActor();
	if ((OutHitResult.bBlockingHit) && (IsValid(HitActor)))
	{
		AWeapon* HitWeapon = Cast<AWeapon>(HitActor);
		if (IsValid(HitWeapon) && IsValid(AbilitySystemComponent))
		{
			UJSMWeaponDataAsset* WeaponDataAsset = HitWeapon->GetWeaponDataAsset();
			if (IsValid(WeaponDataAsset))
			{
				FGameplayEventData EventData;
				EventData.EventTag = WeaponDataAsset->OverlapEventTag;
				EventData.Instigator = this;
				EventData.Target = HitWeapon;
				AbilitySystemComponent->HandleGameplayEvent(WeaponDataAsset->OverlapEventTag, &EventData);
			}
		}
	}
}

void APlayerCharacter::CS_ActivePickupWeaponAbility_Implementation(const FHitResult& OutHitResult)
{
	ActivePickupWeaponAbility(OutHitResult);
}

void APlayerCharacter::SS_InteractionAction(const FHitResult& OutHitResult)
{
	IVehicleInterface* VehicleInterface = Cast<IVehicleInterface>(VehicleActor);
	if (VehicleInterface != nullptr)
	{
		VehicleInterface->ExitVehicle(GetController()->GetUniqueID());
		VehicleActor = nullptr;
		return;
	}

	AActor* HitActor = OutHitResult.GetActor();
	if (!OutHitResult.bBlockingHit || !IsValid(HitActor))
	{
		return;
	}

	AAICharacter* AICharacter = Cast<AAICharacter>(HitActor);
	if (IsValid(AICharacter) && IsValid(InventoryComponent))
	{
		InventoryComponent->LootItems(AICharacter);
		return;
	}

	if (HitActor->Implements<UVehicleInterface>())
	{
		VehicleActor = HitActor;
		EnterVehicle(HitActor);
		return;
	}

	if (HitActor->Implements<UInteractiveInterface>())
	{
		TriggerInteractive(HitActor);
		return;
	}
}

void APlayerCharacter::CS_InteractionAction_Implementation(const FHitResult& OutHitResult)
{
	SS_InteractionAction(OutHitResult);
}

void APlayerCharacter::TraceUnderCrosshairs(FHitResult& OutHitResult, FVector_NetQuantize& EndLoc, const float& LineDistance, const float& TraceRadius, const FVector_NetQuantize& Offset)
{
	UWorld* World = UJSMClientStatics::GetGameWorld();
	if (!IsValid(World))
	{
		return;
	}

	APlayerController* LocalController = Cast<APlayerController>(GetController());
	if (!IsValid(LocalController) || !IsValid(LocalController->PlayerCameraManager))
	{
		return;
	}
	const FVector_NetQuantize Start = LocalController->PlayerCameraManager->GetCameraLocation() + Offset;
	const FVector_NetQuantize End = (LocalController->PlayerCameraManager->GetActorForwardVector() * LineDistance) + Start;
	EndLoc = End;

	FCollisionQueryParams CollisionParams;
	FCollisionResponseParams CollisionResponese;
	CollisionParams.AddIgnoredActor(this);
	CollisionParams.bReturnPhysicalMaterial = true;

	const FCollisionShape SphereCollisionShape = FCollisionShape::MakeSphere(SearchTraceRadius);
	const bool bHit = World->SweepSingleByChannel(OutHitResult, Start, EndLoc, FQuat::Identity,
		ECollisionChannel::ECC_Visibility, SphereCollisionShape, CollisionParams);

	if (OutHitResult.bBlockingHit)
	{
		EndLoc = OutHitResult.Location;
	}

	if (OutHitResult.bBlockingHit && IsValid(OutHitResult.GetActor()))
	{
		UE_LOG(LogTemp, Warning, TEXT("UWeaponCtrlComponent::CS_ApplyWeaponAbilityEffect_Implementation : %s"), *OutHitResult.GetActor()->GetName());
		if (OutHitResult.PhysMaterial.IsValid())
		{
			int PhysicsIndex = static_cast<int32>(OutHitResult.PhysMaterial->SurfaceType);
			UE_LOG(LogTemp, Warning, TEXT("OutHitResult.PhysMaterial PhysicsIndex : %d"), PhysicsIndex);
		}
	}
}

void APlayerCharacter::PredictProjectile(FVector_NetQuantize& LaunchVelocity)
{
	UE_LOG(LogTemp, Warning, TEXT("PredictProjectile"));
	for (int i = 0; i < ThrowableMeshArcList.Num(); i++)
	{
		if (IsValid(ThrowableMeshArcList[i]))
		{
			ThrowableMeshArcList[i]->DestroyComponent();
		}
	}
	ThrowableMeshArcList.Empty();

	if (!IsValid(ThrowableArc))
	{
		return;
	}
	ThrowableArc->ClearSplinePoints();

	APlayerController* LocalController = Cast<APlayerController>(GetController());
	if (!IsValid(GetMesh()) || !IsValid(LocalController) || !IsValid(LocalController->PlayerCameraManager))
	{
		return;
	}

	FHitResult OutHitResult;
	TArray<FVector> OutPathPositions;
	FVector_NetQuantize OutLastTraceDestination;
	FVector_NetQuantize StartPos = GetMesh()->GetSocketLocation(ThrowableSocketName);

	LaunchVelocity = LocalController->PlayerCameraManager->GetActorForwardVector() * 2500;
	TArray<AActor*> ActorsToIgnore;
	ActorsToIgnore.Emplace(this);
	UGameplayStatics::Blueprint_PredictProjectilePath_ByTraceChannel(GetWorld(), OutHitResult, OutPathPositions, OutLastTraceDestination,
		StartPos, LaunchVelocity, true, 0, ECollisionChannel::ECC_Visibility, false, ActorsToIgnore, EDrawDebugTrace::ForDuration, 2);

	//for (int i = 0; i < OutPathPositions.Num(); i++)
	//{
	//	ThrowableArc->AddSplinePointAtIndex(OutPathPositions[i], i, ESplineCoordinateSpace::World);
	//}
	//ThrowableArc->SetSplinePointType(OutPathPositions.Num() - 1,ESplinePointType::CurveClamped);

	//for (int i = 0; i < (ThrowableArc->GetNumberOfSplinePoints() - 1); i++)
	//{
	//	USplineMeshComponent* NewThrowableMeshArc = NewObject<USplineMeshComponent>();
	//	if (!IsValid(NewThrowableMeshArc))
	//	{
	//		return;
	//	}

	//	NewThrowableMeshArc->SetHiddenInGame(false);
	//	UJSMCharacterDataAsset* DataAsset = GetCharacterDataAsset();
	//	if (!IsValid(DataAsset))
	//	{
	//		return;
	//	}
	//	ThrowableMeshArcList.Emplace(NewThrowableMeshArc);

	//	NewThrowableMeshArc->SetStaticMesh(DataAsset->SplineMesh.Get());
	//	NewThrowableMeshArc->SetStartScale(FVector2D(0.05f));
	//	NewThrowableMeshArc->SetEndScale(FVector2D(0.05f));
	//	FVector_NetQuantizeStartPathPos = OutPathPositions[i];
	//	FVector_NetQuantizeStartTangent = ThrowableArc->GetTangentAtSplinePoint(i, ESplineCoordinateSpace::World);
	//	FVector_NetQuantizeEndPathPos = OutPathPositions[i + 1];
	//	FVector_NetQuantizeEndTangent = ThrowableArc->GetTangentAtSplinePoint(i + 1, ESplineCoordinateSpace::World);

	//	NewThrowableMeshArc->SetStartAndEnd(StartPathPos, StartTangent, EndPathPos, EndTangent);
	//}
}

void APlayerCharacter::Custom_AimAction(bool bValue)
{
	Super::Custom_AimAction(bValue);

	if (!IsValid(WeaponCtrlComponent))
	{
		return;
	}

	UJSMThrowableWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMThrowableWeaponDataAsset>(WeaponCtrlComponent->GetRepCurrWeaponKey());
	if (!IsValid(WeaponDataAsset))
	{
		if (bValue)
		{
			// AimAction: Hold "AimAction" to enter the aiming mode, release to revert back the desired rotation mode.
			SetRotationMode(EALSRotationMode::Aiming);
		}
		else
		{
			if (ViewMode == EALSViewMode::ThirdPerson)
			{
				SetRotationMode(DesiredRotationMode);
			}
			else if (ViewMode == EALSViewMode::FirstPerson)
			{
				SetRotationMode(EALSRotationMode::LookingDirection);
			}
		}
	}

	WeaponCtrlComponent->CS_CusomAimAction(bValue);
}

void APlayerCharacter::FireWeapon(FHitResult& OutHitResult, FVector_NetQuantize& EndLoc, const float& LineDistance, const float& TraceRadius, const FVector_NetQuantize& Offset, const FVector_NetQuantize& MuzzleLoc, const FVector_NetQuantize& MuzzleEndLoc)
{
	TraceUnderCrosshairs(OutHitResult, EndLoc, LineDistance, TraceRadius, Offset);
}

void APlayerCharacter::TakeDownAction()
{
	if (!IsLocallyControlled())
	{
		return;
	}

	FHitResult OutHitResult; FVector_NetQuantize EndLoc;
	TraceUnderCrosshairs(OutHitResult, EndLoc, SearchTraceDistance, SearchTraceRadius);

	if (HasAuthority())
	{
		SS_TakeDownAction(OutHitResult);
	}
	else
	{
		CS_TakeDownAction(OutHitResult);
	}
}

void APlayerCharacter::SS_TakeDownAction(const FHitResult& OutHitResult)
{
	ACharacterBase* HitCharacterBase = Cast<ACharacterBase>(OutHitResult.GetActor());
	if (bPlayStealthAttack || !IsValid(HitCharacterBase))
	{
		return;
	}

	float HitDistance = OutHitResult.Distance;
	if ((HitDistance < 400.f) || (HitDistance > 450.f))
	{
		return;
	}

	const FVector_NetQuantize OwnForwardVector = GetActorForwardVector();
	const FVector_NetQuantize HitForwardVector = HitCharacterBase->GetActorForwardVector();
	if ((HitForwardVector - OwnForwardVector).Length() >= 0.09f)
	{
		return;
	}

	bPlayStealthAttack = true;
	UWeaponCtrlComponent* WeaponCtrlComp = GetWeaponCtrl();
	if (IsValid(WeaponCtrlComp))
	{
		WeaponCtrlComp->SM_EquipWeapon(nullptr, nullptr, EWeaponAttachType::None, true);
		WeaponCtrlComp->EquipWeaponToAttachSocket();
	}

	SC_TakeDownAction(OutHitResult);
	FRotator LookatRot = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitCharacterBase->GetActorLocation());
	SetActorRotation(LookatRot);
	HitCharacterBase->TakeDown();

	FTimerHandle DelayHandle;
	const TWeakObjectPtr<APlayerCharacter> WeakPtr(this);
	GetWorld()->GetTimerManager().SetTimer(DelayHandle, FTimerDelegate::CreateLambda([WeakPtr, OutHitResult]()
	{
		if (WeakPtr.IsValid())
		{
			WeakPtr->SS_TakeDownAttack(OutHitResult);
		}
	}), 1.16, false);
}

void APlayerCharacter::CS_TakeDownAction_Implementation(const FHitResult& OutHitResult)
{
	SS_TakeDownAction(OutHitResult);
}

void APlayerCharacter::SM_TakeDownAction_Implementation(const FHitResult& OutHitResult)
{

}

void APlayerCharacter::SC_TakeDownAction_Implementation(const FHitResult& OutHitResult)
{
	UJSMCharacterDataAsset* DataAsset = GetCharacterDataAsset();
	if (!IsValid(DataAsset) || !IsValid(DataAsset->AM_StealthAttack.Get()))
	{
		return;
	}
	Replicated_PlayMontage(DataAsset->AM_StealthAttack.Get(), 1.0f);
	float DelayTime = (DataAsset->AM_StealthAttack->GetPlayLength() / DataAsset->AM_StealthAttack->RateScale);
	FTimerHandle DelayHandle;
	const TWeakObjectPtr<APlayerCharacter> WeakPtr(this);
	GetWorld()->GetTimerManager().SetTimer(DelayHandle, FTimerDelegate::CreateLambda([WeakPtr]()
	{
		if (WeakPtr.IsValid())
		{
			WeakPtr->CS_SetMovementMode(EMovementMode::MOVE_Walking);
		}
	}), DelayTime, false);
}

void APlayerCharacter::SS_TakeDownAttack(const FHitResult& OutHitResult)
{
	UCapsuleComponent* CapsuleComp = GetCapsuleComponent();
	ACharacterBase* HitCharacterBase = Cast<ACharacterBase>(OutHitResult.GetActor());
	if (!IsValid(CapsuleComp) || !IsValid(HitCharacterBase) || !IsValid(HitCharacterBase->TakeDownAttackSocket))
	{
		return;
	}

	CS_SetMovementMode(EMovementMode::MOVE_Flying);
	bool bEaseOut = false;
	bool bEaseIn = false;
	float OverTime = 0.2f;
	bool bForceShortestRotationPath = false;
	FLatentActionInfo LatentInfo;
	UKismetSystemLibrary::MoveComponentTo(CapsuleComp, HitCharacterBase->TakeDownAttackSocket->GetComponentLocation(), HitCharacterBase->TakeDownAttackSocket->GetComponentRotation(),
		bEaseOut, bEaseIn, OverTime, bForceShortestRotationPath, EMoveComponentAction::Move, LatentInfo);
}

void APlayerCharacter::CS_TakeDownAttackEnd_Implementation()
{
	bPlayStealthAttack = false;
	SM_TakeDownAttackEnd();
}

void APlayerCharacter::SM_TakeDownAttackEnd_Implementation()
{
	UWeaponCtrlComponent* WeaponCtrlComp = GetWeaponCtrl();
	if (IsValid(WeaponCtrlComp))
	{
		WeaponCtrlComp->EquipWeaponFromAttachSocket();
	}
}
