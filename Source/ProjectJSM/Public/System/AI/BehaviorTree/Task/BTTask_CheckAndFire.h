// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_CheckAndFire.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UBTTask_CheckAndFire : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTTask_CheckAndFire();

protected:
	virtual EBTNodeResult::Type ExecuteTask(class UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	const float FireMaxDelay = 0.5f;
	float FireCurDelay = 0.0f;
	FVector_NetQuantize FireEndLoc = FVector_NetQuantize();
};
