// Copyright JSM Studio, Inc. All Rights Reserved.


#include "AbilitySystem/Ability/JSMGA_Throw.h"
#include "Abilities/Tasks/AbilityTask_WaitDelay.h"
#include "Abilities/Tasks/AbilityTask_WaitGameplayEvent.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"
#include "Core/Managers/JSMEventManager.h"
#include "Data/DataAssets/ItemData/JSMWeaponDataAsset.h"
#include "Data/DataAssets/ItemData/JSMMeleeWeaponDataAsset.h"
#include "Framework/JSMGameInstance.h"
#include "Objects/Character/CharacterBase.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Objects/Weapons/Weapon.h"
#include "Objects/Weapons/MeleeWeapon.h"
#include "Objects/Weapons/JSMWeaponInfo.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "Statics/JSMClientStatics.h"

UJSMGA_Throw::UJSMGA_Throw()
	: Super()
{
	JSMAbilityID = EJSMGameplayAbilityID::Throw;
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;

	FGameplayTag AbilityTag = REQ_GAMEPLAYTAG("Ability.Attack.Throw");
	AbilityTags.AddTag(AbilityTag);
	ActivationOwnedTags.AddTag(AbilityTag);

	FGameplayTag AbilityTriggerTag = REQ_GAMEPLAYTAG("Event.Attack.ThrowStart");
	FAbilityTriggerData TriggerData;
	TriggerData.TriggerTag = AbilityTriggerTag;
	TriggerData.TriggerSource = EGameplayAbilityTriggerSource::GameplayEvent;
	AbilityTriggers.Add(TriggerData);
}

void UJSMGA_Throw::PostCommitActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::PostCommitActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
	OwnerCharacter = Cast<ACharacterBase>(ActorInfo->AvatarActor);
	if (!IsValid(OwnerCharacter))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	WeaponCtrlComponent = OwnerCharacter->GetWeaponCtrl();
	if (!WeaponCtrlComponent.IsValid())
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	WeaponCtrlComponent->ThrowablePoseWeapon();

	UAbilityTask_WaitGameplayEvent* TaskStop = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this, REQ_GAMEPLAYTAG("Event.Attack.ThrowStop"));
	TaskStop->EventReceived.AddDynamic(this, &UJSMGA_Throw::OnThrowPoseStop);
	TaskStop->ReadyForActivation();

	UAbilityTask_WaitGameplayEvent* TaskCancel = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this, REQ_GAMEPLAYTAG("Event.Attack.ThrowCancel"));
	TaskCancel->EventReceived.AddDynamic(this, &UJSMGA_Throw::OnThrowPoseCancel);
	TaskCancel->ReadyForActivation();
}

void UJSMGA_Throw::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UJSMGA_Throw::OnThrowPoseStop(FGameplayEventData Payload)
{
	UE_LOG(LogTemp, Warning, TEXT("OnThrowPoseStop"));
	if (WeaponCtrlComponent.IsValid())
	{
		WeaponCtrlComponent->ThrowableWeapon();
	}
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}

void UJSMGA_Throw::OnThrowPoseCancel(FGameplayEventData Payload)
{
	UE_LOG(LogTemp, Warning, TEXT("OnThrowPoseCancel"));
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}
