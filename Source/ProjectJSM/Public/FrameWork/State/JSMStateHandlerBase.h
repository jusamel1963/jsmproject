// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "Tickable.h"

template<typename TState>
class FJSMStateHandlerBase : public FTickableGameObject
{
public:
	FJSMStateHandlerBase() = default;
	virtual ~FJSMStateHandlerBase() {}

	typename TState::StateIdType GetCurrentStateID() const
	{
		if (CurrentState.IsValid() == false)
		{
			return TState::StateIdType{};
		}

		return CurrentState->GetStateID();
	}

	float GetCurrentStateElapsedTime() const
	{
		if (CurrentState.IsValid() == false)
		{
			return 0.0f;
		}
		return CurrentState->GetElapsedTime();
	}

	void AddState(TSharedPtr<TState> State)
	{
		if (State.IsValid())
		{
			States.Add(State->GetStateID(), State);
		}
	}

	void ClearStates()
	{
		if (CurrentState.IsValid())
		{
			CurrentState->Leave();
			CurrentState = nullptr;
		}

		States.Empty();
	}

	void InitState(typename TState::StateIdType StateID)
	{
		TransitionState(StateID);
	}

	virtual TStatId GetStatId() const override { RETURN_QUICK_DECLARE_CYCLE_STAT(FJSMGameStateBase, STATGROUP_Tickables); }

	virtual void Tick(float DeltaTime) override
	{
		if (CurrentState.IsValid())
		{
			typename TState::StateIdType TransitionStateID;
			if (CurrentState->HasTransitionState(TransitionStateID))
			{
				if (TransitionState(TransitionStateID))
					return;
			}

			CurrentState->Update(DeltaTime);
		}
	}

	bool TransitionState(typename TState::StateIdType TransitionStateID)
	{
		if (TransitionStateID == typename TState::StateIdType{})
		{
			return false;
		}
		TSharedPtr<TState>* Founder = States.Find(TransitionStateID);
		if (Founder == nullptr)
		{
			return false;
		}
		return TransitionState(*Founder);
	}

private:
	bool TransitionState(const TSharedPtr<TState>& State)
	{
		if (State.IsValid() == false)
		{
			return false;
		}
		if (CurrentState.IsValid())
		{
			CurrentState->Leave();
		}
		CurrentState = State;
		CurrentState->Enter();

		return true;
	}

private:
	TSharedPtr<TState> CurrentState;
	TMap<typename TState::StateIdType, TSharedPtr<TState>> States;

};

