// Copyright JSM Studio, Inc. All Rights Reserved.


#include "AbilitySystem/Ability/JSMGA_Fire.h"
#include "Abilities/Tasks/AbilityTask_WaitDelay.h"
#include "Abilities/Tasks/AbilityTask_WaitGameplayEvent.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"
#include "Core/Managers/JSMEventManager.h"
#include "Data/DataAssets/ItemData/JSMWeaponDataAsset.h"
#include "Data/DataAssets/ItemData/JSMRangeWeaponDataAsset.h"
#include "Framework/JSMGameInstance.h"
#include "Objects/Character/CharacterBase.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Objects/Weapons/Weapon.h"
#include "Objects/Weapons/RangeWeapon.h"
#include "Objects/Weapons/JSMWeaponInfo.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "Statics/JSMClientStatics.h"

UJSMGA_Fire::UJSMGA_Fire() 
	: Super()
{
	JSMAbilityID = EJSMGameplayAbilityID::Fire;
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;

	FGameplayTag AbilityTag = REQ_GAMEPLAYTAG("Ability.Attack.Fire");
	AbilityTags.AddTag(AbilityTag);
	ActivationOwnedTags.AddTag(AbilityTag);

	FGameplayTag AbilityTriggerTag = REQ_GAMEPLAYTAG("Event.Attack.FireStart");
	FAbilityTriggerData TriggerData;
	TriggerData.TriggerTag = AbilityTriggerTag;
	TriggerData.TriggerSource = EGameplayAbilityTriggerSource::GameplayEvent;
	AbilityTriggers.Add(TriggerData);
}

void UJSMGA_Fire::PostCommitActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::PostCommitActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
	OwnerCharacter = Cast<ACharacterBase>(ActorInfo->AvatarActor);
	if (!IsValid(OwnerCharacter))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	WeaponCtrlComponent = OwnerCharacter->GetWeaponCtrl();
	if (!WeaponCtrlComponent.IsValid())
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	RangeWeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMRangeWeaponDataAsset>(WeaponCtrlComponent->GetRepCurrWeaponKey());
	if (!RangeWeaponDataAsset.IsValid())
	{
		return;
	}

	UAbilityTask_WaitGameplayEvent* Task = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this, REQ_GAMEPLAYTAG("Event.Attack.FireStop"));
	Task->EventReceived.AddDynamic(this, &UJSMGA_Fire::OnFireStop);
	Task->ReadyForActivation();

	bReserveEnd = false;

	InternalActivateAbility();
}

void UJSMGA_Fire::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UJSMGA_Fire::InternalActivateAbility()
{
	if (RangeWeaponDataAsset.IsValid() == false || !IsValid(OwnerCharacter))
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
		return;
	}

	if (TryFire() == true)
	{
		float FireInterval = RangeWeaponDataAsset->FireInterval;
		//UJSMAttributeSetBase* AttributeSet = OwnerChar->GetAttributeSet<UJSMAttributeSetBase>();
		//if (IsValid(AttributeSet))
		//{
		//	float SpeedRate = 1.0f + AttributeSet->GetAdditionalFireSpeedRate();
		//	FireInterval /= ensure(SpeedRate > SMALL_NUMBER) ? SpeedRate : 1.0f;
		//}

		UAbilityTask_WaitDelay* Task = UAbilityTask_WaitDelay::WaitDelay(this, FireInterval);
		Task->OnFinish.AddDynamic(this, &UJSMGA_Fire::OnWaitFireInterval);
		Task->ReadyForActivation();
	}
	else
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
	}
}

bool UJSMGA_Fire::TryFire()
{
	WeaponCtrlComponent->FireWeapon();
	return true;
}

void UJSMGA_Fire::OnWaitFireInterval()
{
	if (!RangeWeaponDataAsset.IsValid())
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
		return;
	}

	if (bReserveEnd)
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
		return;
	}

	bool bAutomaticFire = false;
	switch (RangeWeaponDataAsset->FiringMode)
	{
		case EFiringMode::Auto:
		{
			bAutomaticFire = true;
			break;
		}
	}

	if (bAutomaticFire)
	{
		InternalActivateAbility();
	}
	else
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
	}
}

void UJSMGA_Fire::OnFireStop(FGameplayEventData Payload)
{
	bReserveEnd = true;
}