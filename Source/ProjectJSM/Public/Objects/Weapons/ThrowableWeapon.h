// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Weapons/Weapon.h"
#include "ThrowableWeapon.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API AThrowableWeapon : public AWeapon
{
	GENERATED_BODY()
public:
	AThrowableWeapon();
	void ThrowableWeaponMove(const FVector_NetQuantize& InLaunchVelocity);
	UFUNCTION(NetMulticast, Reliable)
	void SM_SetOwnChar(class ACharacterBase* InOwnChar);
protected:
	virtual void BeginPlay() override;
	UFUNCTION()
	void OnProjectileStop(const FHitResult& ImpactResult);

	UFUNCTION()
	void OnHitBegin(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	virtual void HitEvent(class AActor* InOtherActor, const FHitResult& Hit);

	UFUNCTION(NetMulticast, Reliable)
	void SM_OverlapProjectTile(const FHitResult& Hit, class UJSMThrowableWeaponDataAsset* WeaponDataAsset);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<class UProjectileMovementComponent> ProjectileMovementComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<class URotatingMovementComponent> RotatingMovementComp;

	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly)
	TObjectPtr<class UParticleSystemComponent> ParticleSystemComp;

	const FName DistractNoiseName = TEXT("Distract");
	bool bPendingKill = false;

	TWeakObjectPtr<class ACharacterBase> OwnChar;
	static FName ParticleSystemComponentName;
};
