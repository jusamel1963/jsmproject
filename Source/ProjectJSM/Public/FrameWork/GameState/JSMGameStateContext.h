// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

struct FJSMGameStateContext
{
	bool bSessionConnected = false;
	bool bBattleMapGenFinished = false;
	bool bBattleEnded = false;
	bool bBattleLeaved = false;
	bool bDisconnected = false;


	void Init()
	{
		bSessionConnected = false;
		bBattleMapGenFinished = false;
		bBattleEnded = false;
		bBattleLeaved = false;
		bDisconnected = false;
	}
};