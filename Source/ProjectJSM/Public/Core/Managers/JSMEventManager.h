// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Framework/JSMGameInstance.h"
#include "JSMEventManager.generated.h"

USTRUCT()
struct PROJECTJSM_API FInGameEvents
{
	GENERATED_BODY()

public:
	DECLARE_MULTICAST_DELEGATE_TwoParams(FCharacterSpawned, UObject*, class ACharacterBase*);
	FCharacterSpawned CharacterSpawned;

	DECLARE_MULTICAST_DELEGATE_FourParams(FCharacterDamaged, UObject*, ACharacterBase*, ACharacterBase*, float);
	FCharacterDamaged CharacterPhysicalDamaged;

	DECLARE_MULTICAST_DELEGATE_ThreeParams(FCharacterDead, UObject*, class ACharacterBase*, class ACharacterBase*);
	FCharacterDead CharacterDead;

	DECLARE_MULTICAST_DELEGATE_ThreeParams(FCharacterWeaponAttach, UObject* , uint32, int);
	FCharacterWeaponAttach CharacterWeaponAttach;
};

USTRUCT()
struct PROJECTJSM_API FEventCategory
{
	GENERATED_BODY()

public:
	FInGameEvents InGame;
};

UCLASS()
class PROJECTJSM_API UJSMEventManager : public UObject
{
	GENERATED_BODY()
public:
	FEventCategory JSMEvent;
};

#define INVOKE_EVENT_JSM(EventName, ...)																			\
{																													\
	UJSMGameInstance* GameInst__ = UJSMGameInstance::GetGameInstance();												\
	UE_LOG(LogTemp,Warning,TEXT("GameInst__ 1 "));																	\
	if(IsValid(GameInst__))																							\
	{																												\
		UE_LOG(LogTemp,Warning,TEXT("INVOKE GameInst__ 2 : %d"),GameInst__->GetUniqueID());								\
		UJSMEventManager* EventMan__ = GameInst__->GetEventManager();												\
		if(IsValid(EventMan__))																						\
		{																											\
			UE_LOG(LogTemp,Warning,TEXT("IsBound : %d"), EventMan__->EventName.IsBound());							\
			EventMan__->EventName.Broadcast(this, ##__VA_ARGS__);													\
		}																											\
	}																												\
}

#define BIND_EVENT_WITH_GAMEINSTANCE_JSM(GameInstance, EventName, Object, TargetFunction)							\
{																													\
	if (IsValid(GameInstance))																						\
	{																												\
		UE_LOG(LogTemp,Warning,TEXT("BIND_EVENT_WITH_GAMEINSTANCE_JSM : %d"),GameInstance->GetUniqueID());								\
		UJSMEventManager* EventMan__ = GameInstance->GetEventManager();												\
		if (IsValid(EventMan__))																					\
		{																											\
			EventMan__->EventName.AddUObject(Object, TargetFunction);												\
		}																											\
	}																												\
}

#define UNBIND_EVENT_JSM(EventName, Object, TargetFunction)															\
{																													\
	UJSMGameInstance* GameInst__ = UJSMGameInstance::GetGameInstance();												\
	if (IsValid(GameInst__))																						\
	{																												\
		UJSMEventManager* EventMan__ = GameInst__->GetEventManager();												\
		if (IsValid(EventMan__))																					\
		{																											\
			EventMan__->EventName.RemoveAll(Object);																\
		}																											\
	}																												\
}

#define BIND_EVENT_JSM(EventName, Object, TargetFunction)															\
{																													\
	UJSMGameInstance* GameInst__ = UJSMGameInstance::GetGameInstance();												\
	if (IsValid(GameInst__))																						\
	{																												\
		UJSMEventManager* EventMan__ = GameInst__->GetEventManager();												\
		UE_LOG(LogTemp,Warning,TEXT(" BIND_EVENT_JSM : %d"),GameInst__->GetUniqueID());							\
		if (IsValid(EventMan__))																					\
		{																											\
			EventMan__->EventName.AddUObject(Object, TargetFunction);												\
		}																											\
	}																												\
}