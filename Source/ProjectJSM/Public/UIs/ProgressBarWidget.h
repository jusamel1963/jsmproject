// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UIs/BaseUserWidget.h"
#include "ProgressBarWidget.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UProgressBarWidget : public UBaseUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr<class UProgressBar> Pb_Loading;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr <class UTextBlock> TB_LoadingName;

public:
	void SetLoadingName(const FText& LoadingName);
	void Update_Loading(const float& InValue);
};
