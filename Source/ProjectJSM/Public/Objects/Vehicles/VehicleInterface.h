// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "VehicleInterface.generated.h"

DECLARE_DELEGATE(FJSMVehicleDataLoadComplete);

USTRUCT(BlueprintType)
struct FSeatInfo
{
	GENERATED_USTRUCT_BODY()

public:
	FSeatInfo()
	{
		Character = nullptr;
		ControlID = 0;
	}

	FSeatInfo(class ACharacterBase* InChar, const uint32& InControlID)
	{
		Character = InChar;
		ControlID = InControlID;
	}

	UPROPERTY(Transient)
	TObjectPtr<class ACharacterBase> Character;

	UPROPERTY(Transient)
	uint32 ControlID;
};

USTRUCT()
struct PROJECTJSM_API FJSMVehicleAssetLoadRequest
{
	GENERATED_USTRUCT_BODY()

public:
	FJSMVehicleDataLoadComplete CompleteDelegate;
	TSharedPtr<struct FStreamableHandle> AsyncLoadHandle;
};

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UVehicleInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class PROJECTJSM_API IVehicleInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void LoadAssetAsync(FJSMVehicleDataLoadComplete LoadCompleteDelegate) PURE_VIRTUAL(LoadAssetAsync, return;);
	virtual void LoadAssetCompleted() PURE_VIRTUAL(LoadAssetCompleted, return;);
	virtual void InitializeVehicleResources() PURE_VIRTUAL(InitializeVehicleResources, return;);
	virtual void LoadSynchronous() PURE_VIRTUAL(LoadSynchronous, return;);
	virtual void EnterVehicle(class ACharacterBase* InEnterPlayer) PURE_VIRTUAL(EnterVehicle, return;);
	virtual void ExitVehicle(const uint32& InID) PURE_VIRTUAL(ExitVehicle, return;);
	virtual bool IsOverlapPlayer(const uint32& Id) const PURE_VIRTUAL(IsOverlapPlayer, return false;);
	virtual void ForwardMovementAction(const float& InValue) PURE_VIRTUAL(ForwardMovementAction, return;);
	virtual void RightMovementAction(const float& InValue) PURE_VIRTUAL(RightMovementAction, return;);
	virtual void CameraUpAction(const float& InValue) PURE_VIRTUAL(CameraUpAction, return;);
	virtual void CameraRightAction(const float& InValue) PURE_VIRTUAL(CameraRightAction, return;);
};
