// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UIs/BaseUserWidget.h"
#include "CharacterDataInfoWidget.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UCharacterDataInfoWidget : public UBaseUserWidget
{
	GENERATED_BODY()
public:
	virtual void NativeTick(const FGeometry& myGeometry, float inDeltaTime) override;
	void UpdateCharacterDataInfo(const float& inDeltaTime);
protected:
	virtual void NativeConstruct() override;
public:
	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UOverlay> Overlay_Root;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr<class UProgressBar> Pb_Health;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr<class UProgressBar> Pb_Stamina;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr<class UImage> Img_Character;

	void UpdateValue(float& InValue, const float& CompareValue, const float& MaxValue, const float& inDeltaTime);

private:
	const float DecrementSpeed = 100.0f;
	const FLinearColor MinHpColor = FLinearColor(1.0f, 0.0f, 0.0f, 1.0f);
	const FLinearColor MaxHpColor = FLinearColor(0.0f, 1.0f, 0.0f, 1.0f);

	float PreviousHp = -1.0f;
	float PreviousStamina = -1.0f;

}; 
