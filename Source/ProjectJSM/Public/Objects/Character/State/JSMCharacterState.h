// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "JSMCharacterStateDef.h"
#include "Framework/State/JSMStateBase.h"


class FJSMCharacterInitializeState : public FJSMStateBase<EJSMCharacterInitializeStateID>, public TSharedFromThis<FJSMCharacterInitializeState>
{
public:
	FJSMCharacterInitializeState(EJSMCharacterInitializeStateID InStateID)
		: FJSMStateBase<EJSMCharacterInitializeStateID>(InStateID)
	{

	}
};

