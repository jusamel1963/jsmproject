// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Vehicles/WheelVehicleBase.h"

#include "Objects/Vehicles/VehicleInfo.h"
#include "Components/SceneComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/TextRenderComponent.h"
#include "Framework/JSMGameInstance.h"
#include "Data/DataAssets/ItemData/JSMVehicleDataAsset.h"
#include "Framework/JSMItemManagerSubsystem.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "Engine/ActorChannel.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Statics/AssetLoader.h"
#include "Net/UnrealNetwork.h"
#include "Net/Core/PushModel/PushModel.h"
#include "Components/SkeletalMeshComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/PointLightComponent.h"
#include "Components/SphereComponent.h"
#include "Components/BoxComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "ChaosVehicleMovementComponent.h"
#include "ChaosWheeledVehicleMovementComponent.h"

// Sets default values
AWheelVehicleBase::AWheelVehicleBase()
{
	EnterBoxRightComponent = CreateDefaultSubobject<UBoxComponent>(FName(TEXT("RightEnter")));
	if (IsValid(EnterBoxRightComponent))
	{
		EnterBoxRightComponent->SetupAttachment(RootComponent, FName(TEXT("RightEnter")));
		EnterBoxRightComponent->SetBoxExtent(FVector_NetQuantize(100.f, 100.f, 100.f));
	}

	EnterBoxLeftComponent = CreateDefaultSubobject<UBoxComponent>(FName(TEXT("LeftEnter")));
	if (IsValid(EnterBoxLeftComponent))
	{
		EnterBoxLeftComponent->SetupAttachment(RootComponent, FName(TEXT("LeftEnter")));
		EnterBoxLeftComponent->SetBoxExtent(FVector_NetQuantize(100.f, 100.f, 100.f));
	}

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(FName(TEXT("SpringArm")));
	if (IsValid(SpringArmComp))
	{
		SpringArmComp->SetupAttachment(RootComponent);
	}

	CameraComp = CreateDefaultSubobject<UCameraComponent>(FName(TEXT("Camera")));
	if (IsValid(CameraComp))
	{
		CameraComp->SetupAttachment(SpringArmComp);
	}

	SeatArray.Reserve(2);
	SeatArray.Emplace(FName(TEXT("pilot_seat1")), FSeatInfo());
	SeatArray.Emplace(FName(TEXT("pilot_seat2")), FSeatInfo());
}

// Called when the game starts or when spawned
void AWheelVehicleBase::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		SS_CreateVehicleInfo();
		if (IsValid(EnterBoxRightComponent))
		{
			EnterBoxRightComponent->OnComponentBeginOverlap.AddDynamic(this, &AWheelVehicleBase::OnOverlapBegin);
			EnterBoxRightComponent->OnComponentEndOverlap.AddDynamic(this, &AWheelVehicleBase::OnOverlapEnd);
		}

		if (IsValid(EnterBoxLeftComponent))
		{
			EnterBoxLeftComponent->OnComponentBeginOverlap.AddDynamic(this, &AWheelVehicleBase::OnOverlapBegin);
			EnterBoxLeftComponent->OnComponentEndOverlap.AddDynamic(this, &AWheelVehicleBase::OnOverlapEnd);
		}
	}
}

void AWheelVehicleBase::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(AWheelVehicleBase, RepVehicleInfo, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(AWheelVehicleBase, RepVehicleDataKey, SharedParams);
}

void AWheelVehicleBase::OnRep_VehicleInfo()
{
	bLoadCompleted = false;

	if (bUseAsyncLoad)
	{
		LoadAssetAsync(FJSMVehicleDataLoadComplete::CreateUObject(this, &AWheelVehicleBase::InitializeVehicleResources));
	}
	else
	{
		LoadSynchronous();
		InitializeVehicleResources();
	}
}

void AWheelVehicleBase::LoadAssetAsync(FJSMVehicleDataLoadComplete LoadCompleteDelegate)
{
	if (AssetLoadRequest.AsyncLoadHandle != nullptr)
	{
		return;
	}

	UJSMVehicleDataAsset* VehicleDataAsset = GetVehicleDataAsset();

	if (!IsValid(VehicleDataAsset))
	{
		return;
	}

	if (VehicleDataAsset->GetObjectPathsToLoad().Num() == 0)
	{
		LoadCompleteDelegate.ExecuteIfBound();
	}
	else
	{
		FStreamableDelegate StreamableDelegate = FStreamableDelegate::CreateUObject(this, &AWheelVehicleBase::LoadAssetCompleted);
		AssetLoadRequest.CompleteDelegate = LoadCompleteDelegate;
		AssetLoadRequest.AsyncLoadHandle = UAssetLoader::AssetLoadAsyncStart(VehicleDataAsset->GetObjectPathsToLoad(), StreamableDelegate);
	}
}

void AWheelVehicleBase::LoadAssetCompleted()
{
	AssetLoadRequest.CompleteDelegate.ExecuteIfBound();
	AssetLoadRequest.AsyncLoadHandle.Reset();
}

void AWheelVehicleBase::InitializeVehicleResources()
{
	UJSMVehicleDataAsset* VehicleDataAsset = GetVehicleDataAsset();
	if (IsValid(VehicleDataAsset))
	{
		if (VehicleDataAsset->ItemSkeletalMesh.IsValid())
		{
			UClass* AnimInstClass = UAssetLoader::AssetLoadClass(VehicleDataAsset->WheelVehicleBPClass);
			GetMesh()->SetSkeletalMeshAsset(VehicleDataAsset->ItemSkeletalMesh.Get());
			GetMesh()->SetAnimInstanceClass(AnimInstClass);
		}
	}
	bLoadCompleted = true;
	OnLoadComplete.ExecuteIfBound();
}

void AWheelVehicleBase::LoadSynchronous()
{
	if (AssetLoadRequest.AsyncLoadHandle != nullptr)
	{
		return;
	}

	UJSMVehicleDataAsset* VehicleDataAsset = GetVehicleDataAsset();
	if (IsValid(VehicleDataAsset))
	{
		VehicleDataAsset->LoadSynchronous();
	}
}

void AWheelVehicleBase::SS_CreateVehicleInfo()
{
	if (!HasAuthority())
	{
		return;
	}

	UJSMGameInstance* GameInstance = UJSMGameInstance::GetGameInstance();
	check(GameInstance);

	UJSMItemManagerSubsystem* ItemManager = JSMFL::GetGameInstanceSubsystem<UJSMItemManagerSubsystem>();
	if (IsValid(ItemManager) == false)
	{
		return;
	}

	UVehicleInfo* VehicleInfo = ItemManager->CreateJSMVehicleInfo(RepVehicleDataKey, this);
	if (IsValid(VehicleInfo) == false)
	{
		return;
	}

	SS_SetRepVehicleInfo(VehicleInfo);
}

void AWheelVehicleBase::SS_SetRepVehicleInfo(UVehicleInfo* InVehicleInfo)
{
	if (!HasAuthority())
	{
		return;
	}

	MARK_PROPERTY_DIRTY_FROM_NAME(AWheelVehicleBase, RepVehicleInfo, this);
	RepVehicleInfo = InVehicleInfo;
	MARK_PROPERTY_DIRTY_FROM_NAME(AWheelVehicleBase, RepVehicleDataKey, this);
	RepVehicleDataKey = InVehicleInfo->GetRepVehicleDataKey();
	OnRep_VehicleInfo();
}

UVehicleInfo* AWheelVehicleBase::GetRepVehicleInfo() const
{
	return RepVehicleInfo;
}

UJSMVehicleDataAsset* AWheelVehicleBase::GetVehicleDataAsset() const
{
	if (IsValid(RepVehicleInfo) && RepVehicleInfo->GetVehicleDataAsset().IsValid())
	{
		return RepVehicleInfo->GetVehicleDataAsset().Get();
	}
	return nullptr;
}

bool AWheelVehicleBase::ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);
	if (RepVehicleInfo != nullptr)
	{
		wrote |= Channel->ReplicateSubobject(RepVehicleInfo, *Bunch, *RepFlags);
	}
	return wrote;
}

bool AWheelVehicleBase::IsOverlapPlayer(const uint32& Id) const
{
	if (OverlapPlayers.Find(Id) != nullptr)
	{
		return true;
	}
	return false;
}

void AWheelVehicleBase::EnterVehicle(class ACharacterBase* InEnterPlayer)
{
	if (!IsValid(InEnterPlayer))
	{
		return;
	}

	FName SeatName = FName(TEXT("None"));
	for (auto& Seat : SeatArray)
	{
		FSeatInfo SeatInfo = Seat.Value;
		if (IsValid(SeatInfo.Character))
		{
			continue;
		}
		else
		{
			SeatName = Seat.Key;
			break;
		}
	}

	if (SeatName.Compare(TEXT("None")) == 0)
	{
		return;
	}

	InEnterPlayer->AttachToActor(this, FAttachmentTransformRules::SnapToTargetIncludingScale, SeatName);
	OverlapPlayers.Remove(InEnterPlayer->GetUniqueID());

	APlayerController* EnterPlayerController = Cast<APlayerController>(InEnterPlayer->GetController());
	if (IsValid(EnterPlayerController))
	{
		if (DriverID == 0)
		{
			DriverID = EnterPlayerController->GetUniqueID();
			EnterPlayerController->Possess(this);
		}
		else
		{
			EnterPlayerController->SetViewTargetWithBlend(this);
		}
		SeatArray[SeatName] = FSeatInfo(InEnterPlayer, EnterPlayerController->GetUniqueID());
	}
}

void AWheelVehicleBase::ExitVehicle(const uint32& InID)
{
	CS_ExitVehicle(InID);
}

void AWheelVehicleBase::OnOverlapBegin(UPrimitiveComponent* OpverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APlayerCharacter* Character = Cast<APlayerCharacter>(OtherActor);
	if (!IsValid(Character))
	{
		return;
	}

	OverlapPlayers.Emplace(Character->GetUniqueID(), Character);
}

void AWheelVehicleBase::OnOverlapEnd(class UPrimitiveComponent* OpverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	APlayerCharacter* Character = Cast<APlayerCharacter>(OtherActor);
	if (!IsValid(Character))
	{
		return;
	}
	OverlapPlayers.Remove(Character->GetUniqueID());
}

void AWheelVehicleBase::CS_ExitVehicle_Implementation(const uint32& InID)
{
	ForceNetUpdate();
	FName SeatName = FName(TEXT("None"));
	for (auto& Seat : SeatArray)
	{
		FSeatInfo SeatInfo = Seat.Value;
		if (SeatInfo.ControlID != InID)
		{
			continue;
		}
		else
		{
			SeatName = Seat.Key;
			break;
		}
	}

	if (SeatName.Compare(TEXT("None")) == 0)
	{
		return;
	}

	ACharacterBase* SeatCharacter = SeatArray[SeatName].Character;
	if (IsValid(SeatCharacter))
	{
		SeatCharacter->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		if ((DriverID == InID))
		{
			SeatCharacter->SetActorLocation(EnterBoxRightComponent->GetComponentLocation());
			APlayerController* LocalController = Cast<APlayerController>(GetController());
			if (IsValid(LocalController))
			{
				LocalController->Possess(SeatCharacter);
			}
			DriverID = 0;
		}
		else
		{
			(SeatCharacter)->SetActorLocation(EnterBoxLeftComponent->GetComponentLocation());
			APlayerController* PassengerCtr = Cast<APlayerController>(SeatCharacter->GetController());
			if (IsValid(PassengerCtr))
			{
				PassengerCtr->SetViewTargetWithBlend(SeatCharacter);
			}
		}
		(SeatCharacter)->ExitVehicle();
	}

	SeatArray[SeatName] = FSeatInfo();
}

void AWheelVehicleBase::ForwardMovementAction(const float& InValue)
{
	UE_LOG(LogTemp, Warning, TEXT("AWheelVehicleBase::ForwardMovementAction InValue : %lf"), InValue);
	UChaosVehicleMovementComponent* VehicleMovementComp = GetVehicleMovement();

	if (IsValid(VehicleMovementComp))
	{
		if (InValue > 0)
		{
			VehicleMovementComp->SetThrottleInput(InValue);
		}
		else if(InValue < 0)
		{
			VehicleMovementComp->SetBrakeInput(-1 * (InValue));
		}
		else
		{
			VehicleMovementComp->SetBrakeInput(InValue);
			VehicleMovementComp->SetThrottleInput(InValue);
		}
	}
}

//HeliThrottle - Forward, HeliTyurn - CameraRight Turn, HeliPitch - CameraUp

void AWheelVehicleBase::RightMovementAction(const float& InValue)
{
	UChaosVehicleMovementComponent* VehicleMovementComp = GetVehicleMovement();

	if (IsValid(VehicleMovementComp))
	{
		VehicleMovementComp->SetSteeringInput(InValue);
	}
}

void AWheelVehicleBase::CameraRightAction(const float& InValue)
{
	if (IsValid(SpringArmComp))
	{
		//SpringArmComp->AddRelativeRotation(FRotator(0, InValue, 0));
	}
}

void AWheelVehicleBase::CameraUpAction(const float& InValue)
{
	if (IsValid(SpringArmComp))
	{
		//SpringArmComp->AddRelativeRotation(FRotator((-1 *InValue), 0, 0));
	}
}