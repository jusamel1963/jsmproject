// Copyright JSM Studio, Inc. All Rights Reserved.


#include "AbilitySystem/Ability/JSMGA_PickupItem.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Objects/Items/JSMItemActor.h"
#include "Objects/Items/JSMItemInfo.h"
#include "Data/DataAssets/ItemData/JSMItemDataAsset.h"
#include "Data/DataAssets/ItemData/JSMUsableItemDataAsset.h"
#include "AbilitySystem/GameEffect/JSMGameplayEffect.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "Data/DataAssets/ItemData/JSMWeaponAmmoDataAsset.h"

UJSMGA_PickupItem::UJSMGA_PickupItem() : Super()
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::ServerOnly;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::NonInstanced;

	FGameplayTag AbilityTag = REQ_GAMEPLAYTAG("Ability.Inventory.PickupItem");
	AbilityTags.AddTag(AbilityTag);
	ActivationOwnedTags.AddTag(AbilityTag);

	ActivationBlockedTags.Reset();
	ActivationBlockedTags.AddTag(REQ_GAMEPLAYTAG("Status.Character.Dead"));

	FGameplayTag AbilityTriggerTag = REQ_GAMEPLAYTAG("Event.Inventory.PickupItem");
	FAbilityTriggerData TriggerData;
	TriggerData.TriggerTag = AbilityTriggerTag;
	TriggerData.TriggerSource = EGameplayAbilityTriggerSource::GameplayEvent;
	AbilityTriggers.Add(TriggerData);
}


void UJSMGA_PickupItem::PostCommitActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::PostCommitActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	if (HasAuthority(&ActivationInfo) == false)
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	APlayerCharacter* OwnerCharacter = Cast<APlayerCharacter>(ActorInfo->AvatarActor);
	if ((IsValid(OwnerCharacter) == false) || !IsValid(TriggerEventData->Target))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	AJSMItemActor* ItemActor = TriggerEventData != nullptr ? Cast<AJSMItemActor>(const_cast<AActor*>(TriggerEventData->Target.Get())) : nullptr;
	if (IsValid(ItemActor) == false || ItemActor->IsEmpty())
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	UJSMItemInfo* ItemInfo = ItemActor->GetRepItemInfo();
	if (IsValid(ItemInfo) == false || ItemInfo->GetItemDataAsset().IsValid() == false)
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	ItemActor->BeginPickup();

	UAbilitySystemComponent* AbilitySystemComponent = OwnerCharacter->GetAbilitySystemComponent();
	UJSMItemDataAsset* ItemDataAsset = ItemInfo->GetItemDataAsset().Get();
	UWeaponCtrlComponent* WeaponCtrlComponent = OwnerCharacter->GetWeaponCtrl();
	if (!IsValid(ItemDataAsset))
	{
		return;
	}

	if (IsValid(AbilitySystemComponent) && (ItemDataAsset->ItemType == EJSMItemType::UsableItem))
	{
		const TSubclassOf<class UJSMGameplayEffect>& ItemEffect = ItemDataAsset->PickupEffect;
		if (IsValid(ItemEffect))
		{
			FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
			FGameplayEffectSpecHandle EffectSpecHandle = AbilitySystemComponent->MakeOutgoingSpec(ItemEffect, GetAbilityLevel(), EffectContext);
			EffectSpecHandle.Data->GetContext().AddInstigator(OwnerCharacter, ItemActor);
			UJSMUsableItemDataAsset* UsableItemDataAsset = Cast<UJSMUsableItemDataAsset>(ItemDataAsset);
			if (IsValid(UsableItemDataAsset))
			{
				EffectSpecHandle.Data->SetSetByCallerMagnitude(REQ_GAMEPLAYTAG("SetByCaller.AddAmount"), UsableItemDataAsset->Amount);
			}

			AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*EffectSpecHandle.Data.Get());
		}
	}
	else if (IsValid(WeaponCtrlComponent) && (ItemDataAsset->ItemType == EJSMItemType::WeaponAmmo))
	{
		UJSMWeaponAmmoDataAsset* WeaponAmmoDataAsset = Cast<UJSMWeaponAmmoDataAsset>(ItemDataAsset);
		if (IsValid(WeaponAmmoDataAsset))
		{
			bool bEquip = WeaponCtrlComponent->EquipWeaponAmmo(WeaponAmmoDataAsset);
			if (bEquip == false)
			{
				ItemActor->CancelPickup();
				EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
			}
		}
	}

	ItemActor->EndPickup();
	EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
}

void UJSMGA_PickupItem::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}
