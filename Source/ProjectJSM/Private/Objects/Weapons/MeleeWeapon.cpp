// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Weapons/MeleeWeapon.h"
#include "Components/StaticMeshComponent.h"

AMeleeWeapon::AMeleeWeapon()
{
	StaticMeshComp->SetRelativeScale3D(FVector_NetQuantize(0.8f, 0.8f, 0.8f));
	SkeletalMeshComponent->SetRelativeScale3D(FVector_NetQuantize(0.8f, 0.8f, 0.8f));
}