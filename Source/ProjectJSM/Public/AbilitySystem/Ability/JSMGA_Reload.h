// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Ability/JSMGameplayAbility.h"
#include "JSMGA_Reload.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UJSMGA_Reload : public UJSMGameplayAbility
{
	GENERATED_BODY()

public:
	UJSMGA_Reload();
	virtual void PostCommitActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
private:

private:
	float ReloadDuration = 0.0f;
};
