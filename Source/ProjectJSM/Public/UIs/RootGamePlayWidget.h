// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UIs/BaseUserWidget.h"
#include "RootGamePlayWidget.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API URootGamePlayWidget : public UBaseUserWidget
{
	GENERATED_BODY()
public:
	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UCanvasPanel> MainPanel;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UBaseUserWidget> UMG_CrossHiar;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UBaseUserWidget> UMG_WeaponHud;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UBaseUserWidget> UMG_WeaponScope;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UBaseUserWidget> UMG_CharacterData;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UBaseUserWidget> UMG_BloodScreen;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UBaseUserWidget> UMG_ProgressBar;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UBaseUserWidget> UMG_WeaponMenu;

public:
	virtual void NativeTick(const FGeometry& myGeometry, float inDeltaTime) override;
	void Update_WeaponAndCrossHairHud(UTexture2D* PrimaryWeapon, UTexture2D* HolsterWeapon, const FText& MagAmmo, const FText& MagCapacity, UTexture2D* CrossHair, UTexture2D* WeaponScope);
	void Update_WeaponHud(class UTexture2D* PrimaryWeapon, class UTexture2D* HolsterWeapon, const FText& MagAmmo, const FText& MagCapacity);
	void Update_CrossHair(UTexture2D* CrossHair);
	void Update_WeaponScope(UTexture2D* WeaponScope);
	void Update_ProgressBar(const float& InValue);
	void SetVisibleWeaponScope(const bool& bVisible);
	void SetVisibleBloodScreen(const bool& bVisible);
	void SetVisibleProgressBar(const bool& bVisible, const FText& LoadingName);
	void SetVisibleWeaponMenu(const bool& bVisible);

	void SetVisibleWidget(UBaseUserWidget* InWidget, ESlateVisibility SlatVisible);
	void SetParentOwnID(const uint32& InID, class AJSMPlayerController* InController);
	void UpdateBloodScreen(const float& inDeltaTime);
	void SetWeaponImage(UTexture2D* InWeaponImage, const int& Idx, const FText& Ammo = FText());
protected:
	virtual void NativeConstruct() override;

	const float MinBloodScreenOpacity = 1.0f;
	const float MaxBloodScreenOpacity = 0.1f;
};
