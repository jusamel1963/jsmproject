// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbilityTargetActor.h"
#include "GATA_Test.generated.h"

UCLASS()
class PROJECTJSM_API AGATA_Test : public AGameplayAbilityTargetActor
{
	GENERATED_BODY()
	
};
