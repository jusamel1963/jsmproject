// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WheelVehicleSpawnerActor.generated.h"


USTRUCT(Blueprintable)
struct FVehicleSpawnData
{
	GENERATED_BODY()
public:

	UPROPERTY(EditInstanceOnly)
	FName VehicleKeyName;
};


UCLASS()
class PROJECTJSM_API AWheelVehicleSpawnerActor : public AActor
{
	GENERATED_BODY()

public:
	AWheelVehicleSpawnerActor();
	void SetRespawnTimer();
	virtual void Execute();

protected:
	virtual void BeginPlay() override;

	void SpawnVehicle();
	UFUNCTION()
	void OnSpawnTimer();
	UFUNCTION()
	void OnDestroyActor(AActor* DestroyedActor);

public:
	UPROPERTY(EditAnywhere, Category = "Spawn Config")
	TArray<FVehicleSpawnData> SpawnData;
	UPROPERTY(EditAnywhere, Category = "Spawn Config", meta = (EditCondition = "bRespawnable", ShortTooltip = " == 0 : Only once, > n : n time respawn "))
	float RespawnTimeInterval = 0.0f;
	UPROPERTY(EditAnywhere, Category = "Spawn Config")
	bool bRespawnable = false;

protected:
	UPROPERTY(Transient)
	TArray<TObjectPtr<AActor>>	SpawnedActors;
	FTimerHandle SpawnerTimerHandle;
};
