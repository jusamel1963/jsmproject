// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UIs/BaseUserWidget.h"
#include "CrossHairWidget.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UCrossHairWidget : public UBaseUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UOverlay> Overlay_Root;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr<class UImage> Img_CrossHair;
public:
	void Update_CrossHair(class UTexture2D* InCrossHair);
};
