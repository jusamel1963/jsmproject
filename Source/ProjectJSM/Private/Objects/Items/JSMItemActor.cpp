// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Items/JSMItemActor.h"
#include "Objects/Items/JSMItemInfo.h"
#include "Components/SceneComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/TextRenderComponent.h"
#include "Framework/JSMGameInstance.h"
#include "Data/DataAssets/ItemData/JSMItemDataAsset.h"
#include "Framework/JSMItemManagerSubsystem.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "Engine/ActorChannel.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Statics/AssetLoader.h"
#include "Net/UnrealNetwork.h"
#include "Net/Core/PushModel/PushModel.h"
#include "Components/PointLightComponent.h"

FName AJSMItemActor::SceneComponentName(TEXT("SceneComponent"));
FName AJSMItemActor::ItemStaticMeshComponentName(TEXT("ItemMeshComponent"));
FName AJSMItemActor::ItemCollisionProfileName(TEXT("Item"));;
FName AJSMItemActor::VolumeComponentName(TEXT("VolumeComponent"));
FName AJSMItemActor::PointLightComponentName(TEXT("PointLightComponent"));

AJSMItemActor::AJSMItemActor()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	USceneComponent* SceneComponent = CreateDefaultSubobject<USceneComponent>(SceneComponentName);
	RootComponent = SceneComponent;

	ItemStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(ItemStaticMeshComponentName);
	if (IsValid(ItemStaticMeshComponent))
	{
		ItemStaticMeshComponent->bReceivesDecals = false;
		ItemStaticMeshComponent->CastShadow = false;
		//ItemStaticMeshComponent->SetCollisionObjectType(ECC_ITEM);
		ItemStaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		//ItemStaticMeshComponent->SetCollisionProfileName(ItemCollisionProfileName);
		ItemStaticMeshComponent->SetupAttachment(RootComponent);
	}

	VolumeComponent = CreateDefaultSubobject<USphereComponent>(VolumeComponentName);
	if (IsValid(VolumeComponent))
	{
		VolumeComponent->SetupAttachment(RootComponent);
	}

	PointLightComponent = CreateOptionalDefaultSubobject<UPointLightComponent>(PointLightComponentName);
	if (IsValid(PointLightComponent))
	{
		PointLightComponent->SetupAttachment(RootComponent);
		PointLightComponent->SetRelativeLocation(FVector_NetQuantize(10.0f, 10.0f, 5.0f));
		PointLightComponent->Intensity = 3000.0f;
	}
}

// Called when the game starts or when spawned
void AJSMItemActor::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		if (IsValid(VolumeComponent))
		{
			VolumeComponent->OnComponentBeginOverlap.AddDynamic(this, &AJSMItemActor::OnOverlapBegin);
		}
		SS_CreateItemInfo();
	}
}

void AJSMItemActor::OnOverlapBegin(UPrimitiveComponent* OpverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APlayerCharacter* Character = Cast<APlayerCharacter>(OtherActor);

	UJSMItemDataAsset* ItemDataAsset = GetItemDataAsset();
	if (IsValid(Character) && IsValid(ItemDataAsset))
	{
		UJSMAbilitySystemComponent* AbilitySystemComponent = Cast<UJSMAbilitySystemComponent>(Character->GetAbilitySystemComponent());
		if (IsValid(AbilitySystemComponent))
		{
			FGameplayEventData EventData;
			EventData.EventTag = ItemDataAsset->OverlapEventTag;
			EventData.Instigator = Character;
			EventData.Target = this;
			AbilitySystemComponent->HandleGameplayEvent(ItemDataAsset->OverlapEventTag, &EventData);
		}
	}
}

void AJSMItemActor::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(AJSMItemActor, RepItemInfo, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(AJSMItemActor, RepItemDataKey, SharedParams);
}

// Called every frame
void AJSMItemActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

UJSMItemInfo* AJSMItemActor::GetRepItemInfo() const
{
	return RepItemInfo;
}

bool AJSMItemActor::IsLoadCompleted()
{
	return bLoadCompleted;
}

void AJSMItemActor::SS_CreateItemInfo()
{
	if (!HasAuthority())
	{
		return;
	}

	UJSMGameInstance* GameInstance = UJSMGameInstance::GetGameInstance();
	check(GameInstance);

	UJSMItemManagerSubsystem* ItemManager = JSMFL::GetGameInstanceSubsystem<UJSMItemManagerSubsystem>();
	if (IsValid(ItemManager) == false)
	{
		return;
	}

	UJSMItemInfo* JSMItemInfo = ItemManager->CreateJSMItemInfo(RepItemDataKey, this);
	if (IsValid(JSMItemInfo) == false)
	{
		return;
	}

	SS_SetRepItemInfo(JSMItemInfo);
}

void AJSMItemActor::SS_SetRepItemInfo(UJSMItemInfo* InItemInfo)
{
	if (!HasAuthority())
	{
		return;
	}

	MARK_PROPERTY_DIRTY_FROM_NAME(AJSMItemActor, RepItemInfo, this);
	RepItemInfo = InItemInfo;
	MARK_PROPERTY_DIRTY_FROM_NAME(AJSMItemActor, RepItemDataKey, this);
	RepItemDataKey = InItemInfo->GetRepItemDataKey();

	OnRep_ItemInfo();
}

void AJSMItemActor::SS_SetCount(int32 InCount)
{
	if (!HasAuthority())
	{
		return;
	}

	if (ensure(IsValid(RepItemInfo)))
	{
		RepItemInfo->SetRepItemCount(InCount);
	}
}

bool AJSMItemActor::ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);
	if (RepItemInfo != nullptr)
	{
		wrote |= Channel->ReplicateSubobject(RepItemInfo, *Bunch, *RepFlags);
	}
	return wrote;
}

void AJSMItemActor::OnRep_ItemInfo()
{
	bLoadCompleted = false;

	if (bUseAsyncLoad)
	{
		LoadAssetAsync(FJSMItemDataLoadComplete::CreateUObject(this, &AJSMItemActor::InitializeItemResources));
	}
	else
	{
		LoadSynchronous();
		InitializeItemResources();
	}

	UpdateDisplayName();
}

void AJSMItemActor::UpdateDisplayName()
{
	if (IsValid(RepItemInfo) && RepItemInfo->GetItemDataAsset().IsValid())
	{
		UTextRenderComponent* TextRenderComponent = Cast<UTextRenderComponent>(GetComponentByClass(UTextRenderComponent::StaticClass()));
		if (IsValid(TextRenderComponent))
		{
			TextRenderComponent->SetText(RepItemInfo->GetItemDataAsset()->DisplayName);
		}
	}
}

void AJSMItemActor::LoadSynchronous()
{
	// If previous async request is not finished, don't do anything.
	if (AssetLoadRequest.AsyncLoadHandle != nullptr)
	{
		return;
	}

	UJSMItemDataAsset* ItemDataAsset = GetItemDataAsset();
	if (IsValid(ItemDataAsset))
	{
		ItemDataAsset->LoadSynchronous();
	}
}

void AJSMItemActor::InitializeItemResources()
{
	UJSMItemDataAsset* ItemDataAsset = GetItemDataAsset();
	if (IsValid(ItemDataAsset))
	{
		if (ItemDataAsset->ItemStaticMesh.IsValid())
		{
			ItemStaticMeshComponent->SetStaticMesh(ItemDataAsset->ItemStaticMesh.Get());
			ItemStaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
			VolumeComponent->SetSphereRadius(ItemDataAsset->VolumeRadius);
		}
	}
	bLoadCompleted = true;
	OnLoadComplete.ExecuteIfBound();
}


void AJSMItemActor::LoadAssetAsync(FJSMItemDataLoadComplete LoadCompleteDelegate)
{
	if (AssetLoadRequest.AsyncLoadHandle != nullptr)
	{
		return;
	}

	UJSMItemDataAsset* ItemDataAsset = GetItemDataAsset();

	if (!IsValid(ItemDataAsset))
	{
		return;
	}

	if (ItemDataAsset->GetObjectPathsToLoad().Num() == 0)
	{
		LoadCompleteDelegate.ExecuteIfBound();
	}
	else
	{
		FStreamableDelegate StreamableDelegate = FStreamableDelegate::CreateUObject(this, &AJSMItemActor::LoadAssetCompleted);
		AssetLoadRequest.CompleteDelegate = LoadCompleteDelegate;
		AssetLoadRequest.AsyncLoadHandle = UAssetLoader::AssetLoadAsyncStart(ItemDataAsset->GetObjectPathsToLoad(), StreamableDelegate);
	}
}

UJSMItemDataAsset* AJSMItemActor::GetItemDataAsset() const
{
	if (IsValid(RepItemInfo) && RepItemInfo->GetItemDataAsset().IsValid())
	{
		return RepItemInfo->GetItemDataAsset().Get();
	}
	return nullptr;
}

void AJSMItemActor::LoadAssetCompleted()
{
	AssetLoadRequest.CompleteDelegate.ExecuteIfBound();
	AssetLoadRequest.AsyncLoadHandle.Reset();
}

bool AJSMItemActor::IsEmpty() const
{
	return IsValid(RepItemInfo) == false;
}

void AJSMItemActor::BeginPickup()
{
	bBeginPickup = true;
}

void AJSMItemActor::EndPickup()
{
	if (bBeginPickup)
	{
		Destroy();
	}
}

void AJSMItemActor::CancelPickup()
{
	bBeginPickup = false;
}