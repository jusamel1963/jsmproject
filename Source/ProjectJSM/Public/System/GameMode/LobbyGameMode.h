// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "LobbyGameMode.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API ALobbyGameMode : public AGameMode
{
	GENERATED_BODY()
public:
	ALobbyGameMode();

	virtual void BeginPlay() override;

protected:
	UPROPERTY(BlueprintReadWrite, Category = "Widget")
	class UUserWidget* LobbyMenuWidget;
};
