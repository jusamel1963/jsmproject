// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "GameEnum.generated.h"

UENUM(BlueprintType)
enum class EJSMItemType : uint8
{
	None					UMETA(DisplayName = "None"),
	Weapon					UMETA(DisplayName = "Weapon"),
	UsableItem				UMETA(DisplayName = "UsableItem"),
	ActiveItem				UMETA(DisplayName = "ActiveItem"),
	WeaponAmmo				UMETA(DisplayName = "WeaponAmmo"),
	SkillExchangeCard		UMETA(DisplayName = "SkillExchangeCard"),
};

UENUM(BlueprintType)
enum class EJSMGrade : uint8
{
	None					UMETA(DisplayName = "None"),
	Common					UMETA(DisplayName = "Common"),
	Uncommon				UMETA(DisplayName = "Uncommon"),
	Rare					UMETA(DisplayName = "Rare"),
	Unique					UMETA(DisplayName = "Unique"),
	Secret					UMETA(DisplayName = "Secret"),
};

UENUM(BlueprintType)
enum class EWeaponAttachType : uint8
{
	None					UMETA(DisplayName = "None"),
	Hip						UMETA(DisplayName = "Hip"),
	Back					UMETA(DisplayName = "Back"),
	BackMelee				UMETA(DisplayName = "BackMelee"),
	Throwable				UMETA(DisplayName = "Throwable"),
	Max						UMETA(DisplayName = "Max"),
};

UENUM(BlueprintType)
enum class EFiringMode : uint8
{
	None = 0				UMETA(DisplayName = "None"),
	Single					UMETA(DisplayName = "Single"),
	Burst					UMETA(DisplayName = "Burst"),
	Auto					UMETA(DisplayName = "Auto")
};

UENUM(BlueprintType)
enum class EWeaponAmmoType : uint8
{
	None = 0				UMETA(DisplayName = "None"),
	Pistol					UMETA(DisplayName = "Pistol"),
	Rifle					UMETA(DisplayName = "Rifle"),
	Grenade					UMETA(DisplayName = "Grenade"),
	Sniper					UMETA(DisplayName = "Sniper"),
	Shotgun					UMETA(DisplayName = "Shotgun"),
};

UENUM(BlueprintType)
enum class EAIState : uint8
{
	None = 0				UMETA(DisplayName = "None"),
	Patrol					UMETA(DisplayName = "Patrol"),
	Attack					UMETA(DisplayName = "Attack"),
	Investigate				UMETA(DisplayName = "Investigate"),
	Dead					UMETA(DisplayName = "Dead"),
};

UENUM(BlueprintType)
enum class EAISense : uint8
{
	Sight					UMETA(DisplayName = "Sight"),
	Hearing					UMETA(DisplayName = "Hearing"),
	Damage					UMETA(DisplayName = "Damage"),
	Max						UMETA(DisplayName = "Max"),
};

UENUM(BlueprintType)
enum class EThrowable : uint8
{
	Stone					UMETA(DisplayName = "Stone"),
	Bottle					UMETA(DisplayName = "Bottle"),
	Grenade					UMETA(DisplayName = "Grenade"),
	Knife					UMETA(DisplayName = "Knife"),
};

UENUM(BlueprintType)
enum class ECoverType : uint8
{
	None = 0				UMETA(DisplayName = "None"),
	FindCover				UMETA(DisplayName = "FindCover"),
	EnterCover				UMETA(DisplayName = "EnterCover"),
	InCover					UMETA(DisplayName = "InCover"),
	ExitCover				UMETA(DisplayName = "ExitCover"),
	InCoverAiming			UMETA(DisplayName = "InCoverAiming"),
	ExitAiming				UMETA(DisplayName = "ExitAiming"),
};