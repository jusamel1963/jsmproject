#pragma once

#include "CoreMinimal.h"
#include "Data/DataAssets/ItemData/JSMWeaponDataAsset.h"
#include "JSMMeleeWeaponDataAsset.generated.h"

UCLASS()
class PROJECTJSM_API UJSMMeleeWeaponDataAsset : public UJSMWeaponDataAsset
{
	GENERATED_BODY()

public:
	UJSMMeleeWeaponDataAsset()
	{
	}

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TArray<TSoftObjectPtr<UAnimMontage>> AM_MeleeAttackArray;

	// AsyncLoad
	virtual TArray<FSoftObjectPath> GetObjectPathsToLoad() override
	{
		TArray<FSoftObjectPath> Paths = Super::GetObjectPathsToLoad();

		for (auto AM_MeleeAttack : AM_MeleeAttackArray)
		{
			if (AM_MeleeAttack.IsNull() == false)
			{
				if (AM_MeleeAttack.IsPending())
				{
					Paths.Add(AM_MeleeAttack.ToSoftObjectPath());
				}
			}
		}

		return Paths;
	}

	virtual void LoadSynchronous() override
	{
		Super::LoadSynchronous();

		for (auto AM_MeleeAttack : AM_MeleeAttackArray)
		{
			if (AM_MeleeAttack.IsNull() == false)
			{
				AM_MeleeAttack.LoadSynchronous();
			}
		}
	}
};
