// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Sound/SoundCue.h"
#include "JSMCharacterDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UJSMCharacterDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterData")
	FText DisplayName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterData")
	TSoftObjectPtr<class UAnimMontage> AM_Injured;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterData")
	TSoftObjectPtr<class UAnimMontage> AM_Loot;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterData")
	FString EquippedWeaponKeyName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterData")
	TSoftObjectPtr<USkeletalMesh> CharacterMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterData")
	TSoftClassPtr<UAnimInstance> CharacterAnimInstance;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterData")
	TSoftObjectPtr<class UStaticMesh> StaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterData")
	TSoftClassPtr<class ACharacterBase> CharacterBPClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterData")
	TSoftObjectPtr<UTexture2D> EmptyWeaponIcon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterData")
	TSoftObjectPtr<UTexture2D> FistWeaponIcon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterData")
	TSoftObjectPtr<class USoundCue>  WeaponMenuOpenSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CharacterData")
	TSoftObjectPtr<UStaticMesh> SplineMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterData")
	TSoftObjectPtr<class UAnimMontage> AM_StealthAttack;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterData")
	TSoftObjectPtr<class UAnimMontage> AM_StealthDeath;

	virtual TArray<FSoftObjectPath> GetObjectPathsToLoad()
	{
		TArray<FSoftObjectPath> Paths;

		if (AM_Injured.IsNull() == false)
		{
			if (AM_Injured.IsPending())
			{
				Paths.Add(AM_Injured.ToSoftObjectPath());
			}
		}

		if (AM_Loot.IsNull() == false)
		{
			if (AM_Loot.IsPending())
			{
				Paths.Add(AM_Loot.ToSoftObjectPath());
			}
		}

		if (CharacterMesh.IsNull() == false)
		{
			if (CharacterMesh.IsPending())
			{
				Paths.Add(CharacterMesh.ToSoftObjectPath());
			}
		}

		if (CharacterAnimInstance.IsNull() == false)
		{
			if (CharacterAnimInstance.IsPending())
			{
				Paths.Add(CharacterAnimInstance.ToSoftObjectPath());
			}
		}

		if (StaticMesh.IsNull() == false)
		{
			if (StaticMesh.IsPending())
			{
				Paths.Add(StaticMesh.ToSoftObjectPath());
			}
		}

		if (CharacterBPClass.IsNull() == false)
		{
			if (CharacterBPClass.IsPending())
			{
				Paths.Add(CharacterBPClass.ToSoftObjectPath());
			}
		}

		if (EmptyWeaponIcon.IsNull() == false)
		{
			if (EmptyWeaponIcon.IsPending())
			{
				Paths.Add(EmptyWeaponIcon.ToSoftObjectPath());
			}
		}

		if (FistWeaponIcon.IsNull() == false)
		{
			if (FistWeaponIcon.IsPending())
			{
				Paths.Add(FistWeaponIcon.ToSoftObjectPath());
			}
		}

		if (WeaponMenuOpenSound.IsNull() == false)
		{
			if (WeaponMenuOpenSound.IsPending())
			{
				Paths.Add(WeaponMenuOpenSound.ToSoftObjectPath());
			}
		}

		if (SplineMesh.IsNull() == false)
		{
			if (SplineMesh.IsPending())
			{
				Paths.Add(SplineMesh.ToSoftObjectPath());
			}
		}

		if (AM_StealthAttack.IsNull() == false)
		{
			if (AM_StealthAttack.IsPending())
			{
				Paths.Add(AM_StealthAttack.ToSoftObjectPath());
			}
		}

		if (AM_StealthDeath.IsNull() == false)
		{
			if (AM_StealthDeath.IsPending())
			{
				Paths.Add(AM_StealthDeath.ToSoftObjectPath());
			}
		}
		return Paths;
	}

	virtual void LoadSynchronous()
	{
		if (AM_Injured.IsNull() == false)
		{
			AM_Injured.LoadSynchronous();
		}

		if (AM_Loot.IsNull() == false)
		{
			AM_Loot.LoadSynchronous();
		}

		if (CharacterMesh.IsNull() == false)
		{
			CharacterMesh.LoadSynchronous();
		}

		if (StaticMesh.IsNull() == false)
		{
			StaticMesh.LoadSynchronous();
		}

		if (EmptyWeaponIcon.IsNull() == false)
		{
			EmptyWeaponIcon.LoadSynchronous();
		}

		if (FistWeaponIcon.IsNull() == false)
		{
			FistWeaponIcon.LoadSynchronous();
		}

		if (WeaponMenuOpenSound.IsNull() == false)
		{
			WeaponMenuOpenSound.LoadSynchronous();
		}

		if (SplineMesh.IsNull() == false)
		{
			SplineMesh.LoadSynchronous();
		}

		if (AM_StealthAttack.IsNull() == false)
		{
			AM_StealthAttack.LoadSynchronous();
		}

		if (AM_StealthDeath.IsNull() == false)
		{
			AM_StealthDeath.LoadSynchronous();
		}
	}
};
