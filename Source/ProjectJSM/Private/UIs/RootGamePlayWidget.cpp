// Copyright JSM Studio, Inc. All Rights Reserved.


#include "UIs/RootGamePlayWidget.h"
#include "UIs/WeaponHudWidget.h"
#include "UIs/CrossHairWidget.h"
#include "UIs/WeaponScopeHudWidget.h"
#include "UIs/CharacterDataInfoWidget.h"
#include "UIs/BloodScreenWidget.h"
#include "UIs/ProgressBarWidget.h"
#include "UIs/WeaponMenuWidget.h"

#include "Core/Managers/JSMEventManager.h"
#include "FrameWork/JSMGameInstance.h"
#include "FrameWork/JSMUIBridgeSubsystem.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "FrameWork/JSMPlayerController.h"

void URootGamePlayWidget::NativeConstruct()
{
	Super::NativeConstruct();

	SetVisibleWidget(UMG_WeaponScope, ESlateVisibility::Collapsed);
	SetVisibleWidget(UMG_BloodScreen, ESlateVisibility::Collapsed);
	SetVisibleWidget(UMG_ProgressBar, ESlateVisibility::Collapsed);
	SetVisibleWidget(UMG_WeaponMenu, ESlateVisibility::Collapsed);
}

void URootGamePlayWidget::NativeTick(const FGeometry& myGeometry, float inDeltaTime)
{
	Super::NativeTick(myGeometry, inDeltaTime);
	UpdateBloodScreen(inDeltaTime);
}

void URootGamePlayWidget::SetParentOwnID(const uint32& InID, class AJSMPlayerController* InController)
{
	SetOwnIDAndController(InID, InController);
	UMG_CrossHiar->SetOwnIDAndController(InID, InController);
	UMG_WeaponHud->SetOwnIDAndController(InID, InController);
	UMG_WeaponScope->SetOwnIDAndController(InID, InController);
	UMG_CharacterData->SetOwnIDAndController(InID, InController);
	UMG_WeaponMenu->SetOwnIDAndController(InID, InController);
}

void URootGamePlayWidget::Update_WeaponAndCrossHairHud(UTexture2D* PrimaryWeapon, UTexture2D* HolsterWeapon, const FText& MagAmmo, const FText& MagCapacity, UTexture2D* CrossHair, UTexture2D* WeaponScope)
{
	Update_WeaponHud(PrimaryWeapon, HolsterWeapon, MagAmmo, MagCapacity);
	Update_CrossHair(CrossHair);
	Update_WeaponScope(WeaponScope);
}

void URootGamePlayWidget::Update_WeaponHud(UTexture2D* PrimaryWeapon, UTexture2D* HolsterWeapon, const FText& MagAmmo, const FText& MagCapacity)
{
	UWeaponHudWidget* WeaponHuD = Cast<UWeaponHudWidget>(UMG_WeaponHud);
	if (!IsValid(WeaponHuD))
	{
		return;
	}
	WeaponHuD->Update_WeaponHud(PrimaryWeapon, HolsterWeapon, MagAmmo, MagCapacity);
}

void URootGamePlayWidget::Update_CrossHair(UTexture2D* CrossHair)
{
	UCrossHairWidget* CrossHairHuD = Cast<UCrossHairWidget>(UMG_CrossHiar);
	if (!IsValid(CrossHairHuD))
	{
		return;
	}
	CrossHairHuD->Update_CrossHair(CrossHair);
}

void URootGamePlayWidget::Update_WeaponScope(UTexture2D* WeaponScope)
{
	UWeaponScopeHudWidget* WeaponScopeHud = Cast<UWeaponScopeHudWidget>(UMG_WeaponScope);
	if (!IsValid(WeaponScopeHud))
	{
		return;
	}
	WeaponScopeHud->Update_WeaponScope(WeaponScope);
}

void URootGamePlayWidget::Update_ProgressBar(const float& InValue)
{
	UProgressBarWidget* ProgressBarHud = Cast<UProgressBarWidget>(UMG_ProgressBar);
	if (!IsValid(ProgressBarHud))
	{
		return;
	}
	ProgressBarHud->Update_Loading(InValue);
}

void URootGamePlayWidget::SetVisibleWeaponScope(const bool& bVisible)
{
	ESlateVisibility WeaponScopeVisible = (bVisible == true) ? ESlateVisibility::SelfHitTestInvisible : ESlateVisibility::Collapsed;
	ESlateVisibility OtherVisible = (bVisible == true) ? ESlateVisibility::Collapsed : ESlateVisibility::SelfHitTestInvisible;

	SetVisibleWidget(UMG_WeaponScope, WeaponScopeVisible);
	SetVisibleWidget(UMG_WeaponHud, OtherVisible);
	SetVisibleWidget(UMG_CrossHiar, OtherVisible);
}

void URootGamePlayWidget::SetVisibleBloodScreen(const bool& bVisible)
{
	ESlateVisibility BloodScreenVisible = (bVisible == true) ? ESlateVisibility::SelfHitTestInvisible : ESlateVisibility::Collapsed;
	SetVisibleWidget(UMG_BloodScreen, BloodScreenVisible);
}

void URootGamePlayWidget::SetVisibleProgressBar(const bool& bVisible, const FText& LoadingName)
{
	ESlateVisibility ProgressScreenVisible = (bVisible == true) ? ESlateVisibility::SelfHitTestInvisible : ESlateVisibility::Collapsed;
	UProgressBarWidget* ProgressBarHud = Cast<UProgressBarWidget>(UMG_ProgressBar);
	if (IsValid(ProgressBarHud))
	{
		ProgressBarHud->SetLoadingName(LoadingName);
		SetVisibleWidget(UMG_ProgressBar, ProgressScreenVisible);
	}
}

void URootGamePlayWidget::SetVisibleWeaponMenu(const bool& bVisible)
{
	UWeaponMenuWidget* WeaponMenuHud = Cast<UWeaponMenuWidget>(UMG_WeaponMenu);
	if (!IsValid(WeaponMenuHud))
	{
		return;
	}

	ESlateVisibility WeaponMenuVisible = (bVisible == true) ? ESlateVisibility::Visible : ESlateVisibility::Collapsed;
	ESlateVisibility CurrentVisibility = WeaponMenuHud->GetVisibility();
	if (WeaponMenuVisible == CurrentVisibility)
	{
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("SetVisibleWeaponMenu bVisible : %d"), bVisible);

	if (bVisible)
	{
		SetVisibleWidget(UMG_WeaponMenu, WeaponMenuVisible);
	}
	WeaponMenuHud->PlayVisibleToggleAnimation(bVisible);
}

void URootGamePlayWidget::SetWeaponImage(UTexture2D* InWeaponImage, const int& Idx, const FText& Ammo)
{
	UWeaponMenuWidget* WeaponMenuHud = Cast<UWeaponMenuWidget>(UMG_WeaponMenu);
	if (IsValid(WeaponMenuHud))
	{
		WeaponMenuHud->SetWeaponImage(InWeaponImage, Idx, Ammo);
	}
}

void URootGamePlayWidget::SetVisibleWidget(UBaseUserWidget* InWidget, ESlateVisibility SlatVisible)
{
	if (!IsValid(InWidget))
	{
		return;
	}

	ESlateVisibility CurrentVisibility = InWidget->GetVisibility();
	if (SlatVisible != CurrentVisibility)
	{
		InWidget->SetVisibility(SlatVisible);
	}
}

void URootGamePlayWidget::UpdateBloodScreen(const float& inDeltaTime)
{
	UBloodScreenWidget* BloodScreenHud = Cast<UBloodScreenWidget>(UMG_BloodScreen);
	if (!IsValid(BloodScreenHud))
	{
		return;
	}

	UJSMUIBridgeSubsystem* BridgeSubsystem = JSMFL::GetGameInstanceSubsystem<UJSMUIBridgeSubsystem>();
	if (IsValid(BridgeSubsystem))
	{
		float CurrHealth, MaxHealth;
		BridgeSubsystem->GetUIData_CharacterData(OwnID, TEXT("CurrHealth"), CurrHealth);
		BridgeSubsystem->GetUIData_CharacterData(OwnID, TEXT("MaxHealth"), MaxHealth);

		if ((MaxHealth <= 0) || (CurrHealth < 0))
		{
			return;
		}

		MaxHealth = (MaxHealth / 2);
		if (CurrHealth < MaxHealth)
		{
			SetVisibleBloodScreen(true);
			BloodScreenHud->SetBloodScreenOpacity(FMath::Lerp(MinBloodScreenOpacity, MaxBloodScreenOpacity, (CurrHealth / MaxHealth)));
		}
		else
		{
			SetVisibleBloodScreen(false);
		}
	}
}