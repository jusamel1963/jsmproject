// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UIs/BaseUserWidget.h"
#include "LobbyMainMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API ULobbyMainMenuWidget : public UBaseUserWidget
{
	GENERATED_BODY()
protected:
	virtual void NativeConstruct() override;

public:
	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	class UCanvasPanel* RootPanel;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	class UButton* Button_CreateSession;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	class UButton* Button_FindSession;

protected:
	UFUNCTION()
	void OnButton_CreateSessionPressed();

	UFUNCTION()
	void OnButton_FindSessionPressed();
};
