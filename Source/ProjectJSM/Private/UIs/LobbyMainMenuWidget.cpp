// Copyright JSM Studio, Inc. All Rights Reserved.


#include "UIs/LobbyMainMenuWidget.h"
#include "Components/Button.h"
#include "FrameWork/JSMGameInstance.h"

void ULobbyMainMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();
	if (IsValid(Button_CreateSession))
	{
		Button_CreateSession->OnClicked.AddDynamic(this, &ULobbyMainMenuWidget::OnButton_CreateSessionPressed);
	}

	if (IsValid(Button_FindSession))
	{
		Button_FindSession->OnClicked.AddDynamic(this, &ULobbyMainMenuWidget::OnButton_FindSessionPressed);
	}
}

void ULobbyMainMenuWidget::OnButton_CreateSessionPressed()
{
	UJSMGameInstance* GameInst = Cast<UJSMGameInstance>(GetGameInstance());
	check(GameInst);
	GameInst->CreateSession();
}

void ULobbyMainMenuWidget::OnButton_FindSessionPressed()
{
	UJSMGameInstance* GameInst = Cast<UJSMGameInstance>(GetGameInstance());
	check(GameInst);
	GameInst->FindSession();
}
