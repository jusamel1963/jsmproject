// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "JSMDataManagerSubsystem.generated.h"

UCLASS()
class PROJECTJSM_API UJSMDataManagerSubsystem : public UEngineSubsystem
{
	GENERATED_BODY()

public:
	UJSMDataManagerSubsystem();
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;

	template<class T>
	const T* GetDataCollection() const
	{
		return Cast<const T>(JSMDataCollection);
	}

private:
	void LoadImpl();


protected:
	UPROPERTY(EditDefaultsOnly)
	FSoftObjectPath DataCollectionPath;

private:
	UPROPERTY(Transient)
	TObjectPtr<UJSMDataCollection> JSMDataCollection;

};


UCLASS()
class PROJECTJSM_API UJSMDataCollection : public UDataAsset
{
	GENERATED_BODY()

public:
	void LoadSyncronous();

	UPROPERTY(EditDefaultsOnly) TSoftObjectPtr<UDataTable> CharacterTable;
	UPROPERTY(EditDefaultsOnly) TSoftObjectPtr<UDataTable> ItemTable;
	UPROPERTY(EditDefaultsOnly) TSoftObjectPtr<UDataTable> WidgetTable;
	UPROPERTY(EditDefaultsOnly) TSoftObjectPtr<UDataTable> CharacterAttributeSetTable;

	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<class URootGamePlayWidget> MasterWidgetClass;

	UPROPERTY(EditDefaultsOnly)
	TSoftClassPtr<class ULobbyMainMenuWidget> LobbyMasterWidgetClass;
protected:
	template<class T, typename std::enable_if<std::is_base_of<UDataAsset, T>::value, bool>::type = 0>
	void LoadAsset(TSoftObjectPtr<T>& SoftRef)
	{
		LoadAssetImpl(SoftRef);
	}

	template<class T, typename std::enable_if<std::is_base_of<UDataTable, T>::value, bool>::type = 0>
	void LoadAsset(TSoftObjectPtr<T>& SoftRef)
	{
		LoadAssetImpl(SoftRef);
	}

private:
	UPROPERTY(Transient)
	TArray<UObject*> HardReferences;

	template<class T>
	void LoadAssetImpl(TSoftObjectPtr<T>& AssetRef)
	{
		if (AssetRef.IsNull())
		{
			return;
		}

		T* LoadedAsset = AssetRef.LoadSynchronous();
		check(LoadedAsset);

		HardReferences.Add(LoadedAsset);
	}
};