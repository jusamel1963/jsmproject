// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Data/DataAssets/ItemData/JSMItemDataAsset.h"
#include "JSMWeaponAmmoDataAsset.generated.h"

UCLASS()
class PROJECTJSM_API UJSMWeaponAmmoDataAsset : public UJSMItemDataAsset
{
	GENERATED_BODY()
public:
	UJSMWeaponAmmoDataAsset()
	{
		ItemType = EJSMItemType::WeaponAmmo;
		AmmoCategory = EWeaponAmmoType::None;
	}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponAmmo")
	float Amount;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponAmmo")
	EWeaponAmmoType AmmoCategory;
};
