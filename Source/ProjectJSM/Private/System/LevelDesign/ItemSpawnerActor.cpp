// Copyright JSM Studio, Inc. All Rights Reserved.


#include "System/LevelDesign/ItemSpawnerActor.h"
#include "Framework/JSMItemManagerSubsystem.h"
#include "Objects/Items/JSMItemActor.h"
#include "Objects/Items/JSMItemInfo.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"

AItemSpawnerActor::AItemSpawnerActor()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AItemSpawnerActor::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		Execute();
	}
}

void AItemSpawnerActor::Execute()
{
	if (!HasAuthority())
	{
		return;
	}

	if (SpawnedActors.Num() > 0)
	{
		return;
	}

	if (bRespawnable)
	{
		SetRespawnTimer();
	}
	else
	{
		SpawnItem();
	}
}

void AItemSpawnerActor::SpawnItem()
{
	if (!HasAuthority())
	{
		return;
	}

	for (FItemSpawnData& ItemSpawnData : SpawnData)
	{
		UJSMItemManagerSubsystem* ItemManager = JSMFL::GetGameInstanceSubsystem<UJSMItemManagerSubsystem>();
		if (IsValid(ItemManager))
		{
			FTransform SpawnTransform = GetTransform();
			AJSMItemActor* ItemActor = ItemManager->SpawnItemActor(GetWorld(), AJSMItemActor::StaticClass(), ItemSpawnData.ItemKeyName, SpawnTransform);
			if (IsValid(ItemActor))
			{
				if (ensure(ItemActor->GetRepItemInfo()))
				{
					ItemActor->SS_SetCount(ItemSpawnData.ItemAmount);
				}
				SpawnedActors.Add(ItemActor);
				ItemActor->OnDestroyed.AddDynamic(this, &AItemSpawnerActor::OnDestroyActor);
			}
		}
	}
}

void AItemSpawnerActor::SetRespawnTimer()
{
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	TimerManager.SetTimer(SpawnerTimerHandle, this, &AItemSpawnerActor::OnSpawnTimer, RespawnTimeInterval, false, RespawnTimeInterval);
}

void AItemSpawnerActor::OnSpawnTimer()
{
	SpawnItem();
}

void AItemSpawnerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AItemSpawnerActor::OnDestroyActor(AActor* DestroyedActor)
{
	SpawnedActors.Remove(DestroyedActor);

	if (SpawnedActors.Num() == 0 && bRespawnable)
	{
		SetRespawnTimer();
	}
}