// Copyright JSM Studio, Inc. All Rights Reserved.

#include "FrameWork/JSMItemManagerSubsystem.h"
#include "FrameWork/JSMActorManagerSubsystem.h"
#include "Objects/Items/JSMItemInfo.h"
#include "Objects/Items/JSMItemActor.h"
#include "Objects/Weapons/JSMWeaponInfo.h"
#include "Objects/Weapons/Weapon.h"
#include "Objects/Weapons/ProjectTileBase.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "Objects/Weapons/ShellEjectBase.h"
#include "Objects/Vehicles/VehicleInfo.h"
#include "Objects/Vehicles/NoWheelVehicleBase.h"
#include "Objects/Vehicles/WheelVehicleBase.h"

UJSMItemInfo* UJSMItemManagerSubsystem::CreateJSMItemInfo(FName DataKey, AActor* InOwnerActor)
{
	UJSMGameInstance* GameInstance = UJSMGameInstance::GetGameInstance();
	check(GameInstance);

	UClass* ItemClass = UJSMItemInfo::StaticClass();

	UJSMItemInfo* ItemInfo = NewObject<UJSMItemInfo>(InOwnerActor, ItemClass);
	if (IsValid(ItemInfo))
	{
		ItemInfo->SetRepItemDataKey(DataKey);
		ItemInfo->SetRepItemCount(1);
		ItemInfo->SetOwnerActor(InOwnerActor);
		return ItemInfo;
	}

	return nullptr;
}

UJSMWeaponInfo* UJSMItemManagerSubsystem::CreateJSMWeaponInfo(FName DataKey, AActor* InOwnerActor)
{
	UJSMGameInstance* GameInstance = UJSMGameInstance::GetGameInstance();
	check(GameInstance);

	UClass* WeaponClass = UJSMWeaponInfo::StaticClass();

	UJSMWeaponInfo* WeaponInfo = NewObject<UJSMWeaponInfo>(InOwnerActor, WeaponClass);
	if (IsValid(WeaponInfo))
	{
		WeaponInfo->SetRepWeaponDataKey(DataKey);
		WeaponInfo->SetOwnerActor(InOwnerActor);
		return WeaponInfo;
	}

	return nullptr;
}

UVehicleInfo* UJSMItemManagerSubsystem::CreateJSMVehicleInfo(FName DataKey, AActor* InOwnerActor)
{
	UJSMGameInstance* GameInstance = UJSMGameInstance::GetGameInstance();
	check(GameInstance);

	UClass* VehicleClass = UVehicleInfo::StaticClass();

	UVehicleInfo* VehicleInfo = NewObject<UVehicleInfo>(InOwnerActor, VehicleClass);
	if (IsValid(VehicleInfo))
	{
		VehicleInfo->SetRepVehicleDataKey(DataKey);
		VehicleInfo->SetOwnerActor(InOwnerActor);
		return VehicleInfo;
	}

	return nullptr;
}

AJSMItemActor* UJSMItemManagerSubsystem::SpawnItemActor(UWorld* InWorld, UClass* Class, FName ItemDataKey, FTransform const& Transform, const FActorSpawnParameters& SpawnParameters)
{
	check(InWorld);
	AJSMItemActor* ItemActor = JSMFL::GetGameInstanceSubsystem<UJSMActorManagerSubsystem>()->SpawnActor<AJSMItemActor>(InWorld, Class, Transform, SpawnParameters);
	if (IsValid(ItemActor))
	{
		UJSMItemInfo* ItemInfo = CreateJSMItemInfo(ItemDataKey, ItemActor);
		ItemActor->SS_SetRepItemInfo(ItemInfo);
		return ItemActor;
	}

	return nullptr;
}

AWeapon* UJSMItemManagerSubsystem::SpawnWeaponActor(UWorld* InWorld, UClass* Class, FName WeaponDataKey, FTransform const& Transform, const FActorSpawnParameters& SpawnParameters)
{
	check(InWorld);
	AWeapon* WeaponActor = JSMFL::GetGameInstanceSubsystem<UJSMActorManagerSubsystem>()->SpawnActor<AWeapon>(InWorld, Class, Transform, SpawnParameters);
	if (IsValid(WeaponActor))
	{
		UJSMWeaponInfo* WeaponInfo = CreateJSMWeaponInfo(WeaponDataKey, WeaponActor);
		WeaponActor->SS_SetRepWeaponInfo(WeaponInfo);
		return WeaponActor;
	}

	return nullptr;
}

AProjectTileBase* UJSMItemManagerSubsystem::SpawnProjectTileActor(UWorld* InWorld, UClass* Class, FName WeaponDataKey, FTransform const& Transform, const FActorSpawnParameters& SpawnParameters)
{
	check(InWorld);
	AProjectTileBase* ProjectTileActor = JSMFL::GetGameInstanceSubsystem<UJSMActorManagerSubsystem>()->SpawnActor<AProjectTileBase>(InWorld, Class, Transform, SpawnParameters);
	return ProjectTileActor;
}

AShellEjectBase* UJSMItemManagerSubsystem::SpawnShellEjectBaseActor(UWorld* InWorld, UClass* Class, FName WeaponDataKey, FTransform const& Transform, const FActorSpawnParameters& SpawnParameters)
{
	check(InWorld);
	AShellEjectBase* ShellEjectActor = JSMFL::GetGameInstanceSubsystem<UJSMActorManagerSubsystem>()->SpawnActor<AShellEjectBase>(InWorld, Class, Transform, SpawnParameters);
	return ShellEjectActor;
}

ANoWheelVehicleBase* UJSMItemManagerSubsystem::SpawnNoWheelVehicleActor(UWorld* InWorld, UClass* Class, FName VehicleDataKey, FTransform const& Transform, const FActorSpawnParameters& SpawnParameters)
{
	check(InWorld);
	ANoWheelVehicleBase* VehicleActor = JSMFL::GetGameInstanceSubsystem<UJSMActorManagerSubsystem>()->SpawnActor<ANoWheelVehicleBase>(InWorld, Class, Transform, SpawnParameters);
	if (IsValid(VehicleActor))
	{
		UVehicleInfo* VehicleInfo = CreateJSMVehicleInfo(VehicleDataKey, VehicleActor);
		VehicleActor->SS_SetRepVehicleInfo(VehicleInfo);
		return VehicleActor;
	}

	return nullptr;
}

AWheelVehicleBase* UJSMItemManagerSubsystem::SpawnWheelVehicleActor(UWorld* InWorld, UClass* Class, FName VehicleDataKey, FTransform const& Transform, const FActorSpawnParameters& SpawnParameters)
{
	check(InWorld);
	AWheelVehicleBase* VehicleActor = JSMFL::GetGameInstanceSubsystem<UJSMActorManagerSubsystem>()->SpawnActor<AWheelVehicleBase>(InWorld, Class, Transform, SpawnParameters);
	if (IsValid(VehicleActor))
	{
		UVehicleInfo* VehicleInfo = CreateJSMVehicleInfo(VehicleDataKey, VehicleActor);
		//VehicleActor->SS_SetRepVehicleInfo(VehicleInfo);
		return VehicleActor;
	}

	return nullptr;
}