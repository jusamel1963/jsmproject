// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Weapons/Weapon.h"
#include "Objects/Weapons/JSMWeaponInfo.h"
#include "Components/SceneComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/TextRenderComponent.h"
#include "Framework/JSMGameInstance.h"
#include "Data/DataAssets/ItemData/JSMWeaponDataAsset.h"
#include "Framework/JSMItemManagerSubsystem.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "Engine/ActorChannel.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Statics/AssetLoader.h"
#include "Net/UnrealNetwork.h"
#include "Net/Core/PushModel/PushModel.h"
#include "Components/SkeletalMeshComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/PointLightComponent.h"
#include "Components/SphereComponent.h"

FName AWeapon::SceneComponentName(TEXT("SceneComp"));
FName AWeapon::StaticMeshComponentName(TEXT("StaticMeshComp"));
FName AWeapon::SkeletalMeshComponentName(TEXT("SkeletalMeshComp"));
FName AWeapon::CameraComponentName(TEXT("ADS"));
FName AWeapon::PointLightComponentName(TEXT("PointLightComponent"));

AWeapon::AWeapon()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	//USceneComponent* SceneComp = CreateDefaultSubobject<USceneComponent>(SceneComponentName);
	//RootComponent = SceneComp;

	RootSphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	RootSphereComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RootSphereComp->SetSphereRadius(5);
	RootComponent = RootSphereComp;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(StaticMeshComponentName);
	StaticMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StaticMeshComp->SetupAttachment(RootComponent);
	StaticMeshComp->SetLightingChannels(false, true, false);

	SkeletalMeshComponent = CreateOptionalDefaultSubobject<USkeletalMeshComponent>(SkeletalMeshComponentName);
	SkeletalMeshComponent->bReceivesDecals = false;
	SkeletalMeshComponent->CastShadow = false;
	SkeletalMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SkeletalMeshComponent->SetupAttachment(RootComponent);

	CameraComponent = CreateOptionalDefaultSubobject<UCameraComponent>(CameraComponentName);
	if (IsValid(CameraComponent))
	{
		CameraComponent->SetupAttachment(RootComponent);
		CameraComponent->SetRelativeRotation(FRotator(0, 90, 0));
		CameraComponent->SetRelativeLocation(FVector_NetQuantize(0.0f, 100.0f, 20.0f));
		CameraComponent->SetRelativeScale3D(FVector_NetQuantize(0.2f, 0.2f, 0.2f));
	}

	PointLightComponent = CreateOptionalDefaultSubobject<UPointLightComponent>(PointLightComponentName);
	if (IsValid(PointLightComponent))
	{
		PointLightComponent->SetupAttachment(RootComponent);
		PointLightComponent->SetRelativeLocation(FVector_NetQuantize(10.0f, 10.0f, 5.0f));
		PointLightComponent->Intensity = 3880.f;
	}
	SetResetWeaponScopeFOV();
}

void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		SS_CreateWeaponInfo();
	}
}

void AWeapon::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(AWeapon, RepWeaponInfo, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(AWeapon, RepWeaponDataKey, SharedParams);
}

void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

UJSMWeaponInfo* AWeapon::GetRepWeaponInfo() const
{
	return RepWeaponInfo;
}

bool AWeapon::IsLoadCompleted()
{
	return bLoadCompleted;
}

void AWeapon::SS_CreateWeaponInfo()
{
	if (!HasAuthority())
	{
		return;
	}

	UJSMGameInstance* GameInstance = UJSMGameInstance::GetGameInstance();
	check(GameInstance);

	UJSMItemManagerSubsystem* ItemManager = JSMFL::GetGameInstanceSubsystem<UJSMItemManagerSubsystem>();
	if (IsValid(ItemManager) == false)
	{
		return;
	}

	UJSMWeaponInfo* JSMWeaponInfo = ItemManager->CreateJSMWeaponInfo(RepWeaponDataKey, this);
	if (IsValid(JSMWeaponInfo) == false)
	{
		return;
	}

	SS_SetRepWeaponInfo(JSMWeaponInfo);
}

void AWeapon::SS_SetRepWeaponInfo(UJSMWeaponInfo* InWeaponInfo)
{
	if (!HasAuthority())
	{
		return;
	}

	MARK_PROPERTY_DIRTY_FROM_NAME(AWeapon, RepWeaponInfo, this);
	RepWeaponInfo = InWeaponInfo;
	MARK_PROPERTY_DIRTY_FROM_NAME(AWeapon, RepWeaponDataKey, this);
	RepWeaponDataKey = InWeaponInfo->GetRepWeaponDataKey();
	OnRep_WeaponInfo();
}

bool AWeapon::ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);
	if (RepWeaponInfo != nullptr)
	{
		wrote |= Channel->ReplicateSubobject(RepWeaponInfo, *Bunch, *RepFlags);
	}
	return wrote;
}

void AWeapon::OnRep_WeaponInfo()
{
	bLoadCompleted = false;

	if (bUseAsyncLoad)
	{
		LoadAssetAsync(FJSMWeaponDataLoadComplete::CreateUObject(this, &AWeapon::InitializeWeaponResources));
	}
	else
	{
		LoadSynchronous();
		InitializeWeaponResources();
	}
}

void AWeapon::LoadSynchronous()
{
	// If previous async request is not finished, don't do anything.
	if (AssetLoadRequest.AsyncLoadHandle != nullptr)
	{
		return;
	}

	UJSMWeaponDataAsset* WeaponDataAsset = GetWeaponDataAsset();
	if (IsValid(WeaponDataAsset))
	{
		WeaponDataAsset->LoadSynchronous();
	}
}

void AWeapon::InitializeWeaponResources()
{
	UJSMWeaponDataAsset* WeaponDataAsset = GetWeaponDataAsset();
	if (IsValid(WeaponDataAsset))
	{
		if (WeaponDataAsset->ItemStaticMesh.IsValid())
		{
			StaticMeshComp->SetStaticMesh(WeaponDataAsset->ItemStaticMesh.Get());
			StaticMeshComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		}

		MinScopeRange = WeaponDataAsset->MinScopeRange;
		MaxScopeRange = WeaponDataAsset->MaxScopeRange;

		if (IsValid(CameraComponent))
		{
			CameraComponent->SetFieldOfView(MinScopeRange);
		}
	}
	bLoadCompleted = true;
	OnLoadComplete.ExecuteIfBound();
}

void AWeapon::LoadAssetAsync(FJSMWeaponDataLoadComplete LoadCompleteDelegate)
{
	if (AssetLoadRequest.AsyncLoadHandle != nullptr)
	{
		return;
	}

	UJSMWeaponDataAsset* WeaponDataAsset = GetWeaponDataAsset();

	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	if (WeaponDataAsset->GetObjectPathsToLoad().Num() == 0)
	{
		LoadCompleteDelegate.ExecuteIfBound();
	}
	else
	{
		FStreamableDelegate StreamableDelegate = FStreamableDelegate::CreateUObject(this, &AWeapon::LoadAssetCompleted);
		AssetLoadRequest.CompleteDelegate = LoadCompleteDelegate;
		AssetLoadRequest.AsyncLoadHandle = UAssetLoader::AssetLoadAsyncStart(WeaponDataAsset->GetObjectPathsToLoad(), StreamableDelegate);
	}
}

UJSMWeaponDataAsset* AWeapon::GetWeaponDataAsset() const
{
	if (IsValid(RepWeaponInfo) && RepWeaponInfo->GetWeaponDataAsset().IsValid())
	{
		return RepWeaponInfo->GetWeaponDataAsset().Get();
	}
	return nullptr;
}

bool AWeapon::IsEmpty() const
{
	return IsValid(RepWeaponInfo) == false;
}

void AWeapon::LoadAssetCompleted()
{
	AssetLoadRequest.CompleteDelegate.ExecuteIfBound();
	AssetLoadRequest.AsyncLoadHandle.Reset();
}

void AWeapon::BeginPickup()
{
	bBeginPickup = true;
}

void AWeapon::EndPickup()
{
	if (bBeginPickup)
	{
		Destroy();
	}
}

UStaticMesh* AWeapon::GetStaticMesh() const
{
	if (IsValid(StaticMeshComp))
	{
		return StaticMeshComp->GetStaticMesh();
	}

	return nullptr;
}

void AWeapon::SetWeaponHideAndCollision(bool bHide, bool bCollide)
{
	SetActorHiddenInGame(bHide);
	SetActorEnableCollision(bCollide);
}

void AWeapon::SetWeaponNotUse()
{
	StaticMeshComp->SetStaticMesh(nullptr);
	SkeletalMeshComponent->SetSkeletalMesh(nullptr);
	PointLightComponent->Intensity = 0;
}

void AWeapon::SetWeaponScopeFOV(const float& InValue)
{
	if (IsValid(CameraComponent))
	{
		float ADSZoomValue = FMath::Clamp((InValue * 10.0f + CameraComponent->FieldOfView), MinScopeRange, MaxScopeRange);
		CameraComponent->SetFieldOfView(ADSZoomValue);
	}
}

void AWeapon::SetResetWeaponScopeFOV()
{
	if (IsValid(CameraComponent))
	{
		CameraComponent->SetFieldOfView(MaxScopeRange);
	}
}

void AWeapon::WaitDestroy(float InWaitTime)
{
	FTimerHandle DelayHandle;
	const TWeakObjectPtr<AWeapon> WeakPtr(this);
	GetWorld()->GetTimerManager().SetTimer(DelayHandle, FTimerDelegate::CreateLambda([WeakPtr]()
	{
		if (WeakPtr.IsValid())
		{
			WeakPtr->Destroy();
		}
	}), InWaitTime, false);
}