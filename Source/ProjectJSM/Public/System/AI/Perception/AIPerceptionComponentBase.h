// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Perception/AIPerceptionComponent.h"
#include "AIController.h"
#include "AIPerceptionComponentBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UAIPerceptionComponentBase : public UAIPerceptionComponent
{
	GENERATED_BODY()
public:
	UAIPerceptionComponentBase();

	virtual void OnRegister() override;
	virtual void OnUnregister() override;

	void OnCharacterDead(UObject* Invoker, class ACharacterBase* Victim, class ACharacterBase* Attacker);

	UFUNCTION(BlueprintPure)
	FORCEINLINE AActor* GetTargetActor() const { return TargetActor; }
public:
	void InitializeAIData(const class UJSMAIDataAsset* aiData);
	void InitAISenseSightConfig(const UJSMAIDataAsset* aiData);
	void InitAISenseHearingConfig(const UJSMAIDataAsset* aiData);
	void InitAISenseTeamConfig(const UJSMAIDataAsset* aiData);
	void InitAISenseDamageConfig(const UJSMAIDataAsset* aiData);
	void UpdateStimuliStrength(const AActor* Actor);
	void UpdateTargets();

	void AISightSense(const AActor* Actor, const FAIStimulus& InStimulus);
	void AIHearSense(const AActor* Actor, const FAIStimulus& InStimulus);
	void AIDamageSense(const AActor* Actor, const FAIStimulus& InStimulus);

protected:
	UFUNCTION()
	void OnTargetPerceptionUpdatedImpl(AActor* Actor, FAIStimulus Stimulus);

	UFUNCTION()
	void OnPerceptionUpdatedImpl(const TArray<AActor*>& UpdatedActors);

private:
	UPROPERTY(VisibleInstanceOnly, Transient)
	TObjectPtr<AActor> TargetActor;

	UPROPERTY(Transient)
	TMap<AActor*, float> StimuliStrengthMap;
};
