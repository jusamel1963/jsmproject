// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Vehicles/Helicopter/HelicopterBase.h"
#include "Components/StaticMeshComponent.h"
#include "Data/DataAssets/ItemData/JSMVehicleDataAsset.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "Objects/Character/PlayerCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

FName AHelicopterBase::RotorComponentName(TEXT("RotorComponent"));
FName AHelicopterBase::BladeComponentName(TEXT("BladeComponent"));
FName AHelicopterBase::TailRotorComponentName(TEXT("TailRotorComponent"));
FName AHelicopterBase::RearBladeComponentName(TEXT("RearBladeComponent"));

AHelicopterBase::AHelicopterBase()
{
	RotorComponent = CreateDefaultSubobject<UStaticMeshComponent>(RotorComponentName);
	if (IsValid(RotorComponent))
	{
		RotorComponent->SetupAttachment(RootComponent, FName(TEXT("main_rotor")));
	}

	BladeComponent = CreateDefaultSubobject<UStaticMeshComponent>(BladeComponentName);
	if (IsValid(BladeComponent))
	{
		BladeComponent->SetupAttachment(RotorComponent);
	}

	TailRotorComponent = CreateDefaultSubobject<UStaticMeshComponent>(TailRotorComponentName);
	if (IsValid(TailRotorComponent))
	{
		TailRotorComponent->SetupAttachment(RootComponent, FName(TEXT("tail_rotor")));
	}

	RearBladeComponent = CreateDefaultSubobject<UStaticMeshComponent>(RearBladeComponentName);
	if (IsValid(RearBladeComponent))
	{
		RearBladeComponent->SetupAttachment(TailRotorComponent);
	}

	if (IsValid(SpringArmComp))
	{
		SpringArmComp->SetRelativeRotation(FRotator(0, 90.f, 0));
		SpringArmComp->SetRelativeLocation(FVector_NetQuantize(0, -550.f, 250.f));
		SpringArmComp->SocketOffset = FVector_NetQuantize(0, 0, 500.f);
		SpringArmComp->bInheritPitch = false;
		SpringArmComp->bEnableCameraLag = true;
		SpringArmComp->bEnableCameraRotationLag = true;
		SpringArmComp->CameraLagSpeed = 5.f;
		SpringArmComp->CameraRotationLagSpeed = 5.f;
	}

	if (IsValid(CameraComp))
	{
		CameraComp->SetRelativeRotation(FRotator(-25.f, 0, 0));
	}
	SetActorRotation(FRotator(0, -90.f, 0));
}

void AHelicopterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!IsValid(RotorComponent) || !IsValid(BladeComponent) || !IsValid(TailRotorComponent) || !IsValid(RearBladeComponent))
	{
		return;
	}

	UJSMVehicleDataAsset* VehicleDataAsset = GetVehicleDataAsset();
	if (!IsValid(VehicleDataAsset))
	{
		return;
	}

	if (CurrentBladeRotationSpeed > VehicleDataAsset->BlurBladeSpeed)
	{
		BladeComponent->SetStaticMesh(VehicleDataAsset->BlurBladeStaticMesh.Get());
		RearBladeComponent->SetStaticMesh(VehicleDataAsset->BlurRearBladeStaticMesh.Get());
		RotorComponent->AddLocalRotation(FRotator(0, (VehicleDataAsset->BlurBladeSpeed * DeltaTime), 0));
		TailRotorComponent->AddLocalRotation(FRotator(0, 0, (VehicleDataAsset->BlurBladeSpeed * DeltaTime * 2.0f)));
	}
	else
	{
		BladeComponent->SetStaticMesh(VehicleDataAsset->BladeStaticMesh.Get());
		RearBladeComponent->SetStaticMesh(VehicleDataAsset->RearBladeStaticMesh.Get());
		RotorComponent->AddLocalRotation(FRotator(0, (CurrentBladeRotationSpeed * DeltaTime), 0));
		TailRotorComponent->AddLocalRotation(FRotator(0, 0, (CurrentBladeRotationSpeed * DeltaTime * 2.0f)));
	}
}

void AHelicopterBase::BeginPlay()
{
	Super::BeginPlay();
}

void AHelicopterBase::InitializeVehicleResources()
{
	UJSMVehicleDataAsset* VehicleDataAsset = GetVehicleDataAsset();
	if (IsValid(VehicleDataAsset))
	{
		if (VehicleDataAsset->RotorStaticMesh.IsValid())
		{
			RotorComponent->SetStaticMesh(VehicleDataAsset->RotorStaticMesh.Get());
		}

		if (VehicleDataAsset->BladeStaticMesh.IsValid())
		{
			BladeComponent->SetStaticMesh(VehicleDataAsset->BladeStaticMesh.Get());
		}

		if (VehicleDataAsset->TailRotorStaticMesh.IsValid())
		{
			TailRotorComponent->SetStaticMesh(VehicleDataAsset->TailRotorStaticMesh.Get());
		}

		if (VehicleDataAsset->RearBladeStaticMesh.IsValid())
		{
			RearBladeComponent->SetStaticMesh(VehicleDataAsset->RearBladeStaticMesh.Get());
		}
	}

	Super::InitializeVehicleResources();
}