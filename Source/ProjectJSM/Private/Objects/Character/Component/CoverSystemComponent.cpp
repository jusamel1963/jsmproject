// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Character/Component/CoverSystemComponent.h"
#include "Objects/Character/CharacterBase.h"
#include "Statics/JSMClientStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/ALSDebugComponent.h"
#include "Net/UnrealNetwork.h"
#include "Net/Core/PushModel/PushModel.h"
#include "Components/ArrowComponent.h"

// Sets default values for this component's properties
UCoverSystemComponent::UCoverSystemComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
	bWantsInitializeComponent = true;
}

void UCoverSystemComponent::InitializeComponent()
{
	Super::InitializeComponent();
	OwnerChar = Cast<ACharacterBase>(GetOwner());
}

void UCoverSystemComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(UCoverSystemComponent, CurrentCoverState, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(UCoverSystemComponent, bCanCover, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(UCoverSystemComponent, bMoveLeft, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(UCoverSystemComponent, bMoveRight, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(UCoverSystemComponent, bCanFire, SharedParams);
}

// Called when the game starts
void UCoverSystemComponent::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void UCoverSystemComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!IsValid(OwnerChar) || !OwnerChar->HasAuthority())
	{
		return;
	}

	switch (CurrentCoverState)
	{
		case ECoverType::FindCover:
		{
			FindCover();
			break;
		}
		case ECoverType::EnterCover:
		{
			EnterCover();
			break;
		}
		case ECoverType::InCover:
		{
			InCover();
			break;
		}
		case ECoverType::ExitCover:
		{
			ExitCover();
			break;
		}
		case ECoverType::InCoverAiming:
		{
			InCoverAiming();
			break;
		}
		case ECoverType::ExitAiming:
		{
			ExitAiming();
			break;
		}
		default:
		{
			break;
		}
	}
}

void UCoverSystemComponent::FindCover()
{
	UWorld* World = UJSMClientStatics::GetGameWorld();
	if (!IsValid(World) || !IsValid(OwnerChar))
	{
		return;
	}

	EALSMovementState CurrentMovementState = OwnerChar->GetMovementState();
	if ((CurrentMovementState == EALSMovementState::Grounded) || (CurrentMovementState == EALSMovementState::Cover))
	{
		FVector_NetQuantize StartLoc = OwnerChar->GetActorLocation();
		FVector_NetQuantize EndLoc = StartLoc + (OwnerChar->GetActorForwardVector() * 100);
		FHitResult OutHitResult;
		TraceUnderCrosshairs(OutHitResult, StartLoc, EndLoc, 15.f, ECC_CoverTrace);
		if (OutHitResult.bBlockingHit)
		{
			CoverWallNormal = OutHitResult.Normal;
			CoverWallLoc = OutHitResult.Location + (CoverWallNormal * 10);
			FVector_NetQuantize WorldDirection = UKismetMathLibrary::GetRightVector(UKismetMathLibrary::MakeRotFromX(OutHitResult.Normal)) * -1.0f;
			OwnerChar->SetCover_WorldDirection(WorldDirection);
		}

		MARK_PROPERTY_DIRTY_FROM_NAME(UCoverSystemComponent, bCanCover, this);
		bCanCover = OutHitResult.bBlockingHit;
	}
}

void UCoverSystemComponent::EnterCover()
{
	if (!IsValid(OwnerChar) || !bCanCover)
	{
		return;
	}

	OwnerChar->SetActorLocation(CoverWallLoc);
	FRotator NewRot = UKismetMathLibrary::MakeRotFromX(CoverWallNormal * -1.0f);
	OwnerChar->SetActorRotation(NewRot);
	MARK_PROPERTY_DIRTY_FROM_NAME(UCoverSystemComponent, CurrentCoverState, this);
	CurrentCoverState = ECoverType::InCover;
	OwnerChar->SetDesiredGait(EALSGait::Walking);
}

void UCoverSystemComponent::ExitCover()
{
	MARK_PROPERTY_DIRTY_FROM_NAME(UCoverSystemComponent, CurrentCoverState, this);
	CurrentCoverState = ECoverType::FindCover;
}

void UCoverSystemComponent::InCover()
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("InCover bCanFire : %d"), bCanFire);
	if ((OwnerChar->GetRotationMode() == EALSRotationMode::Aiming) && bCanFire)
	{
		MARK_PROPERTY_DIRTY_FROM_NAME(UCoverSystemComponent, CurrentCoverState, this);
		CurrentCoverState = ECoverType::InCoverAiming;
	}
	else
	{
		OwnerChar->SetDesiredGait(EALSGait::Walking);
		FindCover();

		MARK_PROPERTY_DIRTY_FROM_NAME(UCoverSystemComponent, bCanFire, this);
		bCanFire = false;
		FindSideCover(true, OwnerChar->LeftTraceComponent);
		FindSideCover(false, OwnerChar->RightTraceComponent);
	}
}

void UCoverSystemComponent::FindSideCover(bool bLeft, class UArrowComponent* ArrowComp)
{
	if (!IsValid(ArrowComp) || !IsValid(OwnerChar))
	{
		return;
	}
	FVector_NetQuantize AddVector1 = ArrowComp->GetComponentLocation() + FVector_NetQuantize(0, 0, 50);
	FVector_NetQuantize AddVector2 = OwnerChar->GetActorRotation().Vector() * 30.f;
	FVector_NetQuantize StartLoc = AddVector1 + AddVector2;
	FVector_NetQuantize EndLoc = StartLoc + FVector_NetQuantize(0, 0, -100);
	FHitResult OutHitResult;
	TraceUnderCrosshairs(OutHitResult, StartLoc, EndLoc, 20.f, ECC_CoverTrace);

	UE_LOG(LogTemp, Warning, TEXT("FindSideCover : bBlockingHit : %d"), OutHitResult.bBlockingHit);
	if (OutHitResult.bBlockingHit)
	{
		UE_LOG(LogTemp, Warning, TEXT("FindSideCover : HitName : %s"), *OutHitResult.GetActor()->GetName());
	}

	if (bLeft)
	{
		OwnerChar->SetMoveLeft(OutHitResult.bBlockingHit);
		MARK_PROPERTY_DIRTY_FROM_NAME(UCoverSystemComponent, bMoveLeft, this);
		bMoveLeft = ((OwnerChar->MoveRightValue < 0) && OutHitResult.bBlockingHit) ? true : false;
	}
	else
	{
		OwnerChar->SetMoveRight(OutHitResult.bBlockingHit);
		MARK_PROPERTY_DIRTY_FROM_NAME(UCoverSystemComponent, bMoveRight, this);
		bMoveRight = ((OwnerChar->MoveRightValue > 0) && OutHitResult.bBlockingHit) ? true : false;
	}

	if (!OutHitResult.bBlockingHit)
	{
		if (bLeft == true)
		{
			SC_SetRightShoulder(false);
			MoveToLoc = OwnerChar->LeftTraceComponent->GetComponentLocation();
		}
		else
		{
			SC_SetRightShoulder(true);
			MoveToLoc = OwnerChar->RightTraceComponent->GetComponentLocation();
		}
		UE_LOG(LogTemp, Warning, TEXT("FindSideCover MoveToBackLoc : %s"), *MoveToBackLoc.ToString());
		MoveToBackLoc = CoverWallLoc;

		MARK_PROPERTY_DIRTY_FROM_NAME(UCoverSystemComponent, bCanFire, this);
		bCanFire = true;
	}
}

void UCoverSystemComponent::TraceUnderCrosshairs(FHitResult& OutHitResult, FVector_NetQuantize& StartLoc, FVector_NetQuantize& EndLoc, const float& TraceRadius, ECollisionChannel TraceChannel)
{
	UWorld* World = UJSMClientStatics::GetGameWorld();
	if (!IsValid(World))
	{
		return;
	}

	FCollisionQueryParams CollisionParams;
	FCollisionResponseParams CollisionResponese;
	CollisionParams.AddIgnoredActor(OwnerChar);
	CollisionParams.bReturnPhysicalMaterial = true;

	const FCollisionShape SphereCollisionShape = FCollisionShape::MakeSphere(TraceRadius);
	const bool bHit = World->SweepSingleByChannel(OutHitResult, StartLoc, EndLoc, FQuat::Identity,
		TraceChannel, SphereCollisionShape, CollisionParams);

	if (OutHitResult.bBlockingHit)
	{
		EndLoc = OutHitResult.Location;
	}

	if (OutHitResult.bBlockingHit && IsValid(OutHitResult.GetActor()))
	{
		if (OutHitResult.PhysMaterial.IsValid())
		{
			int PhysicsIndex = static_cast<int32>(OutHitResult.PhysMaterial->SurfaceType);
		}
	}

	UALSDebugComponent* DebugComponent = OwnerChar->FindComponentByClass<UALSDebugComponent>();
	if (DebugComponent)
	{
		UALSDebugComponent::DrawDebugSphereTraceSingle(World,
			StartLoc,
			EndLoc,
			SphereCollisionShape,
			EDrawDebugTrace::ForOneFrame,
			bHit,
			OutHitResult,
			FLinearColor::Red,
			FLinearColor::Green,
			1.0f);
	}
}

void UCoverSystemComponent::CS_UpdateCoverState_Implementation()
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	ECoverType NewCoverType = CurrentCoverState;
	EALSMovementState NewMovementState = OwnerChar->GetMovementState();

	switch (CurrentCoverState)
	{
		case ECoverType::FindCover:
		{
			if (bCanCover)
			{
				NewCoverType = ECoverType::EnterCover;
				NewMovementState = EALSMovementState::Cover;
			}
			break;
		}
		case ECoverType::EnterCover:
		case ECoverType::InCover:
		{
			NewCoverType = ECoverType::ExitCover;
			NewMovementState = EALSMovementState::Grounded;
			break;
		}
		case ECoverType::ExitCover: 
		{
			break;
		}
		case ECoverType::InCoverAiming:
		{
			break;
		}
		case ECoverType::ExitAiming:
		{
			break;
		}
		default:
		{
			break;
		}
	}

	MARK_PROPERTY_DIRTY_FROM_NAME(UCoverSystemComponent, CurrentCoverState, this);
	CurrentCoverState = NewCoverType;
	SM_UpdateCoverState(NewMovementState);
}

void UCoverSystemComponent::SM_UpdateCoverState_Implementation(const EALSMovementState& NewMoveState)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	OwnerChar->SetMovementState(NewMoveState);
}

void UCoverSystemComponent::InCoverAiming()
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	if ((OwnerChar->GetRotationMode() == EALSRotationMode::Aiming) && bCanFire)
	{
		UE_LOG(LogTemp, Warning, TEXT("InCoverAiming MoveToLoc : %s"), *MoveToLoc.ToString());
		OwnerChar->SetActorLocation(MoveToLoc);
	}
	else
	{
		MARK_PROPERTY_DIRTY_FROM_NAME(UCoverSystemComponent, CurrentCoverState, this);
		CurrentCoverState = ECoverType::ExitAiming;
	}
}

void UCoverSystemComponent::ExitAiming()
{
	if (!IsValid(OwnerChar))
	{
		return;
	}
	UE_LOG(LogTemp, Warning, TEXT("ExitAiming MoveToBackLoc : %s"), *MoveToBackLoc.ToString());
	OwnerChar->SetActorLocation(MoveToBackLoc);

	FTimerHandle DelayHandle;
	const TWeakObjectPtr<UCoverSystemComponent> WeakPtr(this);
	GetWorld()->GetTimerManager().SetTimer(DelayHandle, FTimerDelegate::CreateLambda([WeakPtr]()
	{
		if (WeakPtr.IsValid())
		{
			MARK_PROPERTY_DIRTY_FROM_NAME(UCoverSystemComponent, CurrentCoverState, WeakPtr.Get());
			WeakPtr->CurrentCoverState = ECoverType::InCover;
		}
	}), 0.15, false);
}

void UCoverSystemComponent::SC_SetRightShoulder_Implementation(bool bFlag)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}
	OwnerChar->SetRightShoulder(bFlag);
}

void UCoverSystemComponent::RefreshCoverSystemComponent()
{
	SM_RefreshCoverSystemComponent(CurrentCoverState, bCanCover, bMoveLeft, bMoveRight, bCanFire);
}

void UCoverSystemComponent::SM_RefreshCoverSystemComponent_Implementation(const ECoverType& InCurrentCoverState, bool InbCanCover, bool InbMoveLeft, bool InbMoveRight, bool InbCanFire)
{
	CurrentCoverState = InCurrentCoverState;
	bCanCover = InbCanCover;
	bMoveLeft = InbMoveLeft;
	bMoveRight = InbMoveRight;
	bCanFire = InbCanFire;
}