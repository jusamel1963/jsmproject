// Copyright JSM Studio, Inc. All Rights Reserved.


#include "AbilitySystem/Ability/JSMGameplayAbility.h"
#include "AbilitySystemComponent.h"

void UJSMGameplayAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	bool bReturn = false;

	PreCommitAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData, bReturn);
	if (bReturn)
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, false, true);
		return;
	}

	if (CommitAbility(Handle, ActorInfo, ActivationInfo) == false)
	{
		OnFailedCommitAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
		EndAbility(Handle, ActorInfo, ActivationInfo, false, true);
		return;
	}

	PostCommitActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
}

void UJSMGameplayAbility::PreCommitAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData, bool& bReturn)
{
}

void UJSMGameplayAbility::OnFailedCommitAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
}

void UJSMGameplayAbility::PostCommitActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	if (ReplicationTags.Num() > 0)
	{
		if (ensure(ActorInfo) && ensure(ActorInfo->AbilitySystemComponent.IsValid()))
		{
			ActorInfo->AbilitySystemComponent->AddMinimalReplicationGameplayTags(ReplicationTags);
		}
	}
}

void UJSMGameplayAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);

	if (ReplicationTags.Num() > 0)
	{
		if (ensure(ActorInfo) && ensure(ActorInfo->AbilitySystemComponent.IsValid()))
		{
			ActorInfo->AbilitySystemComponent->RemoveMinimalReplicationGameplayTags(ReplicationTags);
		}
	}
}
