// Copyright JSM Studio, Inc. All Rights Reserved.


#include "System/GameMode/LobbyGameMode.h"
#include "Statics/JSMClientStatics.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "Data/Descriptions/JSMDataManagerSubsystem.h"
#include "Blueprint/UserWidget.h"
#include "UIs/LobbyMainMenuWidget.h"

ALobbyGameMode::ALobbyGameMode()
{

}

void ALobbyGameMode::BeginPlay()
{
	Super::BeginPlay();

	LobbyMenuWidget = CreateWidget(GetGameInstance(), UJSMClientStatics::GetJSMDataCollection()->LobbyMasterWidgetClass.LoadSynchronous());
	if (IsValid(LobbyMenuWidget))
	{
		LobbyMenuWidget->AddToViewport();
	}

	UWorld* World = UJSMClientStatics::GetGameWorld();
	APlayerController* PlayerController = UJSMClientStatics::GetLocalPlayerController(World);
	if (IsValid(PlayerController))
	{
		PlayerController->bShowMouseCursor = true;
		PlayerController->SetInputMode(FInputModeUIOnly());
	}
}
