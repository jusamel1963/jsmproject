// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Animation/AnimNotify/StealthAttackEndAnimNotify.h"
#include "Objects/Character/PlayerCharacter.h"

void UStealthAttackEndAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	if (!IsValid(MeshComp))
	{
		return;
	}

	APlayerCharacter* Owner = Cast<APlayerCharacter>(MeshComp->GetOwner());
	if (!IsValid(Owner))
	{
		return;
	}

	Owner->CS_TakeDownAttackEnd();
}

