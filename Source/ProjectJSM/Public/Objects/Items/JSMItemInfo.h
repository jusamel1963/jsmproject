// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Network/JSMReplicatedObjectInterface.h"
#include "UObject/NoExportTypes.h"
#include "Data/Common/GameEnum.h"
#include "JSMItemInfo.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChangeItemInfo);

UCLASS(BlueprintType, Blueprintable)
class PROJECTJSM_API UJSMItemInfo : public UObject, public IJSMReplicatedObjectInterface
{
	GENERATED_BODY()

public:
	virtual void SetOwnerActor(AActor* InOwnerActor);
	virtual void GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const override;
	virtual bool HasAuthority() override;
	FName GetRepItemDataKey() const;
	void SetRepItemDataKey(FName InRepItemDataKey);
	int32 GetRepItemCount() const;
	void SetRepItemCount(int32 InRepItemCount);
	virtual TWeakObjectPtr<class UJSMItemDataAsset> GetItemDataAsset() const;
	virtual EJSMItemType GetItemType();
	virtual bool IsSupportedForNetworking() const override { return true; }
protected:
	UFUNCTION()
	void OnRep_ItemDataKey();
	UFUNCTION()
	void OnRep_ItemCount();

public:
	FOnChangeItemInfo OnChangelItemInfo;

protected:	
	UPROPERTY(Transient, VisibleInstanceOnly, Replicated)
	uint64 RepUniqueId;

	UPROPERTY(Transient, VisibleInstanceOnly, ReplicatedUsing = "OnRep_ItemDataKey")
	FName RepItemDataKey;

	UPROPERTY(Transient, VisibleInstanceOnly, ReplicatedUsing = "OnRep_ItemCount")
	int32 RepItemCount;

	UPROPERTY(Transient)
	TObjectPtr<AActor> OwnerActor;

	UPROPERTY(VisibleInstanceOnly)
	TWeakObjectPtr<class UJSMItemDataAsset> ItemDataAsset;
};
