// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Interactives/InteractiveActor.h"
#include "InteractiveChair.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API AInteractiveChair : public AInteractiveActor
{
	GENERATED_BODY()
	
};
