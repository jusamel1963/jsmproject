// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTask_SetFocusToPlayer.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UBTTask_SetFocusToPlayer : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
public:
	UBTTask_SetFocusToPlayer();

protected:
	virtual EBTNodeResult::Type ExecuteTask(class UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
