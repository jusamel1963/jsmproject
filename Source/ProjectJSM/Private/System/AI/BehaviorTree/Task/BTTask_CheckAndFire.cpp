// Copyright JSM Studio, Inc. All Rights Reserved.


#include "System/AI/BehaviorTree/Task/BTTask_CheckAndFire.h"
#include "System/AI/BlackboardKeys.h"
#include "FrameWork/AIBaseController.h"
#include "Objects/Character/AICharacter.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "Data/DataAssets/CharacterData/JSMAIDataAsset.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "BehaviorTree/Tasks/BTTask_Wait.h"

UBTTask_CheckAndFire::UBTTask_CheckAndFire()
{
	bNotifyTick = true;
}

EBTNodeResult::Type UBTTask_CheckAndFire::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	check(IsValid(BlackboardComponent));

	AAIBaseController* AIController = Cast<AAIBaseController>(OwnerComp.GetAIOwner());
	if (!ensure(IsValid(AIController)))
	{
		return EBTNodeResult::Failed;
	}

	AAICharacter* AICharacter = Cast<AAICharacter>(AIController->GetCharacter());
	if (!ensure(IsValid(AICharacter)))
	{
		return EBTNodeResult::Failed;
	}

	UAbilitySystemComponent* AbilitySytemComponent = AICharacter->GetAbilitySystemComponent();
	if (!ensure(IsValid(AbilitySytemComponent)))
	{
		return EBTNodeResult::Failed;
	}

	UJSMAIDataAsset* AIDataAsset = AICharacter->GetAIDataAsset();
	if (!ensure(IsValid(AIDataAsset)))
	{
		return EBTNodeResult::Failed;
	}

	ACharacterBase* TargetActor = Cast<ACharacterBase>(AIController->GetBlackboardComponent()->GetValueAsObject(BlackboardKeys::TargetActor));
	if (!IsValid(TargetActor))
	{
		return EBTNodeResult::Failed;
	}

	FireEndLoc = TargetActor->GetMesh()->GetSocketLocation(ACharacterBase::MeshBodySocketName);
	return EBTNodeResult::InProgress;
}

void UBTTask_CheckAndFire::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);

	AAIBaseController* AIController = Cast<AAIBaseController>(OwnerComp.GetAIOwner());
	if (!IsValid(AIController))
	{
		return;
	}
	AAICharacter* AICharacter = Cast<AAICharacter>(AIController->GetCharacter());
	if (!IsValid(AICharacter) || !IsValid(AICharacter->GetWeaponCtrl()))
	{
		return;
	}

	FireCurDelay += DeltaSeconds;
	if (FireCurDelay >= FireMaxDelay)
	{
		AICharacter->GetWeaponCtrl()->CS_FireAIWeapon(FireEndLoc);
		FireCurDelay = 0;
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
}