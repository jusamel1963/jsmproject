// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Statics/JSMClientStatics.h"
#include "Data/Descriptions/ItemDesc.h"
#include "Data/Descriptions/CharacterDesc.h"

#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "FrameWork/JSMGameInstance.h"
#include "Data/Descriptions/JSMDataManagerSubsystem.h"

UWorld* UJSMClientStatics::GetGameWorld()
{
	if (!IsValid(GEngine) || !IsValid(GEngine->GameViewport))
	{
		return nullptr;
	}
	UWorld* World = GEngine->GameViewport->GetWorld();

	if (IsValid(World) && World->bIsTearingDown)
	{
		return nullptr;
	}
	return World;
}

UJSMItemDataAsset* UJSMClientStatics::GetItemDataAsset(FName DataKey)
{
	const FItemDesc* ItemDesc = GetItemDesc(DataKey);
	if (ItemDesc == nullptr)
	{
		return nullptr;
	}

	return ItemDesc->ItemDataAsset;
}

FItemDesc* UJSMClientStatics::GetItemDesc(FName DataKey)
{
	const FString GeneralFindContext = TEXT("GENERAL");

	UDataTable* ItemTable = GetJSMDataCollection()->ItemTable.Get();
	if (IsValid(ItemTable))
	{
		return ItemTable->FindRow<FItemDesc>(DataKey, GeneralFindContext);
	}

	return nullptr;
}

UJSMCharacterDataAsset* UJSMClientStatics::GetCharacterDataAsset(FName DataKey)
{
	const FCharacterDesc* CharacterDesc = GetCharacterDesc(DataKey);
	if (CharacterDesc == nullptr)
	{
		return nullptr;
	}

	return CharacterDesc->CharacterDataAsset;
}

FCharacterDesc* UJSMClientStatics::GetCharacterDesc(FName DataKey)
{
	const FString GeneralFindContext = TEXT("GENERAL");

	UDataTable* CharacterTable = GetJSMDataCollection()->CharacterTable.Get();
	if (IsValid(CharacterTable))
	{
		return CharacterTable->FindRow<FCharacterDesc>(DataKey, GeneralFindContext);
	}

	return nullptr;
}

const class UJSMDataCollection* UJSMClientStatics::GetJSMDataCollection()
{
	const UJSMDataManagerSubsystem* DataManagerSubsystem = JSMFL::GetEngineSubsystem<UJSMDataManagerSubsystem>();
	check(DataManagerSubsystem);
	const UJSMDataCollection* DataCollection = DataManagerSubsystem->GetDataCollection<UJSMDataCollection>();
	check(DataCollection);
	return DataCollection;
}

UJSMGameInstance* UJSMClientStatics::GetGameInstance(const UObject* WorldContextObject)
{
	if (UWorld* World = WorldContextObject->GetWorld())
	{
		return Cast<UJSMGameInstance>(World->GetGameInstance());
	}

	return nullptr;
}

APlayerController* UJSMClientStatics::GetLocalPlayerController(const UObject* worldContextObject)
{
	UWorld* world = worldContextObject->GetWorld();

	if (IsValid(world))
	{
		for (FConstPlayerControllerIterator Iterator = world->GetPlayerControllerIterator(); Iterator; ++Iterator)
		{
			APlayerController* PlayerController = Iterator->Get();
			if (PlayerController->IsLocalController())
			{
				return PlayerController;
			}
		}
	}

	return nullptr;
}