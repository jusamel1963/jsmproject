// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "FrameWork/AbilitySystem/JSMGamePlayEffectTypes.h"
#include "JSMAbilitySystemComponent.generated.h"

enum class EJSMGameplayAbilityID : uint8;

UCLASS()
class PROJECTJSM_API UJSMAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()
	
public:
	UJSMAbilitySystemComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	void AddStartingAbility();
	void AddDefaultEffects();
	virtual void BindAbilityInput(UInputComponent* InInputComponent);
	bool TryActivateAbilityFromID(EJSMGameplayAbilityID AbilityID, FGameplayEventData* EventData);
	int32 TryActivateAbilities(FGameplayTagContainer ActiveTags, FGameplayEventData* EventData);
	FGameplayAbilitySpec* GetAbilitySpecFromID(EJSMGameplayAbilityID AbilityID);
	FGameplayAbilitySpecHandle GetAbilityHandle(uint8 InAbilityID);
	FActiveGameplayEffectHandle ApplyGameplayEffectToSelf(const UObject* Source, AActor* Instigator, TSubclassOf<class UJSMGameplayEffect> GameplayEffectClass, float Level, const TArray <FJSMGameplayEffectArgument> * InArguments);
	bool AttachAbilities(const TArray<TSubclassOf<class UJSMGameplayAbility>>& InAbilities);
	void DetachAbilities(const TArray<TSubclassOf<class UJSMGameplayAbility>>& InAbilities);

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TArray<TSubclassOf<class UJSMGameplayAbility>> StartingAbilities;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TArray<TSubclassOf<class UJSMGameplayAbility>> AttachedAbilities;

	UPROPERTY(Transient)
	TMap<int32, FGameplayAbilitySpecHandle> GrantedAbilities;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "GameplayEffect")
	TArray<TSubclassOf<class UJSMGameplayEffect>> DefaultEffects;

	bool bStartingAbilitiesAdded;
	bool bDefaultEffectsAdded;
	bool bGameplayAbilityInputBound = false;
};
