// Copyright JSM Studio, Inc. All Rights Reserved.


#include "AbilitySystem/AttributeSet/JSMAttributeSet.h"
#include "Net/UnrealNetwork.h"
#include "Data/Descriptions/JSMDataManagerSubsystem.h"
#include "Statics/JSMClientStatics.h"
#include "Objects/Character/CharacterBase.h"
#include "GameplayEffectExtension.h"

JSM_IMPL_REPL_ATTRIBUTE(UJSMAttributeSet, CurrHealth);
JSM_IMPL_REPL_ATTRIBUTE(UJSMAttributeSet, MaxHealth);
JSM_IMPL_REPL_ATTRIBUTE(UJSMAttributeSet, WoundedPenalizeMaxHealth);

JSM_IMPL_REPL_ATTRIBUTE(UJSMAttributeSet, CurrStamina);
JSM_IMPL_REPL_ATTRIBUTE(UJSMAttributeSet, MaxStamina);

void UJSMAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION_NOTIFY(UJSMAttributeSet, CurrHealth, COND_OwnerOnly, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UJSMAttributeSet, MaxHealth, COND_OwnerOnly, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UJSMAttributeSet, CurrStamina, COND_OwnerOnly, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UJSMAttributeSet, MaxStamina, COND_OwnerOnly, REPNOTIFY_Always);
}

void UJSMAttributeSet::Initialize(ACharacterBase* Owner)
{
	const UDataTable* CharacterAttributeSet = UJSMClientStatics::GetJSMDataCollection()->CharacterAttributeSetTable.LoadSynchronous();
	if (ensure(CharacterAttributeSet))
	{
		InitFromMetaDataTable(CharacterAttributeSet);
	}

	SetCalculatedMaxHealth(GetMaxHealth() + GetWoundedPenalizeMaxHealth());

	if (Owner->HasAuthority())
	{
		SetCurrHealth(GetCalculatedMaxHealth());
	}

	JSM_ADD_ATTRIBUTE_CHANGE_CALLBACK(MaxHealth, Owner, this, &UJSMAttributeSet::OnAttriebuteValueChanged_MaxHealth);
	JSM_ADD_ATTRIBUTE_CHANGE_CALLBACK(WoundedPenalizeMaxHealth, Owner, this, &UJSMAttributeSet::OnAttriebuteValueChanged_WoundedPenalizeMaxHealth);
}

void UJSMAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	Super::PreAttributeChange(Attribute, NewValue);

	if (Attribute == GetCurrHealthAttribute())
	{
		NewValue = FMath::Clamp(NewValue, 0.0f, GetCalculatedMaxHealth());
	}
	else if (Attribute == GetCurrStaminaAttribute())
	{
		NewValue = FMath::Clamp(NewValue, 0.0f, GetMaxStamina());
	}
}

void UJSMAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	if (Data.EvaluatedData.Attribute == GetCurrHealthAttribute())
	{
		SetCurrHealth(FMath::Clamp(GetCurrHealth(), 0.0f, GetCalculatedMaxHealth()));
		HealthDamagedEvent.Broadcast(Data);
	}
	else if (Data.EvaluatedData.Attribute == GetCurrStaminaAttribute())
	{
		SetCurrStamina(FMath::Clamp(GetCurrStamina(), 0.0f, GetMaxStamina()));
	}
}

void UJSMAttributeSet::OnAttriebuteValueChanged_MaxHealth(const struct FOnAttributeChangeData& Data)
{
	SetCalculatedMaxHealth(FMath::Clamp(GetWoundedPenalizeMaxHealth() + Data.NewValue, 0.0f, GetMaxHealth()));
}

void UJSMAttributeSet::OnAttriebuteValueChanged_WoundedPenalizeMaxHealth(const FOnAttributeChangeData& Data)
{
	SetCalculatedMaxHealth(FMath::Clamp(GetMaxHealth() + Data.NewValue, 0.0f, GetMaxHealth()));
}
