// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "Framework/State/JSMStateHandlerBase.h"

#include "JSMCharacterState.h"

class FJSMCharacterInitializeStateHandler : public FJSMStateHandlerBase<FJSMCharacterInitializeState>
{
public:
	virtual bool IsTickable() const override;

	FJSMCharacterInitializeState& AddCharacterState(EJSMCharacterInitializeStateID StateID);
};
