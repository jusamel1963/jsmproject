// Copyright JSM Studio, Inc. All Rights Reserved.


#include "UIs/WeaponMenuWidget.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"
#include "Core/Managers/JSMEventManager.h"
#include <Components/Button.h>
#include "FrameWork/JSMPlayerController.h"

void UWeaponMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();
	bIsFocusable = true;

	if (IsValid(Btn_Fist))
	{
		Btn_Fist->OnReleased.AddDynamic(this, &UWeaponMenuWidget::OnBtn_FistPressed);
	}

	if (IsValid(Btn_HipWeapon))
	{
		Btn_HipWeapon->OnReleased.AddDynamic(this, &UWeaponMenuWidget::OnBtn_HipWeaponPressed);
	}

	if (IsValid(Btn_BackWeapon))
	{
		Btn_BackWeapon->OnReleased.AddDynamic(this, &UWeaponMenuWidget::OnBtn_BackWeaponPressed);
	}

	if (IsValid(Btn_MeleeWeapon))
	{
		Btn_MeleeWeapon->OnReleased.AddDynamic(this, &UWeaponMenuWidget::OnBtn_MeleeWeaponPressed);
	}

	if (IsValid(Btn_Throwable))
	{
		Btn_Throwable->OnReleased.AddDynamic(this, &UWeaponMenuWidget::OnBtn_ThrowablePressed);
	}
}

void UWeaponMenuWidget::SetWeaponImage(UTexture2D* InWeaponImage, const int& Idx, const FText& Ammo)
{
	if ((Idx == 0) && IsValid(Img_HipWeapon) && IsValid(HipWeapon_AC))
	{
		Img_HipWeapon->SetBrushFromTexture(InWeaponImage);
		HipWeapon_AC->SetText(Ammo);
	}
	else if ((Idx == 1) && IsValid(Img_BackWeapon) && IsValid(BackWeapon_AC))
	{
		Img_BackWeapon->SetBrushFromTexture(InWeaponImage);
		BackWeapon_AC->SetText(Ammo);
	}
	else if ((Idx == 2) && IsValid(Img_MeleeWeapon))
	{
		Img_MeleeWeapon->SetBrushFromTexture(InWeaponImage);
	}
	else if ((Idx == 3) && IsValid(Img_Throwable))
	{
		Img_Throwable->SetBrushFromTexture(InWeaponImage);
	}
}

void UWeaponMenuWidget::PlayVisibleToggleAnimation(bool bVisible)
{
	if (IsAnimationPlaying(AppearAnimation))
	{
		return;
	}

	EUMGSequencePlayMode::Type PlayMode = (bVisible == true) ? EUMGSequencePlayMode::Forward : EUMGSequencePlayMode::Reverse;

	if (!bVisible)
	{
		VisibleWidgetAnimationEvent.Clear();
		VisibleWidgetAnimationEvent.BindUFunction(this, FName(FString(TEXT("HideWidget"))));
		BindToAnimationFinished(AppearAnimation, VisibleWidgetAnimationEvent);
	}
	else
	{
		UnbindFromAnimationFinished(AppearAnimation, VisibleWidgetAnimationEvent);
	}

	if (IsValid(AppearAnimation))
	{
		PlayAnimation(AppearAnimation, 0, 1, PlayMode);
	}
}

void UWeaponMenuWidget::HideWidget()
{
	SetVisibility(ESlateVisibility::Collapsed);
	if (OwnController.IsValid() && OwnController->IsWeaponMenuPressed())
	{
		UE_LOG(LogTemp, Warning, TEXT("UWeaponMenuWidget ID : %d"), OwnController->GetUniqueID());
		OwnController->WeaponAttach(PressedWeaponIdx);
	}
}

void UWeaponMenuWidget::OnBtn_FistPressed()
{
	OnWeaponBtn_Pressed(OwnID, -1);
}

void UWeaponMenuWidget::OnBtn_HipWeaponPressed()
{
	OnWeaponBtn_Pressed(OwnID, 0);
}

void UWeaponMenuWidget::OnBtn_BackWeaponPressed()
{
	OnWeaponBtn_Pressed(OwnID, 1);
}

void UWeaponMenuWidget::OnBtn_MeleeWeaponPressed()
{
	OnWeaponBtn_Pressed(OwnID, 2);
}

void UWeaponMenuWidget::OnBtn_ThrowablePressed()
{
	OnWeaponBtn_Pressed(OwnID, 3);
}

void UWeaponMenuWidget::OnWeaponBtn_Pressed(uint32 InID, int InIdx)
{
	PressedWeaponIdx = InIdx;
	PlayVisibleToggleAnimation(false);
}
