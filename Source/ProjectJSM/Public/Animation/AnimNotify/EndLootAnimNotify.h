// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotify/AnimNotifyBase.h"
#include "EndLootAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UEndLootAnimNotify : public UAnimNotifyBase
{
	GENERATED_BODY()
public:
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation);
};
