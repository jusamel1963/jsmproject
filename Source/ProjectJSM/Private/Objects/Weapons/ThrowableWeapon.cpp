// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Weapons/ThrowableWeapon.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "GameFramework/RotatingMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Perception/AISense_Hearing.h"
#include "Data/DataAssets/ItemData/JSMThrowableWeaponDataAsset.h"
#include "Objects/Character/CharacterBase.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"

FName AThrowableWeapon::ParticleSystemComponentName(TEXT("ParticleSystemComp"));

AThrowableWeapon::AThrowableWeapon()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	SetReplicateMovement(true);

	ProjectileMovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComp"));
	ProjectileMovementComp->Velocity = FVector_NetQuantize(0, 0, 0);
	ProjectileMovementComp->ProjectileGravityScale = 0.0f;
	ProjectileMovementComp->bShouldBounce = true;
	ProjectileMovementComp->Bounciness = 0.0001f;
	ProjectileMovementComp->Friction = 0.7f;

	ParticleSystemComp = CreateDefaultSubobject<UParticleSystemComponent>(ParticleSystemComponentName);
	ParticleSystemComp->SetupAttachment(RootComponent);

	RotatingMovementComp = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("RotatingMovementComp"));
	RotatingMovementComp->RotationRate = FRotator(0, 0, 0);
}

void AThrowableWeapon::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		if (IsValid(RootSphereComp))
		{
			RootSphereComp->OnComponentHit.AddDynamic(this, &AThrowableWeapon::OnHitBegin);
		}

		if (IsValid(ProjectileMovementComp))
		{
			ProjectileMovementComp->OnProjectileStop.AddDynamic(this, &AThrowableWeapon::OnProjectileStop);
		}
	}
}

void AThrowableWeapon::ThrowableWeaponMove(const FVector_NetQuantize& InLaunchVelocity)
{
	if (!IsValid(ProjectileMovementComp))
	{
		return;
	}

	UJSMThrowableWeaponDataAsset* WeaponDataAsset = Cast<UJSMThrowableWeaponDataAsset>(GetWeaponDataAsset());
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	RotatingMovementComp->RotationRate = FRotator(0, 360, 0);
	ProjectileMovementComp->Velocity = InLaunchVelocity;
	ProjectileMovementComp->ProjectileGravityScale = 1.0f;
	RootSphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AThrowableWeapon::OnProjectileStop(const FHitResult& ImpactResult)
{
	RotatingMovementComp->RotationRate = FRotator(0, 0, 0);
}

void AThrowableWeapon::OnHitBegin(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!HasAuthority())
	{
		return;
	}

	HitEvent(OtherActor, Hit);
}

void AThrowableWeapon::HitEvent(class AActor* InOtherActor, const FHitResult& Hit)
{

}

void AThrowableWeapon::SM_SetOwnChar_Implementation(class ACharacterBase* InOwnChar)
{
	OwnChar = InOwnChar;
}

void AThrowableWeapon::SM_OverlapProjectTile_Implementation(const FHitResult& Hit, UJSMThrowableWeaponDataAsset* WeaponDataAsset)
{
	ParticleSystemComp->SetTemplate(WeaponDataAsset->ProjectileHitEffect.Get());
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponDataAsset->ProjectileHitSound.Get(), GetActorLocation());
	UAISense_Hearing::ReportNoiseEvent(GetWorld(), Hit.Location, WeaponDataAsset->NoiseLoudness, this, WeaponDataAsset->NoiseDistance, DistractNoiseName);
}