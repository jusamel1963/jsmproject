// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "AbilitySystem/JSMAbilitySystemImpl.h"
#include "JSMGameplayAbility.generated.h"

UENUM()
enum class EJSMGameplayAbilityID : uint8
{
	// 0 None
	None			UMETA(DisplayName = "None"),
	// 1 Confirm
	Confirm			UMETA(DisplayName = "Confirm"),
	// 2 Cancel
	Cancel			UMETA(DisplayName = "Cancel"),

	Fire			UMETA(DisplayName = "Fire"),
	Reload			UMETA(DisplayName = "Reload"),
	Drop			UMETA(DisplayName = "Drop"),
	Pickup			UMETA(DisplayName = "Pickup"),

	MeleeAttack		UMETA(DisplayName = "MeleeAttack"),
	Throw			UMETA(DisplayName = "Throw"),
	Max = 255,
};

UCLASS()
class PROJECTJSM_API UJSMGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
public:
	FORCEINLINE int32 GetAbilityID() const { return static_cast<int32>(JSMAbilityID); }
	// UGameplayAbility
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	// ActivateAbility sub func
	virtual void PreCommitAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData, bool& bReturn);
	virtual void OnFailedCommitAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData);
	virtual void PostCommitActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData);
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled);

protected:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Ability")
	EJSMGameplayAbilityID JSMAbilityID = EJSMGameplayAbilityID::None;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Ability")
	FGameplayTagContainer ReplicationTags;
};
