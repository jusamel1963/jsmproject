// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ProjectJSM : ModuleRules
{
	public ProjectJSM(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NetCore", "GameplayAbilities", "GameplayTags", "GameplayTasks", "EnhancedInput", "UMG" , "AdvancedSessions", "AIModule" });
		PrivateDependencyModuleNames.AddRange(new string[] { "ReplicationGraph" });

        bool bTargetConfig = Target.Configuration != UnrealTargetConfiguration.Shipping && Target.Configuration != UnrealTargetConfiguration.Test;
        if (Target.bBuildDeveloperTools || bTargetConfig)
        {
            PrivateDependencyModuleNames.Add("GameplayDebugger");
            PublicDefinitions.Add("WITH_GAMEPLAY_DEBUGGER=1");
        }
        else
        {
            PublicDefinitions.Add("WITH_GAMEPLAY_DEBUGGER=0");
        }
    }
}
