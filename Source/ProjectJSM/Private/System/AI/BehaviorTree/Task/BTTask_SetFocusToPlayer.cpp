// Copyright JSM Studio, Inc. All Rights Reserved.


#include "System/AI/BehaviorTree/Task/BTTask_SetFocusToPlayer.h"
#include "System/AI/BlackboardKeys.h"
#include "FrameWork/AIBaseController.h"
#include "Objects/Character/AICharacter.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "Data/DataAssets/CharacterData/JSMAIDataAsset.h"
#include "GameFramework/Actor.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"

UBTTask_SetFocusToPlayer::UBTTask_SetFocusToPlayer()
{
	BlackboardKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_SetFocusToPlayer, BlackboardKey), AActor::StaticClass());
}

EBTNodeResult::Type UBTTask_SetFocusToPlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	check(IsValid(BlackboardComponent));

	AAIBaseController* AIController = Cast<AAIBaseController>(OwnerComp.GetAIOwner());
	if (!ensure(IsValid(AIController)))
	{
		return EBTNodeResult::Failed;
	}

	AAICharacter* AICharacter = Cast<AAICharacter>(AIController->GetCharacter());
	if (!ensure(IsValid(AICharacter)))
	{
		return EBTNodeResult::Failed;
	}

	UAbilitySystemComponent* AbilitySytemComponent = AICharacter->GetAbilitySystemComponent();
	if (!ensure(IsValid(AbilitySytemComponent)))
	{
		return EBTNodeResult::Failed;
	}

	UJSMAIDataAsset* AIDataAsset = AICharacter->GetAIDataAsset();
	if (!ensure(IsValid(AIDataAsset)))
	{
		return EBTNodeResult::Failed;
	}

	if (BlackboardKey.SelectedKeyType == UBlackboardKeyType_Object::StaticClass())
	{
		UObject* KeyValue = BlackboardComponent->GetValue<UBlackboardKeyType_Object>(BlackboardKey.GetSelectedKeyID());
		AActor* ActorValue = Cast<AActor>(KeyValue);
		OwnerComp.GetAIOwner()->SetFocus(ActorValue);
	}
	return EBTNodeResult::Succeeded;
}

