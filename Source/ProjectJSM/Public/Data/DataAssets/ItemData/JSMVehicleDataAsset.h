// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Data/DataAssets/ItemData/JSMItemDataAsset.h"
#include "JSMVehicleDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UJSMVehicleDataAsset : public UJSMItemDataAsset
{
	GENERATED_BODY()

public:

	UJSMVehicleDataAsset()
	{
		MaxBladeRotationSpeed = 1500.f;
		ThrottleUpSpeed = 200.0f;
		TurnSpeed = 0.1f;
		BlurBladeSpeed = 900.f;
	}

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vehicle")
	TSoftClassPtr<class ANoWheelVehicleBase> NoWheelVehicleBPClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vehicle")
	TSoftClassPtr<class AWheelVehicleBase> WheelVehicleBPClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vehicle")
	TSoftClassPtr<class UAnimInstance> VehicleAnimInstance;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vehicle")
	TSoftObjectPtr<UStaticMesh> RotorStaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vehicle")
	TSoftObjectPtr<UStaticMesh> TailRotorStaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vehicle")
	TSoftObjectPtr<UStaticMesh> BladeStaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vehicle")
	TSoftObjectPtr<UStaticMesh> RearBladeStaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vehicle")
	TSoftObjectPtr<UStaticMesh> BlurBladeStaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vehicle")
	TSoftObjectPtr<UStaticMesh> BlurRearBladeStaticMesh;

	float MaxBladeRotationSpeed;
	float ThrottleUpSpeed;
	float TurnSpeed;
	float BlurBladeSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Vehicle")
	TSoftObjectPtr<UCurveFloat> LiftCurve;

	virtual TArray<FSoftObjectPath> GetObjectPathsToLoad() override
	{
		TArray<FSoftObjectPath> Paths = Super::GetObjectPathsToLoad();

		if (NoWheelVehicleBPClass.IsNull() == false)
		{
			if (NoWheelVehicleBPClass.IsPending())
			{
				Paths.Add(NoWheelVehicleBPClass.ToSoftObjectPath());
			}
		}

		if (WheelVehicleBPClass.IsNull() == false)
		{
			if (WheelVehicleBPClass.IsPending())
			{
				Paths.Add(WheelVehicleBPClass.ToSoftObjectPath());
			}
		}

		if (RotorStaticMesh.IsNull() == false)
		{
			if (RotorStaticMesh.IsPending())
			{
				Paths.Add(RotorStaticMesh.ToSoftObjectPath());
			}
		}

		if (TailRotorStaticMesh.IsNull() == false)
		{
			if (TailRotorStaticMesh.IsPending())
			{
				Paths.Add(TailRotorStaticMesh.ToSoftObjectPath());
			}
		}

		if (BladeStaticMesh.IsNull() == false)
		{
			if (BladeStaticMesh.IsPending())
			{
				Paths.Add(BladeStaticMesh.ToSoftObjectPath());
			}
		}

		if (RearBladeStaticMesh.IsNull() == false)
		{
			if (RearBladeStaticMesh.IsPending())
			{
				Paths.Add(RearBladeStaticMesh.ToSoftObjectPath());
			}
		}

		if (LiftCurve.IsNull() == false)
		{
			if (LiftCurve.IsPending())
			{
				Paths.Add(LiftCurve.ToSoftObjectPath());
			}
		}

		if (BlurBladeStaticMesh.IsNull() == false)
		{
			if (BlurBladeStaticMesh.IsPending())
			{
				Paths.Add(BlurBladeStaticMesh.ToSoftObjectPath());
			}
		}

		if (BlurRearBladeStaticMesh.IsNull() == false)
		{
			if (BlurRearBladeStaticMesh.IsPending())
			{
				Paths.Add(BlurRearBladeStaticMesh.ToSoftObjectPath());
			}
		}

		if (WheelVehicleBPClass.IsNull() == false)
		{
			if (WheelVehicleBPClass.IsPending())
			{
				Paths.Add(WheelVehicleBPClass.ToSoftObjectPath());
			}
		}
		return Paths;
	}

	virtual void LoadSynchronous() override
	{
		Super::LoadSynchronous();

		if (RotorStaticMesh.IsNull() == false)
		{
			RotorStaticMesh.LoadSynchronous();
		}

		if (TailRotorStaticMesh.IsNull() == false)
		{
			TailRotorStaticMesh.LoadSynchronous();
		}

		if (BladeStaticMesh.IsNull() == false)
		{
			BladeStaticMesh.LoadSynchronous();
		}

		if (RearBladeStaticMesh.IsNull() == false)
		{
			RearBladeStaticMesh.LoadSynchronous();
		}

		if (LiftCurve.IsNull() == false)
		{
			LiftCurve.LoadSynchronous();
		}

		if (BlurBladeStaticMesh.IsNull() == false)
		{
			BlurBladeStaticMesh.LoadSynchronous();
		}

		if (BlurRearBladeStaticMesh.IsNull() == false)
		{
			BlurRearBladeStaticMesh.LoadSynchronous();
		}
	}
};
