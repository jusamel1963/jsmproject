// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Weapons/ThrowableWeapon.h"
#include "ThrowableGrenadeWeapon.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API AThrowableGrenadeWeapon : public AThrowableWeapon
{
	GENERATED_BODY()
public:
	AThrowableGrenadeWeapon();

public:
	virtual void HitEvent(class AActor* InOtherActor, const FHitResult& Hit) override;
};
