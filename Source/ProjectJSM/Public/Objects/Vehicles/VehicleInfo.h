// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Network/JSMReplicatedObjectInterface.h"
#include "UObject/NoExportTypes.h"
#include "Data/Common/GameEnum.h"
#include "VehicleInfo.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChangeVehicleInfo);

UCLASS(BlueprintType, Blueprintable)
class PROJECTJSM_API UVehicleInfo : public UObject, public IJSMReplicatedObjectInterface
{
	GENERATED_BODY()

public:
	virtual void SetOwnerActor(AActor* InOwnerActor);
	virtual void GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const override;
	virtual bool HasAuthority() override;
	FName GetRepVehicleDataKey() const;
	void SetRepVehicleDataKey(FName InRepVehicleDataKey);
	virtual TWeakObjectPtr<class UJSMVehicleDataAsset> GetVehicleDataAsset() const;
	virtual bool IsSupportedForNetworking() const override { return true; }
protected:
	UFUNCTION()
	void OnRep_VehicleDataKey();

public:
	FOnChangeVehicleInfo OnChangelVehicleInfo;

protected:
	UPROPERTY(Transient, VisibleInstanceOnly, Replicated)
	uint64 RepUniqueId;

	UPROPERTY(Transient, VisibleInstanceOnly, ReplicatedUsing = "OnRep_VehicleDataKey")
	FName RepVehicleDataKey;

	UPROPERTY(Transient)
	TObjectPtr<AActor> OwnerActor;

	UPROPERTY(VisibleInstanceOnly)
	TWeakObjectPtr<class UJSMVehicleDataAsset> VehicleDataAsset;
};
