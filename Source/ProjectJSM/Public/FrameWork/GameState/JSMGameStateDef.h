// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once


enum class EJSMGameStateID : uint8
{
	None = 0,
	Init,
	OutGame,
	Loading,
	LocalLoading,
	LoadingDone,
	BattleReady,
	Battle,
	BattleEnd,
};