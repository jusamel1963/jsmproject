// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "JSMAttributeSet.generated.h"

#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

#define JSM_IMPL_REPL_ATTRIBUTE(Type, PropertyName)\
void Type::OnRep_##PropertyName(const FGameplayAttributeData& Old##PropertyName)\
{\
	GAMEPLAYATTRIBUTE_REPNOTIFY(Type, PropertyName, Old##PropertyName);\
}

#define JSM_ADD_ATTRIBUTE_CHANGE_CALLBACK(AttributeName, Owner, TargetObject, TargetFunction)																			\
if(IsValid(Owner))																																						\
{																																										\
	UAbilitySystemComponent* __AbilitySystemComponent = Owner->GetAbilitySystemComponent();																				\
	const UJSMAttributeSet* __JSMAttributeSet = Owner->GetAttributeSet<UJSMAttributeSet>();																		\
	if (ensure(IsValid(__AbilitySystemComponent) && IsValid(__JSMAttributeSet)))																							\
	{																																									\
		__AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(__JSMAttributeSet->Get##AttributeName##Attribute()).AddUObject(TargetObject, TargetFunction);	\
	}																																									\
}

UCLASS()
class PROJECTJSM_API UJSMAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:
	void Initialize(class ACharacterBase* Owner);
	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	UFUNCTION() virtual void OnRep_CurrHealth(const FGameplayAttributeData& OldValue);
	UFUNCTION()	virtual void OnRep_MaxHealth(const FGameplayAttributeData& OldValue);
	UFUNCTION()	virtual void OnRep_WoundedPenalizeMaxHealth(const FGameplayAttributeData& OldValue);

	UFUNCTION() virtual void OnRep_CurrStamina(const FGameplayAttributeData& OldValue);
	UFUNCTION()	virtual void OnRep_MaxStamina(const FGameplayAttributeData& OldValue);
private:
	void OnAttriebuteValueChanged_MaxHealth(const struct FOnAttributeChangeData& Data);
	void OnAttriebuteValueChanged_WoundedPenalizeMaxHealth(const struct FOnAttributeChangeData& Data);

public:
	UPROPERTY(ReplicatedUsing = OnRep_CurrHealth);
	FGameplayAttributeData CurrHealth;
	ATTRIBUTE_ACCESSORS(UJSMAttributeSet, CurrHealth);

	UPROPERTY(ReplicatedUsing = OnRep_MaxHealth);
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UJSMAttributeSet, MaxHealth);

	UPROPERTY(ReplicatedUsing = OnRep_WoundedPenalizeMaxHealth);
	FGameplayAttributeData WoundedPenalizeMaxHealth;
	ATTRIBUTE_ACCESSORS(UJSMAttributeSet, WoundedPenalizeMaxHealth);

	UPROPERTY(BlueprintReadOnly);
	FGameplayAttributeData CalculatedMaxHealth;
	ATTRIBUTE_ACCESSORS(UJSMAttributeSet, CalculatedMaxHealth);

	UPROPERTY(ReplicatedUsing = OnRep_CurrStamina);
	FGameplayAttributeData CurrStamina;
	ATTRIBUTE_ACCESSORS(UJSMAttributeSet, CurrStamina);

	UPROPERTY(ReplicatedUsing = OnRep_MaxStamina);
	FGameplayAttributeData MaxStamina;
	ATTRIBUTE_ACCESSORS(UJSMAttributeSet, MaxStamina);

	DECLARE_EVENT_OneParam(UJSMAttributeSet, FGameplayEffectExecuted, const FGameplayEffectModCallbackData&);
	FGameplayEffectExecuted& OnHealthDamaged() { return HealthDamagedEvent; }

private:
	FGameplayEffectExecuted HealthDamagedEvent;
};
