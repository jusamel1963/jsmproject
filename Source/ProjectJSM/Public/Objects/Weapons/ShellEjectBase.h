// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ShellEjectBase.generated.h"

UCLASS()
class PROJECTJSM_API AShellEjectBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShellEjectBase();
	UFUNCTION(NetMulticast, Reliable)
	void SM_InitResource(class UStaticMesh* NewMesh);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly)
	TObjectPtr<class UStaticMeshComponent> StaticMeshComp;
	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly)
	TObjectPtr<class UProjectileMovementComponent> ProjectileMovementComp;
	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly)
	TObjectPtr<class URotatingMovementComponent> ProjectileRotatingMovementComp;
public:
	static FName StaticMeshComponentName;
	static FName SceneComponentName;
	static FName ProjectileMovementComponentName;
	static FName ProjectileRotatingMovementComponentName;
};
