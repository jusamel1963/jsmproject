// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Ability/JSMGameplayAbility.h"
#include "JSMGA_Fire.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UJSMGA_Fire : public UJSMGameplayAbility
{
	GENERATED_BODY()

public:
	UJSMGA_Fire();

	virtual void PostCommitActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

private:
	void InternalActivateAbility();

	bool TryFire();

	UFUNCTION()
	void OnWaitFireInterval();

	UFUNCTION()
	void OnFireStop(FGameplayEventData Payload);

private:
	bool bReserveEnd;

	UPROPERTY()
	TWeakObjectPtr<class UJSMRangeWeaponDataAsset> RangeWeaponDataAsset;

	UPROPERTY()
	TWeakObjectPtr<class UWeaponCtrlComponent> WeaponCtrlComponent;

	UPROPERTY(Transient)
	class ACharacterBase* OwnerCharacter;
};
