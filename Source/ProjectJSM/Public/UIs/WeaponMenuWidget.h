// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UIs/BaseUserWidget.h"
#include "WeaponMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UWeaponMenuWidget : public UBaseUserWidget
{
	GENERATED_BODY()
public:
	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UOverlay> Overlay_Root;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UButton> Btn_Fist;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UImage> Img_Fist;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UButton> Btn_HipWeapon;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr<class UImage> Img_HipWeapon;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr <class UTextBlock> HipWeapon_AC;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UButton> Btn_BackWeapon;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr<class UImage> Img_BackWeapon;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr <class UTextBlock> BackWeapon_AC;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UButton> Btn_MeleeWeapon;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr<class UImage> Img_MeleeWeapon;

	UPROPERTY(meta = (BindWidgetAnimOptional), Transient, BlueprintReadOnly)
	TObjectPtr<class UWidgetAnimation> AppearAnimation;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UButton> Btn_Throwable;

	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UImage> Img_Throwable;

	FWidgetAnimationDynamicEvent VisibleWidgetAnimationEvent;
	int32 PressedWeaponIdx = -1;
public:
	void SetWeaponImage(UTexture2D* InWeaponImage, const int& Idx, const FText& Ammo = FText());
	void PlayVisibleToggleAnimation(bool bVisible);

	UFUNCTION()
	void HideWidget();

	UFUNCTION()
	void OnBtn_FistPressed();

	UFUNCTION()
	void OnBtn_HipWeaponPressed();

	UFUNCTION()
	void OnBtn_BackWeaponPressed();

	UFUNCTION()
	void OnBtn_MeleeWeaponPressed();

	UFUNCTION()
	void OnBtn_ThrowablePressed();

	void OnWeaponBtn_Pressed(uint32 InID, int InIdx);
protected:
	virtual void NativeConstruct() override;
};
