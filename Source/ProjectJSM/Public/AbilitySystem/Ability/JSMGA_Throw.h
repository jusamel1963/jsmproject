// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Ability/JSMGameplayAbility.h"
#include "JSMGA_Throw.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UJSMGA_Throw : public UJSMGameplayAbility
{
	GENERATED_BODY()
public:
	UJSMGA_Throw();

	virtual void PostCommitActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

private:
	UFUNCTION()
	void OnThrowPoseStop(FGameplayEventData Payload);

	UFUNCTION()
	void OnThrowPoseCancel(FGameplayEventData Payload);
private:
	UPROPERTY()
	TWeakObjectPtr<class UWeaponCtrlComponent> WeaponCtrlComponent;

	UPROPERTY(Transient)
	class ACharacterBase* OwnerCharacter;
};
