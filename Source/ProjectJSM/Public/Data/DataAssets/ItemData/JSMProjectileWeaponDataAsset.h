#pragma once

#include "CoreMinimal.h"
#include "Data/DataAssets/ItemData/JSMRangeWeaponDataAsset.h"
#include "Sound/SoundCue.h"
#include "JSMProjectileWeaponDataAsset.generated.h"

UCLASS()
class PROJECTJSM_API UJSMProjectileWeaponDataAsset : public UJSMRangeWeaponDataAsset
{
	GENERATED_BODY()

public:
	UJSMProjectileWeaponDataAsset()
	{
		HitRadius = 200.0f;
	}

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Grenade Weapon")
	TSoftObjectPtr<class UParticleSystem> ProjectileHitEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Grenade Weapon")
	TSoftObjectPtr<class USoundCue> ProjectileHitSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Grenade Weapon")
	TSoftObjectPtr<UStaticMesh> ProjectileStaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Grenade Weapon")
	TSoftClassPtr<class AProjectTileBase> ProjectTileBPClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Grenade Weapon")
	float HitRadius;

	// AsyncLoad
	virtual TArray<FSoftObjectPath> GetObjectPathsToLoad() override
	{
		TArray<FSoftObjectPath> Paths = Super::GetObjectPathsToLoad();

		if (ProjectileHitEffect.IsNull() == false)
		{
			if (ProjectileHitEffect.IsPending())
			{
				Paths.Add(ProjectileHitEffect.ToSoftObjectPath());
			}
		}

		if (ProjectileHitSound.IsNull() == false)
		{
			if (ProjectileHitSound.IsPending())
			{
				Paths.Add(ProjectileHitSound.ToSoftObjectPath());
			}
		}

		if (ProjectileStaticMesh.IsNull() == false)
		{
			if (ProjectileStaticMesh.IsPending())
			{
				Paths.Add(ProjectileStaticMesh.ToSoftObjectPath());
			}
		}

		if (ProjectTileBPClass.IsNull() == false)
		{
			if (ProjectTileBPClass.IsPending())
			{
				Paths.Add(ProjectTileBPClass.ToSoftObjectPath());
			}
		}
		return Paths;
	}

	virtual void LoadSynchronous() override
	{
		Super::LoadSynchronous();

		if (ProjectileHitEffect.IsNull() == false)
		{
			ProjectileHitEffect.LoadSynchronous();
		}

		if (ProjectileHitSound.IsNull() == false)
		{
			ProjectileHitSound.LoadSynchronous();
		}

		if (ProjectileStaticMesh.IsNull() == false)
		{
			ProjectileStaticMesh.LoadSynchronous();
		}
	}
};