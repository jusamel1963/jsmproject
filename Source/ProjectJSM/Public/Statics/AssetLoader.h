// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/StreamableManager.h"
#include "AssetLoader.generated.h"


UCLASS()
class PROJECTJSM_API UAssetLoader : public UObject
{
	GENERATED_BODY()

public:
	UAssetLoader();

	static TSharedPtr<FStreamableHandle>	AssetLoadAsyncStart(const FSoftObjectPath& objPath, FStreamableDelegate finishDelegate);
	static TSharedPtr<FStreamableHandle>	AssetLoadAsyncStart(const FSoftObjectPath& objPath);

	static TSharedPtr<FStreamableHandle>	AssetLoadAsyncStart(const TArray<FSoftObjectPath>& _ObjectList, FStreamableDelegate _DelegateToCall);
	static TSharedPtr<FStreamableHandle> 	AssetLoadAsyncStart(const TArray<FSoftObjectPath>& objPaths);

	static bool								IsAssetAsyncLoading(const FSoftObjectPath& objPath);
	static void								FinishAssetAsyncLoading(const FSoftObjectPath& objPath);

	template<typename T>
	static T* AssetLoadObject(const TSoftObjectPtr<T>& Reference)
	{
		T* Asset = Reference.Get();
		if (Asset == nullptr)
		{
			Asset = Reference.LoadSynchronous();
		}
		return Asset;
	}

	template<typename T>
	static UClass* AssetLoadClass(const TSoftClassPtr<T>& Reference)
	{
		UClass* AssetClass = Reference.Get();
		if (AssetClass == nullptr)
		{
			AssetClass = Reference.LoadSynchronous();
		}

		return AssetClass;
	}
};
