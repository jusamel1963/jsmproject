// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Animation/AnimNotify/DrawWeaponAnimNotify.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"

void UDrawWeaponAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	if (!IsValid(MeshComp))
	{
		return;
	}

	AActor* Owner = MeshComp->GetOwner();
	if (!IsValid(Owner))
	{
		return;
	}

	UWeaponCtrlComponent* WeaponCtrlComponent = Owner->FindComponentByClass<UWeaponCtrlComponent>();
	if (!IsValid(WeaponCtrlComponent))
	{
		return;
	}

	WeaponCtrlComponent->CS_ExchangeWeaponDraw();
}