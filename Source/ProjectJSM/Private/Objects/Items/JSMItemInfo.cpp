// Copyright JSM Studio, Inc. All Rights Reserved.

#include "Objects/Items/JSMItemInfo.h"
#include "Net/UnrealNetwork.h"
#include "Data/DataAssets/ItemData/JSMItemDataAsset.h"
#include "Net/Core/PushModel/PushModel.h"
#include "Statics/JSMClientStatics.h"
#include "Log.h"

FName UJSMItemInfo::GetRepItemDataKey() const
{
	return RepItemDataKey;
}

void UJSMItemInfo::SetRepItemDataKey(FName InRepItemDataKey)
{
	if (!HasAuthority())
	{
		return;
	}

	MARK_PROPERTY_DIRTY_FROM_NAME(UJSMItemInfo, RepItemDataKey, this);
	RepItemDataKey = InRepItemDataKey;
	OnRep_ItemDataKey();
}

int32 UJSMItemInfo::GetRepItemCount() const
{
	return RepItemCount;
}

void UJSMItemInfo::SetRepItemCount(int InRepItemCount)
{
	if (ensure(HasAuthority()))
	{
		MARK_PROPERTY_DIRTY_FROM_NAME(UJSMItemInfo, RepItemCount, this);
		RepItemCount = InRepItemCount;
		OnRep_ItemCount();
	}
}

void UJSMItemInfo::SetOwnerActor(AActor* InOwnerActor)
{
	OwnerActor = InOwnerActor;
}

void UJSMItemInfo::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(UJSMItemInfo, RepUniqueId, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(UJSMItemInfo, RepItemDataKey, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(UJSMItemInfo, RepItemCount, SharedParams);
}

bool UJSMItemInfo::HasAuthority()
{
	AActor* OuterActor = Cast<AActor>(GetOuter());

	if (IsValid(OuterActor) && OuterActor->HasAuthority())
	{
		return true;
	}

	return false;
}

TWeakObjectPtr<class UJSMItemDataAsset> UJSMItemInfo::GetItemDataAsset() const
{
	return ItemDataAsset;
}

EJSMItemType UJSMItemInfo::GetItemType()
{
	if (ItemDataAsset.IsValid())
	{
		return ItemDataAsset->ItemType;
	}

	return EJSMItemType::None;
}

void UJSMItemInfo::OnRep_ItemDataKey()
{
	ItemDataAsset = UJSMClientStatics::GetItemDataAsset(RepItemDataKey);
	OnChangelItemInfo.Broadcast();
}

void UJSMItemInfo::OnRep_ItemCount()
{
	OnChangelItemInfo.Broadcast();
}