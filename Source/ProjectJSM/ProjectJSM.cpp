// Copyright JSM Studio, Inc. All Rights Reserved.

#include "ProjectJSM.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ProjectJSM, "ProjectJSM" );
 