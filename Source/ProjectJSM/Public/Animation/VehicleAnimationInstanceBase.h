// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleAnimationInstance.h"
#include "VehicleAnimationInstanceBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UVehicleAnimationInstanceBase : public UVehicleAnimationInstance
{
	GENERATED_BODY()
	
};
