// Copyright JSM Studio, Inc. All Rights Reserved.


#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "AbilitySystem/Ability/JSMGameplayAbility.h"
#include "AbilitySystem/GameEffect/JSMGameplayEffect.h"

UJSMAbilitySystemComponent::UJSMAbilitySystemComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, bStartingAbilitiesAdded(false)
	, bDefaultEffectsAdded(false)
{
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;

	SetIsReplicatedByDefault(true);
}

void UJSMAbilitySystemComponent::AddStartingAbility()
{
	if (GetOwnerRole() != ROLE_Authority)
	{
		return;
	}

	if (bStartingAbilitiesAdded == true)
	{
		return;
	}
	bStartingAbilitiesAdded = true;

	for (TSubclassOf<UJSMGameplayAbility>& StartingAbility : StartingAbilities)
	{
		if (StartingAbility)
		{
			const int32 AbilityLevel = 1;
			const int32 AbilityID = static_cast<int32>(StartingAbility.GetDefaultObject()->GetAbilityID());

			FGameplayAbilitySpecHandle AbilityHandle = GiveAbility(FGameplayAbilitySpec(StartingAbility.GetDefaultObject(), AbilityLevel, AbilityID, this));
			GrantedAbilities.Add(AbilityID, AbilityHandle);
		}
	}
}

void UJSMAbilitySystemComponent::AddDefaultEffects()
{
	if (GetOwnerRole() != ROLE_Authority)
	{
		return;
	}

	if (bDefaultEffectsAdded)
	{
		return;
	}
	bDefaultEffectsAdded = true;

	for (TSubclassOf<UJSMGameplayEffect>& GameplayEffect : DefaultEffects)
	{
		ApplyGameplayEffectToSelf(GetOwnerActor(), GetAvatarActor(), GameplayEffect, -1, nullptr);
	}
}

void UJSMAbilitySystemComponent::BindAbilityInput(UInputComponent* InInputComponent)
{
	if (!bGameplayAbilityInputBound && IsValid(InInputComponent))
	{
		bGameplayAbilityInputBound = true;
	}
}

//ApplyGameplayEffectSpecToTarget(*HitEffectSpecHandle.Data.Get(), TargetASC)
FActiveGameplayEffectHandle UJSMAbilitySystemComponent::ApplyGameplayEffectToSelf(const UObject* Source, AActor* Instigator, TSubclassOf<UJSMGameplayEffect> GameplayEffectClass, float Level, const TArray<FJSMGameplayEffectArgument>* InArguments)
{
	FGameplayEffectContextHandle EffectContext = MakeEffectContext();
	EffectContext.AddSourceObject(Source);
	EffectContext.AddInstigator(Instigator, Instigator);

	// Build Spec
	FGameplayEffectSpecHandle NewHandle = MakeOutgoingSpec(GameplayEffectClass, Level, EffectContext);

	if (NewHandle.IsValid())
	{
		if (InArguments != nullptr && 0 < InArguments->Num())
		{
			// Argument
			FGameplayEffectContext* Context = NewHandle.Data->GetContext().Get();
			FJSMGameplayEffectContext* JSMContext = static_cast<FJSMGameplayEffectContext*>(Context);
			if (JSMContext != nullptr)
			{
				JSMContext->AddArguments(*InArguments);

				// Argument 중 float 값들은 SetByCaller 에도 적용
				JSMContext->TransferFloatArgumentsToSetByCallerMagnitudes(NewHandle.Data);
			}
		}

		// Apply
		return ApplyGameplayEffectSpecToSelf(*NewHandle.Data.Get());
	}

	return FActiveGameplayEffectHandle();
}

//playerChar->GetAbilitySystemComponent()->TryActivateAbilitiesByTag(FGameplayTagContainer(REQ_GAMEPLAYTAG("Ability.Action.Interaction")));
bool UJSMAbilitySystemComponent::TryActivateAbilityFromID(EJSMGameplayAbilityID AbilityID, FGameplayEventData* EventData)
{
	if (FGameplayAbilitySpec* Spec = GetAbilitySpecFromID(AbilityID))
	{
		return TriggerAbilityFromGameplayEvent(Spec->Handle, AbilityActorInfo.Get(), FGameplayTag(), EventData, *this);
	}

	return false;
}

int32 UJSMAbilitySystemComponent::TryActivateAbilities(FGameplayTagContainer Tags, FGameplayEventData* EventData)
{
	TArray<FGameplayAbilitySpec*> Specs;
	GetActivatableGameplayAbilitySpecsByAllMatchingTags(Tags, Specs);

	int32 ActivatedCount = 0;
	for (FGameplayAbilitySpec* Spec : Specs)
	{
		if (TriggerAbilityFromGameplayEvent(Spec->Handle, AbilityActorInfo.Get(), FGameplayTag(), EventData, *this))
		{
			++ActivatedCount;
		}
	}

	return ActivatedCount;
}

FGameplayAbilitySpec* UJSMAbilitySystemComponent::GetAbilitySpecFromID(EJSMGameplayAbilityID AbilityID)
{
	FGameplayAbilitySpecHandle Handle = GetAbilityHandle(static_cast<uint8>(AbilityID));
	return FindAbilitySpecFromHandle(Handle);
}

FGameplayAbilitySpecHandle UJSMAbilitySystemComponent::GetAbilityHandle(uint8 InAbilityID)
{
	FGameplayAbilitySpecHandle* FoundHandle = GrantedAbilities.Find(InAbilityID);
	if (FoundHandle != nullptr)
	{
		return *FoundHandle;
	}

	return FGameplayAbilitySpecHandle();
}

bool UJSMAbilitySystemComponent::AttachAbilities(const TArray<TSubclassOf<UJSMGameplayAbility>>& InAbilities)
{
	if (GetOwnerRole() != ROLE_Authority)
	{
		return false;
	}

	if (bStartingAbilitiesAdded == false)
	{
		return false;
	}

	for (const TSubclassOf<UJSMGameplayAbility>& AbilityClass : InAbilities)
	{
		int32 FoundIndex = AttachedAbilities.Find(AbilityClass);
		if (FoundIndex == INDEX_NONE)
		{
			UJSMGameplayAbility* Ability = AbilityClass.GetDefaultObject();
			if (IsValid(Ability))
			{
				const int32 AbilityLevel = 1;
				const int32 AbilityID = static_cast<int32>(Ability->GetAbilityID());

				FGameplayAbilitySpecHandle AbilityHandle = GiveAbility(FGameplayAbilitySpec(Ability, 1, AbilityID, this));
				GrantedAbilities.Add(AbilityID, AbilityHandle);
				AttachedAbilities.Add(AbilityClass);
			}
		}
	}

	return true;
}

void UJSMAbilitySystemComponent::DetachAbilities(const TArray<TSubclassOf<class UJSMGameplayAbility>>& InAbilities)
{
	if (GetOwnerRole() != ROLE_Authority)
	{
		return;
	}

	for (const TSubclassOf<UJSMGameplayAbility>& AbilityClass : InAbilities)
	{
		int32 AbilityID = AbilityClass.GetDefaultObject()->GetAbilityID();
		if (GrantedAbilities.Contains(AbilityID))
		{
			ClearAbility(GrantedAbilities[AbilityID]);
			GrantedAbilities.Remove(AbilityID);
			AttachedAbilities.Remove(AbilityClass);
		}
	}
}
