// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Data/DataAssets/ItemData/JSMWeaponDataAsset.h"
#include "Sound/SoundCue.h"
#include "JSMThrowableWeaponDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UJSMThrowableWeaponDataAsset : public UJSMWeaponDataAsset
{
	GENERATED_BODY()
public:
	UJSMThrowableWeaponDataAsset()
	{
		ThrowableType = EThrowable::Stone;
	}

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Throwable")
	EThrowable ThrowableType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Throwable")
	TSoftObjectPtr<UAnimMontage> AM_ThrowPose;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Throwable")
	TSoftObjectPtr<UAnimMontage> AM_Throw;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Throwable")
	TSoftObjectPtr<class UParticleSystem> ProjectileHitEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Throwable")
	TSoftObjectPtr<class USoundCue> ProjectileHitSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Throwable")
	float HitRadius;

	// AsyncLoad
	virtual TArray<FSoftObjectPath> GetObjectPathsToLoad() override
	{
		TArray<FSoftObjectPath> Paths = Super::GetObjectPathsToLoad();

		if (AM_ThrowPose.IsNull() == false)
		{
			if (AM_ThrowPose.IsPending())
			{
				Paths.Add(AM_ThrowPose.ToSoftObjectPath());
			}
		}

		if (AM_Throw.IsNull() == false)
		{
			if (AM_Throw.IsPending())
			{
				Paths.Add(AM_Throw.ToSoftObjectPath());
			}
		}

		if (ProjectileHitEffect.IsNull() == false)
		{
			if (ProjectileHitEffect.IsPending())
			{
				Paths.Add(ProjectileHitEffect.ToSoftObjectPath());
			}
		}

		if (ProjectileHitSound.IsNull() == false)
		{
			if (ProjectileHitSound.IsPending())
			{
				Paths.Add(ProjectileHitSound.ToSoftObjectPath());
			}
		}
		return Paths;
	}

	virtual void LoadSynchronous() override
	{
		Super::LoadSynchronous();

		if (AM_ThrowPose.IsNull() == false)
		{
			AM_ThrowPose.LoadSynchronous();
		}

		if (AM_Throw.IsNull() == false)
		{
			AM_Throw.LoadSynchronous();
		}

		if (ProjectileHitEffect.IsNull() == false)
		{
			ProjectileHitEffect.LoadSynchronous();
		}

		if (ProjectileHitSound.IsNull() == false)
		{
			ProjectileHitSound.LoadSynchronous();
		}
	}
};
