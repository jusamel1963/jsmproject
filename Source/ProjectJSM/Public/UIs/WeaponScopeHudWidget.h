// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UIs/BaseUserWidget.h"
#include "WeaponScopeHudWidget.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UWeaponScopeHudWidget : public UBaseUserWidget
{
	GENERATED_BODY()
public:
	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UOverlay> Overlay_Root;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr<class UImage> Img_WeaponScope;
public:
	void Update_WeaponScope(class UTexture2D* InWeaponScope);
};
