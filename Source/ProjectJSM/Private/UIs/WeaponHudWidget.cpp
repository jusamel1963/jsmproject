// Copyright JSM Studio, Inc. All Rights Reserved.


#include "UIs/WeaponHudWidget.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Materials/MaterialInstance.h"

void UWeaponHudWidget::Update_WeaponHud(UTexture2D* InPrimaryWeapon, UTexture2D* InHolsterWeapon, const FText& InMagAmmo, const FText& InMagCapacity)
{
	if (!IsValid(PrimaryWeapon) || !IsValid(HolsterWeapon) || !IsValid(MagAmmo) || !IsValid(MagCapacity))
	{
		return;
	}
	if (IsValid(InPrimaryWeapon))
	{
		PrimaryWeapon->SetBrushFromTexture(InPrimaryWeapon);
	}
	if (IsValid(InHolsterWeapon))
	{
		HolsterWeapon->SetBrushFromTexture(InHolsterWeapon);
	}
	MagAmmo->SetText(InMagAmmo);
	MagCapacity->SetText(InMagCapacity);
}