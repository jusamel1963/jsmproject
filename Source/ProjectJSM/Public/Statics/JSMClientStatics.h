// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/NoExportTypes.h"
#include "Framework/JSMGameInstance.h"
#include "JSMClientStatics.generated.h"


UCLASS()
class PROJECTJSM_API UJSMClientStatics : public UObject
{
	GENERATED_BODY()
	
public:
	static class UJSMItemDataAsset* GetItemDataAsset(FName DataKey);
	template<typename T>
	static T* GetItemDataAsset(FName DataKey)
	{
		return Cast<T>(GetItemDataAsset(DataKey));
	}

	static class UJSMCharacterDataAsset* GetCharacterDataAsset(FName DataKey);
	template<typename T>
	static T* GetCharacterDataAsset(FName DataKey)
	{
		return Cast<T>(GetCharacterDataAsset(DataKey));
	}

	static const class UJSMDataCollection* GetJSMDataCollection();
	static struct FItemDesc* GetItemDesc(FName DataKey);
	static struct FCharacterDesc* GetCharacterDesc(FName DataKey);

	static UJSMGameInstance* GetGameInstance(const UObject* WorldContextObject);
	static UWorld* GetGameWorld();
	static class APlayerController* GetLocalPlayerController(const UObject* worldContextObject);
};
