// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Character/ALSPlayerController.h"
#include "JSMPlayerController.generated.h"

UCLASS()
class PROJECTJSM_API AJSMPlayerController : public AALSPlayerController
{
	GENERATED_BODY()
	
public:
	virtual void AcknowledgePossession(class APawn* InPawn) override;
	virtual void SetupInputComponent() override;
	virtual void OnPossess(APawn* NewPawn) override;
	virtual void OnRep_Pawn() override;
	void WeaponHudUpdate(UTexture2D* InPrimaryWeapon, UTexture2D* InHolsterWeapon, const FText& InMagAmmo, const FText& InMagCapacity, UTexture2D* CrossHair, UTexture2D* WeaponScope);
	void ProgressHudUpdate(const float& InValue);
	void SetVisibleWeaponScope(const bool& bValue);
	void SetVisibleProgressBar(const bool& bVisible, const FText& LoadingName);
	void NotifyOtherUserEnterMe();
	void RefreshCharacterData();
	void WeaponMenuUpdate(UTexture2D* InHipWeapon, UTexture2D* InBackWeapon, UTexture2D* InMeleeWeapon, UTexture2D* InThrowableWeapon,
		const FText& InHipWeaponAmmo, const FText& InBackWeaponAmmo);

	UFUNCTION()
	void SetInputMode_Game();
	UFUNCTION()
	void SetInputMode_GameAndUI();
	void WeaponAttach(int InWeaponIdx);
	bool IsWeaponMenuPressed() { return OverlayMenuOpen; }
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void PickupWeaponAction(const FInputActionValue& Value);
	UFUNCTION()
	void WeaponCycleAction(const FInputActionValue& Value);
	UFUNCTION()
	void AttackAction(const FInputActionValue& Value);
	UFUNCTION()
	void WeaponrReloadAction(const FInputActionValue& Value);
	void SetupGameInput();
	UFUNCTION()
	void InteractionAction(const FInputActionValue& Value);

	UFUNCTION(Server, Reliable)
	void CS_InteractionAction();

	UFUNCTION()
	void OpenWeaponMenuAction(const FInputActionValue& Value);

	UFUNCTION()
	void TakeDownAction(const FInputActionValue& Value);

	UFUNCTION()
	void CoverAction(const FInputActionValue& Value);

	virtual void Custom_ForwardMovementAction(const FInputActionValue& Value) override;
	virtual void Custom_RightMovementAction(const FInputActionValue& Value) override;

	virtual void Custom_CameraRightAction(const FInputActionValue& Value) override;
	virtual void Custom_CameraUpAction(const FInputActionValue& Value) override;
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Input")
	TObjectPtr<UInputMappingContext> GameInputMappingContext = nullptr;

	UPROPERTY(BlueprintReadWrite, Category = "Widget")
	class UUserWidget* RootGamePlayWidget;

	bool OverlayMenuOpen = false;
};
