// Copyright JSM Studio, Inc. All Rights Reserved.


#include "System/LevelDesign/AISpawnerActor.h"
#include "Framework/JSMItemManagerSubsystem.h"
#include "Objects/Character/AICharacter.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "Statics/JSMClientStatics.h"
#include "Data/DataAssets/CharacterData/JSMAIDataAsset.h"
#include "Kismet/GameplayStatics.h"
#include "System/GameMode/MultiplayGameMode.h"

AAISpawnerActor::AAISpawnerActor()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AAISpawnerActor::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		Execute();
	}
}

void AAISpawnerActor::Execute()
{
	if (!HasAuthority())
	{
		return;
	}

	if (SpawnedActors.Num() > 0)
	{
		return;
	}

	if (bRespawnable)
	{
		SetRespawnTimer();
	}
	else
	{
		SpawnAI();
	}
}

void AAISpawnerActor::SpawnAI()
{
	if (!HasAuthority())
	{
		return;
	}

	for (FAISpawnData& AISpawnData : SpawnData)
	{
		UJSMAIDataAsset* AIDataAsset = UJSMClientStatics::GetCharacterDataAsset<UJSMAIDataAsset>(AISpawnData.AIKeyName);

		if (!IsValid(AIDataAsset) || AIDataAsset->CharacterBPClass.IsNull())
		{
			return;
		}
		UClass* bpClass = AIDataAsset->CharacterBPClass.LoadSynchronous();
		FTransform SpawnTransform = GetTransform();
		AAICharacter* defaultChar = Cast<AAICharacter>(ACharacterBase::InstantiateCharacter(this, TSubclassOf<ACharacterBase>(bpClass), SpawnTransform, AISpawnData.AIKeyName.ToString()));

		if (IsValid(defaultChar))
		{
			defaultChar->SpawnDefaultController();
			SpawnedActors.Add(defaultChar);
			defaultChar->OnDestroyed.AddDynamic(this, &AAISpawnerActor::OnDestroyActor);
			defaultChar->SetPatrolPoints(AISpawnData.PatrolPoints);

			AMultiplayGameMode* MultiGameMode = Cast<AMultiplayGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
			if (IsValid(MultiGameMode))
			{
				MultiGameMode->AddAICharacter(defaultChar);
			}
		}
	}
}

void AAISpawnerActor::SetRespawnTimer()
{
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	TimerManager.SetTimer(SpawnerTimerHandle, this, &AAISpawnerActor::OnSpawnTimer, RespawnTimeInterval, false, RespawnTimeInterval);
}

void AAISpawnerActor::OnSpawnTimer()
{
	SpawnAI();
}

void AAISpawnerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AAISpawnerActor::OnDestroyActor(AActor* DestroyedActor)
{
	SpawnedActors.Remove(DestroyedActor);

	if (SpawnedActors.Num() == 0 && bRespawnable)
	{
		SetRespawnTimer();
	}
}