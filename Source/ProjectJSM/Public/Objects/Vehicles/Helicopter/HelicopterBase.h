// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Vehicles/NoWheelVehicleBase.h"
#include "HelicopterBase.generated.h"

UCLASS()
class PROJECTJSM_API AHelicopterBase : public ANoWheelVehicleBase
{
	GENERATED_BODY()

public:
	AHelicopterBase();
	virtual void Tick(float DeltaTime) override;
protected:
	virtual void InitializeVehicleResources() override;
	virtual void BeginPlay() override;

public:
	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class UStaticMeshComponent> RotorComponent;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class UStaticMeshComponent> TailRotorComponent;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class UStaticMeshComponent> BladeComponent;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class UStaticMeshComponent> RearBladeComponent;

	static FName RotorComponentName;
	static FName TailRotorComponentName;
	static FName BladeComponentName;
	static FName RearBladeComponentName;
};
