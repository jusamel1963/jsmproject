// Copyright JSM Studio, Inc. All Rights Reserved.


#include "UIs/BaseUserWidget.h"
#include "FrameWork/JSMPlayerController.h"

void UBaseUserWidget::SetOwnIDAndController(uint32 InID, class AJSMPlayerController* InController)
{
	OwnID = InID;
	OwnController = InController;
}