// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

template<typename TStateID>
class FJSMStateBase
{
public:
	using StateIdType = typename TStateID;
	using TransitionCondition = TFunction<bool()>;
public:
	FJSMStateBase(TStateID InStateID) : StateID(InStateID)
	{

	}

	virtual ~FJSMStateBase() { }

	FORCEINLINE TStateID GetStateID() const { return StateID; }

	FJSMStateBase<TStateID>& AddTranstion(const TransitionCondition& Condition, TStateID TranstionState)
	{
		TransitionStates.Add(TPair<TransitionCondition, TStateID>(Condition, TranstionState));
		return *this;
	}

	template<typename FunctorType, typename... VarTypes>
	FJSMStateBase<TStateID>& AddLambdaOnEnter(FunctorType&& InFunctor, VarTypes... Vars)
	{
		OnEventEnter.AddLambda(Forward<FunctorType>(InFunctor), Vars...);
		return *this;
	}

	template<typename FunctorType, typename... VarTypes>
	FJSMStateBase<TStateID>& AddLambdaOnLeave(FunctorType&& InFunctor, VarTypes... Vars)
	{
		OnEventLeave.AddLambda(Forward<FunctorType>(InFunctor), Vars...);
		return *this;
	}

	template<typename FunctorType, typename... VarTypes>
	FJSMStateBase<TStateID>& AddLambdaOnUpdate(FunctorType&& InFunctor, VarTypes... Vars)
	{
		OnEventUpdate.AddLambda(Forward<FunctorType>(InFunctor), Vars...);
		return *this;
	}

	void Enter() { ElapsedTime = 0.0f;  OnEnter();  OnEventEnter.Broadcast(); }
	void Leave() { OnLeave();  OnEventLeave.Broadcast(); }
	void Update(float DeltaTime) { ElapsedTime += DeltaTime; OnUpdate(DeltaTime);  OnEventUpdate.Broadcast(DeltaTime); }

	bool HasTransitionState(OUT TStateID& OutTransitionState) const
	{
		OutTransitionState = TStateID{};

		for (auto& Pair : TransitionStates)
		{
			if (Pair.Key() == true)
			{
				OutTransitionState = Pair.Value;
				return true;
			}
		}

		return false;
	}

	FORCEINLINE float GetElapsedTime() const { return ElapsedTime; }

	DECLARE_EVENT(FJSMStateBase<TStateID>, FGameStateEnterLeaveCallback);
	FGameStateEnterLeaveCallback OnEventEnter;
	FGameStateEnterLeaveCallback OnEventLeave;

	DECLARE_EVENT_OneParam(FJSMStateBase<TStateID>, FGameStateUpdateCallback, float);
	FGameStateUpdateCallback OnEventUpdate;

protected:
	virtual void OnEnter() {}
	virtual void OnLeave() {}
	virtual void OnUpdate(float DeltaTime) {}

private:
	TStateID StateID;
	float ElapsedTime;

	TArray <TPair<TransitionCondition, TStateID>> TransitionStates;
};

