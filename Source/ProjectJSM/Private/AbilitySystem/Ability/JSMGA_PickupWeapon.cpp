// Copyright JSM Studio, Inc. All Rights Reserved.


#include "AbilitySystem/Ability/JSMGA_PickupWeapon.h"
#include "Abilities/Tasks/AbilityTask_WaitDelay.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"
#include "Core/Managers/JSMEventManager.h"
#include "Data/DataAssets/ItemData/JSMWeaponDataAsset.h"
#include "Framework/JSMGameInstance.h"
#include "Objects/Character/CharacterBase.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Objects/Weapons/Weapon.h"
#include "Objects/Weapons/JSMWeaponInfo.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "Objects/Weapons/RangeWeapon.h"

UJSMGA_PickupWeapon::UJSMGA_PickupWeapon()
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::ServerOnly;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::NonInstanced;

	FGameplayTag AbilityTag = REQ_GAMEPLAYTAG("Ability.Inventory.PickupWeapon");
	AbilityTags.AddTag(AbilityTag);
	ActivationOwnedTags.AddTag(AbilityTag);

	ActivationBlockedTags.Reset();
	ActivationBlockedTags.AddTag(REQ_GAMEPLAYTAG("Status.Character.Dead"));

	FGameplayTag AbilityTriggerTag = REQ_GAMEPLAYTAG("Event.Inventory.PickupWeapon");
	FAbilityTriggerData TriggerData;
	TriggerData.TriggerTag = AbilityTriggerTag;
	TriggerData.TriggerSource = EGameplayAbilityTriggerSource::GameplayEvent;
	AbilityTriggers.Add(TriggerData);
}

void UJSMGA_PickupWeapon::PostCommitActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::PostCommitActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
	if (HasAuthority(&ActivationInfo) == false)
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	APlayerCharacter* OwnerCharacter = Cast<APlayerCharacter>(ActorInfo->AvatarActor);
	if ((IsValid(OwnerCharacter) == false) || !IsValid(TriggerEventData->Target))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	AWeapon* WeaponActor = TriggerEventData != nullptr ? Cast<AWeapon>(const_cast<AActor*>(TriggerEventData->Target.Get())) : nullptr;
	if (IsValid(WeaponActor) == false || WeaponActor->IsEmpty())
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	UJSMWeaponInfo* WeaponInfo = WeaponActor->GetRepWeaponInfo();
	if (IsValid(WeaponInfo) == false || WeaponInfo->GetWeaponDataAsset().IsValid() == false)
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	WeaponActor->BeginPickup();

	int32 MagAmmo = 1;
	int32 BagAmmo = 1;

	ARangeWeapon* RangeWeaponActor = Cast<ARangeWeapon>(WeaponActor);
	if (IsValid(RangeWeaponActor))
	{
		MagAmmo = RangeWeaponActor->GetCurrentMagAmmo();
		BagAmmo = RangeWeaponActor->GetCurrentBagAmmo();
	}

	UWeaponCtrlComponent* WeaponCtrlComponent = OwnerCharacter->GetWeaponCtrl();
	if (IsValid(WeaponCtrlComponent))
	{
		WeaponCtrlComponent->SS_PickupWeapon(WeaponInfo->GetRepWeaponDataKey(), MagAmmo, BagAmmo, false);
	}

	WeaponActor->EndPickup();

	EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
}

void UJSMGA_PickupWeapon::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}
