// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FrameWork/Interface/CoverValues.h"
#include "Data/Common/GameEnum.h"
#include "Data/Common/Constants.h"
#include "CoverSystemComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTJSM_API UCoverSystemComponent : public UActorComponent, public ICoverValues
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCoverSystemComponent();
	virtual void InitializeComponent() override;
	void FindCover();
	void FindSideCover(bool bLeft, class UArrowComponent* ArrowComp);
	void EnterCover();
	void ExitCover();
	void InCover();
	void InCoverAiming();
	void ExitAiming();
	UFUNCTION(Server, Reliable)
	void CS_UpdateCoverState();

	UFUNCTION(NetMulticast, Reliable)
	void SM_UpdateCoverState(const EALSMovementState& NewMoveState);

	UFUNCTION(Client, Reliable)
	void SC_SetRightShoulder(bool bFlag);

	void TraceUnderCrosshairs(FHitResult& OutHitResult, FVector_NetQuantize& StartLoc, FVector_NetQuantize& EndLoc, const float& TraceRadius, ECollisionChannel TraceChannel);
	bool IsCover() const { return bCanCover; }

	virtual ECoverType GetCoverType() override { return CurrentCoverState; }
	virtual bool IsMoveLeft() override { return bMoveLeft; }
	virtual bool IsMoveRight() override { return bMoveRight; }

	void RefreshCoverSystemComponent();
	UFUNCTION(NetMulticast, Reliable)
	void SM_RefreshCoverSystemComponent(const ECoverType& InCurrentCoverState, bool InbCanCover, bool InbMoveLeft, bool InbMoveRight, bool InbCanFire);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY(Transient)
	TObjectPtr<class ACharacterBase> OwnerChar;
	
	UPROPERTY(Replicated)
	ECoverType CurrentCoverState = ECoverType::FindCover;
	UPROPERTY(Replicated)
	bool bCanCover = false;

	UPROPERTY(Replicated)
	bool bMoveLeft = false;

	UPROPERTY(Replicated)
	bool bMoveRight = false;

	UPROPERTY(Replicated)
	bool bCanFire = false;

	FVector_NetQuantize CoverWallNormal;
	FVector_NetQuantize CoverWallLoc;

	FVector_NetQuantize MoveToLoc;
	FVector_NetQuantize MoveToBackLoc;
};
