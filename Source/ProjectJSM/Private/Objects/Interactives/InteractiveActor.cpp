// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Interactives/InteractiveActor.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Components/SceneComponent.h"

AInteractiveActor::AInteractiveActor()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	bReplicates = true;

	USceneComponent* SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComp"));
	RootComponent = SceneComponent;

	TriggerComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerComp"));
	if (IsValid(TriggerComponent))
	{
		TriggerComponent->SetupAttachment(RootComponent);
	}

	InteractiveStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("InteractiveStaticMesh"));
	if (IsValid(InteractiveStaticMeshComponent))
	{
		InteractiveStaticMeshComponent->bReceivesDecals = false;
		InteractiveStaticMeshComponent->CastShadow = false;
		InteractiveStaticMeshComponent->SetupAttachment(RootComponent);
	}
}

void AInteractiveActor::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		if (IsValid(TriggerComponent))
		{
			TriggerComponent->OnComponentBeginOverlap.AddDynamic(this, &AInteractiveActor::OnOverlapBegin);
			TriggerComponent->OnComponentEndOverlap.AddDynamic(this, &AInteractiveActor::OnOverlapEnd);
		}
	}
}

void AInteractiveActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

void AInteractiveActor::OnOverlapBegin(UPrimitiveComponent* OpverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APlayerCharacter* Character = Cast<APlayerCharacter>(OtherActor);
	if (!IsValid(Character))
	{
		return;
	}

	OverlapPlayers.Emplace(Character->GetUniqueID(), Character);
}

void AInteractiveActor::OnOverlapEnd(class UPrimitiveComponent* OpverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	APlayerCharacter* Character = Cast<APlayerCharacter>(OtherActor);
	if (!IsValid(Character))
	{
		return;
	}
	OverlapPlayers.Remove(Character->GetUniqueID());
}

bool AInteractiveActor::IsOverlapPlayer(const uint32& Id) const
{
	if (OverlapPlayers.Find(Id) != nullptr)
	{
		return true;
	}
	return false;
}