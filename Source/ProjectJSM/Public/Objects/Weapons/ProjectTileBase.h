// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProjectTileBase.generated.h"

UCLASS()
class PROJECTJSM_API AProjectTileBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectTileBase();

	UFUNCTION(NetMulticast, Reliable)
	void SM_InitResource(class ACharacterBase* InOwnChar, class UParticleSystem* InProjectileHitEffect, class USoundCue* InProjectileHitSound, class UStaticMesh* InStaticMesh, const float& InHitRadius, const float& InImpulse);

	UFUNCTION(NetMulticast, Reliable)
	void SM_OverlapProjectTile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OpverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly)
	TObjectPtr<class UStaticMeshComponent> StaticMeshComp;

	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly)
	TObjectPtr<class UProjectileMovementComponent> ProjectileMovementComp;

	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly)
	TObjectPtr<class UParticleSystemComponent> ParticleSystemComp;

	UPROPERTY(EditDefaultsOnly)
	TObjectPtr<class USphereComponent> VolumeComponent;

	UPROPERTY(EditDefaultsOnly, Transient, BlueprintReadOnly, Category = "Grenade Weapon")
	TObjectPtr<class UParticleSystem> ProjectileHitEffect;

	UPROPERTY(EditDefaultsOnly, Transient, BlueprintReadOnly, Category = "Grenade Weapon")
	TObjectPtr<class USoundCue> ProjectileHitSound;

	bool bPendingKill = false;
	TWeakObjectPtr<class ACharacterBase> OwnChar;
	float HitRadius;
	float Impulse;
public:
	static FName StaticMeshComponentName;
	static FName SceneComponentName;
	static FName ProjectileMovementComponentName;
	static FName VolumeComponentName;
	static FName ParticleSystemComponentName;
};
