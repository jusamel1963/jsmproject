// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Weapons/ThrowableKnifeWeapon.h"
#include "Data/DataAssets/ItemData/JSMThrowableWeaponDataAsset.h"
#include "Kismet/GameplayStatics.h"
#include "Objects/Character/CharacterBase.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "Perception/AISense_Hearing.h"
#include "Particles/ParticleSystemComponent.h"

AThrowableKnifeWeapon::AThrowableKnifeWeapon()
{

}

void AThrowableKnifeWeapon::HitEvent(class AActor* InOtherActor, const FHitResult& Hit)
{
	UJSMThrowableWeaponDataAsset* WeaponDataAsset = Cast<UJSMThrowableWeaponDataAsset>(GetWeaponDataAsset());
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	if (!bPendingKill && IsValid(ParticleSystemComp))
	{
		bPendingKill = true;
		//SM_OverlapProjectTile(Hit, WeaponDataAsset);

		FTimerHandle DelayHandle;
		const TWeakObjectPtr<AThrowableWeapon> WeakPtr(this);
		GetWorld()->GetTimerManager().SetTimer(DelayHandle, FTimerDelegate::CreateLambda([WeakPtr]()
		{
			if (WeakPtr.IsValid())
			{
				WeakPtr->Destroy();
			}
		}), 1.0f, false);

		ACharacterBase* HitChar = Cast<ACharacterBase>(InOtherActor);
		if (!OwnChar.IsValid() || !IsValid(HitChar))
		{
			return;
		}

		UWeaponCtrlComponent* WeaponCtrlComp = OwnChar->GetWeaponCtrl();
		if (!IsValid(WeaponCtrlComp))
		{
			return;
		}

		FVector_NetQuantize HitLoc = HitChar->GetMesh()->GetSocketLocation(ACharacterBase::MeshBodySocketName);
		FVector_NetQuantize ImpulseValue = (HitLoc - GetActorLocation()).GetSafeNormal2D() * WeaponDataAsset->Impulse;
		WeaponCtrlComp->ApplyWeaponAbilityEffect(this, HitChar, ImpulseValue, HitLoc, ACharacterBase::MeshBodySocketName);
	}
}