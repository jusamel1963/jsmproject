// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Objects/Vehicles/VehicleInterface.h"
#include "NoWheelVehicleBase.generated.h"

UCLASS()
class PROJECTJSM_API ANoWheelVehicleBase : public APawn, public IVehicleInterface
{
	GENERATED_BODY()

public:
	ANoWheelVehicleBase();
	void SS_CreateVehicleInfo();
	void SS_SetRepVehicleInfo(class UVehicleInfo* InRepVehicleInfo);
	class UVehicleInfo* GetRepVehicleInfo() const;
	class UJSMVehicleDataAsset* GetVehicleDataAsset() const;
	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void EnterVehicle(class ACharacterBase* InEnterPlayer) override;
	virtual void ExitVehicle(const uint32& InID) override;
	virtual bool IsOverlapPlayer(const uint32& Id) const override;
	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OpverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnOverlapEnd(class UPrimitiveComponent* OpverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(Server, Reliable)
	void CS_ExitVehicle(const uint32& InID);

	virtual void ForwardMovementAction(const float& InValue) override;
	virtual void RightMovementAction(const float& InValue) override;
	virtual void CameraUpAction(const float& InValue) override;
	virtual void CameraRightAction(const float& InValue) override;

	UFUNCTION(Server, Reliable)
	void CS_ForwardMovementAction(const float& InValue);

	UFUNCTION(Server, Reliable)
	void CS_CameraRightAction(const float& InValue);

	UFUNCTION(NetMulticast, Reliable)
	void SM_CameraRightAction(const FRotator& AddRot, const FRotator& SetRot);

	UFUNCTION(Server, Reliable)
	void CS_CameraUpAction(const float& InValue);

	UFUNCTION(NetMulticast, Reliable)
	void SM_CameraUpAction(const FRotator& AddRot);

	float GetCurrentLift() const;
	float GetTurnSpeed() const;

	UFUNCTION(NetMulticast, Reliable)
	void SM_AddActorWorldOffset(const FVector_NetQuantize& InValue, bool bSweep);

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnRep_VehicleInfo();

	virtual void LoadAssetAsync(FJSMVehicleDataLoadComplete LoadCompleteDelegate) override;
	virtual void LoadAssetCompleted() override;
	virtual void InitializeVehicleResources() override;
	virtual void LoadSynchronous() override;

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	FSimpleDelegate OnLoadComplete;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class UBoxComponent> EnterBoxRightComponent;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class UBoxComponent> EnterBoxLeftComponent;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class USpringArmComponent> SpringArmComp;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class UCameraComponent> CameraComp;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class USceneComponent> SceneComp;

protected:
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Replicated, Category = "Vehicle")
	FName RepVehicleDataKey;

	UPROPERTY(VisibleAnywhere)
	TObjectPtr<class UStaticMeshComponent> VehicleStaticMeshComponent;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class USkeletalMeshComponent> VehicleSkeletalMeshComponent;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_VehicleInfo)
	TObjectPtr<class UVehicleInfo> RepVehicleInfo;

	static FName SceneComponentName;
	static FName VehicleStaticMeshComponentName;
	static FName VehicleSkeletalMeshComponentName;

	bool bUseAsyncLoad = true;
	bool bLoadCompleted = false;

	FJSMVehicleAssetLoadRequest AssetLoadRequest;

	UPROPERTY(Transient)
	TMap<uint32, TWeakObjectPtr<class ACharacterBase>> OverlapPlayers;

	UPROPERTY(Transient)
	TMap<FName, FSeatInfo> SeatArray;

	UPROPERTY(Replicated)
	float TargetBladeRotationSpeed = 0;

	float CurrentBladeRotationSpeed = 0;
	FVector_NetQuantize Velocity = FVector_NetQuantize::ZeroVector;
	FVector_NetQuantize Acceleration = FVector_NetQuantize::ZeroVector;
	FVector_NetQuantize PrevVelocity = FVector_NetQuantize::ZeroVector;
	FVector_NetQuantize PrevAcceleration = FVector_NetQuantize::ZeroVector;
	FVector_NetQuantize PrevPosition = FVector_NetQuantize::ZeroVector;

	UPROPERTY(Replicated)
	float CurrentTurn = 0;
	UPROPERTY(Replicated)
	float CurrentRoll = 0;

	uint32 DriverID = 0;
};
