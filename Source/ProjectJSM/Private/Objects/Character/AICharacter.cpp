// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Character/AICharacter.h"
#include "AbilitySystem/AttributeSet/JSMAttributeSet.h"
#include "Statics/JSMClientStatics.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "FrameWork/AIBaseController.h"
#include "Data/DataAssets/CharacterData/JSMAIDataAsset.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Perception/AISense_Sight.h"
#include "Components/ALSDebugComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "System/AI/BlackboardKeys.h"
#include "Components/SphereComponent.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Net/UnrealNetwork.h"
#include "Net/Core/PushModel/PushModel.h"
#include "System/GameMode/MultiplayGameMode.h"

AAICharacter::AAICharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
}

void AAICharacter::BeginPlay()
{
	Super::BeginPlay();
}

void AAICharacter::InitializeCharacter(const FCharacterDesc* charDesc)
{
	Super::InitializeCharacter(charDesc);

	AAIBaseController* aiController = Cast<AAIBaseController>(GetOwner());
	if (IsValid(aiController))
	{
		UJSMAIDataAsset* aiDataAsset = Cast<UJSMAIDataAsset>(charDesc->CharacterDataAsset);

		if (IsValid(aiDataAsset))
		{
			aiDataAsset->LoadSynchronous();
			aiController->InitializeAIData(aiDataAsset);
		}

		TeamId = FGenericTeamId::GetTeamIdentifier(GetOwner());
	}

	if (IsValid(WeaponCtrlComponent))
	{
		WeaponCtrlComponent->InitializeWeaponComponent();
	}
}

void AAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (GetNetMode() == NM_Client)
	{
		return;
	}

	if (IsDie() && !bWaitDestroy)
	{
		bWaitDestroy = true;

		FTimerHandle DelayHandle;
		const TWeakObjectPtr<AAICharacter> WeakPtr(this);
		GetWorld()->GetTimerManager().SetTimer(DelayHandle, FTimerDelegate::CreateLambda([WeakPtr]()
		{
			if (WeakPtr.IsValid())
			{
				WeakPtr->Destroy();
			}
		}), DestroyDelayTime, false);
	}
}

void AAICharacter::FireWeapon(FHitResult& OutHitResult, FVector_NetQuantize& EndLoc, const float& LineDistance, const float& TraceRadius, const FVector_NetQuantize& Offset, const FVector_NetQuantize& MuzzleLoc, const FVector_NetQuantize& MuzzleEndLoc)
{
	UWorld* World = UJSMClientStatics::GetGameWorld();
	if (!IsValid(World))
	{
		return;
	}

	AAIBaseController* aiController = Cast<AAIBaseController>(GetOwner());
	if(!IsValid(aiController) || !IsValid(aiController->GetBlackboardComponent()))
	{
		return;
	}

	ACharacterBase* TargetActor = Cast<ACharacterBase>(aiController->GetBlackboardComponent()->GetValueAsObject(BlackboardKeys::TargetActor));
	if (!IsValid(TargetActor))
	{
		return;
	}

	const FVector_NetQuantize Start = MuzzleLoc;	
	const FVector_NetQuantize End = MuzzleEndLoc;
	EndLoc = End;

	FCollisionQueryParams CollisionParams;
	FCollisionResponseParams CollisionResponese;
	CollisionParams.AddIgnoredActor(this);
	CollisionParams.bReturnPhysicalMaterial = true;

	const FCollisionShape SphereCollisionShape = FCollisionShape::MakeSphere(SearchTraceRadius);
	const bool bHit = World->SweepSingleByChannel(OutHitResult, Start, EndLoc, FQuat::Identity,
		ECollisionChannel::ECC_Visibility, SphereCollisionShape, CollisionParams);

	if (OutHitResult.bBlockingHit)
	{
		EndLoc = OutHitResult.Location;
	}

	if (OutHitResult.bBlockingHit && IsValid(OutHitResult.GetActor()))
	{
		UE_LOG(LogTemp, Warning, TEXT("UWeaponCtrlComponent::CS_ApplyWeaponAbilityEffect_Implementation : %s"), *OutHitResult.GetActor()->GetName());
		if (OutHitResult.PhysMaterial.IsValid())
		{
			int PhysicsIndex = static_cast<int32>(OutHitResult.PhysMaterial->SurfaceType);
			UE_LOG(LogTemp, Warning, TEXT("OutHitResult.PhysMaterial PhysicsIndex : %d"), PhysicsIndex);
		}
	}

	UALSDebugComponent* DebugComponent = FindComponentByClass<UALSDebugComponent>();
	if (DebugComponent)
	{
		UALSDebugComponent::DrawDebugSphereTraceSingle(World,
			Start,
			EndLoc,
			SphereCollisionShape,
			EDrawDebugTrace::ForDuration,
			bHit,
			OutHitResult,
			FLinearColor::Red,
			FLinearColor::Green,
			5.0f);
	}
}

UJSMAIDataAsset* AAICharacter::GetAIDataAsset() const
{
	const FCharacterDesc* charDesc = GetCharacterDescription();
	return Cast<UJSMAIDataAsset>(charDesc->CharacterDataAsset);
}

void AAICharacter::SetPatrolPoints(TArray<TObjectPtr<class AActor>> InPatrolPoints)
{
	PatrolPoints = InPatrolPoints;
}

FVector_NetQuantize AAICharacter::NextPatrolLocation()
{
	if (PatrolPoints.Num() == 0)
	{
		return FVector_NetQuantize();
	}

	CurrentPatrolIdx = CurrentPatrolIdx + 1;
	if ((PatrolPoints.Num() <= CurrentPatrolIdx))
	{
		CurrentPatrolIdx = 0;
	}

	return PatrolPoints[CurrentPatrolIdx]->GetActorLocation();
}

void AAICharacter::RagdollToggle()
{
	Super::RagdollToggle();
}

void AAICharacter::RefershAICharacter()
{
	EALSRotationMode CurrentRotMode = GetRotationMode();
	if (IsDie())
	{
		SetOverlayState(EALSOverlayState::Default);
		ReplicatedRagdollStart();
	}
	else
	{
		SM_RefershAICharacter(CurrentRotMode);
	}
}

void AAICharacter::SM_RefershAICharacter_Implementation(const EALSRotationMode& InRotateMode)
{
	SetRotationMode(InRotateMode);
}

void AAICharacter::DieCharacter(ACharacterBase* killer)
{
	Super::DieCharacter(killer);
	AAIBaseController* AIBaseController = Cast<AAIBaseController>(GetController());
	if (IsValid(AIBaseController))
	{
		EAIState CurrentAIState = static_cast<EAIState>(AIBaseController->GetBlackboardComponent()->GetValueAsEnum(BlackboardKeys::AIStatus));
		if (CurrentAIState == EAIState::Dead)
		{
			return;
		}
		AIBaseController->GetBlackboardComponent()->SetValueAsEnum(BlackboardKeys::AIStatus, static_cast<uint8>(EAIState::Dead));
		UWeaponCtrlComponent* HitWeaponCtrlComp = GetWeaponCtrl();
		if (!IsValid(HitWeaponCtrlComp))
		{
			return;
		}
		HitWeaponCtrlComp->AIDie();
	}
}