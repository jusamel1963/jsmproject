// Copyright JSM Studio, Inc. All Rights Reserved.

#include "FrameWork/JSMGameInstance.h"
#include "Statics/JSMClientStatics.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "Data/Descriptions/JSMDataManagerSubsystem.h"
#include "Core/Managers/JSMEventManager.h"
#include "FindSessionsCallbackProxy.h"
#include "CreateSessionCallbackProxyAdvanced.h"
#include "AdvancedSessions/Classes/AdvancedSessionsLibrary.h"

UJSMGameInstance::UJSMGameInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UJSMGameInstance::Init()
{
	EventManager = NewObject<UJSMEventManager>(this, UJSMEventManager::StaticClass());
	EventManager->AddToRoot();
	Super::Init();
}

void UJSMGameInstance::Shutdown()
{
	Super::Shutdown();
	EventManager->RemoveFromRoot();
}

void UJSMGameInstance::OnStart()
{
	Super::OnStart();
}

UJSMGameInstance* UJSMGameInstance::GetGameInstance()
{
	UWorld* world = UJSMClientStatics::GetGameWorld();
	if (IsValid(world))
	{
		return UJSMClientStatics::GetGameInstance(world);
	}
	return nullptr;
}