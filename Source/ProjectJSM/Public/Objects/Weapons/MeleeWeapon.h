// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Weapons/Weapon.h"
#include "MeleeWeapon.generated.h"


UCLASS()
class PROJECTJSM_API AMeleeWeapon : public AWeapon
{
	GENERATED_BODY()
public:
	AMeleeWeapon();
};
