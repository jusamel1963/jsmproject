// Copyright JSM Studio, Inc. All Rights Reserved.

#include "Data/Descriptions/JSMDataManagerSubsystem.h"

UJSMDataManagerSubsystem::UJSMDataManagerSubsystem()
{
	DataCollectionPath = FSoftObjectPath(TEXT("/Game/Data/JSMDataCollection.JSMDataCollection"));
}

void UJSMDataManagerSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	LoadImpl();
}

void UJSMDataManagerSubsystem::LoadImpl()
{
	if (!DataCollectionPath.IsNull())
	{
		JSMDataCollection = Cast<UJSMDataCollection>(DataCollectionPath.TryLoad());
		if (!IsValid(JSMDataCollection))
		{
			return;
		}

		check(JSMDataCollection);
		JSMDataCollection->LoadSyncronous();
	}
}

void UJSMDataCollection::LoadSyncronous()
{
	LoadAsset(CharacterTable);
	LoadAsset(ItemTable);
	LoadAsset(WidgetTable);
	LoadAsset(CharacterAttributeSetTable);
}