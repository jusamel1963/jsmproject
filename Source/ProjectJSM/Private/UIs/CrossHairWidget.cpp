// Copyright JSM Studio, Inc. All Rights Reserved.


#include "UIs/CrossHairWidget.h"
#include "Components/Image.h"

void UCrossHairWidget::Update_CrossHair(UTexture2D* InCrossHair)
{
	if (!IsValid(Img_CrossHair))
	{
		return;
	}

	if (IsValid(InCrossHair))
	{
		Img_CrossHair->SetBrushFromTexture(InCrossHair);
	}
}
