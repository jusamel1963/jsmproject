// Copyright JSM Studio, Inc. All Rights Reserved.


#include "System/LevelDesign/WeaponSpawnerActor.h"
#include "Framework/JSMItemManagerSubsystem.h"
#include "Objects/Weapons/Weapon.h"
#include "Objects/Weapons/JSMWeaponInfo.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "Statics/JSMClientStatics.h"
#include "Data/DataAssets/ItemData/JSMWeaponDataAsset.h"

AWeaponSpawnerActor::AWeaponSpawnerActor()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AWeaponSpawnerActor::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		Execute();
	}
}

void AWeaponSpawnerActor::Execute()
{
	if (!HasAuthority())
	{
		return;
	}

	if (SpawnedActors.Num() > 0)
	{
		return;
	}

	if (bRespawnable)
	{
		SetRespawnTimer();
	}
	else
	{
		SpawnWeapon();
	}
}

void AWeaponSpawnerActor::SpawnWeapon()
{
	if (!HasAuthority())
	{
		return;
	}

	for (FWeaponSpawnData& WeaponSpawnData : SpawnData)
	{
		UJSMItemManagerSubsystem* ItemManager = JSMFL::GetGameInstanceSubsystem<UJSMItemManagerSubsystem>();
		if (IsValid(ItemManager))
		{
			UJSMWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMWeaponDataAsset>(WeaponSpawnData.WeaponKeyName);

			if (!IsValid(WeaponDataAsset) || WeaponDataAsset->WeaponBPClass.IsNull())
			{
				return;
			}
			UClass* bpClass = WeaponDataAsset->WeaponBPClass.LoadSynchronous();

			FTransform SpawnTransform = GetTransform();
			AWeapon* WeaponActor = ItemManager->SpawnWeaponActor(GetWorld(), bpClass, WeaponSpawnData.WeaponKeyName, SpawnTransform);
			if (IsValid(WeaponActor))
			{
				SpawnedActors.Add(WeaponActor);
				WeaponActor->OnDestroyed.AddDynamic(this, &AWeaponSpawnerActor::OnDestroyActor);
			}
		}
	}
}

void AWeaponSpawnerActor::SetRespawnTimer()
{
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	TimerManager.SetTimer(SpawnerTimerHandle, this, &AWeaponSpawnerActor::OnSpawnTimer, RespawnTimeInterval, false, RespawnTimeInterval);
}

void AWeaponSpawnerActor::OnSpawnTimer()
{
	SpawnWeapon();
}

void AWeaponSpawnerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWeaponSpawnerActor::OnDestroyActor(AActor* DestroyedActor)
{
	SpawnedActors.Remove(DestroyedActor);

	if (SpawnedActors.Num() == 0 && bRespawnable)
	{
		SetRespawnTimer();
	}
}