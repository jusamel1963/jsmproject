// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Character/ALSCharacter.h"
#include "AbilitySystemInterface.h"
#include "JSMCharacterEvent.h"
#include "GenericTeamAgentInterface.h"
#include "Data/Descriptions/CharacterDesc.h"
#include "State/JSMCharacterStateHandler.h"
#include "CharacterBase.generated.h"

UCLASS()
class PROJECTJSM_API ACharacterBase : public AALSCharacter, public IAbilitySystemInterface, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:
	ACharacterBase(const FObjectInitializer& ObjectInitializer);
	virtual void PostInitializeComponents() override;
	template<class AttributeSetType>
	AttributeSetType* GetAttributeSet() const
	{
		if (AttributeSet.IsValid() == false)
		{
			return nullptr;
		}

		return Cast<AttributeSetType>(AttributeSet);
	}
	virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	virtual FString GetDisplayName() const;
	FORCEINLINE const FString& GetCharacterDescKeyName() const { return CharacterDescKeyName; }
	const FCharacterDesc* GetCharacterDescription() const;
	bool IsLocalPlayerCharacter() const;
	virtual bool IsPlayerCharacter() const { return false; }
	virtual void PossessedBy(AController* NewController) override;
	virtual void Tick(float DeltaTime) override;

	virtual void SetupAbilitySystemForClient();
	FORCEINLINE class UWeaponCtrlComponent* GetWeaponCtrl() const { return WeaponCtrlComponent; }
	FORCEINLINE class UCoverSystemComponent* GetCoverSystemCtrl() const { return CoverSystemComponent; }
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "ALS|Movement System")
	UAnimMontage* GetFireAnimation();
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "ALS|Movement System")
	UAnimMontage* GetReloadAnimation();

	UAnimMontage* GetLootAnimation();

	virtual void MovementToNone() override;
	virtual void MovementToMantle() override;
	void UpdateSprintCalculateStamina(float DeltaTime);
	void UpdateHealthPainAnim();
	class UJSMCharacterDataAsset* GetCharacterDataAsset() const;
	virtual void FireWeapon(FHitResult& OutHitResult, FVector_NetQuantize& EndLoc, const float& LineDistance, const float& TraceRadius, const FVector_NetQuantize& Offset, const FVector_NetQuantize& MuzzleLoc, const FVector_NetQuantize& MuzzleEndLoc = FVector_NetQuantize());
	virtual void PredictProjectile(FVector_NetQuantize& LaunchVelocity);
	static ACharacterBase* InstantiateCharacter(const UObject* worldContextObj,
		const TSubclassOf<ACharacterBase>& charClass,
		const FTransform& spawnTransform,
		const FString& CharacterKeyName,
		const int32& Level = 1,
		ESpawnActorCollisionHandlingMethod SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

	virtual void DieCharacter(ACharacterBase* killer);
	bool IsDie() const;
	virtual FGenericTeamId GetGenericTeamId() const override;
	void AddImpulseCharacter(const FVector_NetQuantize& Impulse, const FVector_NetQuantize& Location, const FName& BoneName);
	virtual void RagdollToggle();
	void TakeDown();
	
	UFUNCTION(NetMulticast, Reliable)
	void SM_TakeDown();

	UFUNCTION(Client, Reliable)
	void SC_TakeDown();

	void SetCover_WorldDirection(FVector_NetQuantize& InDirection) { Cover_WorldDirection = InDirection; }

	void EnterVehicle(AActor* HitActor);
	void TriggerInteractive(AActor* HitActor);

	UFUNCTION()
	void ExitVehicle();

	UFUNCTION(NetMulticast, Reliable)
	void SM_SetCollisionProfileName(const FName& InName);

	UFUNCTION(Server, Reliable)
	void CS_SetMovementMode(const EMovementMode& InMoveMode);

	UFUNCTION(NetMulticast, Reliable)
	void SM_SetCollisionEnabled(const ECollisionEnabled::Type& InType);


protected: 
	virtual void BeginPlay() override;
	void CreateInitializeStates();
	virtual void SetupAbilitySystemForServer();
	virtual void InitializeCharacter(const FCharacterDesc* charDesc);
	virtual void PostInitializeCharacter();

	UFUNCTION()
	void OnRep_LoadCompleted();
	void SS_CharacterDataLoad();
	//virtual void OnGameplayTagEvent_Dead(const FGameplayTag CallbackTag, int32 NewCount);
public:
	//Add Ability System Component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "GASGameplayAbility")
	TObjectPtr<class UJSMAbilitySystemComponent> AbilitySystemComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<class UWeaponCtrlComponent> WeaponCtrlComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<class UCoverSystemComponent> CoverSystemComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<class UArrowComponent> RightTraceComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<class UArrowComponent> LeftTraceComponent;

	static FName AttributeSetName;
	static FName AbilitySystemComponentName;
	static FName WeaponCtrlComponentName;
	static FName CoverSystemComponentName;
	static FName VisualMeshesComponentName;
	static FName BodyMeshComponentName;
	static FName HipWeaponSocketComponentName;
	static FName HipWeaponComponentName;
	static FName BackWeaponSocketComponentName;
	static FName BackWeaponComponentName;
	static FName WeaponScopeComponentName;
	static FName MeleeWeaponSocketComponentName;
	static FName MeleeWeaponComponentName;
	static FName MeleeWeaponHeldSocketComponentName;
	static FName MeleeWeaponHeldComponentName;
	static FName ThrowableWeaponSocketComponentName;
	static FName ThrowableWeaponComponentName;
	static FName DamageCtrlComponentName;
	static FName MeshBodySocketName;
	static FName ThrowableSocketName;
	static FName TakeDownAttackSocketName;
	static FName RightTraceComponentName;
	static FName LeftTraceComponentName;

	FJSMCharacterEvent Events;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<USceneComponent> HipWeaponSocket;

	UPROPERTY(Transient, EditAnywhere, BlueprintReadWrite)
	TObjectPtr<class USkeletalMeshComponent> HipWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<USceneComponent> BackWeaponSocket;

	UPROPERTY(Transient, EditAnywhere, BlueprintReadWrite)
	TObjectPtr<class USkeletalMeshComponent> BackWeapon;

	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly)
	TObjectPtr<class UChildActorComponent> WeaponScopeComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<class AWeapon> WeaponScopeClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<USceneComponent> MeleeWeaponSocket;

	UPROPERTY(Transient, EditAnywhere, BlueprintReadWrite)
	TObjectPtr<class USkeletalMeshComponent> MeleeWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<USceneComponent> MeleeWeaponHeldSocket;

	UPROPERTY(Transient, EditAnywhere, BlueprintReadWrite)
	TObjectPtr<class USkeletalMeshComponent> MeleeWeaponHeld;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<USceneComponent> ThrowableWeaponHeldSocket;

	UPROPERTY(Transient, EditAnywhere, BlueprintReadWrite)
	TObjectPtr<class UStaticMeshComponent> ThrowableWeaponHeld;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UJSMDamageCtrlComponent* DamageCtrlComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<USceneComponent> TakeDownAttackSocket;

	UPROPERTY(BlueprintReadOnly, Replicated)
	bool bRiding = false;

	float OriginZVelocity = 420.f;

protected:
	UPROPERTY()
	TWeakObjectPtr<class UAttributeSet> AttributeSet;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Replicated, Category = "Character Info")
	FString CharacterDescKeyName;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, ReplicatedUsing = OnRep_LoadCompleted, Category = "Character Info")
	bool bLoadCompleted = false;

	const float SearchTraceDistance = 500.0f;
	const float InteractionTraceDistance = 400.0f;
	const float SearchTraceRadius = 10.0f;
	const float TakeDownDistance = 400.0f;

	const uint8 NoTeamID = 255;
	FGenericTeamId TeamId = NoTeamID;

	UPROPERTY(VisibleAnywhere)
	TObjectPtr<class USplineComponent> ThrowableArc;

	UPROPERTY(Transient)
	TArray<TObjectPtr<class USplineMeshComponent>> ThrowableMeshArcList;

	TWeakObjectPtr<class AActor> VehicleActor;

private:
	FJSMCharacterInitializeStateHandler InitializeStateHandler;

	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<USkeletalMeshComponent> BodyMesh;

	const float DecrementStaminaSpeed = 10.0f;

	FVector_NetQuantize Cover_WorldDirection;
};
