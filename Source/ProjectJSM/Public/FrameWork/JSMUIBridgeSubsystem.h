// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "UIs/JSMUIData.h"
#include "JSMUIBridgeSubsystem.generated.h"

UCLASS()
class PROJECTJSM_API UJSMUIBridgeSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	UJSMUIBridgeSubsystem();
	void InternalCharacterSpawned(ACharacterBase* Character);
	void InteranlCharacterDestroyed(class ACharacterBase* Character);

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;

	template<typename T>
	void SetUIData_CharacterData(int32 UID, const TCHAR* InDataKey, const T& Data)
	{
		FString DataKey = FString::Printf(TEXT("UIData.IngameData.CharacterData[%d].%s"), UID, InDataKey);
		SetUIData_Game(DataKey, Data);

		if (UID == LocalCharacterUID)
		{
			FString LocalCharacterDataKey = FString::Printf(TEXT("UIData.IngameData.UserData.LocalCharacterData.%s"), InDataKey);
			SetUIData_Game(LocalCharacterDataKey, Data);
		}
	}

	template<typename T>
	bool GetUIData_CharacterData(int32 UID, const TCHAR* InDataKey, T& OutData)
	{
		FString DataKey = FString::Printf(TEXT("UIData.IngameData.CharacterData[%d].%s"), UID, InDataKey);
		return GetUIData_Game(DataKey, OutData);
	}

	bool SetUIData_Game(const FString& DataKey, bool Value);
	bool SetUIData_Game(const FString& DataKey, int32 Value);
	bool SetUIData_Game(const FString& DataKey, float Value);
	bool SetUIData_Game(const FString& DataKey, const FString& Value);
	bool SetUIData_Game(const FString& DataKey, const FText& Value);

	bool SetUIData(const FString& DataKey, bool Value, bool IsGameSide);
	bool SetUIData(const FString& DataKey, int32 Value, bool IsGameSide);
	bool SetUIData(const FString& DataKey, float Value, bool IsGameSide);
	bool SetUIData(const FString& DataKey, const FString& Value, bool IsGameSide);
	bool SetUIData(const FString& DataKey, const FText& Value, bool IsGameSide);

	bool GetUIData_Game(const FString& DataKey, bool& OutValue) const;
	bool GetUIData_Game(const FString& DataKey, int32& OutValue) const;
	bool GetUIData_Game(const FString& DataKey, float& OutValue) const;
	bool GetUIData_Game(const FString& DataKey, FString& OutValue) const;
	bool GetUIData_Game(const FString& DataKey, FText& OutValue) const;

	bool GetUIData(const FString& DataKey, bool& OutValue, bool IsGameSide) const;
	bool GetUIData(const FString& DataKey, int32& OutValue, bool IsGameSide) const;
	bool GetUIData(const FString& DataKey, float& OutValue, bool IsGameSide) const;
	bool GetUIData(const FString& DataKey, FString& OutValue, bool IsGameSide) const;
	bool GetUIData(const FString& DataKey, FText& OutValue, bool IsGameSide) const;

	const void* GetUIDataPtr() const;
	const void* GetUIData_UIPtr() const;
	const void* GetUIData_UIPrevPtr() const;

	static FProperty* GetUIDataProperty();
	static FProperty* GetUIData_UIProperty();
	static FProperty* GetUIData_UIPrevProperty();
	static bool IsUIDataReady();

	void InitializeUIData();
	void InitializeTestUIData();
protected:
	static UClass* GetDynamicClass();
	static void FindMemberProperty(const void* ValuePtr, FProperty* Prop, TArray<FString>& PropertyNames, int32 CurrDepth, OUT FProperty*& FoundProperty, OUT const void*& FoundValuePtr);
	static void SplitCollectionName(const FString& InArrayName, OUT FString& OutCollectionName, OUT int32& OutIndex, OUT FString& OutMapKey);

private:
	void InitilaizeLocalCharacterData(class ACharacterBase* Character);
	void BindGlobalEvent(class UJSMGameInstance* GameInst);
	void UnbindGlobalEvent();

	void OnCharacterPhysicalDamaged(UObject* Invoker, class ACharacterBase* Victim, class ACharacterBase* Attacker, float DamageAmount);
	void OnCharacterSpawned(UObject* Invoker, class ACharacterBase* Character);

	UFUNCTION()
	void OnCharacterEndPlay(AActor* Actor, EEndPlayReason::Type EndPlayReason);
	void ResetUIData();
	void ResetUIData_Outgame();
	void ResetUIData_Ingame();
private:
	int32 LocalCharacterUID;

	UPROPERTY(Transient)
	FUIData UIData_UIPrev;

	UPROPERTY(Transient)
	FUIData UIData;

	UPROPERTY(Transient)
	FUIData UIData_UI;

	UPROPERTY(Transient)
	UClass* CachedChildSubsystemClass;
};
