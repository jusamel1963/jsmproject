// Copyright JSM Studio, Inc. All Rights Reserved.

#include "Objects/Character/Component/JSMInventoryComponent.h"
#include "Objects/Character/AICharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "FrameWork/JSMPlayerController.h"
#include "Data/Common/GameEnum.h"

UJSMInventoryComponent::UJSMInventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
	bWantsInitializeComponent = true;
}

void UJSMInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		ProgressTimelineBind();
	}
}

void UJSMInventoryComponent::InitializeComponent()
{
	Super::InitializeComponent();
	OwnerChar = Cast<ACharacterBase>(GetOwner());
}

void UJSMInventoryComponent::ProgressTimelineBind()
{
	if (IsValid(ProgressCurve))
	{
		FOnTimelineFloat CurveCallback;
		FOnTimelineEvent TimelineFinishedCallback;

		CurveCallback.BindUFunction(this, FName("ProgressUpdate"));
		TimelineFinishedCallback.BindUFunction(this, FName("FinishProgress"));

		ProgressTimeline.AddInterpFloat(ProgressCurve, CurveCallback);
		ProgressTimeline.SetTimelineFinishedFunc(TimelineFinishedCallback);
	}
}

void UJSMInventoryComponent::ProgressUpdate(const float& Value)
{
	UE_LOG(LogTemp, Warning, TEXT("UJSMInventoryComponent::ProgressUpdate : %lf"), Value);
	SM_SetStance(EALSStance::Crouching);
	SC_UpdateProgressBar(Value);
}

void UJSMInventoryComponent::FinishProgress()
{
	SC_SetVisibleProgressBar(false);
}

void UJSMInventoryComponent::SC_SetVisibleProgressBar_Implementation(bool bVisible, const FText& LoadingName)
{
	if (!IsValid(OwnerChar) || !OwnerChar->IsLocallyControlled())
	{
		return;
	}

	AJSMPlayerController* OwnController = Cast<AJSMPlayerController>(OwnerChar->GetController());
	if (IsValid(OwnController))
	{
		OwnController->SetVisibleProgressBar(bVisible, LoadingName);
	}
}

void UJSMInventoryComponent::SC_UpdateProgressBar_Implementation(const float& Value)
{
	if (!IsValid(OwnerChar) || !OwnerChar->IsLocallyControlled())
	{
		return;
	}

	AJSMPlayerController* OwnController = Cast<AJSMPlayerController>(OwnerChar->GetController());
	if (IsValid(OwnController))
	{
		OwnController->ProgressHudUpdate(Value);
	}
}

void UJSMInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	ProgressTimeline.TickTimeline(DeltaTime);
}

TArray<FString> UJSMInventoryComponent::GetWeaponItemKeys() const
{
	TArray<FString> WeaponKeys;
	return WeaponKeys;
}

void UJSMInventoryComponent::LootItems(class AAICharacter* InLootAI)
{
	if (!IsValid(OwnerChar) || !IsValid(OwnerChar->GetCharacterMovement()) || !IsValid(InLootAI) || !InLootAI->IsDie())
	{
		return;
	}

	UWeaponCtrlComponent* WeaponCtrlComp = OwnerChar->GetWeaponCtrl();
	if(IsValid(WeaponCtrlComp))
	{
		WeaponCtrlComp->SM_EquipWeapon(nullptr, nullptr, EWeaponAttachType::None, true);
		WeaponCtrlComp->EquipWeaponToAttachSocket();
	}

	SM_SetStance(EALSStance::Crouching);
	SM_PlayMontage(OwnerChar->GetLootAnimation(), 1.0f);
	OwnerChar->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_None);

	FString InProgressName = TEXT("Looting");
	SC_SetVisibleProgressBar(true, FText::FromString(InProgressName));
	ProgressTimeline.PlayFromStart();
}

void UJSMInventoryComponent::SM_PlayMontage_Implementation(class UAnimMontage* Montage, const float& PlayRate)
{
	if (IsValid(OwnerChar) || OwnerChar->GetMesh()->GetAnimInstance())
	{
		OwnerChar->GetMesh()->GetAnimInstance()->Montage_Play(Montage, PlayRate);
	}
}

void UJSMInventoryComponent::SM_SetStance_Implementation(EALSStance InStance)
{
	if (IsValid(OwnerChar) && (OwnerChar->GetStance() != InStance))
	{
		OwnerChar->SetStance(InStance);
	}
}

void UJSMInventoryComponent::CS_EndLootItems_Implementation()
{
	if (!IsValid(OwnerChar) || !IsValid(OwnerChar->GetCharacterMovement()))
	{
		return;
	}

	SM_EndLootItems();
	OwnerChar->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
}

void UJSMInventoryComponent::SM_EndLootItems_Implementation()
{
	UWeaponCtrlComponent* WeaponCtrlComp = OwnerChar->GetWeaponCtrl();
	if (IsValid(WeaponCtrlComp))
	{
		WeaponCtrlComp->EquipWeaponFromAttachSocket();
	}

	if (IsValid(OwnerChar))
	{
		OwnerChar->SetStance(EALSStance::Standing);
	}
}

void UJSMInventoryComponent::RefreshCharacterWeaponCtrlComponent()
{

}
