#pragma once

#include "CoreMinimal.h"
#include "Runtime/Core/Public/UObject/NameTypes.h"
#include "Runtime/Core/Public/Containers//UnrealString.h"

namespace BlackboardKeys
{
	TCHAR const* const MoveToLocation = TEXT("MoveToLocation");
	TCHAR const* const AIStatus = TEXT("AIStatus");
	TCHAR const* const TargetActor = TEXT("TargetActor");
}

