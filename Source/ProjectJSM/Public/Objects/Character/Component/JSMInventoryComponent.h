// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/TimeLineComponent.h"
#include "JSMInventoryComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTJSM_API UJSMInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UJSMInventoryComponent();
	void LootItems(class AAICharacter* InLootAI);
	void EndLootItems();
	virtual void InitializeComponent() override;
	UFUNCTION(NetMulticast, Reliable)
	void SM_PlayMontage(class UAnimMontage* Montage, const float& PlayRate);
	UFUNCTION(Server, Reliable)
	void CS_EndLootItems();
	UFUNCTION(NetMulticast, Reliable)
	void SM_EndLootItems();
	void ProgressTimelineBind();
	UFUNCTION()
	void ProgressUpdate(const float& Value);
	UFUNCTION()
	void FinishProgress();

	UFUNCTION(Client, Reliable)
	void SC_UpdateProgressBar(const float& Value);

	UFUNCTION(Client, Reliable)
	void SC_SetVisibleProgressBar(bool bVisible, const FText& LoadingName = FText());
	void RefreshCharacterWeaponCtrlComponent();

	UFUNCTION(NetMulticast, Reliable)
	void SM_SetStance(EALSStance InStance);
protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	TArray<FString> GetWeaponItemKeys() const;

private:
	UPROPERTY(Transient)
	TObjectPtr<class ACharacterBase> OwnerChar;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UCurveFloat> ProgressCurve;
	FTimeline ProgressTimeline;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Timeline", meta = (AllowPrivateAccess = "true"))
	float ProgressTimelineLength = 2.0f;
};
