// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TestActor.generated.h"

UCLASS()
class PROJECTJSM_API ATestActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATestActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


protected:
	UFUNCTION()
	void OnRep_ItemCount();

	UPROPERTY(ReplicatedUsing = "OnRep_ItemCount")
	int32 RepItemCount;

public:
	UPROPERTY(VisibleAnywhere, Transient)
	class UStaticMeshComponent* ItemStaticMeshComponent;

	UPROPERTY(EditAnyWhere) TSoftObjectPtr<UStaticMesh> TestActor2;
	TSet<FString> TsE;
};
