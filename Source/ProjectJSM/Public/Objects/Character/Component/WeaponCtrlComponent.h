// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "WeaponCtrlComponent.generated.h"

enum class EALSOverlayState : uint8;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTJSM_API UWeaponCtrlComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UWeaponCtrlComponent();
	virtual void InitializeComponent() override;

	void SS_PickupWeapon(const FName& InWeaponKey, const int32& WeaponMagAmmo = 1, const int32& WeaponBagAmmo = 1, const bool& bDefault = true);
	UFUNCTION(Client, Reliable)
	void SC_EquipWeaponAnim(const EALSOverlayState& WeaponCategory);
	UFUNCTION(NetMulticast, Reliable)
	void SM_EquipWeapon(USkeletalMesh* NewMesh, UStaticMesh* StaticNewMesh, EWeaponAttachType InAttachType, bool bClear = false);

	UFUNCTION(NetMulticast, Reliable)
	void SM_AttachHipWeapon(USkeletalMesh* NewMesh);

	UFUNCTION(NetMulticast, Reliable)
	void SM_AttachBackWeapon(USkeletalMesh* NewMesh);

	UFUNCTION(NetMulticast, Reliable)
	void SM_AttachMeleeBackWeapon(USkeletalMesh* NewMesh);

	UFUNCTION(Server, Reliable)
	void CS_ExchangeWeaponUnDraw();

	UFUNCTION(Server, Reliable)
	void CS_ExchangeWeaponDraw();

	void ExchangeWeapon(const bool& bUp);
	void SS_UpdateAttachWeaponMesh(USkeletalMesh* NewMesh, const EWeaponAttachType& InType);
	void SS_DetachWeaponAbility(const FName& InCurrWeaponKey);
	void SS_AttachWeaponAbility(const FName& InCurrWeaponKey);

	UFUNCTION(Server, Reliable)
	void CS_FireWeapon(const FVector_NetQuantize& MuzzleEnd = FVector_NetQuantize());
	void SS_FireWeapon(const FVector_NetQuantize& MuzzleEnd = FVector_NetQuantize());

	void ThrowablePoseWeapon();
	void ThrowableWeapon();
	void SS_ThrowableWeapon(const FVector_NetQuantize& InLaunchVelocity);
	UFUNCTION(Client, Reliable)
	void SC_ThrowableWeapon();
	UFUNCTION(Server, Reliable)
	void CS_ThrowableWeapon(const FVector_NetQuantize& InLaunchVelocity);

	UFUNCTION(Client, Reliable)
	void SC_FireWeapon(const float& InTraceDistance, const float& InTraceRadius, const uint32& AmmoCount, const float& FireSpread, const FVector_NetQuantize& MuzzleLoc, const FVector_NetQuantize& MuzzleEnd = FVector_NetQuantize());
	void FireWeapon(const FVector_NetQuantize& MuzzleEnd = FVector_NetQuantize());
	UFUNCTION(Server, Reliable)
	void CS_ApplyWeaponAbilityEffect(const FHitResult& OutHitResult, const FVector_NetQuantize& EndLoc);

	void ApplyWeaponAbilityEffect(class AWeapon* InstigatorWeapon, class ACharacterBase* HitActor, const FVector_NetQuantize& InImpulseValue, const FVector_NetQuantize& InLocation, const FName& InBoneName);
	void ApplyWeaponAbilityEffect(class ACharacterBase* HitActor, const FVector_NetQuantize& InImpulseValue, const FVector_NetQuantize& InLocation, const FName& InBoneName);

	class AWeapon* GetCurrentWeapon();
	AWeapon* GetHolsterWeapon();
	FORCEINLINE bool IsGrabWeapon() { return RepCurrWeaponIdx >= 0 && RepCurrWeaponIdx < MyWeapons.Num(); }

	UFUNCTION(Client, Reliable)
	void SC_WeaponRecoil(const float& Value);

	FORCEINLINE int32 GetRepCurrWeaponIdx() const { return RepCurrWeaponIdx; }
	FORCEINLINE FName GetRepCurrWeaponKey() const { return RepCurrWeaponKey; }

	void ReloadWeapon();
	UFUNCTION(Server, Reliable)
	void CS_ReloadWeapon();

	void WeaponUIInfoUpdate();

	void WeaponHudUpdate();

	UFUNCTION(Client, Reliable)
	void SC_WeaponHudUpdate(UTexture2D* InPrimaryWeapon, UTexture2D* InHolsterWeapon, const FText& InMagAmmo, const FText& InMagCapacity, UTexture2D* CrossHair, UTexture2D* WeaponScope);

	void WeaponMenuUpdate();
	UFUNCTION(Client, Reliable)
	void SC_WeaponMenuUpdate(UTexture2D* InHipWeapon, UTexture2D* InBackWeapon, UTexture2D* InMeleeWeapon, UTexture2D* InThrowableWeapon,
		const FText& InHipWeaponAmmo, const FText& InBackWeaponAmmo);

	UFUNCTION(NetMulticast, Reliable)
	void SM_SpawnParticle(class UParticleSystem* Particle, const FVector_NetQuantize& HitLoc);

	UFUNCTION(NetMulticast, Reliable)
	void SM_SpawnDecal(class UMaterialInterface* Material, const FHitResult& HitResult, const FVector_NetQuantize& InScale);

	UFUNCTION(NetMulticast, Reliable)
	void SM_EquipWeaponAnimation(class UAnimSequence* Animation);

	UFUNCTION(Server, Reliable)
	void CS_CusomAimAction(bool bValue);
	void SS_SetWeaponScope(bool bValue);
	void SS_CancelThrowable(bool bValue);

	UFUNCTION(Client, Reliable)
	void SC_WeaponScope(bool bValue);

	void SetWeaponScopeFOV(const float& Value);

	void SS_DropWeapon(const int32& DropInex, bool bDestroy = false);
	void SS_DestroyEquipWeapon();

	UFUNCTION(Server, Reliable)
	void CS_HolsterAndPickWeapon(bool bFlag);
	void MovementToNone();
	void MovementToMantle();

	void PlayMontageDrawWeapon();
	void PlayMontageUnDrawWeapon();

	void MeleeWeaponAttack();
	UFUNCTION()
	void AttackAction(const FInputActionValue& Value);

	UFUNCTION()
	bool EquipWeaponAmmo(class UJSMWeaponAmmoDataAsset* InDataAsset);

	void RefreshCharacterWeaponCtrlComponent();
	void InitializeWeaponComponent();

	void AIDie();
	void EquipWeaponToAttachSocket();
	void EquipWeaponFromAttachSocket();

	UFUNCTION(Server, Reliable)
	void CS_FireAIWeapon(const FVector_NetQuantize& MuzzleEnd);
	void SetWeaponAttach(int InWeaponIdx);

	UFUNCTION(Server, Reliable)
	void CS_SetWeaponAttach(int InWeaponIdx);

	UFUNCTION(Client, Reliable)
	void SC_SetWeaponAttach(const FName InWeaponKey);

	UFUNCTION(NetMulticast, Reliable)
	void SM_StopMontage(class UAnimMontage* Montage);

	UFUNCTION(Client, Reliable)
	void SC_SetCustomTimeDilation(const float& InTime);

	class ACharacterBase* GetOwnChar() { return OwnerChar; }
protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	static FName FireMuzzleSocketName;
	static FName ShellEjectSocketName;

private:
	UPROPERTY(Transient)
	TObjectPtr<class ACharacterBase> OwnerChar;
		
	UPROPERTY(Transient)
	TArray<TObjectPtr<class AWeapon>> MyWeapons;

	UPROPERTY(VisibleInstanceOnly, Category = "Weapon Info", Replicated, DisplayName = "Current Weapon Index")
	int32 RepCurrWeaponIdx;

	UPROPERTY(VisibleInstanceOnly, Category = "Weapon Info", Replicated, DisplayName = "Current Weapon Key")
	FName RepCurrWeaponKey;

	UPROPERTY(VisibleInstanceOnly, Category = "Weapon Info", Replicated, DisplayName = "Current Weapon Index")
	int32 RepNewWeaponIdx;

	const float DrawRifleRateScale = 3.0f;
	const FName FireNoiseName = TEXT("FireNoise");

	bool bReloadWeapon = false;

	const float DestroyDelayTime = 10.0f;
};
