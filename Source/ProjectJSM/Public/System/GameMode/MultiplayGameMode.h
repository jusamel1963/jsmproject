// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "System/GameMode/JSMGameModeBase.h"
#include "MultiplayGameMode.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API AMultiplayGameMode : public AJSMGameModeBase
{
	GENERATED_BODY()

public:
	AMultiplayGameMode();

	virtual void BeginPlay() override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual APlayerController* Login(UPlayer* NewPlayer, ENetRole InRemoteRole, const FString& Portal, const FString& Options, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;
	virtual APawn* SpawnDefaultPawnAtTransform_Implementation(AController* NewPlayer, const FTransform& SpawnTransform) override;
	void AddAICharacter(class AAICharacter* InRefAI);
	void RefershAICharacter();

private:
	FString	_charType;
	TArray<TWeakObjectPtr<class AAICharacter>> AIList;
};
