// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once
#include "CoreMinimal.h"

#include "Engine/DataTable.h"
#include "ItemDesc.generated.h"


USTRUCT(BlueprintType)
struct PROJECTJSM_API FItemDesc : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item Description")
	FString KeyName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item Description")
	TObjectPtr<class UJSMItemDataAsset> ItemDataAsset;
};