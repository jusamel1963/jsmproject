// Copyright JSM Studio, Inc. All Rights Reserved.


#include "System/AI/BehaviorTree/Task/BTTask_GetPatrolPoint.h"
#include "System/AI/BlackboardKeys.h"
#include "FrameWork/AIBaseController.h"
#include "Objects/Character/AICharacter.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "Data/DataAssets/CharacterData/JSMAIDataAsset.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"


UBTTask_GetPatrolPoint::UBTTask_GetPatrolPoint()
{
	BlackboardKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_GetPatrolPoint, BlackboardKey));
}

EBTNodeResult::Type UBTTask_GetPatrolPoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	check(IsValid(BlackboardComponent));

	AAIBaseController* AIController = Cast<AAIBaseController>(OwnerComp.GetAIOwner());
	if (!ensure(IsValid(AIController)))
	{
		return EBTNodeResult::Failed;
	}

	AAICharacter* AICharacter = Cast<AAICharacter>(AIController->GetCharacter());
	if (!ensure(IsValid(AICharacter)))
	{
		return EBTNodeResult::Failed;
	}

	UAbilitySystemComponent* AbilitySytemComponent = AICharacter->GetAbilitySystemComponent();
	if (!ensure(IsValid(AbilitySytemComponent)))
	{
		return EBTNodeResult::Failed;
	}

	UJSMAIDataAsset* AIDataAsset = AICharacter->GetAIDataAsset();
	if (!ensure(IsValid(AIDataAsset)))
	{
		return EBTNodeResult::Failed;
	}

	AIController->GetBlackboardComponent()->SetValueAsVector(BlackboardKey.SelectedKeyName, AICharacter->NextPatrolLocation());
	return EBTNodeResult::Succeeded;
}
