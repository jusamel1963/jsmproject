// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Data/Common/GameEnum.h"
#include "AIBaseController.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API AAIBaseController : public AAIController
{
	GENERATED_BODY()

public:
	AAIBaseController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	void InitializeAIData(const class UJSMAIDataAsset* aiDataAsset);

protected:
	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* const ParamPawn) override;
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(EditInstanceOnly, Transient, BlueprintReadWrite, Category = "AI")
	TObjectPtr<class UBehaviorTreeComponent> BehaviorTreeComponent;

	UPROPERTY(EditInstanceOnly, Transient, BlueprintReadWrite, Category = "AI")
	TObjectPtr<class UBehaviorTree> Btree;
};
