// Copyright JSM Studio, Inc. All Rights Reserved.


#include "UIs/WeaponScopeHudWidget.h"
#include "Components/Image.h"

void UWeaponScopeHudWidget::Update_WeaponScope(UTexture2D* InWeaponScope)
{
	if (!IsValid(Img_WeaponScope))
	{
		return;
	}

	if (IsValid(InWeaponScope))
	{
		Img_WeaponScope->SetBrushFromTexture(InWeaponScope);
	}
}
