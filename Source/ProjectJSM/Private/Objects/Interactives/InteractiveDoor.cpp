// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Interactives/InteractiveDoor.h"

AInteractiveDoor::AInteractiveDoor()
{

}

void AInteractiveDoor::TriggerInteractive()
{
	SetActorTickEnabled(true);
	DoorRotYawTimeline.PlayFromStart();
}

void AInteractiveDoor::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		RotateTimelineBind();
		OriginRotation = RootComponent->GetRelativeRotation();
	}
}

void AInteractiveDoor::RotateTimelineBind()
{
	if (IsValid(DoorRotYawCurve))
	{
		FOnTimelineFloat CurveCallback;
		FOnTimelineEvent TimelineFinishedCallback;

		CurveCallback.BindUFunction(this, FName("DoorRotUpdate"));
		TimelineFinishedCallback.BindUFunction(this, FName("FinishRot"));

		DoorRotYawTimeline.AddInterpFloat(DoorRotYawCurve, CurveCallback);
		DoorRotYawTimeline.SetTimelineFinishedFunc(TimelineFinishedCallback);
	}
}

void AInteractiveDoor::DoorRotUpdate(const float& Value)
{
	UE_LOG(LogTemp, Warning, TEXT("UJSMInventoryComponent::DoorRotUpdate : %lf"), Value);
	
	FRotator NewRot = OriginRotation;

	if (bOpen)
	{
		NewRot.Yaw -= Value;
	}
	else
	{
		NewRot.Yaw += Value;
	}
	SM_SetActorRelativeRotation(NewRot);
}

void AInteractiveDoor::FinishRot()
{
	SetActorTickEnabled(false);
	OriginRotation = RootComponent->GetRelativeRotation();
	bOpen = !bOpen;
}

void AInteractiveDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	DoorRotYawTimeline.TickTimeline(DeltaTime);
}

void AInteractiveDoor::SM_SetActorRelativeRotation_Implementation(const FRotator& NewRot)
{
	SetActorRelativeRotation(NewRot);
}