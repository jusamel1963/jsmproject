// Copyright JSM Studio, Inc. All Rights Reserved.


#include "UIs/CharacterDataInfoWidget.h"
#include "Components/ProgressBar.h"
#include "Components/Image.h"
#include "Core/Managers/JSMEventManager.h"
#include "FrameWork/JSMGameInstance.h"
#include "FrameWork/JSMUIBridgeSubsystem.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"

void UCharacterDataInfoWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UCharacterDataInfoWidget::NativeTick(const FGeometry& myGeometry, float inDeltaTime)
{
	Super::NativeTick(myGeometry, inDeltaTime);
	UpdateCharacterDataInfo(inDeltaTime);
}

void UCharacterDataInfoWidget::UpdateCharacterDataInfo(const float& inDeltaTime)
{
	UJSMUIBridgeSubsystem* BridgeSubsystem = JSMFL::GetGameInstanceSubsystem<UJSMUIBridgeSubsystem>();
	if (IsValid(BridgeSubsystem))
	{
		float CurrHealth, MaxHealth;
		float CurrStamina, MaxStamina;

		BridgeSubsystem->GetUIData_CharacterData(OwnID, TEXT("CurrHealth"), CurrHealth);
		BridgeSubsystem->GetUIData_CharacterData(OwnID, TEXT("MaxHealth"), MaxHealth);
		BridgeSubsystem->GetUIData_CharacterData(OwnID, TEXT("CurrStamina"), CurrStamina);
		BridgeSubsystem->GetUIData_CharacterData(OwnID, TEXT("MaxStamina"), MaxStamina);

		if ((MaxHealth <= 0) || (CurrHealth < 0) || (MaxStamina <= 0) || (CurrStamina < 0))
		{
			return;
		}

		UpdateValue(PreviousHp, CurrHealth, MaxHealth, inDeltaTime);
		Pb_Health->SetPercent(PreviousHp / MaxHealth);
		Pb_Health->SetFillColorAndOpacity(FMath::Lerp(MinHpColor, MaxHpColor, Pb_Health->GetPercent()));

		UpdateValue(PreviousStamina, CurrStamina, MaxStamina, inDeltaTime);
		Pb_Stamina->SetPercent(PreviousStamina / MaxStamina);
	}
}

void UCharacterDataInfoWidget::UpdateValue(float& InValue, const float& CompareValue, const float& MaxValue, const float& inDeltaTime)
{
	if (InValue == -1)
	{
		InValue = CompareValue;
	}
	else if (InValue > CompareValue)
	{
		InValue -= (inDeltaTime * DecrementSpeed);
		InValue = FMath::Clamp(InValue, CompareValue, MaxValue);
	}
	else
	{
		InValue += (inDeltaTime * DecrementSpeed);
		InValue = FMath::Clamp(InValue, 0, CompareValue);
	}
}