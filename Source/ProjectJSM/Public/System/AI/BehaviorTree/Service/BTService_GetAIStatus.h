// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Services/BTService_BlackboardBase.h"
#include "BTService_GetAIStatus.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UBTService_GetAIStatus : public UBTService_BlackboardBase
{
	GENERATED_BODY()
	
};
