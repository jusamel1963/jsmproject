// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Character/CharacterBase.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "AbilitySystem/AttributeSet/JSMAttributeSet.h"
#include "FrameWork/JSMGameInstance.h"
#include "Core/Managers/JSMEventManager.h"
#include "Statics/JSMClientStatics.h"
#include "Data/Descriptions/JSMDataManagerSubsystem.h"
#include "Net/Core/PushModel/PushModel.h"
#include "Net/UnrealNetwork.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "Statics/AssetLoader.h"
#include "Components/ChildActorComponent.h"
#include "Objects/Weapons/Weapon.h"
#include "FrameWork/JSMUIBridgeSubsystem.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "Data/DataAssets/CharacterData/JSMCharacterDataAsset.h"
#include "Objects/Character/Component/JSMDamageCtrlComponent.h"
#include "AbilitySystem/JSMAbilitySystemImpl.h"
#include "Components/SplineMeshComponent.h"
#include "Components/SplineComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Objects/Character/Component/CoverSystemComponent.h"
#include "Components/ArrowComponent.h"
#include "Objects/Vehicles/Helicopter/HelicopterBase.h"
#include "Objects/Interactives/InteractiveInterface.h"

FName ACharacterBase::AbilitySystemComponentName(TEXT("AbilitySystemComponent"));
FName ACharacterBase::AttributeSetName(TEXT("AttributeSet"));
FName ACharacterBase::WeaponCtrlComponentName(TEXT("WeaponCtrlComponent"));

FName ACharacterBase::VisualMeshesComponentName(TEXT("VisualMeshes"));
FName ACharacterBase::BodyMeshComponentName(TEXT("BodyMesh"));

FName ACharacterBase::HipWeaponSocketComponentName(TEXT("HipWeaponSocket"));
FName ACharacterBase::HipWeaponComponentName(TEXT("HipWeapon"));

FName ACharacterBase::BackWeaponSocketComponentName(TEXT("BackWeaponSocket"));
FName ACharacterBase::BackWeaponComponentName(TEXT("BackWeapon"));

FName ACharacterBase::WeaponScopeComponentName(TEXT("WeaponScopeComponent"));
FName ACharacterBase::MeleeWeaponSocketComponentName(TEXT("MeleeWeaponSocket"));
FName ACharacterBase::MeleeWeaponComponentName(TEXT("MeleeWeapon"));

FName ACharacterBase::MeleeWeaponHeldSocketComponentName(TEXT("MeleeWeaponHeldSocket"));
FName ACharacterBase::MeleeWeaponHeldComponentName(TEXT("MeleeWeaponHeld"));

FName ACharacterBase::ThrowableWeaponSocketComponentName(TEXT("ThrowableWeaponSocket"));
FName ACharacterBase::ThrowableWeaponComponentName(TEXT("ThrowableWeaponComp"));

FName ACharacterBase::DamageCtrlComponentName(TEXT("DamageCtrlComp"));

FName ACharacterBase::MeshBodySocketName(TEXT("Pelvis"));
FName ACharacterBase::ThrowableSocketName(TEXT("ThrowableSocket"));

FName ACharacterBase::TakeDownAttackSocketName(TEXT("TakeDownAttackSocket"));
FName ACharacterBase::CoverSystemComponentName(TEXT("CoverSystemComponent"));

FName ACharacterBase::RightTraceComponentName(TEXT("RightTraceComponent"));
FName ACharacterBase::LeftTraceComponentName(TEXT("LeftTraceComponent"));

ACharacterBase::ACharacterBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	AbilitySystemComponent = CreateDefaultSubobject<UJSMAbilitySystemComponent>(AbilitySystemComponentName);
	AbilitySystemComponent->SetIsReplicated(true);
	AttributeSet = CreateDefaultSubobject<UJSMAttributeSet>(AttributeSetName);
	AbilitySystemComponent->AddAttributeSetSubobject(AttributeSet.Get());

	WeaponCtrlComponent = CreateOptionalDefaultSubobject<UWeaponCtrlComponent>(WeaponCtrlComponentName);
	DamageCtrlComp = CreateDefaultSubobject<UJSMDamageCtrlComponent>(DamageCtrlComponentName);
	CoverSystemComponent = CreateOptionalDefaultSubobject<UCoverSystemComponent>(CoverSystemComponentName);

	USceneComponent* SceneComponent = CreateDefaultSubobject<USceneComponent>(VisualMeshesComponentName);
	if (IsValid(SceneComponent))
	{
		SceneComponent->SetupAttachment(GetMesh());
	}

	BodyMesh = CreateDefaultSubobject<USkeletalMeshComponent>(BodyMeshComponentName);
	if (IsValid(BodyMesh))
	{
		BodyMesh->SetupAttachment(SceneComponent);
		BodyMesh->SetLeaderPoseComponent(GetMesh());
	}

	HipWeaponSocket = CreateDefaultSubobject<USceneComponent>(HipWeaponSocketComponentName);
	if (IsValid(HipWeaponSocket))
	{
		HipWeaponSocket->SetupAttachment(GetMesh(), FName(TEXT("HipWeaponSocket")));
	}

	HipWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(HipWeaponComponentName);
	if (IsValid(HipWeapon))
	{
		HipWeapon->SetupAttachment(HipWeaponSocket);
		HipWeapon->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	BackWeaponSocket = CreateDefaultSubobject<USceneComponent>(BackWeaponSocketComponentName);
	if (IsValid(BackWeaponSocket))
	{
		BackWeaponSocket->SetupAttachment(GetMesh(), FName(TEXT("BackWeaponSocket")));
	}

	BackWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(BackWeaponComponentName);
	if (IsValid(BackWeapon))
	{
		BackWeapon->SetupAttachment(BackWeaponSocket);
		BackWeapon->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	WeaponScopeComp = CreateDefaultSubobject<UChildActorComponent>(WeaponScopeComponentName);
	if (IsValid(WeaponScopeComp))
	{
		WeaponScopeComp->SetupAttachment(HeldObjectRoot);
	}

	MeleeWeaponSocket = CreateDefaultSubobject<USceneComponent>(MeleeWeaponSocketComponentName);
	if (IsValid(MeleeWeaponSocket))
	{
		MeleeWeaponSocket->SetupAttachment(GetMesh(), FName(TEXT("MeleeWeaponSocket")));
	}

	MeleeWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(MeleeWeaponComponentName);
	if (IsValid(MeleeWeapon))
	{
		MeleeWeapon->SetupAttachment(MeleeWeaponSocket);
		MeleeWeapon->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	MeleeWeaponHeldSocket = CreateDefaultSubobject<USceneComponent>(MeleeWeaponHeldSocketComponentName);
	if (IsValid(MeleeWeaponHeldSocket))
	{
		MeleeWeaponHeldSocket->SetupAttachment(GetMesh(), FName(TEXT("MeleeWeaponHand")));
	}

	MeleeWeaponHeld = CreateDefaultSubobject<USkeletalMeshComponent>(MeleeWeaponHeldComponentName);
	if (IsValid(MeleeWeaponHeld))
	{
		MeleeWeaponHeld->SetupAttachment(MeleeWeaponHeldSocket);
		MeleeWeaponHeld->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	ThrowableWeaponHeldSocket = CreateDefaultSubobject<USceneComponent>(ThrowableWeaponSocketComponentName);
	if (IsValid(ThrowableWeaponHeldSocket))
	{
		ThrowableWeaponHeldSocket->SetupAttachment(GetMesh(), FName(TEXT("ThrowableSocket")));
	}

	ThrowableWeaponHeld = CreateDefaultSubobject<UStaticMeshComponent>(ThrowableWeaponComponentName);
	if (IsValid(ThrowableWeaponHeld))
	{
		ThrowableWeaponHeld->SetupAttachment(MeleeWeaponHeldSocket);
		ThrowableWeaponHeld->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		ThrowableWeaponHeld->SetRelativeScale3D(FVector_NetQuantize(1.0f, 1.0f, 1.0f));
	}

	ThrowableArc = CreateDefaultSubobject<USplineComponent>(TEXT("SplineComponent"));
	if (IsValid(ThrowableArc))
	{
		ThrowableArc->SetupAttachment(GetMesh(), ThrowableSocketName);
	}

	TakeDownAttackSocket = CreateDefaultSubobject<USceneComponent>(TakeDownAttackSocketName);
	if (IsValid(TakeDownAttackSocket))
	{
		TakeDownAttackSocket->SetupAttachment(RootComponent);
		TakeDownAttackSocket->SetRelativeLocation(FVector_NetQuantize(-60.0f, -8.0f, 0.0f));
	}

	RightTraceComponent = CreateDefaultSubobject<UArrowComponent>(RightTraceComponentName);
	if (IsValid(RightTraceComponent))
	{
		RightTraceComponent->SetupAttachment(RootComponent);
		RightTraceComponent->SetRelativeLocation(FVector_NetQuantize(0, 45.0f, 0.0f));
	}

	LeftTraceComponent = CreateDefaultSubobject<UArrowComponent>(LeftTraceComponentName);
	if (IsValid(LeftTraceComponent))
	{
		LeftTraceComponent->SetupAttachment(RootComponent);
		LeftTraceComponent->SetRelativeLocation(FVector_NetQuantize(0, -45.0f, 0.0f));
	}
	GetMesh()->SetVisibility(false);
}

void ACharacterBase::BeginPlay()
{
	Super::BeginPlay();

	if (IsValid(WeaponScopeComp))
	{
		WeaponScopeComp->SetChildActorClass(WeaponScopeClass);
		if (GetNetMode() != NM_Client)
		{
			AActor* WeaponScope = WeaponScopeComp->GetChildActor();
			WeaponScope->SetActorHiddenInGame(true);
		}
	}
}

void ACharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UpdateSprintCalculateStamina(DeltaTime);
	UpdateHealthPainAnim();
}

void ACharacterBase::UpdateSprintCalculateStamina(float DeltaTime)
{
	UJSMAttributeSet* MyAttributeSet = GetAttributeSet<UJSMAttributeSet>();
	if (IsValid(MyAttributeSet))
	{
		EALSGait CurrentGait = GetGait();
		float CurrentStamina = MyAttributeSet->GetCurrStamina();
		float MaxStamina = MyAttributeSet->GetMaxStamina();
		if ((CurrentStamina < 0) || (MaxStamina <= 0))
		{
			return;
		}

		if (CurrentGait == EALSGait::Sprinting)
		{
			if (CurrentStamina == 0)
			{
				SetGait(EALSGait::Walking);
			}
			else
			{
				MyAttributeSet->SetCurrStamina(CurrentStamina - (DecrementStaminaSpeed * DeltaTime));
			}
		}
		else
		{
			if (CurrentStamina < MaxStamina)
			{
				MyAttributeSet->SetCurrStamina(CurrentStamina + (DecrementStaminaSpeed * DeltaTime));
			}
		}
	}
}

void ACharacterBase::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(ACharacterBase, CharacterDescKeyName, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(ACharacterBase, bLoadCompleted, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(ACharacterBase, bRiding, SharedParams);
}

UAbilitySystemComponent* ACharacterBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

void ACharacterBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	CreateInitializeStates();
}

void ACharacterBase::CreateInitializeStates()
{
	TWeakObjectPtr<ACharacterBase> ThisWeak = TWeakObjectPtr<ACharacterBase>(this);

	InitializeStateHandler.AddCharacterState(EJSMCharacterInitializeStateID::Spawned)
		.AddTranstion([ThisWeak]()
			{
				if (ThisWeak.IsValid() == false)
				{
					return false;
				}
				return ThisWeak->GetCharacterDescription() != nullptr;
			}, EJSMCharacterInitializeStateID::Initialize);

	InitializeStateHandler.AddCharacterState(EJSMCharacterInitializeStateID::Initialize)
		.AddLambdaOnEnter([ThisWeak]()
			{
				if (ThisWeak.IsValid())
				{
					const FCharacterDesc* CharacterDesc = ThisWeak->GetCharacterDescription();
					check(CharacterDesc);

					ThisWeak->InitializeCharacter(CharacterDesc);
				}
			})
		.AddTranstion([ThisWeak]()
			{
				return true;
			}, EJSMCharacterInitializeStateID::PostInitialize);

	InitializeStateHandler.AddCharacterState(EJSMCharacterInitializeStateID::PostInitialize)
		.AddLambdaOnEnter([ThisWeak] ()
			{
				if (ThisWeak.IsValid())
				{
					ThisWeak->PostInitializeCharacter();
				}

				UJSMGameInstance* GameInst = UJSMGameInstance::GetGameInstance();
				check(GameInst);
				UJSMEventManager* EventMananger = GameInst->GetEventManager();
				if (IsValid(EventMananger))
				{
					EventMananger->JSMEvent.InGame.CharacterSpawned.Broadcast(ThisWeak.Get(), ThisWeak.Get());
				}
			})
		.AddTranstion([]()
			{
				return true;
			}, EJSMCharacterInitializeStateID::Done);


	InitializeStateHandler.AddCharacterState(EJSMCharacterInitializeStateID::Done);
	InitializeStateHandler.InitState(EJSMCharacterInitializeStateID::Spawned);
}

void ACharacterBase::PostInitializeCharacter()
{
	UJSMAttributeSet* MyAttributeSet = GetAttributeSet<UJSMAttributeSet>();
	if (ensure(IsValid(MyAttributeSet)))
	{
		MyAttributeSet->Initialize(this);
	}
}

void ACharacterBase::InitializeCharacter(const FCharacterDesc* charDesc)
{
	SS_CharacterDataLoad();
	if (IsValid(BodyMesh))
	{
		UJSMCharacterDataAsset* DataAsset = GetCharacterDataAsset();
		BodyMesh->SetSkeletalMeshAsset(UAssetLoader::AssetLoadObject(DataAsset->CharacterMesh));
	}
}

FString ACharacterBase::GetDisplayName() const
{
	const FCharacterDesc* CharacterDesc = GetCharacterDescription();
	if (!ensure(CharacterDesc))
	{
		return FString();
	}
	return CharacterDesc->DisplayName.ToString();
}

const FCharacterDesc* ACharacterBase::GetCharacterDescription() const
{
	if (CharacterDescKeyName.IsEmpty())
	{
		return nullptr;
	}

	UDataTable* CharacterTable = UJSMClientStatics::GetJSMDataCollection()->CharacterTable.Get();
	if (IsValid(CharacterTable))
	{
		return CharacterTable->FindRow<FCharacterDesc>(FName(*CharacterDescKeyName), TEXT("CharacterDesc"));
	}

	return nullptr;
}

bool ACharacterBase::IsLocalPlayerCharacter() const
{
	if (GetNetMode() == NM_Standalone)
	{
		if (IsPlayerCharacter())
		{
			return true;
		}
	}
	else
	{
		if (Controller)
		{
			return Controller->IsLocalPlayerController();
		}
	}
	return false;
}

void ACharacterBase::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	SetupAbilitySystemForServer();
}

void ACharacterBase::SetupAbilitySystemForServer()
{
	if (IsValid(AbilitySystemComponent))
	{
		AbilitySystemComponent->InitAbilityActorInfo(this, this);
		AbilitySystemComponent->AddStartingAbility();
		AbilitySystemComponent->AddDefaultEffects();
	}
}

void ACharacterBase::SetupAbilitySystemForClient()
{
	if (IsValid(AbilitySystemComponent))
	{
		AbilitySystemComponent->InitAbilityActorInfo(this, this);
		AbilitySystemComponent->BindAbilityInput(InputComponent);
	}
}

void ACharacterBase::MovementToNone()
{
	if (IsValid(WeaponCtrlComponent))
	{
		WeaponCtrlComponent->MovementToNone();
	}
}

void ACharacterBase::MovementToMantle()
{
	if (IsValid(WeaponCtrlComponent))
	{
		WeaponCtrlComponent->MovementToMantle();
	}
}

UJSMCharacterDataAsset* ACharacterBase::GetCharacterDataAsset() const
{
	UJSMCharacterDataAsset* CharacterDataAsset = UJSMClientStatics::GetCharacterDataAsset(FName(*CharacterDescKeyName));
	return CharacterDataAsset;
}

void ACharacterBase::OnRep_LoadCompleted()
{
	UJSMCharacterDataAsset* CharacterDataAsset = GetCharacterDataAsset();
	if (IsValid(CharacterDataAsset))
	{
		CharacterDataAsset->LoadSynchronous();
	}
}

void ACharacterBase::SS_CharacterDataLoad()
{
	MARK_PROPERTY_DIRTY_FROM_NAME(ACharacterBase, bLoadCompleted, this);
	bLoadCompleted = true;
	OnRep_LoadCompleted();
}

void ACharacterBase::UpdateHealthPainAnim()
{
	UJSMAttributeSet* MyAttributeSet = GetAttributeSet<UJSMAttributeSet>();
	UJSMCharacterDataAsset* CharacterDataAsset = GetCharacterDataAsset();
	if (!IsValid(MyAttributeSet) || !IsValid(CharacterDataAsset))
	{
		return;
	}

	float CurrentHealth = MyAttributeSet->GetCurrHealth();
	float MaxHealth = MyAttributeSet->GetMaxHealth();
	if ((CurrentHealth < 0) || (MaxHealth <= 0))
	{
		return;
	}

	MaxHealth = (MaxHealth / 5);
	if ((CurrentHealth > 0) && (CurrentHealth < MaxHealth))
	{
		SetGait(EALSGait::Walking);
		UAnimMontage* InjuredMontage = CharacterDataAsset->AM_Injured.Get();
		if (!GetMesh()->GetAnimInstance()->Montage_IsPlaying(InjuredMontage))
		{
			Replicated_PlayMontage(CharacterDataAsset->AM_Injured.Get(), 1.0f);
		}
	}
	RagdollToggle();
}

void ACharacterBase::FireWeapon(FHitResult& OutHitResult, FVector_NetQuantize& EndLoc, const float& LineDistance, const float& TraceRadius, const FVector_NetQuantize& Offset, const FVector_NetQuantize& MuzzleLoc, const FVector_NetQuantize& MuzzleEndLoc)
{

}

void ACharacterBase::PredictProjectile(FVector_NetQuantize& LaunchVelocity)
{

}

ACharacterBase* ACharacterBase::InstantiateCharacter(const UObject* worldContextObj,
	const TSubclassOf<ACharacterBase>& charClass,
	const FTransform& spawnTransform,
	const FString& CharacterKeyName,
	const int32& Level,
	ESpawnActorCollisionHandlingMethod SpawnCollisionHandlingOverride)
{
	UWorld* gameWorld = worldContextObj->GetWorld();

	if (!IsValid(gameWorld))
	{
		return nullptr;
	}

	if (!IsValid(charClass))
	{
		UE_LOG(LogTemp, Warning, TEXT("Wrong pawn class in %s"));
		return nullptr;
	}

	FCharacterDesc* charDesc = UJSMClientStatics::GetJSMDataCollection()->CharacterTable->FindRow<FCharacterDesc>(FName(CharacterKeyName), TEXT("CharacterDesc"));

	if (!ensure(charDesc))
	{
		return nullptr;
	}

	if (!ensure(charDesc))
	{
		return nullptr;
	}

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.ObjectFlags |= RF_Transient;
	SpawnInfo.SpawnCollisionHandlingOverride = SpawnCollisionHandlingOverride;
	ACharacterBase* SpawnedChar = gameWorld->SpawnActor<ACharacterBase>(charClass, spawnTransform, SpawnInfo);

	if (SpawnedChar == nullptr)
	{
		return nullptr;
	}

	SpawnedChar->CharacterDescKeyName = CharacterKeyName;
	return SpawnedChar;
}

void ACharacterBase::DieCharacter(ACharacterBase* killer)
{
	if (!HasAuthority())
	{
		return;
	}
	INVOKE_EVENT_JSM(JSMEvent.InGame.CharacterDead, this, killer);
	RagdollToggle();
}

bool ACharacterBase::IsDie() const
{
	UJSMAttributeSet* MyAttributeSet = GetAttributeSet<UJSMAttributeSet>();
	if (!IsValid(MyAttributeSet))
	{
		return true;
	}

	return (((MyAttributeSet->GetCurrHealth() <= 0) && (MyAttributeSet->GetMaxHealth() > 0)) ? true : false);
}

FGenericTeamId ACharacterBase::GetGenericTeamId() const
{
	return TeamId;
}

void ACharacterBase::AddImpulseCharacter(const FVector_NetQuantize& Impulse, const FVector_NetQuantize& Location, const FName& BoneName)
{
	GetMesh()->AddImpulseAtLocation(Impulse, Location, BoneName);
}

void ACharacterBase::RagdollToggle()
{
	UJSMAttributeSet* MyAttributeSet = GetAttributeSet<UJSMAttributeSet>();
	if (!IsValid(MyAttributeSet))
	{
		return;
	}

	float CurrentHealth = MyAttributeSet->GetCurrHealth();
	if ((CurrentHealth == 0) && (GetMovementState() != EALSMovementState::Ragdoll))
	{
		SetOverlayState(EALSOverlayState::Default);
		ReplicatedRagdollStart();
	}
	else if ((CurrentHealth > 0) && (GetMovementState() == EALSMovementState::Ragdoll))
	{
		ReplicatedRagdollEnd();
	}
}

UAnimMontage* ACharacterBase::GetLootAnimation()
{
	UJSMCharacterDataAsset* CharacterDataAsset = GetCharacterDataAsset();
	if (!IsValid(CharacterDataAsset))
	{
		return nullptr;
	}
	return CharacterDataAsset->AM_Loot.Get();
}

void ACharacterBase::TakeDown()
{
	SM_TakeDown();

	FTimerHandle DelayHandle;
	const TWeakObjectPtr<ACharacterBase> WeakPtr(this);
	GetWorld()->GetTimerManager().SetTimer(DelayHandle, FTimerDelegate::CreateLambda([WeakPtr]()
	{
		if (WeakPtr.IsValid())
		{
			UJSMAttributeSet* MyAttributeSet = WeakPtr->GetAttributeSet<UJSMAttributeSet>();
			if (IsValid(MyAttributeSet))
			{
				MyAttributeSet->SetCurrHealth(0);
			}
		}
	}), 4.5, false);
}

void ACharacterBase::SM_TakeDown_Implementation()
{
	UCapsuleComponent* CapsuleComp = GetCapsuleComponent();
	if (IsValid(CapsuleComp))
	{
		CapsuleComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	UCharacterMovementComponent* CharacterMov = GetCharacterMovement();
	if (IsValid(CharacterMov))
	{
		CharacterMov->DisableMovement();
	}

	UJSMCharacterDataAsset* DataAsset = GetCharacterDataAsset();
	if (!IsValid(DataAsset) || !IsValid(GetMesh()) || !IsValid(GetMesh()->GetAnimInstance()))
	{
		return;
	}
	GetMesh()->GetAnimInstance()->Montage_Play(DataAsset->AM_StealthDeath.Get(), 1.0f);
}

void ACharacterBase::SC_TakeDown_Implementation()
{
	UJSMCharacterDataAsset* DataAsset = GetCharacterDataAsset();
	if (!IsValid(DataAsset))
	{
		return;
	}

	Replicated_PlayMontage(DataAsset->AM_StealthDeath.Get(), 1.0f);
}

void ACharacterBase::EnterVehicle(AActor* HitActor)
{
	if (!HitActor->Implements<UVehicleInterface>())
	{
		return;
	}

	IVehicleInterface* VehicleInterface = Cast<IVehicleInterface>(HitActor);
	if (VehicleInterface == nullptr)
	{
		return;
	}

	if (VehicleInterface->IsOverlapPlayer(GetUniqueID()))
	{
		MARK_PROPERTY_DIRTY_FROM_NAME(ACharacterBase, bRiding, this);
		bRiding = true;

		SM_SetCollisionEnabled(ECollisionEnabled::NoCollision);
		SM_SetCollisionProfileName(FName(TEXT("NoCollision")));
		VehicleInterface->EnterVehicle(this);

		CS_SetMovementMode(EMovementMode::MOVE_None);
	}
}

void ACharacterBase::TriggerInteractive(AActor* HitActor)
{
	if (!HitActor->Implements<UInteractiveInterface>())
	{
		return;
	}

	IInteractiveInterface* InteractiveInterface = Cast<IInteractiveInterface>(HitActor);
	if (InteractiveInterface == nullptr)
	{
		return;
	}

	if (InteractiveInterface->IsOverlapPlayer(GetUniqueID()))
	{
		InteractiveInterface->TriggerInteractive();
	}
}

void ACharacterBase::SM_SetCollisionEnabled_Implementation(const ECollisionEnabled::Type& InType)
{
	GetMesh()->SetCollisionEnabled(InType);
}

void ACharacterBase::CS_SetMovementMode_Implementation(const EMovementMode& InMoveMode)
{
	UCharacterMovementComponent* CharacterMov = GetCharacterMovement();
	if (IsValid(CharacterMov))
	{
		CharacterMov->SetMovementMode(InMoveMode);
	}
}

void ACharacterBase::ExitVehicle()
{
	MARK_PROPERTY_DIRTY_FROM_NAME(ACharacterBase, bRiding, this);
	bRiding = false;

	SM_SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SM_SetCollisionProfileName(FName(TEXT("Pawn")));

	CS_SetMovementMode(EMovementMode::MOVE_Walking);
	if (IsValid(WeaponCtrlComponent))
	{
		WeaponCtrlComponent->RefreshCharacterWeaponCtrlComponent();
	}
}

void ACharacterBase::SM_SetCollisionProfileName_Implementation(const FName& InName)
{
	UCapsuleComponent* CapsuleComp = GetCapsuleComponent();
	if (IsValid(CapsuleComp))
	{
		CapsuleComp->SetCollisionProfileName(InName);
	}
}