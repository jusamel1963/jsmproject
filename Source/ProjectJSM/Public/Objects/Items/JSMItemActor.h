// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "JSMItemActor.generated.h"

#define ECC_ITEM ECC_GameTraceChannel1

DECLARE_DELEGATE(FJSMItemDataLoadComplete);

USTRUCT()
struct PROJECTJSM_API FJSMItemAssetLoadRequest
{
	GENERATED_USTRUCT_BODY()

public:
	FJSMItemDataLoadComplete CompleteDelegate;
	TSharedPtr<struct FStreamableHandle> AsyncLoadHandle;
};

UCLASS()
class PROJECTJSM_API AJSMItemActor : public AActor
{
	GENERATED_BODY()
	
public:	
	AJSMItemActor();
	class UJSMItemInfo* GetRepItemInfo() const;
	class UJSMItemDataAsset* GetItemDataAsset() const;
	bool IsLoadCompleted();

	void SS_CreateItemInfo();
	void SS_SetRepItemInfo(class UJSMItemInfo* InRepItemInfo);
	void SS_SetCount(int32 InCount);
	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	bool IsEmpty() const;

	void BeginPickup();
	void EndPickup();
	void CancelPickup();
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnRep_ItemInfo();

	virtual void LoadAssetAsync(FJSMItemDataLoadComplete LoadCompleteDelegate);
	virtual void LoadAssetCompleted();
	virtual void InitializeItemResources();
	virtual void LoadSynchronous();
	void UpdateDisplayName();
	
	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OpverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:	
	virtual void Tick(float DeltaTime) override;
	
public:
	FSimpleDelegate OnLoadComplete;

protected:
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Replicated, Category = "Item")
	FName RepItemDataKey;
	
	UPROPERTY(Transient, ReplicatedUsing = OnRep_ItemInfo)
	TObjectPtr<class UJSMItemInfo> RepItemInfo;

	FJSMItemAssetLoadRequest AssetLoadRequest;
	bool bUseAsyncLoad = true;
	bool bLoadCompleted = false;

	static FName SceneComponentName;
	static FName ItemStaticMeshComponentName;
	static FName ItemCollisionProfileName;
	static FName VolumeComponentName;
	static FName PointLightComponentName;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class UStaticMeshComponent> ItemStaticMeshComponent;

	UPROPERTY(EditDefaultsOnly)
	TObjectPtr<class USphereComponent> VolumeComponent;

	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly)
	TObjectPtr<class UPointLightComponent> PointLightComponent;

	bool bBeginPickup = false;
};
