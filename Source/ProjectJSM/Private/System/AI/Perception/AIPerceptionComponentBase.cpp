// Copyright JSM Studio, Inc. All Rights Reserved.


#include "System/AI/Perception/AIPerceptionComponentBase.h"
#include "Data/DataAssets/CharacterData/JSMAIDataAsset.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISenseConfig_Damage.h"
#include "Perception/AISenseConfig_Hearing.h"
#include "Perception/AISenseConfig_Team.h"
#include "FrameWork/AIBaseController.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Data/Common/GameEnum.h"
#include "Objects/Character/AICharacter.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "System/AI/BlackboardKeys.h"
#include "Core/Managers/JSMEventManager.h"
#include "FrameWork/JSMGameInstance.h"
#include "System/AI/BlackboardKeys.h"

UAIPerceptionComponentBase::UAIPerceptionComponentBase()
{
	bAllowAnyoneToDestroyMe = true;
}

void UAIPerceptionComponentBase::InitializeAIData(const UJSMAIDataAsset* aiData)
{
	if (ensure(aiData != nullptr))
	{
		InitAISenseSightConfig(aiData);
		InitAISenseHearingConfig(aiData);
		InitAISenseDamageConfig(aiData);
		InitAISenseTeamConfig(aiData);
		//InitAISenseLightConfig(aiData);

		UAIPerceptionSystem* perceptionSystem = UAIPerceptionSystem::GetCurrent(AIOwner);
		if (perceptionSystem != nullptr)
		{
			perceptionSystem->UpdateListener(*this);
		}
	}
}

void UAIPerceptionComponentBase::InitAISenseSightConfig(const UJSMAIDataAsset* aiData)
{
	if (aiData->bHasSightSense)
	{
		UAISenseConfig_Sight* sightConfig = NewObject<UAISenseConfig_Sight>(AIOwner);

		sightConfig->DetectionByAffiliation.bDetectEnemies = true;
		sightConfig->DetectionByAffiliation.bDetectNeutrals = true;
		sightConfig->DetectionByAffiliation.bDetectFriendlies = false;
		sightConfig->SetMaxAge(aiData->SightMaxAge);

		sightConfig->SightRadius = aiData->SightDistance;
		sightConfig->LoseSightRadius = aiData->LoseSightDistance;
		//sightConfig->PeripheralVisionAngleDegrees = aiData->SightAngle;

		SetDominantSense(*sightConfig->GetSenseImplementation());
		ConfigureSense(*sightConfig);
	}
}

void UAIPerceptionComponentBase::InitAISenseHearingConfig(const UJSMAIDataAsset* aiData)
{
	if (aiData->bHasHearingSense)
	{
		UAISenseConfig_Hearing* hearingConfig = NewObject<UAISenseConfig_Hearing>(AIOwner);
		hearingConfig->DetectionByAffiliation.bDetectEnemies = true;
		hearingConfig->DetectionByAffiliation.bDetectNeutrals = true;
		hearingConfig->DetectionByAffiliation.bDetectFriendlies = false;

		hearingConfig->HearingRange = aiData->HearingRange;
		hearingConfig->SetMaxAge(aiData->HearingMaxAge);

		ConfigureSense(*hearingConfig);
	}
}

void UAIPerceptionComponentBase::InitAISenseDamageConfig(const UJSMAIDataAsset* aiData)
{
	UAISenseConfig_Damage* damageConfig = NewObject<UAISenseConfig_Damage>(AIOwner);
	damageConfig->SetMaxAge(aiData->DamageSenseMaxAge);
	ConfigureSense(*damageConfig);
}

void UAIPerceptionComponentBase::InitAISenseTeamConfig(const UJSMAIDataAsset* aiData)
{
	if (aiData->AlertTeam)
	{
		UAISenseConfig_Team* teamConfig = NewObject<UAISenseConfig_Team>(AIOwner);
		ConfigureSense(*teamConfig);
	}
}

void UAIPerceptionComponentBase::OnRegister()
{
	Super::OnRegister();

	UAIPerceptionSystem* perceptionSystem = UAIPerceptionSystem::GetCurrent(AIOwner);
	if (perceptionSystem != nullptr)
	{
		perceptionSystem->UpdateListener(*this);
	}

	OnTargetPerceptionUpdated.AddDynamic(this, &UAIPerceptionComponentBase::OnTargetPerceptionUpdatedImpl);
	OnPerceptionUpdated.AddDynamic(this, &UAIPerceptionComponentBase::OnPerceptionUpdatedImpl);

	BIND_EVENT_JSM(JSMEvent.InGame.CharacterDead, this, &UAIPerceptionComponentBase::OnCharacterDead);
}

void UAIPerceptionComponentBase::OnUnregister()
{
	Super::OnUnregister();

	OnTargetPerceptionUpdated.RemoveDynamic(this, &UAIPerceptionComponentBase::OnTargetPerceptionUpdatedImpl);
	OnPerceptionUpdated.RemoveDynamic(this, &UAIPerceptionComponentBase::OnPerceptionUpdatedImpl);

	UNBIND_EVENT_JSM(JSMEvent.InGame.CharacterDead, this, &UAIPerceptionComponentBase::OnCharacterDead);
}

void UAIPerceptionComponentBase::OnTargetPerceptionUpdatedImpl(AActor* Actor, FAIStimulus Stimulus)
{
}

void UAIPerceptionComponentBase::OnPerceptionUpdatedImpl(const TArray<AActor*>& UpdatedActors)
{
	AAICharacter* OwnChar = Cast<AAICharacter>(AIOwner->GetCharacter());
	if (!IsValid(OwnChar) || OwnChar->IsDie())
	{
		return;
	}

	for (const AActor* Actor : UpdatedActors)
	{
		check(IsValid(Actor));
		UpdateStimuliStrength(Actor);
	}

	UpdateTargets();
}

void UAIPerceptionComponentBase::OnCharacterDead(UObject* Invoker, class ACharacterBase* Victim, class ACharacterBase* Attacker)
{
	StimuliStrengthMap.Remove(Victim);
	UpdateTargets();
}

void UAIPerceptionComponentBase::UpdateTargets()
{
	AAICharacter* OwnChar = Cast<AAICharacter>(AIOwner->GetCharacter());
	if (!IsValid(OwnChar))
	{
		return;
	}

	FVector_NetQuantize OwnerChracterLocation = OwnChar->GetActorLocation();
	AActor* CandidateStimuliTarget = nullptr;

	float BestStimuliStrength = 0;
	for (const auto& Iter : StimuliStrengthMap)
	{
		if (Iter.Value > BestStimuliStrength || CandidateStimuliTarget == nullptr)
		{
			BestStimuliStrength = Iter.Value;
			CandidateStimuliTarget = Iter.Key;
		}
		else if (Iter.Value == BestStimuliStrength)
		{
			float Dist_Candidate = FVector_NetQuantize::DistSquared2D(OwnerChracterLocation, CandidateStimuliTarget->GetActorLocation());
			float Dist_Iter = FVector_NetQuantize::DistSquared2D(OwnerChracterLocation, Iter.Key->GetActorLocation());
			if (Dist_Iter < Dist_Candidate)
			{
				BestStimuliStrength = Iter.Value;
				CandidateStimuliTarget = Iter.Key;
			}
		}
	}

	TargetActor = CandidateStimuliTarget;
	AAIBaseController* OwnController = Cast<AAIBaseController>(AIOwner);
	if (!IsValid(OwnController) || !IsValid(OwnController->GetBlackboardComponent()))
	{
		return;
	}

	OwnController->GetBlackboardComponent()->SetValueAsObject(BlackboardKeys::TargetActor, TargetActor);
}

void UAIPerceptionComponentBase::UpdateStimuliStrength(const AActor* Actor)
{
	const ACharacterBase* CharacterBase = Cast<ACharacterBase>(Actor);
	const FActorPerceptionInfo* PerceptionInfo = GetActorInfo(*Actor);
	check(PerceptionInfo);

	float& StimuliStrength = StimuliStrengthMap.FindOrAdd(const_cast<AActor*>(Actor));
	StimuliStrength = 0;

	for (int i = 0; i < PerceptionInfo->LastSensedStimuli.Num(); i++)
	{
		const FAIStimulus& Stimulus = PerceptionInfo->LastSensedStimuli[i];
		EAISense JSMAISense = static_cast<EAISense>(i);

		if (JSMAISense == EAISense::Sight)
		{
			if (IsValid(CharacterBase) == false || CharacterBase->IsDie() == true)
			{
				continue;
			}
		}

		switch (JSMAISense)
		{
			case EAISense::Sight:
			{
				AISightSense(Actor, Stimulus);
				break;
			}
			case EAISense::Hearing:
			{
				AIHearSense(Actor, Stimulus);
				break;
			}
			case EAISense::Damage:
			{
				AIDamageSense(Actor, Stimulus);
				break;
			}
			default:
			{
				break;
			}
		}

		if (Stimulus.IsValid() == false || Stimulus.IsExpired() || !Stimulus.WasSuccessfullySensed())
		{
			continue;
		}

		StimuliStrength += Stimulus.Strength;
	}
	UE_LOG(LogTemp, Warning, TEXT("AISightSense StimuliStrengthMap Num : %d"), StimuliStrengthMap.Num());
	UE_LOG(LogTemp, Warning, TEXT("AISightSense StimuliStrength : %lf"), StimuliStrength);

	if (StimuliStrength == 0)
	{
		StimuliStrengthMap.Remove(const_cast<AActor*>(Actor));
	}
}

void UAIPerceptionComponentBase::AISightSense(const AActor* Actor, const FAIStimulus& InStimulus)
{
	AAICharacter* OwnChar = Cast<AAICharacter>(AIOwner->GetCharacter());
	if (!IsValid(OwnChar))
	{
		return;
	}

	AAIBaseController* OwnController = Cast<AAIBaseController>(AIOwner);
	if (!IsValid(OwnController) || !IsValid(OwnController->GetBlackboardComponent()))
	{
		return;
	}

	EAIState CurrentAIState = static_cast<EAIState>(OwnController->GetBlackboardComponent()->GetValueAsEnum(BlackboardKeys::AIStatus));
	EAIState NewAIState = CurrentAIState;

	if (InStimulus.IsValid() == false || InStimulus.IsExpired() || !InStimulus.WasSuccessfullySensed())
	{
		StimuliStrengthMap.Remove(const_cast<AActor*>(Actor));
		UE_LOG(LogTemp, Warning, TEXT("AISightSense : Sight False"));

		switch (CurrentAIState)
		{
			case EAIState::Attack:
			{
				NewAIState = EAIState::Investigate;
				OwnController->SetFocalPoint(InStimulus.StimulusLocation);
				break;
			}
			case EAIState::Patrol:
			{
				OwnChar->SetRotationMode(EALSRotationMode::LookingDirection);
				break;
			}
			case EAIState::Investigate:
			default:
			{

			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AISightSense : Sight True"));

		switch (CurrentAIState)
		{
			case EAIState::Patrol:
			case EAIState::Investigate:
			{
				NewAIState = EAIState::Attack;
			}
			case EAIState::Attack:
			{
				OwnChar->SetRotationMode(EALSRotationMode::Aiming);
				break;
			}
			default:
			{
				;
			}
		}
	}

	OwnController->GetBlackboardComponent()->SetValueAsEnum(BlackboardKeys::AIStatus, static_cast<uint8>(NewAIState));
}

void UAIPerceptionComponentBase::AIHearSense(const AActor* Actor, const FAIStimulus& InStimulus)
{
	AAICharacter* OwnChar = Cast<AAICharacter>(AIOwner->GetCharacter());
	if (!IsValid(OwnChar))
	{
		return;
	}

	AAIBaseController* OwnController = Cast<AAIBaseController>(AIOwner);
	if (!IsValid(OwnController) || !IsValid(OwnController->GetBlackboardComponent()))
	{
		return;
	}

	EAIState CurrentAIState = static_cast<EAIState>(OwnController->GetBlackboardComponent()->GetValueAsEnum(BlackboardKeys::AIStatus));
	EAIState NewAIState = CurrentAIState;

	if (InStimulus.IsValid() == false || InStimulus.IsExpired() || !InStimulus.WasSuccessfullySensed())
	{
		UE_LOG(LogTemp, Warning, TEXT("AISightSense : Hear False"));
		switch (CurrentAIState)
		{
			case EAIState::Investigate:
			case EAIState::Patrol:
			{
				OwnChar->SetRotationMode(EALSRotationMode::LookingDirection);
				OwnController->ClearFocus(EAIFocusPriority::Gameplay);
				break;
			}
			default:
			{

			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AISightSense : Hear True"));
		if (InStimulus.Tag.IsEqual(FName(TEXT("FireNoise"))) || InStimulus.Tag.IsEqual(FName(TEXT("Distract"))))
		{
			switch (CurrentAIState)
			{
				case EAIState::Patrol:
				case EAIState::Investigate:
				{
					NewAIState = EAIState::Investigate;
					OwnChar->SetRotationMode(EALSRotationMode::Aiming);
					OwnController->SetFocalPoint(InStimulus.StimulusLocation);
				}
				default:
				{

				}
			}
		}
	}

	OwnController->GetBlackboardComponent()->SetValueAsEnum(BlackboardKeys::AIStatus, static_cast<uint8>(NewAIState));
}

void UAIPerceptionComponentBase::AIDamageSense(const AActor* Actor, const FAIStimulus& InStimulus)
{
	AAICharacter* OwnChar = Cast<AAICharacter>(AIOwner->GetCharacter());
	if (!IsValid(OwnChar))
	{
		return;
	}

	AAIBaseController* OwnController = Cast<AAIBaseController>(AIOwner);
	if (!IsValid(OwnController) || !IsValid(OwnController->GetBlackboardComponent()))
	{
		return;
	}

	EAIState CurrentAIState = static_cast<EAIState>(OwnController->GetBlackboardComponent()->GetValueAsEnum(BlackboardKeys::AIStatus));
	EAIState NewAIState = CurrentAIState;

	if (InStimulus.IsValid() == false || InStimulus.IsExpired() || !InStimulus.WasSuccessfullySensed())
	{
		UE_LOG(LogTemp, Warning, TEXT("AISightSense : Damage False"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AISightSense : Damage True"));
		switch (CurrentAIState)
		{
			case EAIState::Patrol:
			case EAIState::Investigate:
			{
				NewAIState = EAIState::Investigate;
				OwnChar->SetRotationMode(EALSRotationMode::Aiming);
				OwnController->SetFocalPoint(InStimulus.StimulusLocation);
			}
			default:
			{

			}
		}
	}
	OwnController->GetBlackboardComponent()->SetValueAsEnum(BlackboardKeys::AIStatus, static_cast<uint8>(NewAIState));
}