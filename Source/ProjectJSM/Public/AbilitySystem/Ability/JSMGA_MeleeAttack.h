// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Ability/JSMGameplayAbility.h"
#include "JSMGA_MeleeAttack.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UJSMGA_MeleeAttack : public UJSMGameplayAbility
{
	GENERATED_BODY()
public:
	UJSMGA_MeleeAttack();

	virtual void PostCommitActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

private:
	void InternalActivateAbility();

};
