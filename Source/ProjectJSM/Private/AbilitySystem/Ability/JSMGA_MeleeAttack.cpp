// Copyright JSM Studio, Inc. All Rights Reserved.


#include "AbilitySystem/Ability/JSMGA_MeleeAttack.h"
#include "Abilities/Tasks/AbilityTask_WaitDelay.h"
#include "Abilities/Tasks/AbilityTask_WaitGameplayEvent.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"
#include "Core/Managers/JSMEventManager.h"
#include "Data/DataAssets/ItemData/JSMWeaponDataAsset.h"
#include "Data/DataAssets/ItemData/JSMMeleeWeaponDataAsset.h"
#include "Framework/JSMGameInstance.h"
#include "Objects/Character/CharacterBase.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Objects/Weapons/Weapon.h"
#include "Objects/Weapons/MeleeWeapon.h"
#include "Objects/Weapons/JSMWeaponInfo.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "Statics/JSMClientStatics.h"

UJSMGA_MeleeAttack::UJSMGA_MeleeAttack()
	: Super()
{
	JSMAbilityID = EJSMGameplayAbilityID::MeleeAttack;
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;

	FGameplayTag AbilityTag = REQ_GAMEPLAYTAG("Ability.Attack.MeleeAttack");
	AbilityTags.AddTag(AbilityTag);
	ActivationOwnedTags.AddTag(AbilityTag);

	FGameplayTag AbilityTriggerTag = REQ_GAMEPLAYTAG("Event.Attack.MeleeAttackStart");
	FAbilityTriggerData TriggerData;
	TriggerData.TriggerTag = AbilityTriggerTag;
	TriggerData.TriggerSource = EGameplayAbilityTriggerSource::GameplayEvent;
	AbilityTriggers.Add(TriggerData);
}

void UJSMGA_MeleeAttack::PostCommitActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::PostCommitActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
	ACharacterBase* OwnerCharacter = Cast<ACharacterBase>(ActorInfo->AvatarActor);
	if (!IsValid(OwnerCharacter))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	UWeaponCtrlComponent* WeaponCtrlComponent = OwnerCharacter->GetWeaponCtrl();
	if (!IsValid(WeaponCtrlComponent))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	WeaponCtrlComponent->MeleeWeaponAttack();
	EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
}

void UJSMGA_MeleeAttack::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}
