// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Animation/AnimNotify/EndLootAnimNotify.h"
#include "Objects/Character/Component/JSMInventoryComponent.h"

void UEndLootAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	if (!IsValid(MeshComp))
	{
		return;
	}

	AActor* Owner = MeshComp->GetOwner();
	if (!IsValid(Owner))
	{
		return;
	}

	UJSMInventoryComponent* InventoryComponent = Owner->FindComponentByClass<UJSMInventoryComponent>();
	if (!IsValid(InventoryComponent))
	{
		return;
	}

	InventoryComponent->CS_EndLootItems();

}
