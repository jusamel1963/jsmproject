// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Vehicles/NoWheelVehicleBase.h"
#include "Objects/Vehicles/VehicleInfo.h"
#include "Components/SceneComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/TextRenderComponent.h"
#include "Framework/JSMGameInstance.h"
#include "Data/DataAssets/ItemData/JSMVehicleDataAsset.h"
#include "Framework/JSMItemManagerSubsystem.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "Engine/ActorChannel.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Statics/AssetLoader.h"
#include "Net/UnrealNetwork.h"
#include "Net/Core/PushModel/PushModel.h"
#include "Components/SkeletalMeshComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/PointLightComponent.h"
#include "Components/SphereComponent.h"
#include "Components/BoxComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetMathLibrary.h"

FName ANoWheelVehicleBase::SceneComponentName(TEXT("SceneComponent"));
FName ANoWheelVehicleBase::VehicleStaticMeshComponentName(TEXT("VehicleStaticMeshComponent"));
FName ANoWheelVehicleBase::VehicleSkeletalMeshComponentName(TEXT("VehicleSkeletalMeshComponent"));

// Sets default values
ANoWheelVehicleBase::ANoWheelVehicleBase()
{
	VehicleStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(VehicleStaticMeshComponentName);
	RootComponent = VehicleStaticMeshComponent;

	EnterBoxRightComponent = CreateDefaultSubobject<UBoxComponent>(FName(TEXT("RightEnter")));
	if (IsValid(EnterBoxRightComponent))
	{
		EnterBoxRightComponent->SetupAttachment(RootComponent, FName(TEXT("RightEnter")));
		EnterBoxRightComponent->SetBoxExtent(FVector_NetQuantize(100.f, 100.f, 100.f));
	}

	EnterBoxLeftComponent = CreateDefaultSubobject<UBoxComponent>(FName(TEXT("LeftEnter")));
	if (IsValid(EnterBoxLeftComponent))
	{
		EnterBoxLeftComponent->SetupAttachment(RootComponent, FName(TEXT("LeftEnter")));
		EnterBoxLeftComponent->SetBoxExtent(FVector_NetQuantize(100.f, 100.f, 100.f));
	}

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(FName(TEXT("SpringArm")));
	if (IsValid(SpringArmComp))
	{
		SpringArmComp->SetupAttachment(RootComponent);
	}

	CameraComp = CreateDefaultSubobject<UCameraComponent>(FName(TEXT("Camera")));
	if (IsValid(CameraComp))
	{
		CameraComp->SetupAttachment(SpringArmComp);
	}

	SeatArray.Reserve(2);
	SeatArray.Emplace(FName(TEXT("pilot_seat1")), FSeatInfo());
	SeatArray.Emplace(FName(TEXT("pilot_seat2")), FSeatInfo());
}

// Called when the game starts or when spawned
void ANoWheelVehicleBase::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		SS_CreateVehicleInfo();
		if (IsValid(EnterBoxRightComponent))
		{
			EnterBoxRightComponent->OnComponentBeginOverlap.AddDynamic(this, &ANoWheelVehicleBase::OnOverlapBegin);
			EnterBoxRightComponent->OnComponentEndOverlap.AddDynamic(this, &ANoWheelVehicleBase::OnOverlapEnd);
		}

		if (IsValid(EnterBoxLeftComponent))
		{
			EnterBoxLeftComponent->OnComponentBeginOverlap.AddDynamic(this, &ANoWheelVehicleBase::OnOverlapBegin);
			EnterBoxLeftComponent->OnComponentEndOverlap.AddDynamic(this, &ANoWheelVehicleBase::OnOverlapEnd);
		}
	}

	PrevVelocity = Velocity;
	PrevAcceleration = Acceleration;
	PrevPosition = GetActorLocation();
}

void ANoWheelVehicleBase::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(ANoWheelVehicleBase, RepVehicleInfo, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(ANoWheelVehicleBase, RepVehicleDataKey, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(ANoWheelVehicleBase, TargetBladeRotationSpeed, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(ANoWheelVehicleBase, CurrentTurn, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(ANoWheelVehicleBase, CurrentRoll, SharedParams);
}

// Called every frame
void ANoWheelVehicleBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CurrentBladeRotationSpeed = FMath::FInterpTo(CurrentBladeRotationSpeed, TargetBladeRotationSpeed, DeltaTime, 1.f);

	if (HasAuthority())
	{
		Velocity = (GetActorLocation() - PrevPosition) * DeltaTime;
		Acceleration = (Velocity - PrevVelocity) * DeltaTime;
		Acceleration = Acceleration + FVector_NetQuantize(0, 0, -981.f);
		Acceleration = Acceleration + (GetActorUpVector() * GetCurrentLift());
		Acceleration = Acceleration + (FVector_NetQuantize(GetActorUpVector().X, GetActorUpVector().Y, 0) * (GetCurrentLift() * 2.0f));

		FVector_NetQuantize AddVec = (Velocity + (Acceleration * DeltaTime));
		SM_AddActorWorldOffset(AddVec, true);

		PrevVelocity = Velocity;
		PrevAcceleration = Acceleration;
		PrevPosition = GetActorLocation();
	}
}

// Called to bind functionality to input
void ANoWheelVehicleBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ANoWheelVehicleBase::OnRep_VehicleInfo()
{
	bLoadCompleted = false;

	if (bUseAsyncLoad)
	{
		LoadAssetAsync(FJSMVehicleDataLoadComplete::CreateUObject(this, &ANoWheelVehicleBase::InitializeVehicleResources));
	}
	else
	{
		LoadSynchronous();
		InitializeVehicleResources();
	}
}

void ANoWheelVehicleBase::LoadAssetAsync(FJSMVehicleDataLoadComplete LoadCompleteDelegate)
{
	if (AssetLoadRequest.AsyncLoadHandle != nullptr)
	{
		return;
	}

	UJSMVehicleDataAsset* VehicleDataAsset = GetVehicleDataAsset();

	if (!IsValid(VehicleDataAsset))
	{
		return;
	}

	if (VehicleDataAsset->GetObjectPathsToLoad().Num() == 0)
	{
		LoadCompleteDelegate.ExecuteIfBound();
	}
	else
	{
		FStreamableDelegate StreamableDelegate = FStreamableDelegate::CreateUObject(this, &ANoWheelVehicleBase::LoadAssetCompleted);
		AssetLoadRequest.CompleteDelegate = LoadCompleteDelegate;
		AssetLoadRequest.AsyncLoadHandle = UAssetLoader::AssetLoadAsyncStart(VehicleDataAsset->GetObjectPathsToLoad(), StreamableDelegate);
	}
}

void ANoWheelVehicleBase::LoadAssetCompleted()
{
	AssetLoadRequest.CompleteDelegate.ExecuteIfBound();
	AssetLoadRequest.AsyncLoadHandle.Reset();
}

void ANoWheelVehicleBase::InitializeVehicleResources()
{
	UJSMVehicleDataAsset* VehicleDataAsset = GetVehicleDataAsset();
	if (IsValid(VehicleDataAsset))
	{
		if (VehicleDataAsset->ItemStaticMesh.IsValid())
		{
			VehicleStaticMeshComponent->SetStaticMesh(VehicleDataAsset->ItemStaticMesh.Get());
		}
	}
	bLoadCompleted = true;
	OnLoadComplete.ExecuteIfBound();
}

void ANoWheelVehicleBase::LoadSynchronous()
{
	if (AssetLoadRequest.AsyncLoadHandle != nullptr)
	{
		return;
	}

	UJSMVehicleDataAsset* VehicleDataAsset = GetVehicleDataAsset();
	if (IsValid(VehicleDataAsset))
	{
		VehicleDataAsset->LoadSynchronous();
	}
}

void ANoWheelVehicleBase::SS_CreateVehicleInfo()
{
	if (!HasAuthority())
	{
		return;
	}

	UJSMGameInstance* GameInstance = UJSMGameInstance::GetGameInstance();
	check(GameInstance);

	UJSMItemManagerSubsystem* ItemManager = JSMFL::GetGameInstanceSubsystem<UJSMItemManagerSubsystem>();
	if (IsValid(ItemManager) == false)
	{
		return;
	}

	UVehicleInfo* VehicleInfo = ItemManager->CreateJSMVehicleInfo(RepVehicleDataKey, this);
	if (IsValid(VehicleInfo) == false)
	{
		return;
	}

	SS_SetRepVehicleInfo(VehicleInfo);
}

void ANoWheelVehicleBase::SS_SetRepVehicleInfo(UVehicleInfo* InVehicleInfo)
{
	if (!HasAuthority())
	{
		return;
	}

	MARK_PROPERTY_DIRTY_FROM_NAME(ANoWheelVehicleBase, RepVehicleInfo, this);
	RepVehicleInfo = InVehicleInfo;
	MARK_PROPERTY_DIRTY_FROM_NAME(ANoWheelVehicleBase, RepVehicleDataKey, this);
	RepVehicleDataKey = InVehicleInfo->GetRepVehicleDataKey();
	OnRep_VehicleInfo();
}

UVehicleInfo* ANoWheelVehicleBase::GetRepVehicleInfo() const
{
	return RepVehicleInfo;
}

UJSMVehicleDataAsset* ANoWheelVehicleBase::GetVehicleDataAsset() const
{
	if (IsValid(RepVehicleInfo) && RepVehicleInfo->GetVehicleDataAsset().IsValid())
	{
		return RepVehicleInfo->GetVehicleDataAsset().Get();
	}
	return nullptr;
}

bool ANoWheelVehicleBase::ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);
	if (RepVehicleInfo != nullptr)
	{
		wrote |= Channel->ReplicateSubobject(RepVehicleInfo, *Bunch, *RepFlags);
	}
	return wrote;
}

bool ANoWheelVehicleBase::IsOverlapPlayer(const uint32& Id) const
{
	if (OverlapPlayers.Find(Id) != nullptr)
	{
		return true;
	}
	return false;
}

void ANoWheelVehicleBase::EnterVehicle(class ACharacterBase* InEnterPlayer)
{
	if (!IsValid(InEnterPlayer))
	{
		return;
	}

	FName SeatName = FName(TEXT("None"));
	for (auto& Seat: SeatArray)
	{
		FSeatInfo SeatInfo = Seat.Value;
		if (IsValid(SeatInfo.Character))
		{
			continue;
		}
		else
		{
			SeatName = Seat.Key;
			break;
		}
	}

	if (SeatName.Compare(TEXT("None")) == 0)
	{
		return;
	}

	InEnterPlayer->AttachToActor(this, FAttachmentTransformRules::SnapToTargetIncludingScale, SeatName);
	OverlapPlayers.Remove(InEnterPlayer->GetUniqueID());

	APlayerController* EnterPlayerController = Cast<APlayerController>(InEnterPlayer->GetController());
	if (IsValid(EnterPlayerController))
	{
		if (DriverID == 0)
		{
			DriverID = EnterPlayerController->GetUniqueID();
			EnterPlayerController->Possess(this);
		}
		else
		{
			EnterPlayerController->SetViewTargetWithBlend(this);
		}
		SeatArray[SeatName] = FSeatInfo(InEnterPlayer, EnterPlayerController->GetUniqueID());
	}
	//else
	//{
	//	SeatArray[SeatName] = FSeatInfo(InEnterPlayer, InEnterPlayer->GetUniqueID());
	//}
}

void ANoWheelVehicleBase::ExitVehicle(const uint32& InID)
{
	CS_ExitVehicle(InID);
}

void ANoWheelVehicleBase::OnOverlapBegin(UPrimitiveComponent* OpverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APlayerCharacter* Character = Cast<APlayerCharacter>(OtherActor);
	if (!IsValid(Character))
	{
		return;
	}

	OverlapPlayers.Emplace(Character->GetUniqueID(), Character);
}

void ANoWheelVehicleBase::OnOverlapEnd(class UPrimitiveComponent* OpverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	APlayerCharacter* Character = Cast<APlayerCharacter>(OtherActor);
	if (!IsValid(Character))
	{
		return;
	}
	OverlapPlayers.Remove(Character->GetUniqueID());
}

void ANoWheelVehicleBase::CS_ExitVehicle_Implementation(const uint32& InID)
{
	ForceNetUpdate();
	FName SeatName = FName(TEXT("None"));
	for (auto& Seat : SeatArray)
	{
		FSeatInfo SeatInfo = Seat.Value;
		if (SeatInfo.ControlID != InID)
		{
			continue;
		}
		else
		{
			SeatName = Seat.Key;
			break;
		}
	}

	if (SeatName.Compare(TEXT("None")) == 0)
	{
		return;
	}

	ACharacterBase* SeatCharacter = SeatArray[SeatName].Character;
	if (IsValid(SeatCharacter))
	{
		SeatCharacter->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		if ((DriverID == InID))
		{
			SeatCharacter->SetActorLocation(EnterBoxRightComponent->GetComponentLocation());
			APlayerController* LocalController = Cast<APlayerController>(GetController());
			if (IsValid(LocalController))
			{
				LocalController->Possess(SeatCharacter);
			}
			DriverID = 0;
		}
		else
		{
			(SeatCharacter)->SetActorLocation(EnterBoxLeftComponent->GetComponentLocation());
			APlayerController* PassengerCtr = Cast<APlayerController>(SeatCharacter->GetController());
			if (IsValid(PassengerCtr))
			{
				PassengerCtr->SetViewTargetWithBlend(SeatCharacter);
			}
		}
		(SeatCharacter)->ExitVehicle();
	}

	SeatArray[SeatName] = FSeatInfo();
}

void ANoWheelVehicleBase::ForwardMovementAction(const float& InValue)
{
	CS_ForwardMovementAction(InValue);
}

//HeliThrottle - Forward, HeliTyurn - CameraRight Turn, HeliPitch - CameraUp
void ANoWheelVehicleBase::CS_ForwardMovementAction_Implementation(const float& InValue)
{
	UJSMVehicleDataAsset* VehicleDataAsset = GetVehicleDataAsset();
	if (!IsValid(VehicleDataAsset))
	{
		return;
	}

	float WorldSecond = GetWorld()->GetDeltaSeconds();
	MARK_PROPERTY_DIRTY_FROM_NAME(ANoWheelVehicleBase, TargetBladeRotationSpeed, this);
	TargetBladeRotationSpeed = FMath::Clamp(((WorldSecond * VehicleDataAsset->ThrottleUpSpeed * InValue) + TargetBladeRotationSpeed), 0, VehicleDataAsset->MaxBladeRotationSpeed);
}

void ANoWheelVehicleBase::RightMovementAction(const float& InValue)
{

}

float ANoWheelVehicleBase::GetCurrentLift() const
{
	UJSMVehicleDataAsset* VehicleDataAsset = GetVehicleDataAsset();
	if (!IsValid(VehicleDataAsset) || !IsValid(VehicleDataAsset->LiftCurve.Get()))
	{
		return 0.f;
	}

	return VehicleDataAsset->LiftCurve.Get()->GetFloatValue(CurrentBladeRotationSpeed);
}

void ANoWheelVehicleBase::CameraRightAction(const float& InValue)
{
	CS_CameraRightAction(InValue);
}

void ANoWheelVehicleBase::CS_CameraRightAction_Implementation(const float& InValue)
{
	MARK_PROPERTY_DIRTY_FROM_NAME(ANoWheelVehicleBase, CurrentTurn, this);
	CurrentTurn = FMath::FInterpTo(CurrentTurn, (GetTurnSpeed() * InValue), GetWorld()->GetDeltaSeconds(), 2.f);
	FRotator AddRot = FRotator(0, CurrentTurn, 0);
	FRotator SetRot = FRotator(GetActorRotation().Pitch, GetActorRotation().Yaw, CurrentTurn * 40.0f);
	SM_CameraRightAction(AddRot, SetRot);
}

void ANoWheelVehicleBase::SM_CameraRightAction_Implementation(const FRotator& AddRot, const FRotator& SetRot)
{
	AddActorWorldRotation(AddRot.Quaternion());
}

void ANoWheelVehicleBase::CameraUpAction(const float& InValue)
{
	CS_CameraUpAction(InValue);
}

void ANoWheelVehicleBase::CS_CameraUpAction_Implementation(const float& InValue)
{
	MARK_PROPERTY_DIRTY_FROM_NAME(ANoWheelVehicleBase, CurrentRoll, this);
	CurrentRoll = FMath::FInterpTo(CurrentRoll, (GetTurnSpeed() * InValue), GetWorld()->GetDeltaSeconds(), 2.f);

	bool bRange = UKismetMathLibrary::InRange_FloatFloat((CurrentRoll + GetActorRotation().Roll), -89.f, 89.f, true, true);
	if (bRange)
	{
		FRotator AddRot = FRotator(0, 0, CurrentRoll);
		SM_CameraUpAction(AddRot);
	}
}

void ANoWheelVehicleBase::SM_CameraUpAction_Implementation(const FRotator& AddRot)
{
	AddActorLocalRotation(AddRot);
}

float ANoWheelVehicleBase::GetTurnSpeed() const
{
	UJSMVehicleDataAsset* VehicleDataAsset = GetVehicleDataAsset();
	if (!IsValid(VehicleDataAsset))
	{
		return 0.f;
	}

	return ((FMath::GetMappedRangeValueClamped(FVector2D(0.0f, 400.0f), FVector2D(0.0f, 1.0f), GetCurrentLift())) * VehicleDataAsset->TurnSpeed);
}

void ANoWheelVehicleBase::SM_AddActorWorldOffset_Implementation(const FVector_NetQuantize& InValue, bool bSweep)
{
	AddActorWorldOffset(InValue, bSweep);
}