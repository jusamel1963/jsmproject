// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Weapons/RangeWeapon.h"
#include "Data/DataAssets/ItemData/JSMRangeWeaponDataAsset.h"
#include "Net/UnrealNetwork.h"
#include "Net/Core/PushModel/PushModel.h"

ARangeWeapon::ARangeWeapon()
	:CurrentMagAmmo(0), CurrentBagAmmo(0)
{
}

void ARangeWeapon::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(ARangeWeapon, CurrentMagAmmo, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(ARangeWeapon, CurrentBagAmmo, SharedParams);
}

void ARangeWeapon::InitializeWeaponResources()
{
	Super::InitializeWeaponResources();
	UJSMRangeWeaponDataAsset* WeaponDataAsset = Cast<UJSMRangeWeaponDataAsset>(GetWeaponDataAsset());
	if (IsValid(WeaponDataAsset))
	{
		MARK_PROPERTY_DIRTY_FROM_NAME(ARangeWeapon, CurrentMagAmmo, this);
		CurrentMagAmmo = WeaponDataAsset->MagCapacity;
		MARK_PROPERTY_DIRTY_FROM_NAME(ARangeWeapon, CurrentBagAmmo, this);
		CurrentBagAmmo = WeaponDataAsset->BagCapacity;
	}
}

void ARangeWeapon::CurrentMagAmmoCalculate(const int& InCount, const bool& bIncrease)
{
	MARK_PROPERTY_DIRTY_FROM_NAME(ARangeWeapon, CurrentMagAmmo, this);
	if (bIncrease)
	{
		CurrentMagAmmo += InCount;
	}
	else
	{
		CurrentMagAmmo = ((CurrentMagAmmo - InCount) < 0) ? 0 : (CurrentMagAmmo - InCount);
	}
}

void ARangeWeapon::CurrentBagAmmoCalculate(const int& InCount, const bool& bIncrease)
{
	MARK_PROPERTY_DIRTY_FROM_NAME(ARangeWeapon, CurrentBagAmmo, this);
	if (bIncrease)
	{
		CurrentBagAmmo += InCount;
	}
	else
	{
		CurrentBagAmmo = ((CurrentBagAmmo - InCount) < 0) ? 0 : (CurrentBagAmmo - InCount);
	}
}

void ARangeWeapon::Reload()
{
	uint8 MagCapacity = GetMagAmmoCapacity();
	int AddAmmo = (CurrentBagAmmo >= (MagCapacity - CurrentMagAmmo)) ? (MagCapacity - CurrentMagAmmo) : CurrentBagAmmo;
	CurrentBagAmmoCalculate(AddAmmo, false);
	CurrentMagAmmoCalculate(AddAmmo, true);
	UE_LOG(LogTemp, Warning, TEXT("FireAmmoCount CurrentMagAmmo : %d"), CurrentMagAmmo);
	UE_LOG(LogTemp, Warning, TEXT("FireAmmoCount CurrentBagAmmo : %d"), CurrentBagAmmo);
}

void ARangeWeapon::FireConsumeAmmo()
{
	UJSMRangeWeaponDataAsset* WeaponDataAsset = Cast<UJSMRangeWeaponDataAsset>(GetWeaponDataAsset());
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	uint8 FireAmmoCount = WeaponDataAsset->FireAmmoCount;
	CurrentMagAmmoCalculate(FireAmmoCount, false);

	UE_LOG(LogTemp, Warning, TEXT("FireAmmoCount CurrentMagAmmo : %d"), CurrentMagAmmo);
	UE_LOG(LogTemp, Warning, TEXT("FireAmmoCount CurrentBagAmmo : %d"), CurrentBagAmmo);
}

uint8 ARangeWeapon::GetMagAmmoCapacity() const
{
	UJSMRangeWeaponDataAsset* DataAsset = Cast<UJSMRangeWeaponDataAsset>(GetWeaponDataAsset());
	if (!IsValid(DataAsset))
	{
		return 0;
	}
	return DataAsset->MagCapacity;
}

uint8 ARangeWeapon::GetBagAmmoCapacity() const
{
	UJSMRangeWeaponDataAsset* DataAsset = Cast<UJSMRangeWeaponDataAsset>(GetWeaponDataAsset());
	if (!IsValid(DataAsset))
	{
		return 0;
	}
	return DataAsset->BagCapacity;
}