// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Data/Common/GameEnum.h"
#include "GameplayTagContainer.h"
#include "Objects/Items/JSMItemInfo.h"
#include "Particles/ParticleSystem.h"
#include "JSMItemDataAsset.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class PROJECTJSM_API UJSMItemDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UJSMItemDataAsset()
	{
		DisplayName = FText();
		Description = FText();
		ItemType = EJSMItemType::None;
		Grade = EJSMGrade::None;
	}

	// AsyncLoad
	virtual TArray<FSoftObjectPath> GetObjectPathsToLoad()
	{
		TArray<FSoftObjectPath> Paths;
		if (ItemStaticMesh.IsNull() == false)
		{
			if (ItemStaticMesh.IsPending())
			{
				Paths.Add(ItemStaticMesh.ToSoftObjectPath());
			}
		}

		if (ItemSkeletalMesh.IsNull() == false)
		{
			if (ItemSkeletalMesh.IsPending())
			{
				Paths.Add(ItemSkeletalMesh.ToSoftObjectPath());
			}
		}

		if (Icon.IsNull() == false)
		{
			if (Icon.IsPending())
			{
				Paths.Add(Icon.ToSoftObjectPath());
			}
		}

		if (IconBig.IsNull() == false)
		{
			if (IconBig.IsPending())
			{
				Paths.Add(IconBig.ToSoftObjectPath());
			}
		}

		if (ItemActorClass.IsNull() == false)
		{
			if (ItemActorClass.IsPending())
			{
				Paths.Add(ItemActorClass.ToSoftObjectPath());
			}
		}
		return Paths;
	}

	UFUNCTION(BlueprintCallable, Category = "ItemData")
		virtual void LoadSynchronous()
	{
		if (ItemStaticMesh.IsNull() == false)
		{
			ItemStaticMesh.LoadSynchronous();
		}

		if (ItemSkeletalMesh.IsNull() == false)
		{
			ItemSkeletalMesh.LoadSynchronous();
		}

		if (Icon.IsNull() == false)
		{
			Icon.LoadSynchronous();
		}

		if (IconBig.IsNull() == false)
		{
			IconBig.LoadSynchronous();
		}
	}

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	FText DisplayName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	FText Description;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	TSoftObjectPtr<UStaticMesh> ItemStaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	TSoftObjectPtr<USkeletalMesh> ItemSkeletalMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	TSoftObjectPtr<UTexture2D> Icon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	TSoftObjectPtr<UTexture2D> IconBig;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	TSoftClassPtr<class AJSMItemActor> ItemActorClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	EJSMItemType ItemType;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ItemData")
	EJSMGrade Grade;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData", meta = (Categories = "Event"))
	FGameplayTag OverlapEventTag;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	TSubclassOf<class UJSMGameplayEffect> PickupEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	float VolumeRadius;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	float NoiseLoudness;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	float NoiseDistance;
};
