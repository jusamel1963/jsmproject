// Copyright JSM Studio, Inc. All Rights Reserved.

#include "Objects/Character/State/JSMCharacterStateHandler.h"

bool FJSMCharacterInitializeStateHandler::IsTickable() const
{
	EJSMCharacterInitializeStateID CurrentStateID = GetCurrentStateID();
	return CurrentStateID != EJSMCharacterInitializeStateID::None && CurrentStateID != EJSMCharacterInitializeStateID::Done;
}

FJSMCharacterInitializeState& FJSMCharacterInitializeStateHandler::AddCharacterState(EJSMCharacterInitializeStateID StateID)
{
	TSharedRef<FJSMCharacterInitializeState> State = MakeShared<FJSMCharacterInitializeState>(StateID);
	AddState(State);
	return State.Get();
}
