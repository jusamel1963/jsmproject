// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Weapons/ShellEjectBase.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "GameFramework/RotatingMovementComponent.h"

FName AShellEjectBase::SceneComponentName(TEXT("SceneComp"));
FName AShellEjectBase::StaticMeshComponentName(TEXT("StaticMeshComp"));
FName AShellEjectBase::ProjectileMovementComponentName(TEXT("ProjectileMovementComp"));
FName AShellEjectBase::ProjectileRotatingMovementComponentName(TEXT("ProjectileRotatingMovementComp"));

// Sets default values
AShellEjectBase::AShellEjectBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
	SetReplicateMovement(true);

	USceneComponent* SceneComp = CreateDefaultSubobject<USceneComponent>(SceneComponentName);
	RootComponent = SceneComp;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(StaticMeshComponentName);
	StaticMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StaticMeshComp->SetupAttachment(RootComponent);
	StaticMeshComp->SetLightingChannels(false, true, false);
	StaticMeshComp->SetRelativeRotation(FRotator(-90, 0, 0));

	ProjectileMovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(ProjectileMovementComponentName);
	ProjectileMovementComp->InitialSpeed = 600.f;
	ProjectileMovementComp->MaxSpeed = 300.0f;
	ProjectileMovementComp->ProjectileGravityScale = 5.0f;
	ProjectileMovementComp->Velocity = FVector_NetQuantize(-300.0f, 0, 0);

	ProjectileRotatingMovementComp = CreateDefaultSubobject<URotatingMovementComponent>(ProjectileRotatingMovementComponentName);
	ProjectileRotatingMovementComp->RotationRate = FRotator(150, 300, 0);
}

// Called when the game starts or when spawned
void AShellEjectBase::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		FTimerHandle DelayHandle;
		float DelayTime = 1.5f;
		const TWeakObjectPtr<AShellEjectBase> WeakPtr(this);
		GetWorld()->GetTimerManager().SetTimer(DelayHandle, FTimerDelegate::CreateLambda([WeakPtr]()
		{
			if (WeakPtr.IsValid())
			{
				WeakPtr->Destroy();
			}
		}), DelayTime, false);
	}
}

// Called every frame
void AShellEjectBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AShellEjectBase::SM_InitResource_Implementation(UStaticMesh* NewMesh)
{
	if (IsValid(StaticMeshComp))
	{
		StaticMeshComp->SetStaticMesh(NewMesh);
	}
}