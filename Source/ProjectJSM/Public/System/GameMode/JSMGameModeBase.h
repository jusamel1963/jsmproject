// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "JSMGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API AJSMGameModeBase : public AGameMode
{
	GENERATED_BODY()
	
};
