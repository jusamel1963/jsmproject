// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InteractiveInterface.generated.h"

UINTERFACE(MinimalAPI)
class UInteractiveInterface : public UInterface
{
	GENERATED_BODY()
};

//Actor->Implements<UInteractiveInterface>()
class PROJECTJSM_API IInteractiveInterface
{
	GENERATED_BODY()

public:
	virtual void TriggerInteractive() PURE_VIRTUAL(TriggerInteractive, return;);
	virtual bool IsOverlapPlayer(const uint32& Id) const PURE_VIRTUAL(IsOverlapPlayer, return false;);
};
