// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Weapons/ProjectTileBase.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Objects/Character/CharacterBase.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "Data/DataAssets/ItemData/JSMProjectileWeaponDataAsset.h"

FName AProjectTileBase::SceneComponentName(TEXT("SceneComp"));
FName AProjectTileBase::StaticMeshComponentName(TEXT("StaticMeshComp"));
FName AProjectTileBase::ProjectileMovementComponentName(TEXT("ProjectileMovementComp"));
FName AProjectTileBase::VolumeComponentName(TEXT("VolumeComp"));
FName AProjectTileBase::ParticleSystemComponentName(TEXT("ParticleSystemComp"));

AProjectTileBase::AProjectTileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
	SetReplicateMovement(true);
	USceneComponent* SceneComp = CreateDefaultSubobject<USceneComponent>(SceneComponentName);
	RootComponent = SceneComp;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(StaticMeshComponentName);
	StaticMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StaticMeshComp->SetupAttachment(RootComponent);
	StaticMeshComp->SetLightingChannels(false, true, false);

	ParticleSystemComp = CreateDefaultSubobject<UParticleSystemComponent>(ParticleSystemComponentName);
	ParticleSystemComp->SetupAttachment(RootComponent);

	ProjectileMovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(ProjectileMovementComponentName);
	ProjectileMovementComp->InitialSpeed = 3000.f;
	ProjectileMovementComp->MaxSpeed = 3000.f;
	ProjectileMovementComp->Velocity = FVector_NetQuantize(3000.f, 0, 0);

	VolumeComponent = CreateDefaultSubobject<USphereComponent>(VolumeComponentName);
	if (IsValid(VolumeComponent))
	{
		VolumeComponent->SetupAttachment(RootComponent);
		VolumeComponent->SetSphereRadius(5);
	}
}

void AProjectTileBase::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		if (IsValid(VolumeComponent))
		{
			VolumeComponent->OnComponentBeginOverlap.AddDynamic(this, &AProjectTileBase::OnOverlapBegin);
		}
	}
}

void AProjectTileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AProjectTileBase::SM_InitResource_Implementation(class ACharacterBase* InOwnChar, class UParticleSystem* InProjectileHitEffect, class USoundCue* InProjectileHitSound, class UStaticMesh* InStaticMesh, const float& InHitRadius, const float& InImpulse)
{
	OwnChar = InOwnChar;
	ProjectileHitEffect = InProjectileHitEffect;
	ProjectileHitSound = InProjectileHitSound;
	HitRadius = InHitRadius;
	Impulse = InImpulse;
	if (IsValid(StaticMeshComp))
	{
		StaticMeshComp->SetStaticMesh(InStaticMesh);
	}
}

void AProjectTileBase::OnOverlapBegin(UPrimitiveComponent* OpverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!HasAuthority())
	{
		return;
	}

	if (!bPendingKill && IsValid(ParticleSystemComp))
	{
		bPendingKill = true;
		VolumeComponent->SetSphereRadius(HitRadius);
		SM_OverlapProjectTile();

		FTimerHandle DelayHandle;
		const TWeakObjectPtr<AProjectTileBase> WeakPtr(this);
		GetWorld()->GetTimerManager().SetTimer(DelayHandle, FTimerDelegate::CreateLambda([WeakPtr]()
		{
			if (WeakPtr.IsValid())
			{
				WeakPtr->Destroy();
			}
		}), 0.5f, false);
	}

	if (IsValid(OtherActor))
	{
		UE_LOG(LogTemp, Warning, TEXT("AProjectTileBase OnHitBegin OtherActor: %s"), *OtherActor->GetName());
	}

	ACharacterBase* HitChar = Cast<ACharacterBase>(OtherActor);
	if (!OwnChar.IsValid() || !IsValid(HitChar))
	{
		return;
	}
		
	UWeaponCtrlComponent* WeaponCtrlComp = OwnChar->GetWeaponCtrl();
	if (!IsValid(WeaponCtrlComp))
	{
		return;
	}

	FVector_NetQuantize HitLoc = HitChar->GetMesh()->GetSocketLocation(ACharacterBase::MeshBodySocketName);
	FVector_NetQuantize ImpulseValue = (HitLoc - GetActorLocation()).GetSafeNormal2D() * Impulse;
	WeaponCtrlComp->ApplyWeaponAbilityEffect(HitChar, ImpulseValue, HitLoc, ACharacterBase::MeshBodySocketName);
}

void AProjectTileBase::SM_OverlapProjectTile_Implementation()
{
	ParticleSystemComp->SetTemplate(ProjectileHitEffect);
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileHitSound, GetActorLocation());
}