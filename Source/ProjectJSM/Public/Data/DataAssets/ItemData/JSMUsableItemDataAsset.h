// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Data/DataAssets/ItemData/JSMItemDataAsset.h"
#include "JSMUsableItemDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UJSMUsableItemDataAsset : public UJSMItemDataAsset
{
	GENERATED_BODY()
public:
	UJSMUsableItemDataAsset()
	{
		ItemType = EJSMItemType::UsableItem;
	}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Usable Item")
	float Amount;
};
