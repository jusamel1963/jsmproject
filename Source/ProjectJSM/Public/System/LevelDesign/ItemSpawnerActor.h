// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ItemSpawnerActor.generated.h"

USTRUCT(Blueprintable)
struct FItemSpawnData
{
	GENERATED_BODY()
public:

	UPROPERTY(EditInstanceOnly)
	FName ItemKeyName;
	UPROPERTY(EditInstanceOnly, meta = (ClampMin = "1"))
	int32 ItemAmount = 1;
};

UCLASS()
class PROJECTJSM_API AItemSpawnerActor : public AActor
{
	GENERATED_BODY()
	
public:	
	AItemSpawnerActor();
	void SetRespawnTimer();
	virtual void Execute();
protected:
	virtual void BeginPlay() override;
	void SpawnItem();
	UFUNCTION()
	void OnSpawnTimer();
	UFUNCTION()
	void OnDestroyActor(AActor* DestroyedActor);
public:	
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(EditAnywhere, Category = "Spawn Config")
	TArray<FItemSpawnData> SpawnData;
	UPROPERTY(EditAnywhere, Category = "Spawn Config", meta = (EditCondition = "bRespawnable", ShortTooltip = " == 0 : Only once, > n : n time respawn "))
	float RespawnTimeInterval = 0.0f;
	UPROPERTY(EditAnywhere, Category = "Spawn Config")
	bool bRespawnable = false;
protected:
	UPROPERTY(Transient)
	TArray<AActor*>	SpawnedActors;
	FTimerHandle SpawnerTimerHandle;
};
