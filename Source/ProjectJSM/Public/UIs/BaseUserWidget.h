// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BaseUserWidget.generated.h"

UCLASS()
class PROJECTJSM_API UBaseUserWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetOwnIDAndController(uint32 InID, class AJSMPlayerController* InController);
protected:
	uint32 OwnID;
	TWeakObjectPtr<AJSMPlayerController> OwnController;
};
