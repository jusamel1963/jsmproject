// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "JSMReplicatedObjectInterface.generated.h"

UINTERFACE(MinimalAPI)
class UJSMReplicatedObjectInterface : public UInterface
{
	GENERATED_BODY()
};


class PROJECTJSM_API IJSMReplicatedObjectInterface
{
	GENERATED_BODY()

public:
	virtual bool HasAuthority() PURE_VIRTUAL(HasAuthority, return false;);
};
