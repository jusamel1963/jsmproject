// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Character/CharacterBase.h"
#include "AICharacter.generated.h"

USTRUCT(BlueprintType)
struct PROJECTJSM_API FAIRecentRender
{
	GENERATED_BODY()

public:
	UPROPERTY()
	uint32 AIID;

	UPROPERTY()
	bool bRecentRender;
};


UCLASS()
class PROJECTJSM_API AAICharacter : public ACharacterBase
{
	GENERATED_BODY()
public:
	AAICharacter(const FObjectInitializer& ObjectInitializer);
	virtual void Tick(float DeltaTime) override;
	virtual void FireWeapon(FHitResult& OutHitResult, FVector_NetQuantize& EndLoc, const float& LineDistance, const float& TraceRadius, const FVector_NetQuantize& Offset, const FVector_NetQuantize& MuzzleLoc, const FVector_NetQuantize& MuzzleEndLoc = FVector_NetQuantize()) override;
	class UJSMAIDataAsset* GetAIDataAsset() const;
	FVector_NetQuantize NextPatrolLocation();
	void SetPatrolPoints(TArray<TObjectPtr<class AActor>> InPatrolPoints);
	virtual void RagdollToggle() override;
	void RefershAICharacter();

	UFUNCTION(NetMulticast, Reliable)
	void SM_RefershAICharacter(const EALSRotationMode& InRotateMode);

	virtual void DieCharacter(ACharacterBase* killer) override;
protected:
	virtual void BeginPlay() override;
	virtual void InitializeCharacter(const FCharacterDesc* charDesc) override;

protected:
	UPROPERTY(EditInstanceOnly, Transient, meta = (EditCondition = "bPatrol"))
	TArray<TObjectPtr<class AActor>> PatrolPoints;

	int32 CurrentPatrolIdx = 0;
	bool bWaitDestroy = false;
	const float DestroyDelayTime = 10.0f;
};
