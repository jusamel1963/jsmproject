// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Weapons/RangeWeapon.h"
#include "GrenadeWeapon.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API AGrenadeWeapon : public ARangeWeapon
{
	GENERATED_BODY()
	
};
