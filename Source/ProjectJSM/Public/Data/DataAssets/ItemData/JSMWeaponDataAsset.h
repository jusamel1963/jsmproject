// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Data/DataAssets/ItemData/JSMItemDataAsset.h"
#include "Library/ALSCharacterEnumLibrary.h"
#include "Data/Common/GameEnum.h"
#include "JSMWeaponDataAsset.generated.h"

UCLASS()
class PROJECTJSM_API UJSMWeaponDataAsset : public UJSMItemDataAsset
{
	GENERATED_BODY()
public:
	UJSMWeaponDataAsset()
	{
		ItemType = EJSMItemType::Weapon;
		WeaponCategory = EALSOverlayState::Default;
		WeaponAttachType = EWeaponAttachType::None;
		MinScopeRange = 10.0f;
		MaxScopeRange = 90.0f;
		bScope = false;
		Impulse = 20000.f;
	}

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	EWeaponAttachType WeaponAttachType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	EALSOverlayState WeaponCategory;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSoftClassPtr<class AWeapon> WeaponBPClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TArray<TSubclassOf<class UJSMGameplayAbility>> AbilitiesWhenEquip;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<class UGameplayEffect> DamageEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSoftObjectPtr<UTexture2D> CrossHair;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TArray<TSoftObjectPtr<class UParticleSystem>> HitParticleArray;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	float MinScopeRange;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	float MaxScopeRange;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	bool bScope;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSoftObjectPtr<UStaticMesh> ShellEjectMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSoftClassPtr<class AShellEjectBase> ShellEjectBaseBPClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TArray<TSoftObjectPtr<UMaterialInterface>> HitAttachedMaterialArray;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSoftObjectPtr<class UAnimMontage> AM_UnDrawWeapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSoftObjectPtr<UAnimMontage> AM_DrawWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
	float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
	float Impulse;

	// AsyncLoad
	virtual TArray<FSoftObjectPath> GetObjectPathsToLoad() override
	{
		TArray<FSoftObjectPath> Paths = Super::GetObjectPathsToLoad();

		if (WeaponBPClass.IsNull() == false)
		{
			if (WeaponBPClass.IsPending())
			{
				Paths.Add(WeaponBPClass.ToSoftObjectPath());
			}
		}

		if (CrossHair.IsNull() == false)
		{
			if (CrossHair.IsPending())
			{
				Paths.Add(CrossHair.ToSoftObjectPath());
			}
		}

		for (auto HitParticle : HitParticleArray)
		{
			if (HitParticle.IsNull() == false)
			{
				if (HitParticle.IsPending())
				{
					Paths.Add(HitParticle.ToSoftObjectPath());
				}
			}
		}

		if (ShellEjectMesh.IsNull() == false)
		{
			if (ShellEjectMesh.IsPending())
			{
				Paths.Add(ShellEjectMesh.ToSoftObjectPath());
			}
		}

		if (ShellEjectBaseBPClass.IsNull() == false)
		{
			if (ShellEjectBaseBPClass.IsPending())
			{
				Paths.Add(ShellEjectBaseBPClass.ToSoftObjectPath());
			}
		}

		for (auto HitAttachedMaterial : HitAttachedMaterialArray)
		{
			if (HitAttachedMaterial.IsNull() == false)
			{
				if (HitAttachedMaterial.IsPending())
				{
					Paths.Add(HitAttachedMaterial.ToSoftObjectPath());
				}
			}
		}

		if (AM_UnDrawWeapon.IsNull() == false)
		{
			if (AM_UnDrawWeapon.IsPending())
			{
				Paths.Add(AM_UnDrawWeapon.ToSoftObjectPath());
			}
		}

		if (AM_DrawWeapon.IsNull() == false)
		{
			if (AM_DrawWeapon.IsPending())
			{
				Paths.Add(AM_DrawWeapon.ToSoftObjectPath());
			}
		}

		return Paths;
	}

	virtual void LoadSynchronous() override
	{
		Super::LoadSynchronous();

		if (CrossHair.IsNull() == false)
		{
			CrossHair.LoadSynchronous();
		}

		for (auto HitParticle : HitParticleArray)
		{
			if (HitParticle.IsNull() == false)
			{
				HitParticle.LoadSynchronous();
			}
		}

		if (ShellEjectMesh.IsNull() == false)
		{
			ShellEjectMesh.LoadSynchronous();
		}

		for (auto HitAttachedMaterial : HitAttachedMaterialArray)
		{
			if (HitAttachedMaterial.IsNull() == false)
			{
				HitAttachedMaterial.LoadSynchronous();
			}
		}

		if (AM_UnDrawWeapon.IsNull() == false)
		{
			AM_UnDrawWeapon.LoadSynchronous();
		}

		if (AM_DrawWeapon.IsNull() == false)
		{
			AM_DrawWeapon.LoadSynchronous();
		}
	}
};
