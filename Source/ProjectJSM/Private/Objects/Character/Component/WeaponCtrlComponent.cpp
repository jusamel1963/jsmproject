// Copyright JSM Studio, Inc. All Rights Reserved.

#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "Objects/Character/CharacterBase.h"
#include "Data/DataAssets/ItemData/JSMWeaponDataAsset.h"
#include "Data/DataAssets/ItemData/JSMProjectileWeaponDataAsset.h"
#include "Objects/Character/Component/JSMInventoryComponent.h"
#include "Statics/JSMClientStatics.h"
#include "Objects/Weapons/Weapon.h"
#include "Objects/Weapons/RangeWeapon.h"
#include "Objects/Weapons/ThrowableWeapon.h"
#include "Objects/Character/JSMCharacterEvent.h"
#include "Objects/Items/JSMItemInfo.h"
#include "Objects/Weapons/GrenadeWeapon.h"
#include "Objects/Weapons/ProjectTileBase.h"
#include "Net/UnrealNetwork.h"
#include "Net/Core/PushModel/PushModel.h"
#include "Objects/Weapons/JSMWeaponInfo.h"
#include "Framework/JSMItemManagerSubsystem.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "Data/DataAssets/ItemData/JSMRangeWeaponDataAsset.h"
#include "Data/DataAssets/ItemData/JSMMeleeWeaponDataAsset.h"
#include "Data/DataAssets/ItemData/JSMThrowableWeaponDataAsset.h"
#include "Data/DataAssets/CharacterData/JSMCharacterDataAsset.h"
#include "Data/DataAssets/ItemData/JSMWeaponAmmoDataAsset.h"
#include "FrameWork/JSMPlayerController.h"
#include "Components/CapsuleComponent.h"
#include "Objects/Weapons/ShellEjectBase.h"
#include "Kismet/KismetMathLibrary.h"
#include "AbilitySystem/JSMAbilitySystemImpl.h"
#include "Objects/Character/AICharacter.h"
#include "Perception/AISense_Hearing.h"
#include "Perception/AISense_Damage.h"
#include "System/AI/BlackboardKeys.h"
#include "FrameWork/AIBaseController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Core/Managers/JSMEventManager.h"
#include "Objects/Weapons/ThrowableWeapon.h"

FName UWeaponCtrlComponent::FireMuzzleSocketName(TEXT("MuzzleFlash"));
FName UWeaponCtrlComponent::ShellEjectSocketName(TEXT("ShellEject"));

UWeaponCtrlComponent::UWeaponCtrlComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	SetIsReplicatedByDefault(true);

	MyWeapons.AddZeroed(static_cast<int32>(EWeaponAttachType::Max) - 1);
	bWantsInitializeComponent = true;
	RepCurrWeaponIdx = -1;
}

void UWeaponCtrlComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(UWeaponCtrlComponent, RepCurrWeaponIdx, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(UWeaponCtrlComponent, RepCurrWeaponKey, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(UWeaponCtrlComponent, RepNewWeaponIdx, SharedParams);
}

void UWeaponCtrlComponent::InitializeComponent()
{
	Super::InitializeComponent();
	OwnerChar = Cast<ACharacterBase>(GetOwner());
}

void UWeaponCtrlComponent::SS_PickupWeapon(const FName& InWeaponKey, const int32& WeaponMagAmmo, const int32& WeaponBagAmmo, const bool& bDefault)
{
	UJSMItemManagerSubsystem* ItemManager = JSMFL::GetGameInstanceSubsystem<UJSMItemManagerSubsystem>();
	UJSMWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMWeaponDataAsset>(InWeaponKey);

	if (!IsValid(ItemManager) || !IsValid(WeaponDataAsset) || !IsValid(OwnerChar))
	{
		return;
	}

	EWeaponAttachType WpAttachType = WeaponDataAsset->WeaponAttachType;
	int32 WeaponIndex = static_cast<int32>(WpAttachType) - 1;

	if (!IsValid(ItemManager) || WeaponDataAsset->WeaponBPClass.IsNull())
	{
		return;
	}
	UClass* bpClass = WeaponDataAsset->WeaponBPClass.LoadSynchronous();
	AWeapon* WeaponActor = ItemManager->SpawnWeaponActor(GetWorld(), bpClass, InWeaponKey, FTransform::Identity);
	ARangeWeapon* NewRangeWeapon = Cast<ARangeWeapon>(WeaponActor);
	if (IsValid(NewRangeWeapon) && !bDefault)
	{
		NewRangeWeapon->SetCurrentMagAmmo(WeaponMagAmmo);
		NewRangeWeapon->SetCurrentBagAmmo(WeaponBagAmmo);
	}

	if (IsValid(MyWeapons[WeaponIndex]))
	{
		SS_DropWeapon(WeaponIndex, false);
	}

	MyWeapons[WeaponIndex] = WeaponActor;
	if (!IsValid(MyWeapons[WeaponIndex]))
	{
		return;
	}
	MyWeapons[WeaponIndex]->SetWeaponHideAndCollision(true, false);

	if (WeaponIndex != RepCurrWeaponIdx)
	{
		EquipWeaponToAttachSocket();
	}

	MARK_PROPERTY_DIRTY_FROM_NAME(UWeaponCtrlComponent, RepCurrWeaponIdx, this);
	RepCurrWeaponIdx = WeaponIndex;
	MARK_PROPERTY_DIRTY_FROM_NAME(UWeaponCtrlComponent, RepCurrWeaponKey, this);
	RepCurrWeaponKey = InWeaponKey;

	if (!IsValid(WeaponDataAsset->ItemSkeletalMesh.Get()))
	{
		WeaponDataAsset->LoadSynchronous();
	}

	SM_EquipWeapon(WeaponDataAsset->ItemSkeletalMesh.Get(), WeaponDataAsset->ItemStaticMesh.Get(), WeaponDataAsset->WeaponAttachType);
	SS_AttachWeaponAbility(RepCurrWeaponKey);
	SC_EquipWeaponAnim(WeaponDataAsset->WeaponCategory);

	WeaponUIInfoUpdate();
}

void UWeaponCtrlComponent::EquipWeaponToAttachSocket()
{
	AWeapon* CurrentWeapon = GetCurrentWeapon();
	if (!IsValid(CurrentWeapon))
	{
		return;
	}

	AThrowableWeapon* ThrowableWeapon = Cast<AThrowableWeapon>(CurrentWeapon);
	UJSMWeaponDataAsset* ExchangeWeaponDataAsset = CurrentWeapon->GetWeaponDataAsset();
	if (IsValid(ExchangeWeaponDataAsset))
	{
		SS_UpdateAttachWeaponMesh(ExchangeWeaponDataAsset->ItemSkeletalMesh.Get(), ExchangeWeaponDataAsset->WeaponAttachType);
	}
	SS_DetachWeaponAbility(RepCurrWeaponKey);
	SC_EquipWeaponAnim(EALSOverlayState::Default);
}

void UWeaponCtrlComponent::SS_DropWeapon(const int32& DropInex, bool bDestroy)
{
	if (!IsValid(MyWeapons[DropInex]) || !IsValid(OwnerChar) || !IsValid(OwnerChar->GetCapsuleComponent()))
	{
		return;
	}

	UJSMItemManagerSubsystem* ItemManager = JSMFL::GetGameInstanceSubsystem<UJSMItemManagerSubsystem>();
	if (!IsValid(ItemManager))
	{
		return;
	}
	FName KeyName = MyWeapons[DropInex]->GetRepWeaponDataKey();
	UJSMWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMWeaponDataAsset>(KeyName);

	if (!IsValid(WeaponDataAsset) || WeaponDataAsset->WeaponBPClass.IsNull())
	{
		return;
	}

	SS_UpdateAttachWeaponMesh(nullptr, WeaponDataAsset->WeaponAttachType);

	UClass* bpClass = WeaponDataAsset->WeaponBPClass.LoadSynchronous();
	float HalfHeight = OwnerChar->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	FVector_NetQuantize ForwardVec = OwnerChar->GetActorForwardVector();
	FTransform DropTransform = OwnerChar->GetActorTransform();
	FVector_NetQuantize DropLoc = DropTransform.GetLocation();
	DropLoc.Z -= HalfHeight;
	DropLoc.Z += 15.0f;
	DropLoc.Y += (ForwardVec.Y * 50.f);
	DropLoc.X += (ForwardVec.X * 50.f);
	DropTransform.SetLocation(DropLoc);
	ARangeWeapon* SpawnWeapon = Cast<ARangeWeapon>(ItemManager->SpawnWeaponActor(GetWorld(), bpClass, KeyName, DropTransform));
	ARangeWeapon* TargetWeapon = Cast<ARangeWeapon>(MyWeapons[DropInex]);
	if (IsValid(SpawnWeapon) && IsValid(TargetWeapon))
	{
		SpawnWeapon->SetCurrentBagAmmo(TargetWeapon->GetCurrentBagAmmo());
		SpawnWeapon->SetCurrentMagAmmo(TargetWeapon->GetCurrentMagAmmo());
	}

	if (bDestroy)
	{
		SpawnWeapon->WaitDestroy(DestroyDelayTime);
	}

	MyWeapons[DropInex]->Destroy();
	MyWeapons[DropInex] = nullptr;
}

void UWeaponCtrlComponent::SC_EquipWeaponAnim_Implementation(const EALSOverlayState& WeaponCategory)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}
	OwnerChar->SetOverlayState(WeaponCategory, false);
}

void UWeaponCtrlComponent::SM_EquipWeapon_Implementation(USkeletalMesh* NewMesh, UStaticMesh* StaticNewMesh, EWeaponAttachType InAttachType, bool bClear)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	OwnerChar->SkeletalMesh->SetSkeletalMeshAsset(nullptr);
	OwnerChar->MeleeWeaponHeld->SetSkeletalMeshAsset(nullptr);
	OwnerChar->ThrowableWeaponHeld->SetStaticMesh(nullptr);

	if (bClear == false)
	{
		if (((InAttachType == EWeaponAttachType::Back) || (InAttachType == EWeaponAttachType::Hip)) && IsValid(OwnerChar->SkeletalMesh))
		{
			OwnerChar->SkeletalMesh->SetSkeletalMeshAsset(NewMesh);
		}
		else if ((InAttachType == EWeaponAttachType::BackMelee) && IsValid(OwnerChar->MeleeWeaponHeld))
		{
			OwnerChar->MeleeWeaponHeld->SetSkeletalMeshAsset(NewMesh);
		}
		else if ((InAttachType == EWeaponAttachType::Throwable) && IsValid(OwnerChar->ThrowableWeaponHeld))
		{
			OwnerChar->ThrowableWeaponHeld->SetStaticMesh(StaticNewMesh);
		}
	}
}

void UWeaponCtrlComponent::SM_AttachHipWeapon_Implementation(USkeletalMesh* NewMesh)
{
	if (!IsValid(OwnerChar) || !IsValid(OwnerChar->HipWeapon))
	{
		return;
	}

	OwnerChar->HipWeapon->SetSkeletalMeshAsset(NewMesh);
}

void UWeaponCtrlComponent::SM_AttachBackWeapon_Implementation(USkeletalMesh* NewMesh)
{
	if (!IsValid(OwnerChar) || !IsValid(OwnerChar->BackWeapon))
	{
		return;
	}

	OwnerChar->BackWeapon->SetSkeletalMeshAsset(NewMesh);
}

void UWeaponCtrlComponent::SM_AttachMeleeBackWeapon_Implementation(USkeletalMesh* NewMesh)
{
	if (!IsValid(OwnerChar) || !IsValid(OwnerChar->HipWeapon))
	{
		return;
	}

	OwnerChar->MeleeWeapon->SetSkeletalMeshAsset(NewMesh);
}

void UWeaponCtrlComponent::CS_ExchangeWeaponUnDraw_Implementation()
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	//���� ���� ����
	UJSMWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMWeaponDataAsset>(RepCurrWeaponKey);
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	SS_UpdateAttachWeaponMesh(WeaponDataAsset->ItemSkeletalMesh.Get(), WeaponDataAsset->WeaponAttachType);
	SM_EquipWeapon(nullptr, nullptr, EWeaponAttachType::None, true);
	SS_DetachWeaponAbility(RepCurrWeaponKey);
	SC_EquipWeaponAnim(EALSOverlayState::Default);

	if ((RepNewWeaponIdx != -1) && IsValid(MyWeapons[RepNewWeaponIdx]))
	{
		UJSMWeaponInfo* DataInfo = MyWeapons[RepNewWeaponIdx]->GetRepWeaponInfo();
		if (!IsValid(DataInfo))
		{
			return;
		}
		MARK_PROPERTY_DIRTY_FROM_NAME(UWeaponCtrlComponent, RepCurrWeaponKey, this);
		RepCurrWeaponKey = DataInfo->GetRepWeaponDataKey();
	}
	else
	{
		MARK_PROPERTY_DIRTY_FROM_NAME(UWeaponCtrlComponent, RepCurrWeaponIdx, this);
		RepCurrWeaponIdx = -1;

		MARK_PROPERTY_DIRTY_FROM_NAME(UWeaponCtrlComponent, RepCurrWeaponKey, this);
		RepCurrWeaponKey = FName();

		WeaponUIInfoUpdate();
	}
}

void UWeaponCtrlComponent::CS_ExchangeWeaponDraw_Implementation()
{
	MARK_PROPERTY_DIRTY_FROM_NAME(UWeaponCtrlComponent, RepCurrWeaponIdx, this);
	RepCurrWeaponIdx = RepNewWeaponIdx;

	if (RepCurrWeaponIdx == -1)
	{
		return;
	}

	if (!IsValid(MyWeapons[RepCurrWeaponIdx]))
	{
		return;
	}

	//���� ����
	UJSMWeaponDataAsset* WeaponDataAsset = MyWeapons[RepCurrWeaponIdx]->GetWeaponDataAsset();
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	SM_EquipWeapon(WeaponDataAsset->ItemSkeletalMesh.Get(), WeaponDataAsset->ItemStaticMesh.Get(), WeaponDataAsset->WeaponAttachType);
	SS_UpdateAttachWeaponMesh(nullptr, WeaponDataAsset->WeaponAttachType);
	SS_AttachWeaponAbility(RepCurrWeaponKey);
	SC_EquipWeaponAnim(WeaponDataAsset->WeaponCategory);
	WeaponUIInfoUpdate();
}

void UWeaponCtrlComponent::ExchangeWeapon(const bool& bUp)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	int32 NewWeaponIdx;
	if (bUp)
	{
		NewWeaponIdx = (RepCurrWeaponIdx < 0) ? (MyWeapons.Num() - 1) : (RepCurrWeaponIdx - 1);
	}
	else
	{
		NewWeaponIdx = (RepCurrWeaponIdx >= (MyWeapons.Num() - 1)) ? -1 : (RepCurrWeaponIdx + 1);
	}

	SetWeaponAttach(NewWeaponIdx);
}

void UWeaponCtrlComponent::SS_UpdateAttachWeaponMesh(USkeletalMesh* NewMesh, const EWeaponAttachType& InType)
{
	if (InType == EWeaponAttachType::Hip)
	{
		SM_AttachHipWeapon(NewMesh);
	}
	else if (InType == EWeaponAttachType::Back)
	{
		SM_AttachBackWeapon(NewMesh);
	}
	else if (InType == EWeaponAttachType::BackMelee)
	{
		SM_AttachMeleeBackWeapon(NewMesh);
	}
}

void UWeaponCtrlComponent::SS_DetachWeaponAbility(const FName& InCurrWeaponKey)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	UJSMAbilitySystemComponent* AbilityComp = Cast<UJSMAbilitySystemComponent>(OwnerChar->GetAbilitySystemComponent());
	if (!IsValid(AbilityComp))
	{
		return;
	}

	UJSMWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMWeaponDataAsset>(InCurrWeaponKey);
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}
	AbilityComp->DetachAbilities(WeaponDataAsset->AbilitiesWhenEquip);
}

void UWeaponCtrlComponent::SS_AttachWeaponAbility(const FName& InCurrWeaponKey)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	UJSMAbilitySystemComponent* AbilityComp = Cast<UJSMAbilitySystemComponent>(OwnerChar->GetAbilitySystemComponent());
	if (!IsValid(AbilityComp))
	{
		return;
	}

	UJSMWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMWeaponDataAsset>(InCurrWeaponKey);
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}
	AbilityComp->AttachAbilities(WeaponDataAsset->AbilitiesWhenEquip);
}

void UWeaponCtrlComponent::FireWeapon(const FVector_NetQuantize& MuzzleEnd)
{
	if (!IsValid(OwnerChar) || OwnerChar->IsDie())
	{
		return;
	}

	AJSMPlayerController* CurrentOwnController = Cast<AJSMPlayerController>(OwnerChar->GetController());
	if (IsValid(CurrentOwnController) && CurrentOwnController->IsWeaponMenuPressed())
	{
		return;
	}

	OwnerChar->Replicated_PlayMontage(OwnerChar->GetFireAnimation(), 1.0f);

	if (!OwnerChar->IsLocallyControlled())
	{
		return;
	}
	CS_FireWeapon(MuzzleEnd);
}

void UWeaponCtrlComponent::CS_FireWeapon_Implementation(const FVector_NetQuantize& MuzzleEnd)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	SS_FireWeapon(MuzzleEnd);
}

void UWeaponCtrlComponent::SS_FireWeapon(const FVector_NetQuantize& MuzzleEnd)
{
	ARangeWeapon* CurrentWeapon = Cast<ARangeWeapon>(GetCurrentWeapon());
	if (!IsValid(CurrentWeapon) || (CurrentWeapon->GetCurrentMagAmmo() <= 0))
	{
		return;
	}

	TWeakObjectPtr<UJSMRangeWeaponDataAsset> WeaponDataAsset = Cast<UJSMRangeWeaponDataAsset>(CurrentWeapon->GetWeaponDataAsset());
	if (!WeaponDataAsset.IsValid())
	{
		return;
	}

	SM_EquipWeaponAnimation(WeaponDataAsset->FireAnimSequence.Get());
	UJSMItemManagerSubsystem* ItemManager = JSMFL::GetGameInstanceSubsystem<UJSMItemManagerSubsystem>();
	if (!IsValid(ItemManager))
	{
		return;
	}
	TWeakObjectPtr<UJSMProjectileWeaponDataAsset> ProjectileWeaponDataAsset = Cast<UJSMProjectileWeaponDataAsset>(CurrentWeapon->GetWeaponDataAsset());
	APlayerController* CurrentOwnController = Cast<APlayerController>(OwnerChar->GetController());

	FVector_NetQuantize InSapwnLoc = OwnerChar->SkeletalMesh->GetSocketLocation(FireMuzzleSocketName);
	if (ProjectileWeaponDataAsset.IsValid() && !ProjectileWeaponDataAsset->ProjectTileBPClass.IsNull() && IsValid(OwnerChar->SkeletalMesh)
		&& IsValid(CurrentOwnController))
	{
		APlayerCameraManager* CurrentOwnCameraManager = CurrentOwnController->PlayerCameraManager;
		if (IsValid(CurrentOwnCameraManager) && IsValid(CurrentOwnCameraManager->GetTransformComponent()))
		{
			FRotator InSapwnRot = CurrentOwnCameraManager->GetCameraRotation();
			FTransform InSpawnTransform(InSapwnRot, InSapwnLoc, FVector_NetQuantize::OneVector);
			UClass* bpClass = ProjectileWeaponDataAsset->ProjectTileBPClass.LoadSynchronous();
			AProjectTileBase* ProjectTileActor = ItemManager->SpawnProjectTileActor(GetWorld(), bpClass, CurrentWeapon->GetRepWeaponDataKey(), InSpawnTransform);
			if (IsValid(ProjectTileActor))
			{
				ProjectTileActor->SM_InitResource(OwnerChar, ProjectileWeaponDataAsset->ProjectileHitEffect.Get(), ProjectileWeaponDataAsset->ProjectileHitSound.Get(),
					ProjectileWeaponDataAsset->ProjectileStaticMesh.Get(), ProjectileWeaponDataAsset->HitRadius, ProjectileWeaponDataAsset->Impulse);
			}
		}
	}
	else
	{
		uint32 InFireAmmoCnt = (CurrentWeapon->GetCurrentMagAmmo() < WeaponDataAsset->FireAmmoCount) ? CurrentWeapon->GetCurrentMagAmmo() : WeaponDataAsset->FireAmmoCount;
		SC_FireWeapon(WeaponDataAsset->WeaponRangeDistance, WeaponDataAsset->WeaponBulletRadius, InFireAmmoCnt, WeaponDataAsset->FireSpread, InSapwnLoc, MuzzleEnd);
	}

	if (!WeaponDataAsset->ShellEjectBaseBPClass.IsNull())
	{
		UClass* bpClass = WeaponDataAsset->ShellEjectBaseBPClass.LoadSynchronous();
		FTransform InSapwnTransform = OwnerChar->SkeletalMesh->GetSocketTransform(ShellEjectSocketName);
		AShellEjectBase* NewShellEject = ItemManager->SpawnShellEjectBaseActor(GetWorld(), bpClass, FName(), InSapwnTransform);
		if (IsValid(NewShellEject))
		{
			NewShellEject->SM_InitResource(WeaponDataAsset->ShellEjectMesh.Get());
		}
	}
	UAISense_Hearing::ReportNoiseEvent(GetWorld(), InSapwnLoc, WeaponDataAsset->NoiseLoudness, OwnerChar, WeaponDataAsset->NoiseDistance, FireNoiseName);
	CurrentWeapon->FireConsumeAmmo();
	SC_WeaponRecoil(WeaponDataAsset->RecoilRate);
	WeaponUIInfoUpdate();
}


void UWeaponCtrlComponent::CS_ThrowableWeapon_Implementation(const FVector_NetQuantize& InLaunchVelocity)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	SS_ThrowableWeapon(InLaunchVelocity);
}

void UWeaponCtrlComponent::SS_ThrowableWeapon(const FVector_NetQuantize& InLaunchVelocity)
{
	UJSMItemManagerSubsystem* ItemManager = JSMFL::GetGameInstanceSubsystem<UJSMItemManagerSubsystem>();
	UJSMWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMWeaponDataAsset>(RepCurrWeaponKey);

	if (!IsValid(ItemManager) || !IsValid(WeaponDataAsset) || !IsValid(OwnerChar) || !IsValid(OwnerChar->GetMesh()) || WeaponDataAsset->WeaponBPClass.IsNull())
	{
		return;
	}

	if (WeaponDataAsset->WeaponBPClass.IsNull())
	{
		return;
	}

	FTransform InTransform = OwnerChar->GetMesh()->GetSocketTransform(OwnerChar->ThrowableSocketName, ERelativeTransformSpace::RTS_World);
	UClass* bpClass = WeaponDataAsset->WeaponBPClass.LoadSynchronous();
	AThrowableWeapon* WeaponActor = Cast<AThrowableWeapon>(ItemManager->SpawnWeaponActor(GetWorld(), bpClass, RepCurrWeaponKey, InTransform));
	if (IsValid(WeaponActor))
	{
		WeaponActor->SM_SetOwnChar(OwnerChar);
		WeaponActor->ThrowableWeaponMove(InLaunchVelocity);
	}
	SS_DestroyEquipWeapon();
}

void UWeaponCtrlComponent::SS_DestroyEquipWeapon()
{
	SM_EquipWeapon(nullptr, nullptr, EWeaponAttachType::None, true);
	SC_EquipWeaponAnim(EALSOverlayState::Default);

	MyWeapons[RepCurrWeaponIdx]->Destroy();
	MyWeapons[RepCurrWeaponIdx] = nullptr;

	MARK_PROPERTY_DIRTY_FROM_NAME(UWeaponCtrlComponent, RepCurrWeaponIdx, this);
	RepCurrWeaponIdx = -1;

	MARK_PROPERTY_DIRTY_FROM_NAME(UWeaponCtrlComponent, RepCurrWeaponKey, this);
	RepCurrWeaponKey = FName();

	WeaponUIInfoUpdate();
}

void UWeaponCtrlComponent::SC_ThrowableWeapon_Implementation()
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	FVector_NetQuantize LaunchVelocity;
	OwnerChar->PredictProjectile(LaunchVelocity);
	CS_ThrowableWeapon(LaunchVelocity);
}

void UWeaponCtrlComponent::SC_WeaponRecoil_Implementation(const float& Value)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	float PitchVal = FMath::RandRange(-0.5f, (Value * -1));
	float YawVal = FMath::RandRange(((Value / 2) * -1), (Value / 2));

	OwnerChar->AddControllerPitchInput(PitchVal);
	OwnerChar->AddControllerYawInput(YawVal);
}

void UWeaponCtrlComponent::SC_FireWeapon_Implementation(const float& InTraceDistance, const float& InTraceRadius, const uint32& AmmoCount, const float& FireSpread, const FVector_NetQuantize& MuzzleLoc, const FVector_NetQuantize& MuzzleEnd)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	for (uint32 i = 0; i < AmmoCount; i++)
	{
		FHitResult OutHitResult; FVector_NetQuantize EndLoc;
		float RandomY = FMath::RandRange(0.0f, (i * FireSpread));
		float RandomZ = FMath::RandRange(0.0f, (i * FireSpread));
		OwnerChar->FireWeapon(OutHitResult, EndLoc, InTraceDistance, InTraceRadius, FVector_NetQuantize(0, RandomY, RandomZ), MuzzleLoc, MuzzleEnd);
		CS_ApplyWeaponAbilityEffect(OutHitResult, EndLoc);
	}
}

void UWeaponCtrlComponent::CS_ApplyWeaponAbilityEffect_Implementation(const FHitResult& OutHitResult, const FVector_NetQuantize& EndLoc)
{
	AWeapon* CurrentWeapon = GetCurrentWeapon();
	if (!IsValid(CurrentWeapon))
	{
		return;
	}
	TWeakObjectPtr<UJSMWeaponDataAsset> WeaponDataAsset = CurrentWeapon->GetWeaponDataAsset();
	if (!WeaponDataAsset.IsValid())
	{
		return;
	}
	TWeakObjectPtr<UPhysicalMaterial> PM_Hit = OutHitResult.PhysMaterial;
	if (PM_Hit.IsValid())
	{
		int PhysicsIndex = static_cast<int32>(PM_Hit->SurfaceType);
		if (WeaponDataAsset->HitParticleArray.Num() > PhysicsIndex)
		{
			SM_SpawnParticle(WeaponDataAsset->HitParticleArray[PhysicsIndex].Get(), EndLoc);
		}

		if (WeaponDataAsset->HitAttachedMaterialArray.Num() > PhysicsIndex)
		{
			SM_SpawnDecal(WeaponDataAsset->HitAttachedMaterialArray[PhysicsIndex].Get(), OutHitResult, FVector_NetQuantize(10.0f, 10.0f, 10.0f));
		}
	}
	ACharacterBase* HitActor = Cast<ACharacterBase>(OutHitResult.GetActor());
	if (!OutHitResult.bBlockingHit || !IsValid(HitActor) || !IsValid(OwnerChar))
	{
		return;
	}
	UE_LOG(LogTemp, Warning, TEXT("OutHitResult.BoneName : %s, Location : %s"), *OutHitResult.BoneName.ToString(), *OutHitResult.Location.ToString());
	FVector_NetQuantize ImpulseValue = (HitActor->GetActorLocation() - CurrentWeapon->GetActorLocation()).GetSafeNormal2D() * WeaponDataAsset->Impulse;
	ApplyWeaponAbilityEffect(HitActor, ImpulseValue, OutHitResult.ImpactPoint, OutHitResult.BoneName);
}

void UWeaponCtrlComponent::ReloadWeapon()
{
	if (!IsValid(OwnerChar) || !IsValid(OwnerChar->GetReloadAnimation()))
	{
		return;
	}

	bReloadWeapon = true;
	OwnerChar->Replicated_PlayMontage(OwnerChar->GetReloadAnimation(), 1.0f);

	if (!OwnerChar->IsLocallyControlled())
	{
		return;
	}

	FTimerHandle DelayHandle;
	const TWeakObjectPtr<UWeaponCtrlComponent> WeakPtr(this);
	GetWorld()->GetTimerManager().SetTimer(DelayHandle, FTimerDelegate::CreateLambda([WeakPtr]()
	{
		if (WeakPtr.IsValid() )
		{
			WeakPtr->CS_ReloadWeapon();
		}
	}), OwnerChar->GetReloadAnimation()->GetPlayLength(), false);
}

void UWeaponCtrlComponent::CS_ReloadWeapon_Implementation()
{
	bReloadWeapon = false;
	ARangeWeapon* CurrWeapon = Cast<ARangeWeapon>(GetCurrentWeapon());
	if (!IsValid(CurrWeapon))
	{
		return;
	}

	bool bEnableReload =  ((CurrWeapon->GetCurrentMagAmmo() < CurrWeapon->GetMagAmmoCapacity()) && (CurrWeapon->GetCurrentBagAmmo() > 0));
	if (!bEnableReload)
	{
		return;
	}
	CurrWeapon->Reload();
	WeaponUIInfoUpdate();
}

void UWeaponCtrlComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UWeaponCtrlComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

AWeapon* UWeaponCtrlComponent::GetCurrentWeapon()
{
	if (IsGrabWeapon())
	{
		return MyWeapons[RepCurrWeaponIdx].Get();
	}

	return nullptr;
}

AWeapon* UWeaponCtrlComponent::GetHolsterWeapon()
{
	if (RepCurrWeaponIdx < (MyWeapons.Num() - 1))
	{
		return MyWeapons[RepCurrWeaponIdx + 1].Get();
	}

	return nullptr;
}

void UWeaponCtrlComponent::WeaponHudUpdate()
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	UJSMCharacterDataAsset* CharDataAsset = OwnerChar->GetCharacterDataAsset();
	if (!IsValid(CharDataAsset))
	{
		return;
	}

	UTexture2D* InPrimaryWeapon = CharDataAsset->EmptyWeaponIcon.Get();
	UTexture2D* InHolsterWeapon = CharDataAsset->EmptyWeaponIcon.Get();
	FText InMagAmmo = FText::FromString(TEXT("1"));
	FText InMagCapacity = FText::FromString(TEXT("1"));
	UTexture2D* CrossHair = nullptr;
	UTexture2D* InWeaponScope = nullptr;

	AWeapon* CurrentWeapon = GetCurrentWeapon();
	AWeapon* HolsterWeapon = GetHolsterWeapon();

	if ((RepCurrWeaponIdx != -1) && IsValid(CurrentWeapon))
	{
		UJSMWeaponDataAsset* DataAsset = CurrentWeapon->GetWeaponDataAsset();
		if (IsValid(DataAsset))
		{
			InPrimaryWeapon = DataAsset->Icon.Get();
			CrossHair = DataAsset->CrossHair.Get();
		}

		UJSMRangeWeaponDataAsset* RangeDataAsset = Cast<UJSMRangeWeaponDataAsset>(DataAsset);
		if (IsValid(RangeDataAsset))
		{
			InWeaponScope = RangeDataAsset->WeaponScope.Get();
		}

		ARangeWeapon* RangeCurrentWeapon = Cast<ARangeWeapon>(CurrentWeapon);
		if (IsValid(RangeCurrentWeapon))
		{
			InMagAmmo = FText::FromString(FString::FromInt(RangeCurrentWeapon->GetCurrentMagAmmo()));
			InMagCapacity = FText::FromString(FString::FromInt(RangeCurrentWeapon->GetCurrentBagAmmo()));
		}
	}
	else if (RepCurrWeaponIdx == -1)
	{
		InPrimaryWeapon = CharDataAsset->FistWeaponIcon.Get();
	}

	if ((RepCurrWeaponIdx < (MyWeapons.Num() - 1)) && IsValid(HolsterWeapon))
	{
		UJSMWeaponDataAsset* DataAsset = HolsterWeapon->GetWeaponDataAsset();
		if (IsValid(DataAsset))
		{
			InHolsterWeapon = DataAsset->Icon.Get();
		}
	}
	else if (RepCurrWeaponIdx == (MyWeapons.Num() - 1))
	{
		InHolsterWeapon = CharDataAsset->FistWeaponIcon.Get();
	}

	SC_WeaponHudUpdate(InPrimaryWeapon, InHolsterWeapon, InMagAmmo, InMagCapacity, CrossHair, InWeaponScope);
}

void UWeaponCtrlComponent::SC_WeaponHudUpdate_Implementation(UTexture2D* InPrimaryWeapon, UTexture2D* InHolsterWeapon, const FText& InMagAmmo, const FText& InMagCapacity, UTexture2D* CrossHair, UTexture2D* WeaponScope)
{
	if (!IsValid(OwnerChar) || !OwnerChar->IsLocallyControlled())
	{
		return;
	}

	AJSMPlayerController* OwnController = Cast<AJSMPlayerController>(OwnerChar->GetController());
	if (IsValid(OwnController))
	{
		OwnController->WeaponHudUpdate(InPrimaryWeapon, InHolsterWeapon, InMagAmmo, InMagCapacity, CrossHair, WeaponScope);
	}
}

void UWeaponCtrlComponent:: WeaponMenuUpdate()
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	UJSMCharacterDataAsset* CharDataAsset = OwnerChar->GetCharacterDataAsset();
	if (!IsValid(CharDataAsset))
	{
		return;
	}

	UTexture2D* InHipWeapon = CharDataAsset->EmptyWeaponIcon.Get();
	UTexture2D* InBackWeapon = CharDataAsset->EmptyWeaponIcon.Get();
	UTexture2D* InMeleeWeapon = CharDataAsset->EmptyWeaponIcon.Get();
	UTexture2D* InThrowableWeapon = CharDataAsset->EmptyWeaponIcon.Get();

	FText InHipWeaponAmmo = FText::FromString(TEXT(""));
	FText InBackWeaponAmmo = FText::FromString(TEXT(""));

	for (int i = 0; i < MyWeapons.Num(); i++)
	{
		if (!IsValid(MyWeapons[i]))
		{
			continue;
		}

		UJSMWeaponDataAsset* DataAsset = MyWeapons[i]->GetWeaponDataAsset();
		if (!IsValid(DataAsset))
		{
			continue;
		}
		ARangeWeapon* HaveRangeWeapon = Cast<ARangeWeapon>(MyWeapons[i]);

		if (i == 0)
		{
			InHipWeapon = DataAsset->Icon.Get();
			if (IsValid(HaveRangeWeapon))
			{
				InHipWeaponAmmo = FText::FromString(FString::FromInt(HaveRangeWeapon->GetCurrentMagAmmo()));
			}
		}
		else if (i == 1)
		{
			InBackWeapon = DataAsset->Icon.Get();
			if (IsValid(HaveRangeWeapon))
			{
				InBackWeaponAmmo = FText::FromString(FString::FromInt(HaveRangeWeapon->GetCurrentMagAmmo()));
			}
		}
		else if(i == 2)
		{
			InMeleeWeapon = DataAsset->Icon.Get();
		}
		else
		{
			InThrowableWeapon = DataAsset->Icon.Get();
		}
	}

	SC_WeaponMenuUpdate(InHipWeapon, InBackWeapon, InMeleeWeapon, InThrowableWeapon, InHipWeaponAmmo, InBackWeaponAmmo);
}

void UWeaponCtrlComponent::SC_WeaponMenuUpdate_Implementation(UTexture2D* InHipWeapon, UTexture2D* InBackWeapon, UTexture2D* InMeleeWeapon, UTexture2D* InThrowableWeapon,
	const FText& InHipWeaponAmmo, const FText& InBackWeaponAmmo)
{
	if (!IsValid(OwnerChar) || !OwnerChar->IsLocallyControlled())
	{
		return;
	}

	AJSMPlayerController* OwnController = Cast<AJSMPlayerController>(OwnerChar->GetController());
	if (IsValid(OwnController))
	{
		OwnController->WeaponMenuUpdate(InHipWeapon, InBackWeapon, InMeleeWeapon, InThrowableWeapon, InHipWeaponAmmo, InBackWeaponAmmo);
	}
}

void UWeaponCtrlComponent::SM_SpawnParticle_Implementation(class UParticleSystem* Particle, const FVector_NetQuantize& HitLoc)
{
	if (IsValid(Particle))
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Particle, HitLoc, FRotator::ZeroRotator, true);
	}
}

void UWeaponCtrlComponent::SM_EquipWeaponAnimation_Implementation(class UAnimSequence* Animation)
{
	if (!IsValid(OwnerChar) || !IsValid(OwnerChar->SkeletalMesh))
	{
		return;
	}
	OwnerChar->SkeletalMesh->PlayAnimation(Animation, false);
}

void UWeaponCtrlComponent::SS_SetWeaponScope(bool bValue)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	AWeapon* CurrentWeapon = GetCurrentWeapon();
	if (!IsValid(CurrentWeapon))
	{
		return;
	}
	TWeakObjectPtr<UJSMWeaponDataAsset> WeaponDataAsset = CurrentWeapon->GetWeaponDataAsset();
	if (!WeaponDataAsset.IsValid())
	{
		return;
	}

	if (WeaponDataAsset->bScope)
	{
		AJSMPlayerController* OwnController = Cast<AJSMPlayerController>(OwnerChar->GetController());
		if (IsValid(OwnController))
		{
			float InParam = 0.0f;
			AActor* InTarget = nullptr;

			if (bValue)
			{
				SC_SetCustomTimeDilation(0.1f);
				InParam = 1.f;
				if (IsValid(OwnerChar->WeaponScopeComp))
				{
					InTarget = OwnerChar->WeaponScopeComp->GetChildActor();
				}
			}
			else
			{
				SC_SetCustomTimeDilation(1.0f);
				InParam = 0.f;
				InTarget = OwnerChar.Get();
			}

			OwnController->SetViewTargetWithBlend(InTarget);
		}
		SC_WeaponScope(bValue);
	}
}

void UWeaponCtrlComponent::SC_WeaponScope_Implementation(bool bValue)
{
	if (!IsValid(OwnerChar) || !OwnerChar->IsLocallyControlled())
	{
		return;
	}

	AJSMPlayerController* OwnController = Cast<AJSMPlayerController>(OwnerChar->GetController());
	if (!IsValid(OwnController))
	{
		return;
	}

	OwnController->SetVisibleWeaponScope(bValue);
}

void UWeaponCtrlComponent::SetWeaponScopeFOV(const float& Value)
{
	if (!IsValid(OwnerChar) || !IsValid(OwnerChar->WeaponScopeComp))
	{
		return;
	}

	AWeapon* WeaponScope = Cast<AWeapon>(OwnerChar->WeaponScopeComp->GetChildActor());
	if(IsValid(WeaponScope))
	{
		WeaponScope->SetWeaponScopeFOV(Value);
	}
}

void UWeaponCtrlComponent::CS_HolsterAndPickWeapon_Implementation(bool bFlag)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	AWeapon* CurrWeapon = GetCurrentWeapon();
	if (!IsValid(CurrWeapon))
	{
		return;
	}
	UJSMWeaponDataAsset* DataAsset = CurrWeapon->GetWeaponDataAsset();
	if (!IsValid(DataAsset))
	{
		return;
	}

	if (bFlag)
	{
		SM_EquipWeapon(nullptr, nullptr, DataAsset->WeaponAttachType);
		if (DataAsset->WeaponAttachType == EWeaponAttachType::Back)
		{
			SM_AttachBackWeapon(DataAsset->ItemSkeletalMesh.Get());
		}
		else if (DataAsset->WeaponAttachType == EWeaponAttachType::Hip)
		{
			SM_AttachHipWeapon(DataAsset->ItemSkeletalMesh.Get());
		}
		else if (DataAsset->WeaponAttachType == EWeaponAttachType::BackMelee)
		{
			SM_AttachMeleeBackWeapon(DataAsset->ItemSkeletalMesh.Get());
		}

		SC_EquipWeaponAnim(EALSOverlayState::Default);
	}
	else
	{
		if (DataAsset->WeaponAttachType == EWeaponAttachType::Back)
		{
			SM_AttachBackWeapon(nullptr);
		}
		else if (DataAsset->WeaponAttachType == EWeaponAttachType::Hip)
		{
			SM_AttachHipWeapon(nullptr);
		}
		else if (DataAsset->WeaponAttachType == EWeaponAttachType::BackMelee)
		{
			SM_AttachMeleeBackWeapon(nullptr);
		}

		FTimerHandle DelayHandle;
		const TWeakObjectPtr<UWeaponCtrlComponent> WeakPtr(this);
		const TWeakObjectPtr<UJSMWeaponDataAsset> DataWeakPtr = DataAsset;
		GetWorld()->GetTimerManager().SetTimer(DelayHandle, FTimerDelegate::CreateLambda([WeakPtr, DataWeakPtr]()
		{
			if (WeakPtr.IsValid() && DataWeakPtr.IsValid())
			{
				WeakPtr->SM_EquipWeapon(DataWeakPtr->ItemSkeletalMesh.Get(), DataWeakPtr->ItemStaticMesh.Get(),  DataWeakPtr->WeaponAttachType);
				WeakPtr->SC_EquipWeaponAnim(DataWeakPtr->WeaponCategory);
			}
		}), 0.3f, false);
	}
}

void UWeaponCtrlComponent::MovementToNone()
{
	CS_HolsterAndPickWeapon(false);
}

void UWeaponCtrlComponent::MovementToMantle()
{
	CS_HolsterAndPickWeapon(true);
}

void UWeaponCtrlComponent::SM_SpawnDecal_Implementation(class UMaterialInterface* Material, const FHitResult& HitResult, const FVector_NetQuantize& InScale)
{
	if (!HitResult.bBlockingHit)
	{
		return;
	}
	UGameplayStatics::SpawnDecalAttached(Material, InScale, HitResult.GetComponent(), FName(TEXT("None")), HitResult.ImpactPoint, UKismetMathLibrary::MakeRotFromX(HitResult.Normal), EAttachLocation::KeepWorldPosition, 10.0f);
}

void UWeaponCtrlComponent::PlayMontageDrawWeapon()
{
	UE_LOG(LogTemp, Warning, TEXT("PlayMontageDrawWeapon : %s"), *RepCurrWeaponKey.ToString());
	UJSMWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMWeaponDataAsset>(RepCurrWeaponKey);
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	if (IsValid(WeaponDataAsset->AM_DrawWeapon.Get()))
	{
		OwnerChar->Replicated_PlayMontage(WeaponDataAsset->AM_DrawWeapon.Get(), 1.0f);
	}
}

void UWeaponCtrlComponent::PlayMontageUnDrawWeapon()
{
	UE_LOG(LogTemp, Warning, TEXT("PlayMontageUnDrawWeapon : %s"), *RepCurrWeaponKey.ToString());
	UJSMWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMWeaponDataAsset>(RepCurrWeaponKey);
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	if (IsValid(WeaponDataAsset->AM_UnDrawWeapon.Get()))
	{
		OwnerChar->Replicated_PlayMontage(WeaponDataAsset->AM_UnDrawWeapon.Get(), 1.0f);
	}
}

void UWeaponCtrlComponent::MeleeWeaponAttack()
{
	if (!IsValid(OwnerChar) || OwnerChar->IsDie())
	{
		return;
	}

	AJSMPlayerController* CurrentOwnController = Cast<AJSMPlayerController>(OwnerChar->GetController());
	if (IsValid(CurrentOwnController) && CurrentOwnController->IsWeaponMenuPressed())
	{
		return;
	}

	UJSMMeleeWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMMeleeWeaponDataAsset>(RepCurrWeaponKey);
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	int32 RanNum = FMath::RandRange(0, (WeaponDataAsset->AM_MeleeAttackArray.Num() - 1));
	OwnerChar->Replicated_PlayMontage(WeaponDataAsset->AM_MeleeAttackArray[RanNum].Get(), 1.0f);
}

void UWeaponCtrlComponent::AttackAction(const FInputActionValue& Value)
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	UJSMWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMWeaponDataAsset>(RepCurrWeaponKey);
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	UJSMAbilitySystemComponent* AbilitySystemComponent = Cast<UJSMAbilitySystemComponent>(OwnerChar->GetAbilitySystemComponent());
	if (IsValid(AbilitySystemComponent))
	{
		FGameplayEventData EventData;
		FGameplayTag EventTag;
		
		switch (WeaponDataAsset->WeaponCategory)
		{
			case EALSOverlayState::Rifle:
			case EALSOverlayState::PistolOneHanded:
			case EALSOverlayState::PistolTwoHanded:
			{
				if (Value.GetMagnitude() > 0)
				{
					EventTag = REQ_GAMEPLAYTAG("Event.Attack.FireStart");
				}
				else
				{
					EventTag = REQ_GAMEPLAYTAG("Event.Attack.FireStop");
				}
				break;
			}
			case EALSOverlayState::Axe:
			{
				if (Value.GetMagnitude() > 0)
				{
					EventTag = REQ_GAMEPLAYTAG("Event.Attack.MeleeAttackStart");
				}
				break;
			}
			case EALSOverlayState::Throw:
			{
				if (Value.GetMagnitude() > 0)
				{
					EventTag = REQ_GAMEPLAYTAG("Event.Attack.ThrowStart");
				}
				else
				{
					EventTag = REQ_GAMEPLAYTAG("Event.Attack.ThrowStop");
				}
				break;
			}
			default:
			{
				//EventTag = REQ_GAMEPLAYTAG("Event.Attack.Default");
			}
		}

		EventData.EventTag = EventTag;
		EventData.Instigator = OwnerChar;
		AbilitySystemComponent->HandleGameplayEvent(EventTag, &EventData);
	}
}

bool UWeaponCtrlComponent::EquipWeaponAmmo(class UJSMWeaponAmmoDataAsset* InDataAsset)
{
	if (!IsValid(InDataAsset))
	{
		return false;
	}

	for (int i = 0; i < MyWeapons.Num(); i++)
	{
		ARangeWeapon* RangeWeapon = Cast<ARangeWeapon>(MyWeapons[i]);
		if (!IsValid(RangeWeapon))
		{
			continue;
		}

		UJSMRangeWeaponDataAsset* WeaponDataAsset = Cast<UJSMRangeWeaponDataAsset>(RangeWeapon->GetWeaponDataAsset());
		if (!IsValid(WeaponDataAsset))
		{
			continue;
		}

		if (InDataAsset->AmmoCategory == WeaponDataAsset->AmmoCategory)
		{
			RangeWeapon->CurrentBagAmmoCalculate(InDataAsset->Amount, true);
			WeaponHudUpdate();
			return true;
		}
	}

	return true;
}

void UWeaponCtrlComponent::RefreshCharacterWeaponCtrlComponent()
{
	for (int i = 0; i < MyWeapons.Num(); i++)
	{
		if (!IsValid(MyWeapons[i]))
		{
			continue;
		}

		UJSMWeaponDataAsset* WeaponDataAsset = MyWeapons[i]->GetWeaponDataAsset();
		if (!IsValid(WeaponDataAsset))
		{
			continue;
		}

		if (RepCurrWeaponIdx == i)
		{
			SM_EquipWeapon(WeaponDataAsset->ItemSkeletalMesh.Get(), WeaponDataAsset->ItemStaticMesh.Get(), WeaponDataAsset->WeaponAttachType);
			continue;
		}
		EWeaponAttachType SelectAttachType = static_cast<EWeaponAttachType>(i + 1);
		switch (SelectAttachType)
		{
			case EWeaponAttachType::Hip:
			{
				SM_AttachHipWeapon(WeaponDataAsset->ItemSkeletalMesh.Get());
				break;
			}
			case EWeaponAttachType::Back:
			{
				SM_AttachBackWeapon(WeaponDataAsset->ItemSkeletalMesh.Get());
				break;
			}
			case EWeaponAttachType::BackMelee:
			{
				SM_AttachMeleeBackWeapon(WeaponDataAsset->ItemSkeletalMesh.Get());
				break;
			}
		}
	}
}

void UWeaponCtrlComponent::InitializeWeaponComponent()
{
	if (!IsValid(OwnerChar))
	{
		return;
	}

	UJSMCharacterDataAsset* DataAsset = OwnerChar->GetCharacterDataAsset();
	if (!IsValid(DataAsset))
	{
		return;
	}
	if (!DataAsset->EquippedWeaponKeyName.IsEmpty())
	{
		SS_PickupWeapon(FName(*DataAsset->EquippedWeaponKeyName));
	}
}
void UWeaponCtrlComponent::ApplyWeaponAbilityEffect(AWeapon* InstigatorWeapon, ACharacterBase* HitActor, const FVector_NetQuantize& InImpulseValue, const FVector_NetQuantize& InLocation, const FName& InBoneName)
{
	TWeakObjectPtr<UJSMWeaponDataAsset> WeaponDataAsset = InstigatorWeapon->GetWeaponDataAsset();
	if (!WeaponDataAsset.IsValid())
	{
		return;
	}

	UAbilitySystemComponent* SrcAbilitySystemCom = OwnerChar->GetAbilitySystemComponent();
	UAbilitySystemComponent* TarAbilitySystemCom = HitActor->GetAbilitySystemComponent();
	if (IsValid(SrcAbilitySystemCom) && IsValid(TarAbilitySystemCom))
	{
		FGameplayEffectContextHandle EffectContext = SrcAbilitySystemCom->MakeEffectContext();
		FGameplayEffectSpecHandle DamageEffectSpecHandle = SrcAbilitySystemCom->MakeOutgoingSpec(WeaponDataAsset->DamageEffect, 1, EffectContext);
		DamageEffectSpecHandle.Data->GetContext().AddInstigator(OwnerChar, InstigatorWeapon);
		DamageEffectSpecHandle.Data->SetSetByCallerMagnitude(REQ_GAMEPLAYTAG("SetByCaller.AddAmount"), ((WeaponDataAsset->Damage) * -1));
		SrcAbilitySystemCom->ApplyGameplayEffectSpecToTarget(*DamageEffectSpecHandle.Data.Get(), TarAbilitySystemCom);
		UAISense_Damage::ReportDamageEvent(GetWorld(), HitActor, OwnerChar, 1.0f, HitActor->GetActorLocation(), HitActor->GetActorLocation());
	}

	if (HitActor->IsDie())
	{
		HitActor->AddImpulseCharacter(InImpulseValue, InLocation, InBoneName);
	}
}

void UWeaponCtrlComponent::ApplyWeaponAbilityEffect(ACharacterBase* HitActor, const FVector_NetQuantize& InImpulseValue, const FVector_NetQuantize& InLocation, const FName& InBoneName)
{
	AWeapon* CurrentWeapon = GetCurrentWeapon();
	if (!IsValid(CurrentWeapon))
	{
		return;
	}
	ApplyWeaponAbilityEffect(CurrentWeapon, HitActor, InImpulseValue, InLocation, InBoneName);
}

void UWeaponCtrlComponent::AIDie()
{
	AWeapon* CurrentWeapon = GetCurrentWeapon();
	if (!IsValid(CurrentWeapon))
	{
		return;
	}
	TWeakObjectPtr<UJSMWeaponDataAsset> WeaponDataAsset = CurrentWeapon->GetWeaponDataAsset();
	if (!WeaponDataAsset.IsValid())
	{
		return;
	}

	SM_EquipWeapon(WeaponDataAsset->ItemSkeletalMesh.Get(), WeaponDataAsset->ItemStaticMesh.Get(), WeaponDataAsset->WeaponAttachType, true);
	SS_DropWeapon(RepCurrWeaponIdx, true);
}

void UWeaponCtrlComponent::EquipWeaponFromAttachSocket()
{
	AWeapon* CurrentWeapon = GetCurrentWeapon();
	if (!IsValid(CurrentWeapon))
	{
		return;
	}

	UJSMWeaponDataAsset* WeaponDataAsset = CurrentWeapon->GetWeaponDataAsset();
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	SM_EquipWeapon(WeaponDataAsset->ItemSkeletalMesh.Get(), WeaponDataAsset->ItemStaticMesh.Get(), WeaponDataAsset->WeaponAttachType);
	SS_UpdateAttachWeaponMesh(nullptr, WeaponDataAsset->WeaponAttachType);
	SS_AttachWeaponAbility(RepCurrWeaponKey);
	SC_EquipWeaponAnim(WeaponDataAsset->WeaponCategory);
}

void UWeaponCtrlComponent::CS_FireAIWeapon_Implementation(const FVector_NetQuantize& MuzzleEnd)
{
	ARangeWeapon* CurrentWeapon = Cast<ARangeWeapon>(GetCurrentWeapon());
	if (!IsValid(CurrentWeapon) || bReloadWeapon)
	{
		return;
	}

	if (CurrentWeapon->GetCurrentMagAmmo() <= 0)
	{
		ReloadWeapon();
		return;
	}

	FireWeapon(MuzzleEnd);
}

void UWeaponCtrlComponent::WeaponUIInfoUpdate()
{
	WeaponHudUpdate();
	WeaponMenuUpdate();
}

void UWeaponCtrlComponent::SetWeaponAttach(int InWeaponIdx)
{
	if (!IsValid(OwnerChar) || !OwnerChar->IsLocallyControlled())
	{
		return;
	}

	if (RepCurrWeaponIdx == InWeaponIdx)
	{
		return;
	}

	CS_SetWeaponAttach(InWeaponIdx);
}

void UWeaponCtrlComponent::CS_SetWeaponAttach_Implementation(int InWeaponIdx)
{
	MARK_PROPERTY_DIRTY_FROM_NAME(UWeaponCtrlComponent, RepNewWeaponIdx, this);
	RepNewWeaponIdx = InWeaponIdx;

	if (RepCurrWeaponIdx == -1)
	{
		if (!IsValid(MyWeapons[RepNewWeaponIdx]))
		{
			return;
		}

		UJSMWeaponInfo* DataInfo = MyWeapons[RepNewWeaponIdx]->GetRepWeaponInfo();
		if (!IsValid(DataInfo))
		{
			return;
		}

		MARK_PROPERTY_DIRTY_FROM_NAME(UWeaponCtrlComponent, RepCurrWeaponKey, this);
		RepCurrWeaponKey = DataInfo->GetRepWeaponDataKey();
	}
	SC_SetWeaponAttach(RepCurrWeaponKey);
}

void UWeaponCtrlComponent::SC_SetWeaponAttach_Implementation(const FName InWeaponKey)
{
	RepCurrWeaponKey = InWeaponKey;
	if (RepCurrWeaponIdx == -1)
	{
		PlayMontageDrawWeapon();
	}
	else
	{
		PlayMontageUnDrawWeapon();
	}
}

void UWeaponCtrlComponent::ThrowablePoseWeapon()
{
	if (!IsValid(OwnerChar) || OwnerChar->IsDie())
	{
		return;
	}

	AJSMPlayerController* CurrentOwnController = Cast<AJSMPlayerController>(OwnerChar->GetController());
	if (IsValid(CurrentOwnController) && CurrentOwnController->IsWeaponMenuPressed())
	{
		return;
	}

	UJSMThrowableWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMThrowableWeaponDataAsset>(RepCurrWeaponKey);
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	OwnerChar->Replicated_PlayMontage(WeaponDataAsset->AM_ThrowPose.Get(), 1.0f);
}

void UWeaponCtrlComponent::ThrowableWeapon()
{
	if (!IsValid(OwnerChar) || OwnerChar->IsDie())
	{
		return;
	}

	AJSMPlayerController* CurrentOwnController = Cast<AJSMPlayerController>(OwnerChar->GetController());
	if (IsValid(CurrentOwnController) && CurrentOwnController->IsWeaponMenuPressed())
	{
		return;
	}

	UJSMThrowableWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMThrowableWeaponDataAsset>(RepCurrWeaponKey);
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	OwnerChar->Replicated_PlayMontage(WeaponDataAsset->AM_Throw.Get(), 1.0f);

	if (!OwnerChar->IsLocallyControlled())
	{
		return;
	}
	SC_ThrowableWeapon();
}

void UWeaponCtrlComponent::CS_CusomAimAction_Implementation(bool bValue)
{
	if (!IsValid(OwnerChar) || OwnerChar->IsDie())
	{
		return;
	}


	UJSMAbilitySystemComponent* AbilitySystemComponent = Cast<UJSMAbilitySystemComponent>(OwnerChar->GetAbilitySystemComponent());
	if (!IsValid(AbilitySystemComponent))
	{
		return;
	}
	
	FGameplayEventData EventData;
	FGameplayTag EventTag;

	ARangeWeapon* CurrentRangeWeapon = Cast<ARangeWeapon>(GetCurrentWeapon());
	if (IsValid(CurrentRangeWeapon))
	{
		SS_SetWeaponScope(bValue);
		return;
	}

	AThrowableWeapon* CurrentThrowableWeapon = Cast<AThrowableWeapon>(GetCurrentWeapon());
	if (IsValid(CurrentThrowableWeapon))
	{
		SS_CancelThrowable(bValue);
		EventTag = REQ_GAMEPLAYTAG("Event.Attack.ThrowCancel");
	}

	EventData.EventTag = EventTag;
	EventData.Instigator = OwnerChar;
	AbilitySystemComponent->HandleGameplayEvent(EventTag, &EventData);
}

void UWeaponCtrlComponent::SS_CancelThrowable(bool bValue)
{
	UJSMThrowableWeaponDataAsset* WeaponDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMThrowableWeaponDataAsset>(RepCurrWeaponKey);
	if (!IsValid(WeaponDataAsset))
	{
		return;
	}

	SM_StopMontage(WeaponDataAsset->AM_ThrowPose.Get());
}

void UWeaponCtrlComponent::SM_StopMontage_Implementation(class UAnimMontage* Montage)
{
	if (!IsValid(OwnerChar) || OwnerChar->IsDie() || !OwnerChar->GetMesh()->GetAnimInstance())
	{
		return;
	}

	OwnerChar->GetMesh()->GetAnimInstance()->Montage_Stop(0, Montage);
}

void UWeaponCtrlComponent::SC_SetCustomTimeDilation_Implementation(const float& InTime)
{
	if (!IsValid(OwnerChar) || OwnerChar->IsDie())
	{
		return;
	}

	FTimerHandle DelayHandle;
	const TWeakObjectPtr<UWeaponCtrlComponent> WeakPtr(this);
	GetWorld()->GetTimerManager().SetTimer(DelayHandle, FTimerDelegate::CreateLambda([WeakPtr, InTime]()
	{
		if (WeakPtr.IsValid() && IsValid(WeakPtr->GetOwnChar()))
		{
			WeakPtr->GetOwnChar()->CustomTimeDilation = InTime;
		}
	}), 0.1f, false);
}