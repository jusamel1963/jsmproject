// Copyright JSM Studio, Inc. All Rights Reserved.


#include "UIs/ProgressBarWidget.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"

void UProgressBarWidget::Update_Loading(const float& InValue)
{
	if (!IsValid(Pb_Loading))
	{
		return;
	}

	Pb_Loading->SetPercent(InValue);
}

void UProgressBarWidget::SetLoadingName(const FText& LoadingName)
{
	if (!IsValid(TB_LoadingName))
	{
		return;
	}

	TB_LoadingName->SetText(LoadingName);
}