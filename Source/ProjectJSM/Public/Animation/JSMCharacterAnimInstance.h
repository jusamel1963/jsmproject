// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Character/Animation/ALSCharacterAnimInstance.h"
#include "FrameWork/Interface/CoverValues.h"
#include "Data/Common/GameEnum.h"
#include "FrameWork/Interface/CoverValues.h"
#include "Library/ALSCharacterEnumLibrary.h"
#include "Library/ALSCharacterStructLibrary.h"
#include "JSMCharacterAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UJSMCharacterAnimInstance : public UALSCharacterAnimInstance, public ICoverValues
{
	GENERATED_BODY()
public:
	virtual void NativeInitializeAnimation() override;
	virtual void NativeBeginPlay() override;
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	virtual ECoverType GetCoverType() override { return CurrentCoverState; }
	virtual bool IsMoveLeft() override { return bMoveLeft; }
	virtual bool IsMoveRight() override { return bMoveRight; }

public:
	UPROPERTY(BlueprintReadOnly)
	ECoverType CurrentCoverState = ECoverType::FindCover;

	UPROPERTY(BlueprintReadOnly)
	bool bMoveLeft = false;
	UPROPERTY(BlueprintReadOnly)
	bool bMoveRight = false;

	UPROPERTY(BlueprintReadOnly)
	bool bCanMoveLeft = true;
	UPROPERTY(BlueprintReadOnly)
	bool bCanMoveRight = true;

	UPROPERTY(BlueprintReadOnly)
	bool bRiding = false;

	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<class ACharacterBase> JSMCharacter = nullptr;
};
