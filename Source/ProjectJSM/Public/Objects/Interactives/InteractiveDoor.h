// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Interactives/InteractiveActor.h"
#include "InteractiveDoor.generated.h"


UCLASS()
class PROJECTJSM_API AInteractiveDoor : public AInteractiveActor
{
	GENERATED_BODY()

public:
	AInteractiveDoor();
	virtual void TriggerInteractive() override;
	void RotateTimelineBind();
	UFUNCTION()
	void DoorRotUpdate(const float& Value);
	UFUNCTION()
	void FinishRot();

	UFUNCTION(NetMulticast, Reliable)
	void SM_SetActorRelativeRotation(const FRotator& NewRot);

	virtual void Tick(float DeltaTime) override;
protected:
	virtual void BeginPlay() override;

private:
	bool bOpen = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UCurveFloat> DoorRotYawCurve;

	FTimeline DoorRotYawTimeline;

	FRotator OriginRotation = FRotator();
};
