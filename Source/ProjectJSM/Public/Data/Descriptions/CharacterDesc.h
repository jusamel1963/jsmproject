// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "CharacterDesc.generated.h"


USTRUCT(BlueprintType)
struct PROJECTJSM_API FCharacterDesc : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character Description")
	FString KeyName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character Description")
	FText DisplayName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character Description")
	TObjectPtr<class UJSMCharacterDataAsset> CharacterDataAsset;
};
