// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Weapons/Weapon.h"
#include "RangeWeapon.generated.h"


UCLASS()
class PROJECTJSM_API ARangeWeapon : public AWeapon
{
	GENERATED_BODY()
public:
	ARangeWeapon();
	void CurrentMagAmmoCalculate(const int& InCount, const bool& bIncrease);
	void CurrentBagAmmoCalculate(const int& InCount, const bool& bIncrease);
	void Reload();
	void FireConsumeAmmo();

	FORCEINLINE void SetCurrentMagAmmo(const int& InAmmo) { CurrentMagAmmo = InAmmo; }
	FORCEINLINE void SetCurrentBagAmmo(const int& InAmmo) { CurrentBagAmmo = InAmmo; }
	FORCEINLINE uint8 GetCurrentMagAmmo() const { return CurrentMagAmmo; }
	FORCEINLINE uint8 GetCurrentBagAmmo() const { return CurrentBagAmmo; }
	uint8 GetMagAmmoCapacity() const;
	uint8 GetBagAmmoCapacity() const;

protected:
	virtual void InitializeWeaponResources() override;

protected:
	UPROPERTY(EditInstanceOnly, Replicated, Category = "RangeWeapon")
	uint8 CurrentMagAmmo;
	UPROPERTY(EditInstanceOnly, Replicated, Category = "RangeWeapon")
	uint8 CurrentBagAmmo;
};
