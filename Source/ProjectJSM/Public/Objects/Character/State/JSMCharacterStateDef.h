// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once


enum class EJSMCharacterInitializeStateID : uint8
{
	None = 0,
	Spawned,
	Initialize,
	PostInitialize,
	Done,
};