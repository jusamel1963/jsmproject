// Copyright JSM Studio, Inc. All Rights Reserved.

#include "UIs/JSMUIData.h"
#include "Framework/JSMGameInstance.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "Framework/JSMUIBridgeSubSystem.h"
#include "AbilitySystem/AttributeSet/JSMAttributeSet.h"
#include "Objects/Character/CharacterBase.h"

#define JSM_ADD_ATTRIBUTE_CHANGE_CALLBACK_FOR_CHARACTER_DATA(AttributeName, OwnerCharacter, UIBridgeSubSystem)															\
if(IsValid(OwnerCharacter))																																				\
{																																										\
	UAbilitySystemComponent* __AbilitySystemComponent = OwnerCharacter->GetAbilitySystemComponent();																	\
	UJSMAttributeSet* __JSMAttributeSet = OwnerCharacter->GetAttributeSet<UJSMAttributeSet>();																			\
	if (ensure(IsValid(__AbilitySystemComponent) && IsValid(__JSMAttributeSet)))																						\
	{																																									\
		int32 __UID = OwnerCharacter->GetUniqueID();																													\
		TWeakObjectPtr<UJSMUIBridgeSubsystem> TargetBridgeSubsystem = UIBridgeSubSystem;																				\
		AttributeChangeCallback_##AttributeName = __AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(__JSMAttributeSet->Get##AttributeName##Attribute()).AddLambda([__UID, TargetBridgeSubsystem](const struct FOnAttributeChangeData& __Data) \
		{																																								\
			if(TargetBridgeSubsystem.IsValid())																															\
			{																																							\
				TargetBridgeSubsystem->SetUIData_CharacterData(__UID, TEXT(#AttributeName), __Data.NewValue);															\
			}																																							\
		});																																								\
	}																																									\
}

#define JSM_REMOVE_ATTRIBUTE_CHANGE_CALLBACK_FOR_CHARACTER_DATA(AttributeName, OwnerCharacter)																			\
if(IsValid(OwnerCharacter))																																				\
{																																										\
	UAbilitySystemComponent* __AbilitySystemComponent = OwnerCharacter->GetAbilitySystemComponent();																	\
	const UJSMAttributeSet* __JSMAttributeSet = OwnerCharacter->GetAttributeSet<UJSMAttributeSet>();															        \
	if (ensure(IsValid(__AbilitySystemComponent) && IsValid(__JSMAttributeSet)))																						\
	{																																									\
		__AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(__JSMAttributeSet->Get##AttributeName##Attribute()).Remove(AttributeChangeCallback_##AttributeName);			\
	}																																									\
}

void FUIData_Character::Initialize(class ACharacterBase* OwnerCharacter)
{
	check(IsValid(OwnerCharacter));

	UJSMAttributeSet* AttributeSet = OwnerCharacter->GetAttributeSet<UJSMAttributeSet>();
	check(IsValid(AttributeSet));

	// initialize
	CurrHealth = AttributeSet->GetCurrHealth();
	MaxHealth = AttributeSet->GetMaxHealth();
	CalculatedMaxHealth = AttributeSet->GetCalculatedMaxHealth();

	CurrStamina = AttributeSet->GetCurrStamina();
	MaxStamina = AttributeSet->GetMaxStamina();

	UJSMUIBridgeSubsystem* UIBridgeSubSystem = JSMFL::GetGameInstanceSubsystem<UJSMUIBridgeSubsystem>();
	JSM_ADD_ATTRIBUTE_CHANGE_CALLBACK_FOR_CHARACTER_DATA(CurrHealth, OwnerCharacter, UIBridgeSubSystem);
	JSM_ADD_ATTRIBUTE_CHANGE_CALLBACK_FOR_CHARACTER_DATA(MaxHealth, OwnerCharacter, UIBridgeSubSystem);
	JSM_ADD_ATTRIBUTE_CHANGE_CALLBACK_FOR_CHARACTER_DATA(CalculatedMaxHealth, OwnerCharacter, UIBridgeSubSystem);
	JSM_ADD_ATTRIBUTE_CHANGE_CALLBACK_FOR_CHARACTER_DATA(CurrStamina, OwnerCharacter, UIBridgeSubSystem);
	JSM_ADD_ATTRIBUTE_CHANGE_CALLBACK_FOR_CHARACTER_DATA(MaxStamina, OwnerCharacter, UIBridgeSubSystem);
}

void FUIData_Character::Deinitialize(class ACharacterBase* OwnerCharacter)
{
	JSM_REMOVE_ATTRIBUTE_CHANGE_CALLBACK_FOR_CHARACTER_DATA(CurrHealth, OwnerCharacter);
	JSM_REMOVE_ATTRIBUTE_CHANGE_CALLBACK_FOR_CHARACTER_DATA(MaxHealth, OwnerCharacter);
	JSM_REMOVE_ATTRIBUTE_CHANGE_CALLBACK_FOR_CHARACTER_DATA(CalculatedMaxHealth, OwnerCharacter);
	JSM_REMOVE_ATTRIBUTE_CHANGE_CALLBACK_FOR_CHARACTER_DATA(CurrStamina, OwnerCharacter);
	JSM_REMOVE_ATTRIBUTE_CHANGE_CALLBACK_FOR_CHARACTER_DATA(MaxStamina, OwnerCharacter);
}


