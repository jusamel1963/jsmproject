// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#define REQ_GAMEPLAYTAG(a) FGameplayTag::RequestGameplayTag(FName(a))