// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "JSMDamageCtrlComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTJSM_API UJSMDamageCtrlComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UJSMDamageCtrlComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void InitializeComponent() override;
	virtual void UninitializeComponent() override;

protected:
	void OnHealthDamaged(const struct FGameplayEffectModCallbackData& Data);

protected:
	UPROPERTY(Transient)
	class ACharacterBase* OwnerCharacter;
	TWeakObjectPtr<class UJSMAttributeSet> AttributeSetBase;
	TWeakObjectPtr<class UJSMAbilitySystemComponent> AbilitySystemComponent;
};
