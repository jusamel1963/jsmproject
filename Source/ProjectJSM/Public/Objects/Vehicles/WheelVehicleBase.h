// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "WheeledVehiclePawn.h"
#include "Objects/Vehicles/VehicleInterface.h"
#include "WheelVehicleBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API AWheelVehicleBase : public AWheeledVehiclePawn, public IVehicleInterface
{
	GENERATED_BODY()

public:
	AWheelVehicleBase();
	void SS_CreateVehicleInfo();
	void SS_SetRepVehicleInfo(class UVehicleInfo* InRepVehicleInfo);
	class UVehicleInfo* GetRepVehicleInfo() const;
	class UJSMVehicleDataAsset* GetVehicleDataAsset() const;
	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void EnterVehicle(class ACharacterBase* InEnterPlayer) override;
	virtual void ExitVehicle(const uint32& InID) override;
	virtual bool IsOverlapPlayer(const uint32& Id) const override;
	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OpverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnOverlapEnd(class UPrimitiveComponent* OpverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	virtual void ForwardMovementAction(const float& InValue) override;
	virtual void RightMovementAction(const float& InValue) override;
	virtual void CameraUpAction(const float& InValue) override;
	virtual void CameraRightAction(const float& InValue) override;

	UFUNCTION(Server, Reliable)
	void CS_ExitVehicle(const uint32& InID);

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnRep_VehicleInfo();

	virtual void LoadAssetAsync(FJSMVehicleDataLoadComplete LoadCompleteDelegate) override;
	virtual void LoadAssetCompleted() override;
	virtual void InitializeVehicleResources() override;
	virtual void LoadSynchronous() override;

public:
	FSimpleDelegate OnLoadComplete;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class UBoxComponent> EnterBoxRightComponent;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class UBoxComponent> EnterBoxLeftComponent;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class USpringArmComponent> SpringArmComp;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class UCameraComponent> CameraComp;

protected:
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Replicated, Category = "Vehicle")
	FName RepVehicleDataKey;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_VehicleInfo)
	TObjectPtr<class UVehicleInfo> RepVehicleInfo;

	bool bUseAsyncLoad = true;
	bool bLoadCompleted = false;

	FJSMVehicleAssetLoadRequest AssetLoadRequest;

	UPROPERTY(Transient)
	TMap<uint32, TWeakObjectPtr<class ACharacterBase>> OverlapPlayers;

	UPROPERTY(Transient)
	TMap<FName, FSeatInfo> SeatArray;

	uint32 DriverID = 0;
};
