// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Network/JSMReplicatedObjectInterface.h"
#include "UObject/NoExportTypes.h"
#include "Data/Common/GameEnum.h"
#include "Library/ALSCharacterEnumLibrary.h"
#include "JSMWeaponInfo.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChangeWeaponInfo);

UCLASS(BlueprintType, Blueprintable)
class PROJECTJSM_API UJSMWeaponInfo : public UObject, public IJSMReplicatedObjectInterface
{
	GENERATED_BODY()

public:
	virtual void SetOwnerActor(AActor* InOwnerActor);
	virtual void GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const override;
	virtual bool HasAuthority() override;
	FName GetRepWeaponDataKey() const;
	void SetRepWeaponDataKey(FName InRepWeaponDataKey);
	virtual TWeakObjectPtr<class UJSMWeaponDataAsset> GetWeaponDataAsset() const;
	virtual EALSOverlayState GetWeaponCategory();
	virtual bool IsSupportedForNetworking() const override { return true; }

protected:
	UFUNCTION()
	void OnRep_WeaponDataKey();

public:
	FOnChangeWeaponInfo OnChangelWeaponInfo;

protected:
	UPROPERTY(Transient, VisibleInstanceOnly, Replicated)
	uint64 RepUniqueId;

	UPROPERTY(Transient, VisibleInstanceOnly, ReplicatedUsing = "OnRep_WeaponDataKey")
	FName RepWeaponDataKey;

	UPROPERTY(Transient)
	TObjectPtr<AActor> OwnerActor;

	UPROPERTY(VisibleInstanceOnly)
	TWeakObjectPtr<class UJSMWeaponDataAsset> WeaponDataAsset;
};
