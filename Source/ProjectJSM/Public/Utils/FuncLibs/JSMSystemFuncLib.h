// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

namespace JSMFL
{
	PROJECTJSM_API UWorld* GetGameWorld();

	template<typename TGameInstanceSubsystem>
	TGameInstanceSubsystem* GetGameInstanceSubsystem()
	{
		return UJSMGameInstance::GetSubsystem<TGameInstanceSubsystem>(GetGameWorld());
	}

	template<typename TEngineSubsystem>
	TEngineSubsystem* GetEngineSubsystem()
	{
		return GEngine->GetEngineSubsystem<TEngineSubsystem>();
	}
};