// Copyright JSM Studio, Inc. All Rights Reserved.

#include "Test/TestActor.h"
#include "Components/StaticMeshComponent.h"
#include "Net/UnrealNetwork.h"
#include "Net/Core/PushModel/PushModel.h"

ATestActor::ATestActor()
{
	ItemStaticMeshComponent = CreateOptionalDefaultSubobject<UStaticMeshComponent>(TEXT("222"));
	ItemStaticMeshComponent->SetupAttachment(RootComponent);
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	RepItemCount = 0;
}

void ATestActor::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;
	DOREPLIFETIME_WITH_PARAMS_FAST(ATestActor, RepItemCount, SharedParams);
}

// Called when the game starts or when spawned
void ATestActor::BeginPlay()
{
	Super::BeginPlay();
	if (HasAuthority())
	{
		UE_LOG(LogTemp, Error, TEXT("Server ATestActor::BeginPlay()"));
		//MARK_PROPERTY_DIRTY_FROM_NAME(ATestActor, RepItemCount, this);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Client ATestActor::BeginPlay()"));
	}
	UStaticMesh* Test = TestActor2.LoadSynchronous();
	ItemStaticMeshComponent->SetStaticMesh(Test);
	TsE.Emplace("AS");
	TsE.Emplace("As");
	int a = 5 + 4;
}

// Called every frame
void ATestActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UE_LOG(LogTemp, Error, TEXT("TsE Size : %d"), TsE.Num());
	if (HasAuthority())
	{
		++RepItemCount;
		//if (RepItemCount < 10)
		//{
		//	//MARK_PROPERTY_DIRTY_FROM_NAME(ATestActor, RepItemCount, this);
		//}

		GEngine->AddOnScreenDebugMessage((uint64)-1, 0.01f, FColor::Cyan, FString::Printf(TEXT("Server DEBUGServerPushCheck: %d"), RepItemCount));
	}
	else
	{
		GEngine->AddOnScreenDebugMessage((uint64)-1, 0.01f, FColor::Cyan, FString::Printf(TEXT("Client DEBUGServerPushCheck: %d"), RepItemCount));
	}

}

void ATestActor::OnRep_ItemCount()
{
	if (HasAuthority())
	{
		UE_LOG(LogTemp, Error, TEXT("Server OnRep_ItemCount"));
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Client OnRep_ItemCount"));
	}
}