// Copyright JSM Studio, Inc. All Rights Reserved.

#include "FrameWork/JSMUIBridgeSubsystem.h"
#include "FrameWork/JSMGameInstance.h"
#include "Objects/Character/CharacterBase.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetStringLibrary.h"
#include "Core/Managers/JSMEventManager.h"

UJSMUIBridgeSubsystem::UJSMUIBridgeSubsystem()
{

}

void UJSMUIBridgeSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	CachedChildSubsystemClass = GetClass();

#if WITH_EDITOR
	InitializeTestUIData();
#endif
	InitializeUIData();

	UJSMGameInstance* GameInst = Cast<UJSMGameInstance>(GetGameInstance());
	check(GameInst);

	BindGlobalEvent(GameInst);
}
void UJSMUIBridgeSubsystem::InitializeUIData()
{
	ResetUIData();
}

void UJSMUIBridgeSubsystem::InitializeTestUIData()
{
	ResetUIData();
}

void UJSMUIBridgeSubsystem::Deinitialize()
{
	Super::Deinitialize();
	UnbindGlobalEvent();
}

void UJSMUIBridgeSubsystem::OnCharacterSpawned(UObject* Invoker, ACharacterBase* Character)
{
	InternalCharacterSpawned(Character);
	Character->OnEndPlay.AddDynamic(this, &UJSMUIBridgeSubsystem::OnCharacterEndPlay);
}

void UJSMUIBridgeSubsystem::OnCharacterEndPlay(AActor* Actor, EEndPlayReason::Type EndPlayReason)
{
	InteranlCharacterDestroyed(Cast<ACharacterBase>(Actor));
}

void UJSMUIBridgeSubsystem::ResetUIData()
{
	UIData = FUIData();

	ResetUIData_Outgame();
	ResetUIData_Ingame();
}

void UJSMUIBridgeSubsystem::ResetUIData_Outgame()
{
	UIData.OutgameData = FUIData_OutgameData();
}

void UJSMUIBridgeSubsystem::ResetUIData_Ingame()
{
	const int MAX_ACTIVEITEM_COUNT = 3;

	UIData.IngameData = FUIData_IngameData();
}

void UJSMUIBridgeSubsystem::InteranlCharacterDestroyed(ACharacterBase* Character)
{
	if (!ensure(IsValid(Character)))
	{
		return;
	}

	FString UID = FString::FromInt(Character->GetUniqueID());
	FUIData_Character* Founder = UIData.IngameData.CharacterData.Find(UID);
	if (Founder != nullptr)
	{
		Founder->Deinitialize(Character);
		UIData.IngameData.CharacterData.Remove(UID);
	}

	if (Character->IsLocalPlayerCharacter())
	{
		//UnbindLocalCharacterEvent(Character);
		UIData.IngameData.UserData.LocalCharacterData.Deinitialize(Character);
	}
}

void UJSMUIBridgeSubsystem::InternalCharacterSpawned(ACharacterBase* Character)
{
	FString UID = FString::FromInt(Character->GetUniqueID());
	if (UIData.IngameData.CharacterData.Contains(UID))
	{
		return;
	}

	UIData.IngameData.CharacterData.Emplace(UID).Initialize(Character);
	if (Character->IsLocallyControlled())
	{
		//BindLocalCharacterEvent(Character);
		UIData.IngameData.UserData.LocalCharacterData.Initialize(Character);
		//무기 관련 ui 추후
		InitilaizeLocalCharacterData(Character);
	}
}

bool UJSMUIBridgeSubsystem::SetUIData_Game(const FString& DataKey, bool Value)
{
	return SetUIData(DataKey, Value, true);
}

bool UJSMUIBridgeSubsystem::SetUIData_Game(const FString& DataKey, int32 Value)
{
	return SetUIData(DataKey, Value, true);
}

bool UJSMUIBridgeSubsystem::SetUIData_Game(const FString& DataKey, float Value)
{
	return SetUIData(DataKey, Value, true);
}

bool UJSMUIBridgeSubsystem::SetUIData_Game(const FString& DataKey, const FString& Value)
{
	return SetUIData(DataKey, Value, true);
}

bool UJSMUIBridgeSubsystem::SetUIData_Game(const FString& DataKey, const FText& Value)
{
	return SetUIData(DataKey, Value, true);
}

bool UJSMUIBridgeSubsystem::SetUIData(const FString& DataKey, bool Value, bool IsGameSide)
{
	TArray<FString> Keys = UKismetStringLibrary::ParseIntoArray(DataKey, TEXT("."));
	FProperty* FoundProp = nullptr;
	const void* FoundValuePtr = nullptr;

	FindMemberProperty(IsGameSide ? GetUIDataPtr() : GetUIData_UIPtr(), IsGameSide ? GetUIDataProperty() : GetUIData_UIProperty(), Keys, 0, FoundProp, FoundValuePtr);

	if (FBoolProperty* BoolProp = CastField<FBoolProperty>(FoundProp))
	{
		BoolProp->SetPropertyValue(const_cast<void*>(FoundValuePtr), Value);
		return true;
	}

	return false;
}

bool UJSMUIBridgeSubsystem::SetUIData(const FString& DataKey, int32 Value, bool IsGameSide)
{
	TArray<FString> Keys = UKismetStringLibrary::ParseIntoArray(DataKey, TEXT("."));
	FProperty* FoundProp = nullptr;
	const void* FoundValuePtr = nullptr;

	FindMemberProperty(IsGameSide ? GetUIDataPtr() : GetUIData_UIPtr(), IsGameSide ? GetUIDataProperty() : GetUIData_UIProperty(), Keys, 0, FoundProp, FoundValuePtr);

	if (FIntProperty* IntProp = CastField<FIntProperty>(FoundProp))
	{
		IntProp->SetPropertyValue(const_cast<void*>(FoundValuePtr), Value);
		return true;
	}

	return false;
}

bool UJSMUIBridgeSubsystem::SetUIData(const FString& DataKey, float Value, bool IsGameSide)
{
	TArray<FString> Keys = UKismetStringLibrary::ParseIntoArray(DataKey, TEXT("."));
	FProperty* FoundProp = nullptr;
	const void* FoundValuePtr = nullptr;

	FindMemberProperty(IsGameSide ? GetUIDataPtr() : GetUIData_UIPtr(), IsGameSide ? GetUIDataProperty() : GetUIData_UIProperty(), Keys, 0, FoundProp, FoundValuePtr);

	if (FFloatProperty* FloatProp = CastField<FFloatProperty>(FoundProp))
	{
		FloatProp->SetPropertyValue(const_cast<void*>(FoundValuePtr), Value);
		return true;
	}

	return false;
}

bool UJSMUIBridgeSubsystem::SetUIData(const FString& DataKey, const FString& Value, bool IsGameSide)
{
	TArray<FString> Keys = UKismetStringLibrary::ParseIntoArray(DataKey, TEXT("."));
	FProperty* FoundProp = nullptr;
	const void* FoundValuePtr = nullptr;

	FindMemberProperty(IsGameSide ? GetUIDataPtr() : GetUIData_UIPtr(), IsGameSide ? GetUIDataProperty() : GetUIData_UIProperty(), Keys, 0, FoundProp, FoundValuePtr);

	if (FStrProperty* StrProp = CastField<FStrProperty>(FoundProp))
	{
		StrProp->SetPropertyValue(const_cast<void*>(FoundValuePtr), Value);
		return true;
	}

	return false;
}

bool UJSMUIBridgeSubsystem::SetUIData(const FString& DataKey, const FText& Value, bool IsGameSide)
{
	TArray<FString> Keys = UKismetStringLibrary::ParseIntoArray(DataKey, TEXT("."));
	FProperty* FoundProp = nullptr;
	const void* FoundValuePtr = nullptr;

	FindMemberProperty(IsGameSide ? GetUIDataPtr() : GetUIData_UIPtr(), IsGameSide ? GetUIDataProperty() : GetUIData_UIProperty(), Keys, 0, FoundProp, FoundValuePtr);

	if (FTextProperty* TextProp = CastField<FTextProperty>(FoundProp))
	{
		TextProp->SetPropertyValue(const_cast<void*>(FoundValuePtr), Value);
		return true;
	}

	return false;
}

bool UJSMUIBridgeSubsystem::GetUIData_Game(const FString& DataKey, bool& OutValue) const
{
	return GetUIData(DataKey, OutValue, true);
}

bool UJSMUIBridgeSubsystem::GetUIData_Game(const FString& DataKey, int32& OutValue) const
{
	return GetUIData(DataKey, OutValue, true);
}

bool UJSMUIBridgeSubsystem::GetUIData_Game(const FString& DataKey, float& OutValue) const
{
	return GetUIData(DataKey, OutValue, true);
}

bool UJSMUIBridgeSubsystem::GetUIData_Game(const FString& DataKey, FString& OutValue) const
{
	return GetUIData(DataKey, OutValue, true);
}

bool UJSMUIBridgeSubsystem::GetUIData_Game(const FString& DataKey, FText& OutValue) const
{
	return GetUIData(DataKey, OutValue, true);
}

bool UJSMUIBridgeSubsystem::GetUIData(const FString& DataKey, bool& OutValue, bool IsGameSide) const
{
	TArray<FString> Keys = UKismetStringLibrary::ParseIntoArray(DataKey, TEXT("."));
	FProperty* FoundProp = nullptr;
	const void* FoundValuePtr = nullptr;

	FindMemberProperty(IsGameSide ? GetUIDataPtr() : GetUIData_UIPtr(), IsGameSide ? GetUIDataProperty() : GetUIData_UIProperty(), Keys, 0, FoundProp, FoundValuePtr);

	if (FBoolProperty* BoolProp = CastField<FBoolProperty>(FoundProp))
	{
		OutValue = BoolProp->GetPropertyValue(FoundValuePtr);
		return true;
	}
	return false;
}

bool UJSMUIBridgeSubsystem::GetUIData(const FString& DataKey, float& OutValue, bool IsGameSide) const
{
	TArray<FString> Keys = UKismetStringLibrary::ParseIntoArray(DataKey, TEXT("."));
	FProperty* FoundProp = nullptr;
	const void* FoundValuePtr = nullptr;

	FindMemberProperty(IsGameSide ? GetUIDataPtr() : GetUIData_UIPtr(), IsGameSide ? GetUIDataProperty() : GetUIData_UIProperty(), Keys, 0, FoundProp, FoundValuePtr);

	if (FFloatProperty* FloatProp = CastField<FFloatProperty>(FoundProp))
	{
		OutValue = FloatProp->GetPropertyValue(FoundValuePtr);
		return true;
	}

	return false;
}

bool UJSMUIBridgeSubsystem::GetUIData(const FString& DataKey, int32& OutValue, bool IsGameSide) const
{
	TArray<FString> Keys = UKismetStringLibrary::ParseIntoArray(DataKey, TEXT("."));
	FProperty* FoundProp = nullptr;
	const void* FoundValuePtr = nullptr;

	FindMemberProperty(IsGameSide ? GetUIDataPtr() : GetUIData_UIPtr(), IsGameSide ? GetUIDataProperty() : GetUIData_UIProperty(), Keys, 0, FoundProp, FoundValuePtr);

	if (FIntProperty* IntProp = CastField<FIntProperty>(FoundProp))
	{
		OutValue = IntProp->GetPropertyValue(FoundValuePtr);
		return true;
	}
	return false;
}

bool UJSMUIBridgeSubsystem::GetUIData(const FString& DataKey, FString& OutValue, bool IsGameSide) const
{
	TArray<FString> Keys = UKismetStringLibrary::ParseIntoArray(DataKey, TEXT("."));
	FProperty* FoundProp = nullptr;
	const void* FoundValuePtr = nullptr;

	FindMemberProperty(IsGameSide ? GetUIDataPtr() : GetUIData_UIPtr(), IsGameSide ? GetUIDataProperty() : GetUIData_UIProperty(), Keys, 0, FoundProp, FoundValuePtr);

	if (FStrProperty* StrProp = CastField<FStrProperty>(FoundProp))
	{
		OutValue = StrProp->GetPropertyValue(FoundValuePtr);
		return true;
	}
	return false;
}

bool UJSMUIBridgeSubsystem::GetUIData(const FString& DataKey, FText& OutValue, bool IsGameSide) const
{
	TArray<FString> Keys = UKismetStringLibrary::ParseIntoArray(DataKey, TEXT("."));
	FProperty* FoundProp = nullptr;
	const void* FoundValuePtr = nullptr;

	FindMemberProperty(IsGameSide ? GetUIDataPtr() : GetUIData_UIPtr(), IsGameSide ? GetUIDataProperty() : GetUIData_UIProperty(), Keys, 0, FoundProp, FoundValuePtr);

	if (FTextProperty* TextProp = CastField<FTextProperty>(FoundProp))
	{
		OutValue = TextProp->GetPropertyValue(FoundValuePtr);
		return true;
	}

	return false;
}

bool UJSMUIBridgeSubsystem::IsUIDataReady()
{
	return GetUIDataProperty() != nullptr && GetUIData_UIProperty() != nullptr && GetUIData_UIPrevProperty() != nullptr;
}

const void* UJSMUIBridgeSubsystem::GetUIDataPtr() const
{
	if (!IsUIDataReady())
	{
		return nullptr;
	}

	return GetUIDataProperty()->ContainerPtrToValuePtr<const void>(this);
}

const void* UJSMUIBridgeSubsystem::GetUIData_UIPtr() const
{
	if (!IsUIDataReady())
	{
		return nullptr;
	}

	return GetUIData_UIProperty()->ContainerPtrToValuePtr<const void>(this);
}

const void* UJSMUIBridgeSubsystem::GetUIData_UIPrevPtr() const
{
	if (!IsUIDataReady())
	{
		return nullptr;
	}

	return GetUIData_UIPrevProperty()->ContainerPtrToValuePtr<const void>(this);
}

FProperty* UJSMUIBridgeSubsystem::GetUIDataProperty()
{
	UClass* CurrClass = GetDynamicClass();
	FProperty* UIDataProp = CurrClass->FindPropertyByName(TEXT("UIData"));

	return UIDataProp;
}

FProperty* UJSMUIBridgeSubsystem::GetUIData_UIProperty()
{
	UClass* CurrClass = GetDynamicClass();
	FProperty* UIDataProp = CurrClass->FindPropertyByName(TEXT("UIData_UI"));

	return UIDataProp;
}

FProperty* UJSMUIBridgeSubsystem::GetUIData_UIPrevProperty()
{
	UClass* CurrClass = GetDynamicClass();
	FProperty* UIDataProp = CurrClass->FindPropertyByName(TEXT("UIData_UIPrev"));

	return UIDataProp;
}

UClass* UJSMUIBridgeSubsystem::GetDynamicClass()
{
	UWorld* GameWorld = JSMFL::GetGameWorld();
	if (IsValid(GameWorld))
	{
		UGameInstance* GameInst = UGameplayStatics::GetGameInstance(GameWorld);
		if (IsValid(GameInst))
		{
			UJSMUIBridgeSubsystem* Inst = GameInst->GetSubsystem<UJSMUIBridgeSubsystem>();
			if (IsValid(Inst) && Inst->CachedChildSubsystemClass == nullptr)
			{
				Inst->CachedChildSubsystemClass = Inst->GetClass();
			}

			return Inst->CachedChildSubsystemClass;
		}
	}

	UClass* CurrClass = UJSMUIBridgeSubsystem::StaticClass();
	for (TObjectIterator<UClass> It; It; ++It)
	{
		if (It->IsChildOf(UJSMUIBridgeSubsystem::StaticClass()) && *It != CurrClass && !It->HasAnyClassFlags(CLASS_Abstract))
		{
			return *It;
		}
	}

	return UJSMUIBridgeSubsystem::StaticClass();
}

void UJSMUIBridgeSubsystem::FindMemberProperty(const void* ValuePtr, FProperty* Prop, TArray<FString>& PropertyNames, int32 CurrDepth, OUT FProperty*& FoundProperty, OUT const void*& FoundValuePtr)
{
	FString CurrPropertyName;
	int32 ArrayIdx = -1;
	FString MapKey;
	SplitCollectionName(PropertyNames[CurrDepth], CurrPropertyName, ArrayIdx, MapKey);

	if (CurrDepth == PropertyNames.Num() - 1)
	{
		FString PropName = Prop->GetName();

		if (PropName.Compare(TEXT("UIData_UI")) == 0)
		{
			PropName.RemoveFromEnd(TEXT("_UI"), ESearchCase::CaseSensitive);
		}

		if (CurrPropertyName == PropName)
		{
			if (FArrayProperty* ArrayProp = CastField<FArrayProperty>(Prop))
			{
				FScriptArrayHelper ArrayHelper(ArrayProp, ValuePtr);
				if (ArrayIdx >= 0 && ArrayIdx < ArrayHelper.Num())
				{
					FoundProperty = ArrayProp->Inner;
					FoundValuePtr = ArrayHelper.GetRawPtr(ArrayIdx);
					return;
				}

			}

			FoundProperty = Prop;
			FoundValuePtr = ValuePtr;

			return;
		}
	}

	if (CurrDepth < 0 || CurrDepth >= PropertyNames.Num() - 1)
	{
		return;
	}

	if (FStructProperty* StructProp = CastField<FStructProperty>(Prop))
	{
		FString NextPropertyName;
		int32 NextArrayIdx = -1;
		FString NextMapKey;
		SplitCollectionName(PropertyNames[CurrDepth + 1], NextPropertyName, NextArrayIdx, NextMapKey);

		FProperty* InnerProp = StructProp->Struct->FindPropertyByName(FName(NextPropertyName));
		if (InnerProp == nullptr)
		{
			return;
		}

		const void* InnerValuePtr = InnerProp->ContainerPtrToValuePtr<const void>(ValuePtr);

		FindMemberProperty(InnerValuePtr, InnerProp, PropertyNames, ++CurrDepth, FoundProperty, FoundValuePtr);
	}
	else if (FArrayProperty* ArrayProp = CastField<FArrayProperty>(Prop))
	{
		FProperty* InnerProp = ArrayProp->Inner;
		FScriptArrayHelper ArrayHelper(ArrayProp, ValuePtr);

		if (ArrayIdx < 0 || ArrayIdx >= ArrayHelper.Num())
		{
			return;
		}

		uint8* ElemPtr = ArrayHelper.GetRawPtr(ArrayIdx);

		if (CastField<FStructProperty>(InnerProp))
		{
			PropertyNames.Insert(CurrPropertyName, CurrDepth + 1);
		}

		FindMemberProperty(ElemPtr, InnerProp, PropertyNames, ++CurrDepth, FoundProperty, FoundValuePtr);
	}
	else if (FMapProperty* MapProp = CastField<FMapProperty>(Prop))
	{
		FScriptMapHelper MapHelper(MapProp, ValuePtr);
		const int32 NumElements = MapHelper.Num();

		FStrProperty* KeyStrProp = CastField<FStrProperty>(MapProp->KeyProp);
		FProperty* ValueProp = MapProp->ValueProp;

		if (KeyStrProp == nullptr)
		{
			UE_LOG(LogTemp, Error, TEXT("Key is not string"));
			return;
		}

		bool FoundKey = false;
		for (int32 ElementIdx = 0, SparseIdx = 0; ElementIdx < NumElements; ++SparseIdx)
		{
			if (MapHelper.IsValidIndex(SparseIdx))
			{
				uint8* KeyPtr = MapHelper.GetKeyPtr(SparseIdx);
				FString KeyStr = KeyStrProp->GetPropertyValue(KeyPtr);

				if (MapKey.Compare(KeyStr) == 0)
				{
					FoundKey = true;
					uint8* MapValuePtr = MapHelper.GetValuePtr(SparseIdx);

					if (CastField<FStructProperty>(ValueProp))
					{
						PropertyNames.Insert(CurrPropertyName, CurrDepth + 1);
					}

					FindMemberProperty(MapValuePtr, ValueProp, PropertyNames, ++CurrDepth, FoundProperty, FoundValuePtr);
				}

				++ElementIdx;
			}
		}

		if (!FoundKey)
		{
			return;
		}
	}
}

void UJSMUIBridgeSubsystem::SplitCollectionName(const FString& InArrayName, OUT FString& OutCollectionName, OUT int32& OutIndex, OUT FString& OutMapKey)
{
	int32 StartBracketIdx = -1;
	OutIndex = -1;
	OutMapKey = TEXT("");
	if (InArrayName.FindLastChar('[', StartBracketIdx))
	{
		OutCollectionName = InArrayName.Mid(0, StartBracketIdx);
		FString IdxString = InArrayName.Mid(StartBracketIdx + 1, InArrayName.Len() - StartBracketIdx - 2);

		if (IdxString.IsNumeric())
		{
			OutIndex = FCString::Atoi(*IdxString);
		}

		OutMapKey = IdxString;
	}
	else
	{
		OutCollectionName = InArrayName;
		OutIndex = -1;
		OutMapKey = TEXT("");
	}
}

void UJSMUIBridgeSubsystem::InitilaizeLocalCharacterData(ACharacterBase* Character)
{
	if (!ensure(Character))
	{
		return;
	}

	LocalCharacterUID = Character->GetUniqueID();
	UIData.IngameData.UserData.LocalCharacterUID = FString::FromInt(LocalCharacterUID);
}

void UJSMUIBridgeSubsystem::BindGlobalEvent(UJSMGameInstance* GameInst)
{
	if (!IsValid(GameInst))
	{
		return;
	}

	BIND_EVENT_WITH_GAMEINSTANCE_JSM(GameInst, JSMEvent.InGame.CharacterSpawned, this, &UJSMUIBridgeSubsystem::OnCharacterSpawned);
	BIND_EVENT_WITH_GAMEINSTANCE_JSM(GameInst, JSMEvent.InGame.CharacterPhysicalDamaged, this, &UJSMUIBridgeSubsystem::OnCharacterPhysicalDamaged);
}

void UJSMUIBridgeSubsystem::UnbindGlobalEvent()
{
	UNBIND_EVENT_JSM(JSMEvent.InGame.CharacterSpawned, this, &UJSMUIBridgeSubsystem::OnCharacterSpawned);
	UNBIND_EVENT_JSM(JSMEvent.InGame.CharacterPhysicalDamaged, this, &UJSMUIBridgeSubSystem::OnCharacterPhysicalDamaged);
}

void UJSMUIBridgeSubsystem::OnCharacterPhysicalDamaged(UObject* Invoker, ACharacterBase* Victim, ACharacterBase* Attacker, float DamageAmount)
{
	//float PhysicalDamage = 0.0f;
	//GetUIData_CharacterData(Victim->GetUniqueID(), TEXT("DamageData.PhysicalDamage"), PhysicalDamage);
	//SetUIData_CharacterData(Victim->GetUniqueID(), TEXT("DamageData.PhysicalDamage"), PhysicalDamage + DamageAmount);
	//DamagedUIDList.Add(FString::FromInt(Victim->GetUniqueID()));
}