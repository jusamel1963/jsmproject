// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "JSMUIData.generated.h"

USTRUCT(BlueprintType)
struct FUIData_CharacterDamage
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PhysicalDamage;
};

USTRUCT(BlueprintType)
struct FUIData_Character
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString DisplayName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsPlayerCharacter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsLocalPlayerCharacter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CurrHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CurrStamina;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxStamina;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CalculatedMaxHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bDead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FUIData_CharacterDamage DamageData;

public:
	void Initialize(class ACharacterBase* OwnerCharacter);
	void Deinitialize(class ACharacterBase* OwnerCharacter);

private:
	FDelegateHandle AttributeChangeCallback_CurrHealth;
	FDelegateHandle AttributeChangeCallback_MaxHealth;
	FDelegateHandle AttributeChangeCallback_CalculatedMaxHealth;
	FDelegateHandle AttributeChangeCallback_CurrStamina;
	FDelegateHandle AttributeChangeCallback_MaxStamina;
};


USTRUCT(BlueprintType)
struct FUIData_UserData
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FUIData_Character LocalCharacterData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString LocalCharacterUID;
};

USTRUCT(BlueprintType)
struct FUIData_IngameData
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FString, FUIData_Character> CharacterData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FUIData_UserData UserData;
};

USTRUCT(BlueprintType)
struct FUIData_OutgameData
{
	GENERATED_BODY()
public:
};

USTRUCT(BlueprintType)
struct FUIData_PersistentData
{
	GENERATED_BODY()
public:
};

USTRUCT(BlueprintType)
struct FUIData
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FUIData_IngameData IngameData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FUIData_OutgameData OutgameData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FUIData_PersistentData PersistentData;
};
