// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UIs/BaseUserWidget.h"
#include "BloodScreenWidget.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UBloodScreenWidget : public UBaseUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(meta = (BindWidgetOptional), BlueprintReadWrite)
	TObjectPtr<class UOverlay> Overlay_Root;

	UPROPERTY(meta = (BindWidgetOptional), Transient, BlueprintReadWrite)
	TObjectPtr<class UImage> Img_Blood;

	UPROPERTY(meta = (BindWidgetAnimOptional), Transient, BlueprintReadOnly)
	TObjectPtr<class UWidgetAnimation> BloodAnimation;

public:
	void SetBloodScreenOpacity(const float& InOpacity);
	void PlayBloodAnimation();

protected:
	virtual void NativeConstruct() override;
};
