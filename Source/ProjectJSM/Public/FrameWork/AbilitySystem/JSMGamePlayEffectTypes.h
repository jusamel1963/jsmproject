// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectTypes.h"
#include "JSMGameplayEffectTypes.generated.h"

UENUM(BlueprintType)
enum class EJSMGameplayEffectArgumentType : uint8
{
	Float = 0		UMETA(DisplayName = "Floating Point"),
	GameplayEffect	UMETA(DisplayName = "Gameplay Effect"),
};

USTRUCT()
struct PROJECTJSM_API FJSMGameplayEffectArgument
{
	GENERATED_BODY()

	FJSMGameplayEffectArgument() {}

	FJSMGameplayEffectArgument(FGameplayTag InArgumentTag, float InArgumentValue)
		: ArgumentTag(InArgumentTag)
		, ArgumentType(EJSMGameplayEffectArgumentType::Float)
		, FloatValue(InArgumentValue)
	{
	}

	FJSMGameplayEffectArgument(FGameplayTag InArgumentTag, TSubclassOf<class UJSMGameplayEffect> InArgumentValue)
		: ArgumentTag(InArgumentTag)
		, ArgumentType(EJSMGameplayEffectArgumentType::Float)
		, GameplayEffectValue(InArgumentValue)
	{
	}

	bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess);

	UPROPERTY(EditDefaultsOnly)
	FGameplayTag ArgumentTag;

	UPROPERTY(EditDefaultsOnly)
	EJSMGameplayEffectArgumentType ArgumentType;

	UPROPERTY(EditDefaultsOnly)
	float FloatValue;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UJSMGameplayEffect> GameplayEffectValue;
};

USTRUCT()
struct PROJECTJSM_API FJSMGameplayEffectContext : public FGameplayEffectContext
{
	GENERATED_BODY()

	virtual UScriptStruct* GetScriptStruct() const override
	{
		return FJSMGameplayEffectContext::StaticStruct();
	}

	virtual FGameplayEffectContext* Duplicate() const override
	{
		FJSMGameplayEffectContext* NewContext = new FJSMGameplayEffectContext();
		*NewContext = *this;
		NewContext->AddActors(Actors);
		if (GetHitResult())
		{
			NewContext->AddHitResult(*GetHitResult(), true);
		}
		return NewContext;
	}

	virtual bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess) override;

	void SetArguments(const TArray<FJSMGameplayEffectArgument>& InArguments);
	void AddArguments(const TArray<FJSMGameplayEffectArgument>& InArguments);

	void TransferFloatArgumentsToSetByCallerMagnitudes(TSharedPtr<FGameplayEffectSpec> Spec);

	void SetArgument(FGameplayTag ArgumentTag, float Value);
	float GetArgument(FGameplayTag ArgumentTag, float DefaultValue) const;

	void SetArgument(FGameplayTag ArgumentTag, TSubclassOf<class UJSMGameplayEffect> Value);
	TSubclassOf<class UJSMGameplayEffect> GetArgument(FGameplayTag ArgumentTag, TSubclassOf<class UJSMGameplayEffect> DefaultValue) const;

protected:
	UPROPERTY(EditDefaultsOnly)
	TArray<FJSMGameplayEffectArgument> Arguments;

private:
	template<typename T>
	void AddArgument(FGameplayTag ArgumentTag, T ArgumentValue)
	{
		Arguments.Emplace(ArgumentTag, ArgumentValue);
	}
};