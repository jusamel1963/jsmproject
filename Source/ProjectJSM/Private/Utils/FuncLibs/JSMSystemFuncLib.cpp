// Copyright JSM Studio, Inc. All Rights Reserved.

#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "Engine/CollisionProfile.h"

PROJECTJSM_API UWorld* JSMFL::GetGameWorld()
{
	//IsValid(GEngine->GameViewport) Not Play Delicated Type In Editor 

	if (!IsValid(GEngine) || !IsValid(GEngine->GameViewport))
	{
		return nullptr;
	}

	UWorld* World = GEngine->GameViewport->GetWorld();
	if (IsValid(World) && (!World->bIsTearingDown))
	{
		return World;
	}
	return nullptr;
}
