// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "JSMCharacterEvent.generated.h"


USTRUCT()
struct PROJECTJSM_API FJSMCharacterEvent
{
	GENERATED_BODY()

public:	
	DECLARE_EVENT_TwoParams(UWeaponCtrlComponent, FWeaponAttached, class UWeaponCtrlComponent*, int32);
	FWeaponAttached OnWeaponAttached;

	DECLARE_EVENT_OneParam(UJSMInventoryComponent, FItemRemoved, FName);
	FItemRemoved OnWeaponRemoved;

	DECLARE_EVENT_OneParam(UJSMInventoryComponent, FItemAdded, class UJSMItemInfo*);
	FItemAdded OnWeaponAdded;
};

