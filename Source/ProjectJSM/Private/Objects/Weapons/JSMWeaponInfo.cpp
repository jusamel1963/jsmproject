// Copyright JSM Studio, Inc. All Rights Reserved.

#include "Objects/Weapons/JSMWeaponInfo.h"
#include "Net/UnrealNetwork.h"
#include "Data/DataAssets/ItemData/JSMWeaponDataAsset.h"
#include "Net/Core/PushModel/PushModel.h"
#include "Statics/JSMClientStatics.h"
#include "Log.h"

FName UJSMWeaponInfo::GetRepWeaponDataKey() const
{
	return RepWeaponDataKey;
}

void UJSMWeaponInfo::SetRepWeaponDataKey(FName InRepWeaponDataKey)
{
	if (!HasAuthority())
	{
		return;
	}

	MARK_PROPERTY_DIRTY_FROM_NAME(UJSMWeaponInfo, RepWeaponDataKey, this);
	RepWeaponDataKey = InRepWeaponDataKey;
	OnRep_WeaponDataKey();
}

void UJSMWeaponInfo::SetOwnerActor(AActor* InOwnerActor)
{
	OwnerActor = InOwnerActor;
}

void UJSMWeaponInfo::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(UJSMWeaponInfo, RepUniqueId, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(UJSMWeaponInfo, RepWeaponDataKey, SharedParams);
}

bool UJSMWeaponInfo::HasAuthority()
{
	AActor* OuterActor = Cast<AActor>(GetOuter());

	if (IsValid(OuterActor) && OuterActor->HasAuthority())
	{
		return true;
	}

	return false;
}

TWeakObjectPtr<class UJSMWeaponDataAsset> UJSMWeaponInfo::GetWeaponDataAsset() const
{
	return WeaponDataAsset;
}

EALSOverlayState UJSMWeaponInfo::GetWeaponCategory()
{
	if (WeaponDataAsset.IsValid())
	{
		return WeaponDataAsset->WeaponCategory;
	}

	return EALSOverlayState::Default;
}

void UJSMWeaponInfo::OnRep_WeaponDataKey()
{
	WeaponDataAsset = Cast<UJSMWeaponDataAsset>(UJSMClientStatics::GetItemDataAsset(RepWeaponDataKey));
	OnChangelWeaponInfo.Broadcast();
}
