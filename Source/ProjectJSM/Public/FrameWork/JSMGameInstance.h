// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AdvancedFriendsGameInstance.h"
#include "JSMGameInstance.generated.h"


UCLASS()
class PROJECTJSM_API UJSMGameInstance : public UAdvancedFriendsGameInstance
{
	GENERATED_BODY()

public:
    UJSMGameInstance(const FObjectInitializer& ObjectInitializer);

    virtual void Init() override;
    virtual void Shutdown() override;

    virtual void OnStart() override;

    static UJSMGameInstance* GetGameInstance();
	class UJSMEventManager* GetEventManager() const { return EventManager; }
	template<class T>
	static T* GetSubsystem(UWorld* World)
	{
		check(World);
		UGameInstance* GameInstance = World->GetGameInstance();
		if (IsValid(GameInstance))
		{
			return GameInstance->GetSubsystem<T>();
		}
		return nullptr;
	};

	UFUNCTION(BlueprintImplementableEvent)
	void CreateSession();

	UFUNCTION(BlueprintImplementableEvent)
	void FindSession();

private:
	UPROPERTY(Transient)
	TObjectPtr<class UJSMEventManager> EventManager;
};
