// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/TimeLineComponent.h"
#include "InteractiveInterface.h"
#include "InteractiveActor.generated.h"

UCLASS()
class PROJECTJSM_API AInteractiveActor : public AActor, public IInteractiveInterface
{
	GENERATED_BODY()
	
public:	
	AInteractiveActor();
	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OpverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnOverlapEnd(class UPrimitiveComponent* OpverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	virtual bool IsOverlapPlayer(const uint32& Id) const override;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class UStaticMeshComponent> InteractiveStaticMeshComponent;

	UPROPERTY(VisibleAnywhere, Transient)
	TObjectPtr<class UBoxComponent> TriggerComponent;

protected:
	UPROPERTY(Transient)
	TMap<uint32, TWeakObjectPtr<class ACharacterBase>> OverlapPlayers;
};
