// Copyright JSM Studio, Inc. All Rights Reserved.


#include "AbilitySystem/Ability/JSMGA_WeaponCycleDown.h"
#include "Abilities/Tasks/AbilityTask_WaitDelay.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"
#include "Core/Managers/JSMEventManager.h"
#include "Data/DataAssets/ItemData/JSMWeaponDataAsset.h"
#include "Framework/JSMGameInstance.h"
#include "Objects/Character/CharacterBase.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Objects/Weapons/Weapon.h"
#include "Objects/Weapons/JSMWeaponInfo.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"

UJSMGA_WeaponCycleDown::UJSMGA_WeaponCycleDown()
	: Super()
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;

	FGameplayTag AbilityTag = REQ_GAMEPLAYTAG("Ability.Action.WeaponCycleDown");
	AbilityTags.AddTag(AbilityTag);
	ActivationOwnedTags.AddTag(AbilityTag);

	FGameplayTag AbilityTriggerTag = REQ_GAMEPLAYTAG("Event.Action.WeaponCycleDown");
	FAbilityTriggerData TriggerData;
	TriggerData.TriggerTag = AbilityTriggerTag;
	TriggerData.TriggerSource = EGameplayAbilityTriggerSource::GameplayEvent;
	AbilityTriggers.Add(TriggerData);
}

void UJSMGA_WeaponCycleDown::PostCommitActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::PostCommitActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
	APlayerCharacter* OwnerCharacter = Cast<APlayerCharacter>(ActorInfo->AvatarActor);
	if (!IsValid(OwnerCharacter))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	UWeaponCtrlComponent* WeaponCtrlComponent = OwnerCharacter->GetWeaponCtrl();
	if (IsValid(WeaponCtrlComponent))
	{
		WeaponCtrlComponent->ExchangeWeapon(false);
	}
	EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
}

void UJSMGA_WeaponCycleDown::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}
