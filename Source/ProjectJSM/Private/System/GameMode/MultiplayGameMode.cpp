// Copyright JSM Studio, Inc. All Rights Reserved.


#include "System/GameMode/MultiplayGameMode.h"
#include "Statics/JSMClientStatics.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "Data/Descriptions/JSMDataManagerSubsystem.h"
#include "Blueprint/UserWidget.h"
#include "UIs/LobbyMainMenuWidget.h"
#include "FrameWork/JSMPlayerController.h"
#include "Objects/Character/CharacterBase.h"
#include "Objects/Character/AICharacter.h"

AMultiplayGameMode::AMultiplayGameMode()
{

}

void AMultiplayGameMode::PostLogin(APlayerController* NewPlayer)
{
	AJSMPlayerController* JSMPlayerController = Cast<AJSMPlayerController>(NewPlayer);
	if (!IsValid(JSMPlayerController))
	{
		return;
	}

	Super::PostLogin(NewPlayer);

	FTimerHandle DelayHandle;
	const TWeakObjectPtr<AJSMPlayerController> WeakControllerPtr(JSMPlayerController);
	const TWeakObjectPtr<AMultiplayGameMode> WeakPtr(this);

	GetWorld()->GetTimerManager().SetTimer(DelayHandle, FTimerDelegate::CreateLambda([WeakControllerPtr, WeakPtr]()
	{
		if (WeakControllerPtr.IsValid())
		{
			WeakControllerPtr->NotifyOtherUserEnterMe();
		}

		if (WeakPtr.IsValid())
		{
			WeakPtr->RefershAICharacter();
		}

	}), 0.2f, false);
}

APlayerController* AMultiplayGameMode::Login(UPlayer* NewPlayer, ENetRole InRemoteRole, const FString& Portal, const FString& Options, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
	APlayerController* playerController = Super::Login(NewPlayer, InRemoteRole, Portal, Options, UniqueId, ErrorMessage);;

	_charType = TEXT("Player1");
	return playerController;
}

void AMultiplayGameMode::BeginPlay()
{
	Super::BeginPlay();

	UWorld* World = UJSMClientStatics::GetGameWorld();
	APlayerController* PlayerController = UJSMClientStatics::GetLocalPlayerController(World);
	if (IsValid(PlayerController))
	{
		PlayerController->bShowMouseCursor = false;
		PlayerController->SetInputMode(FInputModeGameOnly());
	}
}

APawn* AMultiplayGameMode::SpawnDefaultPawnAtTransform_Implementation(AController* NewPlayer, const FTransform& SpawnTransform)
{
	UClass* charClass = GetDefaultPawnClassForController(NewPlayer);

	ACharacterBase* defaultChar = nullptr;

	if (IsValid(charClass))
	{
		defaultChar = ACharacterBase::InstantiateCharacter(NewPlayer, TSubclassOf<ACharacterBase>(charClass), SpawnTransform, _charType);
	}

	return defaultChar;
}

void AMultiplayGameMode::AddAICharacter(class AAICharacter* InRefAI)
{
	AIList.Emplace(InRefAI);
}

void AMultiplayGameMode::RefershAICharacter()
{
	for (int i = 0; i < AIList.Num(); i++)
	{
		if (AIList[i].IsValid())
		{
			AIList[i]->RefershAICharacter();
		}
	}
}
