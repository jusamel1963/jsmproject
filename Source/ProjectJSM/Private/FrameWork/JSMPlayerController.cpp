// Copyright JSM Studio, Inc. All Rights Reserved.

#include "FrameWork/JSMPlayerController.h"
#include "Objects/Character/CharacterBase.h"
#include "Objects/Character/PlayerCharacter.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputMappingContext.h"
#include "AbilitySystem/JSMAbilitySystemComponent.h"
#include "AbilitySystem/JSMAbilitySystemImpl.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "Data/Descriptions/JSMDataManagerSubsystem.h"
#include "Statics/JSMClientStatics.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "UIs/RootGamePlayWidget.h"
#include "Objects/Character/Component/WeaponCtrlComponent.h"
#include "Objects/Character/Component/JSMInventoryComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Core/Managers/JSMEventManager.h"
#include "Objects/Character/PlayerCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Data/DataAssets/CharacterData/JSMCharacterDataAsset.h"
#include "Objects/Character/Component/CoverSystemComponent.h"
#include "Objects/Vehicles/VehicleInterface.h"
#include "Objects/Character/Component/CoverSystemComponent.h"

void AJSMPlayerController::OnPossess(APawn* NewPawn)
{
	Super::OnPossess(NewPawn);
	SetupGameInput();
}

void AJSMPlayerController::OnRep_Pawn()
{
	Super::OnRep_Pawn();
	SetupGameInput();
}

void AJSMPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (IsLocalController())
	{
		RootGamePlayWidget = CreateWidget(GetGameInstance(), UJSMClientStatics::GetJSMDataCollection()->MasterWidgetClass.LoadSynchronous());
		RootGamePlayWidget->AddToViewport();
		URootGamePlayWidget* RootWidget = Cast< URootGamePlayWidget>(RootGamePlayWidget);
		if (IsValid(RootWidget) && IsValid(PossessedCharacter))
		{
			RootWidget->SetParentOwnID(PossessedCharacter->GetUniqueID(), this);
		}
	}
}

void AJSMPlayerController::AcknowledgePossession(class APawn* InPawn)
{
	Super::AcknowledgePossession(InPawn);

	if (IsValid(InPawn))
	{
		// if character
		ACharacterBase* CharacterBase = Cast<ACharacterBase>(InPawn);
		if (IsValid(CharacterBase))
		{
			CharacterBase->SetupAbilitySystemForClient();
		}
	}
}

void AJSMPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent);
	if (EnhancedInputComponent)
	{
		BindActions(GameInputMappingContext);
	}
	else
	{
		UE_LOG(LogTemp, Fatal, TEXT("GameInput System to be activated in project settings to function properly"));
	}
}

void AJSMPlayerController::SetupGameInput()
{
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		FModifyContextOptions Options;
		Options.bForceImmediately = 1;
		Subsystem->AddMappingContext(GameInputMappingContext, 1, Options);
	}
}

void AJSMPlayerController::WeaponHudUpdate(UTexture2D* InPrimaryWeapon, UTexture2D* InHolsterWeapon, const FText& InMagAmmo, const FText& InMagCapacity, UTexture2D* CrossHair, UTexture2D* WeaponScope)
{
	URootGamePlayWidget* RootGameWidget = Cast<URootGamePlayWidget>(RootGamePlayWidget);
	if (!IsValid(RootGameWidget))
	{
		return;
	}
	RootGameWidget->Update_WeaponAndCrossHairHud(InPrimaryWeapon, InHolsterWeapon, InMagAmmo, InMagCapacity, CrossHair, WeaponScope);
}

void AJSMPlayerController::WeaponMenuUpdate(UTexture2D* InHipWeapon, UTexture2D* InBackWeapon, UTexture2D* InMeleeWeapon, UTexture2D* InThrowableWeapon,
	const FText& InHipWeaponAmmo, const FText& InBackWeaponAmmo)
{
	URootGamePlayWidget* RootGameWidget = Cast<URootGamePlayWidget>(RootGamePlayWidget);
	if (!IsValid(RootGameWidget))
	{
		return;
	}
	
	RootGameWidget->SetWeaponImage(InHipWeapon, 0, InHipWeaponAmmo);
	RootGameWidget->SetWeaponImage(InBackWeapon, 1, InBackWeaponAmmo);
	RootGameWidget->SetWeaponImage(InMeleeWeapon, 2);
	RootGameWidget->SetWeaponImage(InThrowableWeapon, 3);
}

void AJSMPlayerController::ProgressHudUpdate(const float& InValue)
{
	URootGamePlayWidget* RootGameWidget = Cast<URootGamePlayWidget>(RootGamePlayWidget);
	if (!IsValid(RootGameWidget))
	{
		return;
	}
	RootGameWidget->Update_ProgressBar(InValue);
}

void AJSMPlayerController::PickupWeaponAction(const FInputActionValue& Value)
{
	if (!IsLocalController())
	{
		return;
	}

	TObjectPtr<APlayerCharacter> PlayerCharacter = Cast<APlayerCharacter>(PossessedCharacter);
	if (IsValid(PlayerCharacter) && Value.Get<bool>())
	{
		PlayerCharacter->PickupWeaponAction();
	}
}

void AJSMPlayerController::InteractionAction(const FInputActionValue& Value)
{
	if (!IsLocalController() || !Value.Get<bool>())
	{
		return;
	}

	TObjectPtr<APlayerCharacter> PlayerCharacter = Cast<APlayerCharacter>(PossessedCharacter);
	if (IsValid(PlayerCharacter))
	{
		PlayerCharacter->InteractionAction();
		return;
	}
	else
	{
		CS_InteractionAction();
		PossessedCharacter = Cast<AALSBaseCharacter>(GetPawn());
	}
}

void AJSMPlayerController::WeaponCycleAction(const FInputActionValue& Value)
{
	UE_LOG(LogTemp, Warning, TEXT("AJSMPlayerController::WeaponCycleAction InValue : %lf"), Value.GetMagnitude());
	TObjectPtr<APlayerCharacter> PlayerCharacter = Cast<APlayerCharacter>(PossessedCharacter);
	if (!IsValid(PlayerCharacter))
	{
		return;
	}

	if (PlayerCharacter->GetRotationMode() == EALSRotationMode::Aiming)
	{
		UWeaponCtrlComponent* WeaponCtrlComponent = PlayerCharacter->GetWeaponCtrl();
		if (IsValid(WeaponCtrlComponent))
		{
			WeaponCtrlComponent->SetWeaponScopeFOV((Value.GetMagnitude()));
		}
	}
	else
	{
		UJSMAbilitySystemComponent* AbilitySystemComponent = Cast<UJSMAbilitySystemComponent>(PlayerCharacter->GetAbilitySystemComponent());
		if (IsValid(AbilitySystemComponent))
		{
			FGameplayEventData EventData;
			FGameplayTag EventTag;

			if (Value.GetMagnitude() > 0)
			{
				EventTag = REQ_GAMEPLAYTAG("Event.Action.WeaponCycleUp");
			}
			else
			{
				EventTag = REQ_GAMEPLAYTAG("Event.Action.WeaponCycleDown");
			}

			EventData.EventTag = EventTag;
			EventData.Instigator = PlayerCharacter;
			AbilitySystemComponent->HandleGameplayEvent(EventTag, &EventData);
		}
	}
}

void AJSMPlayerController::AttackAction(const FInputActionValue& Value)
{
	UE_LOG(LogTemp, Warning, TEXT("AttackAction"));
	TObjectPtr<APlayerCharacter> PlayerCharacter = Cast<APlayerCharacter>(PossessedCharacter);
	if (!IsValid(PlayerCharacter))
	{
		return;
	}

	UWeaponCtrlComponent* WeaponCtrlComponent = Cast<UWeaponCtrlComponent>(PlayerCharacter->GetWeaponCtrl());
	if (IsValid(WeaponCtrlComponent))
	{
		WeaponCtrlComponent->AttackAction(Value);
	}
}

void AJSMPlayerController::WeaponrReloadAction(const FInputActionValue& Value)
{
	TObjectPtr<APlayerCharacter> PlayerCharacter = Cast<APlayerCharacter>(PossessedCharacter);
	if (!IsValid(PlayerCharacter))
	{
		return;
	}

	UJSMAbilitySystemComponent* AbilitySystemComponent = Cast<UJSMAbilitySystemComponent>(PlayerCharacter->GetAbilitySystemComponent());
	if (IsValid(AbilitySystemComponent))
	{
		FGameplayEventData EventData;
		FGameplayTag EventTag = REQ_GAMEPLAYTAG("Event.Action.Reload");
		EventData.EventTag = EventTag;
		EventData.Instigator = PlayerCharacter;
		AbilitySystemComponent->HandleGameplayEvent(EventTag, &EventData);
	}
}

void AJSMPlayerController::SetVisibleWeaponScope(const bool& bValue)
{
	URootGamePlayWidget* RootGameWidget = Cast<URootGamePlayWidget>(RootGamePlayWidget);
	if (!IsValid(RootGameWidget))
	{
		return;
	}
	RootGameWidget->SetVisibleWeaponScope(bValue);
}

void AJSMPlayerController::SetVisibleProgressBar(const bool& bVisible, const FText& LoadingName)
{
	URootGamePlayWidget* RootGameWidget = Cast<URootGamePlayWidget>(RootGamePlayWidget);
	if (!IsValid(RootGameWidget))
	{
		return;
	}
	RootGameWidget->SetVisibleProgressBar(bVisible, LoadingName);
}

void AJSMPlayerController::NotifyOtherUserEnterMe()
{
	if (IsValid(GetWorld()))
	{
		for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
		{
			AJSMPlayerController* JSMPlayerController = Cast<AJSMPlayerController>(Iterator->Get());
			if (IsValid(JSMPlayerController))
			{
				JSMPlayerController->RefreshCharacterData();
			}
		}
	}
}

void AJSMPlayerController::RefreshCharacterData()
{
	TObjectPtr<APlayerCharacter> PlayerCharacter = Cast<APlayerCharacter>(PossessedCharacter);
	if (!IsValid(PlayerCharacter))
	{
		return;
	}

	UWeaponCtrlComponent* WeaponCtrlComponent = PlayerCharacter->GetWeaponCtrl();
	if (IsValid(WeaponCtrlComponent))
	{
		WeaponCtrlComponent->RefreshCharacterWeaponCtrlComponent();
	}

	UJSMInventoryComponent* InventoryComponent = PlayerCharacter->GetInventoryComponent();
	if (IsValid(InventoryComponent))
	{
		InventoryComponent->RefreshCharacterWeaponCtrlComponent();
	}

	UCoverSystemComponent* CoverSystemComponent = PlayerCharacter->GetCoverSystemCtrl();
	if (IsValid(CoverSystemComponent))
	{
		CoverSystemComponent->RefreshCoverSystemComponent();
	}
}

void AJSMPlayerController::OpenWeaponMenuAction(const FInputActionValue& Value)
{
	TObjectPtr<APlayerCharacter> PlayerCharacter = Cast<APlayerCharacter>(PossessedCharacter);
	if (!IsValid(PlayerCharacter))
	{
		return;
	}

	UJSMCharacterDataAsset* CharDataAsset = PlayerCharacter->GetCharacterDataAsset();
	if (IsValid(CharDataAsset))
	{
		UGameplayStatics::PlaySound2D(GetWorld(), CharDataAsset->WeaponMenuOpenSound.Get());
	}

	URootGamePlayWidget* RootWidget = Cast< URootGamePlayWidget>(RootGamePlayWidget);
	if (!IsValid(RootWidget))
	{
		return;
	}

	OverlayMenuOpen = (OverlayMenuOpen == true) ? false : true;
	RootWidget->SetVisibleWeaponMenu(OverlayMenuOpen);
	if (OverlayMenuOpen)
	{
		SetInputMode_GameAndUI();
	}
	else
	{
		SetInputMode_Game();
	}
}

void AJSMPlayerController::SetInputMode_Game()
{
	bShowMouseCursor = false;
	SetInputMode(FInputModeGameOnly());
}

void AJSMPlayerController::SetInputMode_GameAndUI()
{
	bShowMouseCursor = true;
	SetInputMode(FInputModeGameAndUI());
}

void AJSMPlayerController::WeaponAttach(int InWeaponIdx)
{
	TObjectPtr<APlayerCharacter> PlayerCharacter = Cast<APlayerCharacter>(PossessedCharacter);
	if (!IsValid(PlayerCharacter))
	{
		return;
	}

	UJSMCharacterDataAsset* CharDataAsset = PlayerCharacter->GetCharacterDataAsset();
	if (IsValid(CharDataAsset))
	{
		UGameplayStatics::PlaySound2D(GetWorld(), CharDataAsset->WeaponMenuOpenSound.Get());
	}

	OverlayMenuOpen = false;
	SetInputMode_Game();

	UWeaponCtrlComponent* WeaponCtlComp = PlayerCharacter->GetWeaponCtrl();
	if (!IsValid(WeaponCtlComp))
	{
		return;
	}

	WeaponCtlComp->SetWeaponAttach(InWeaponIdx);
}

void AJSMPlayerController::TakeDownAction(const FInputActionValue& Value)
{
	APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(PossessedCharacter);
	if (!IsValid(PlayerCharacter))
	{
		return;
	}
	PlayerCharacter->TakeDownAction();
}

void AJSMPlayerController::CoverAction(const FInputActionValue& Value)
{
	APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(PossessedCharacter);
	if (!IsValid(PlayerCharacter))
	{
		return;
	}

	UCoverSystemComponent* CoverSystemCtlComp = PlayerCharacter->GetCoverSystemCtrl();
	if (!IsValid(CoverSystemCtlComp))
	{
		return;
	}

	CoverSystemCtlComp->CS_UpdateCoverState();
}

void AJSMPlayerController::Custom_ForwardMovementAction(const FInputActionValue& Value)
{
	if (!IsValid(GetPawn()) || !GetPawn()->Implements<UVehicleInterface>())
	{
		return;
	}

	IVehicleInterface* VehicleInterface = Cast<IVehicleInterface>(GetPawn());
	if (VehicleInterface == nullptr)
	{
		return;
	}
	VehicleInterface->ForwardMovementAction(Value.GetMagnitude());
}

void AJSMPlayerController::Custom_RightMovementAction(const FInputActionValue& Value)
{
	if (!IsValid(GetPawn()) || !GetPawn()->Implements<UVehicleInterface>())
	{
		return;
	}

	IVehicleInterface* VehicleInterface = Cast<IVehicleInterface>(GetPawn());
	if (VehicleInterface == nullptr)
	{
		return;
	}
	VehicleInterface->RightMovementAction(Value.GetMagnitude());
}

void AJSMPlayerController::Custom_CameraUpAction(const FInputActionValue& Value)
{
	if (!IsValid(GetPawn()) || !GetPawn()->Implements<UVehicleInterface>())
	{
		return;
	}

	IVehicleInterface* VehicleInterface = Cast<IVehicleInterface>(GetPawn());
	if (VehicleInterface == nullptr)
	{
		return;
	}
	VehicleInterface->CameraUpAction(Value.GetMagnitude());
}

void AJSMPlayerController::Custom_CameraRightAction(const FInputActionValue& Value)
{
	if (!IsValid(GetPawn()) || !GetPawn()->Implements<UVehicleInterface>())
	{
		return;
	}

	IVehicleInterface* VehicleInterface = Cast<IVehicleInterface>(GetPawn());
	if (VehicleInterface == nullptr)
	{
		return;
	}
	VehicleInterface->CameraRightAction(Value.GetMagnitude());
}

void AJSMPlayerController::CS_InteractionAction_Implementation()
{
	if (!IsValid(GetPawn()) || !GetPawn()->Implements<UVehicleInterface>())
	{
		return;
	}

	IVehicleInterface* VehicleInterface = Cast<IVehicleInterface>(GetPawn());
	if (VehicleInterface == nullptr)
	{
		return;
	}

	VehicleInterface->ExitVehicle(GetUniqueID());
}