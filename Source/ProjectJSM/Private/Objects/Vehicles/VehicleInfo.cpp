// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Objects/Vehicles/VehicleInfo.h"
#include "Net/UnrealNetwork.h"
#include "Data/DataAssets/ItemData/JSMVehicleDataAsset.h"
#include "Net/Core/PushModel/PushModel.h"
#include "Statics/JSMClientStatics.h"
#include "Log.h"

FName UVehicleInfo::GetRepVehicleDataKey() const
{
	return RepVehicleDataKey;
}

void UVehicleInfo::SetRepVehicleDataKey(FName InRepVehicleDataKey)
{
	if (!HasAuthority())
	{
		return;
	}

	MARK_PROPERTY_DIRTY_FROM_NAME(UVehicleInfo, RepVehicleDataKey, this);
	RepVehicleDataKey = InRepVehicleDataKey;
	OnRep_VehicleDataKey();
}

void UVehicleInfo::SetOwnerActor(AActor* InOwnerActor)
{
	OwnerActor = InOwnerActor;
}

void UVehicleInfo::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(UVehicleInfo, RepUniqueId, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(UVehicleInfo, RepVehicleDataKey, SharedParams);
}

bool UVehicleInfo::HasAuthority()
{
	AActor* OuterActor = Cast<AActor>(GetOuter());

	if (IsValid(OuterActor) && OuterActor->HasAuthority())
	{
		return true;
	}

	return false;
}

TWeakObjectPtr<class UJSMVehicleDataAsset> UVehicleInfo::GetVehicleDataAsset() const
{
	return VehicleDataAsset;
}

//EALSOverlayState UVehicleInfo::GetVehicleCategory()
//{
//	if (VehicleDataAsset.IsValid())
//	{
//		return VehicleDataAsset->VehicleCategory;
//	}
//
//	return EALSOverlayState::Default;
//}

void UVehicleInfo::OnRep_VehicleDataKey()
{
	VehicleDataAsset = Cast<UJSMVehicleDataAsset>(UJSMClientStatics::GetItemDataAsset(RepVehicleDataKey));
	OnChangelVehicleInfo.Broadcast();
}
