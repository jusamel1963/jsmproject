// Copyright JSM Studio, Inc. All Rights Reserved.


#include "Animation/JSMCharacterAnimInstance.h"
#include "Objects/Character/CharacterBase.h"
#include "Objects/Character/Component/CoverSystemComponent.h"

void UJSMCharacterAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();
	JSMCharacter = Cast<ACharacterBase>(Character);
}

void UJSMCharacterAnimInstance::NativeBeginPlay()
{
}

void UJSMCharacterAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	if (IsValid(JSMCharacter))
	{
		UCoverSystemComponent* CoverSystemComp = JSMCharacter->GetCoverSystemCtrl();
		if (IsValid(CoverSystemComp))
		{
			bMoveLeft = CoverSystemComp->IsMoveLeft();
			bMoveRight = CoverSystemComp->IsMoveRight();
			CurrentCoverState = CoverSystemComp->GetCoverType();
			bCanMoveLeft = JSMCharacter->IsCanMoveLeft();
			bCanMoveRight = JSMCharacter->IsCanMoveRight();
			bRiding = JSMCharacter->bRiding;
		}
	}
}