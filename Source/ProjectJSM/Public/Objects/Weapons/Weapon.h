// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"


DECLARE_DELEGATE(FJSMWeaponDataLoadComplete);

USTRUCT()
struct PROJECTJSM_API FJSMWeaponAssetLoadRequest
{
	GENERATED_USTRUCT_BODY()

public:
	FJSMWeaponDataLoadComplete CompleteDelegate;
	TSharedPtr<struct FStreamableHandle> AsyncLoadHandle;
};


enum class EALSOverlayState : uint8;

UCLASS()
class PROJECTJSM_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	AWeapon();
	class UJSMWeaponInfo* GetRepWeaponInfo() const;
	class UJSMWeaponDataAsset* GetWeaponDataAsset() const;

	void SS_CreateWeaponInfo();
	void SS_SetRepWeaponInfo(class UJSMWeaponInfo* InRepWeaponInfo);
	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	bool IsLoadCompleted();
	bool IsEmpty() const;

	void BeginPickup();
	void EndPickup();
	class UStaticMesh* GetStaticMesh() const;
	void SetWeaponHideAndCollision(bool bHide, bool bCollide);
	FORCEINLINE UMeshComponent* GetMeshComponent() const { return StaticMeshComp; }
	void SetWeaponScopeFOV(const float& InValue);
	void SetResetWeaponScopeFOV();
	void SetOwnerCharacter(class ACharacterBase* InOwnChar) { OwnerCharacter = InOwnChar; }
	FORCEINLINE FName GetRepWeaponDataKey() const { return RepWeaponDataKey; }
	void SetWeaponNotUse();
	void WaitDestroy(float InWaitTime);
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnRep_WeaponInfo();

	virtual void LoadAssetAsync(FJSMWeaponDataLoadComplete LoadCompleteDelegate);
	virtual void LoadAssetCompleted();
	virtual void InitializeWeaponResources();
	virtual void LoadSynchronous();

public:	
	virtual void Tick(float DeltaTime) override;
	FORCEINLINE bool IsEquipped() { return bEquipped; }

public:
	FSimpleDelegate OnLoadComplete;

protected:
	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly)
	TObjectPtr<class UStaticMeshComponent> StaticMeshComp;

	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly)
	TObjectPtr<class USkeletalMeshComponent> SkeletalMeshComponent;

	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly)
	TObjectPtr<class UCameraComponent> CameraComponent;

	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly)
	TObjectPtr<class UPointLightComponent> PointLightComponent;

	static FName StaticMeshComponentName;
	static FName SceneComponentName;
	static FName SkeletalMeshComponentName;
	static FName CameraComponentName;
	static FName PointLightComponentName;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Replicated, Category = "Weapon")
	FName RepWeaponDataKey;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_WeaponInfo)
	TObjectPtr<class UJSMWeaponInfo> RepWeaponInfo;

	UPROPERTY(Transient)
	TObjectPtr<class ACharacterBase> OwnerCharacter;

	bool bEquipped = false;

	FJSMWeaponAssetLoadRequest AssetLoadRequest;

	bool bUseAsyncLoad = true;
	bool bLoadCompleted = false;

	bool bBeginPickup = false;

	const float DefaultMinScopeRange = 10.0f;
	const float DefaultMaxScopeRange = 90.0f;

	float MinScopeRange = DefaultMinScopeRange;
	float MaxScopeRange = DefaultMaxScopeRange;

	UPROPERTY(EditDefaultsOnly)
	TObjectPtr<class USphereComponent> RootSphereComp;
};
