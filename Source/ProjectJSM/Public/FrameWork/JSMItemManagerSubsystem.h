// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "Framework/JSMGameInstance.h"
#include "JSMItemManagerSubsystem.generated.h"

UCLASS()
class PROJECTJSM_API UJSMItemManagerSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
public:
	class UJSMItemInfo* CreateJSMItemInfo(FName DataKey, AActor* InOwnerActor);
	class UJSMWeaponInfo* CreateJSMWeaponInfo(FName DataKey, AActor* InOwnerActor);
	class UVehicleInfo* CreateJSMVehicleInfo(FName DataKey, AActor* InOwnerActor);

	class AJSMItemActor* SpawnItemActor(UWorld* InWorld, UClass* Class, FName ItemDataKey, FTransform const& Transform, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters());
	class AWeapon* SpawnWeaponActor(UWorld* InWorld, UClass* Class, FName WeaponDataKey, FTransform const& Transform, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters());
	class AProjectTileBase* SpawnProjectTileActor(UWorld* InWorld, UClass* Class, FName WeaponDataKey, FTransform const& Transform, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters());
	class AShellEjectBase* SpawnShellEjectBaseActor(UWorld* InWorld, UClass* Class, FName WeaponDataKey, FTransform const& Transform, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters());
	class ANoWheelVehicleBase* SpawnNoWheelVehicleActor(UWorld* InWorld, UClass* Class, FName VehicleDataKey, FTransform const& Transform, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters());
	class AWheelVehicleBase* SpawnWheelVehicleActor(UWorld* InWorld, UClass* Class, FName VehicleDataKey, FTransform const& Transform, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters());

};
