// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "AnimNotifyBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UAnimNotifyBase : public UAnimNotify
{
	GENERATED_BODY()
	
};
