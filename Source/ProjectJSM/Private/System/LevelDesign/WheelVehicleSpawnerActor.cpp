// Copyright JSM Studio, Inc. All Rights Reserved.


#include "System/LevelDesign/WheelVehicleSpawnerActor.h"
#include "Framework/JSMItemManagerSubsystem.h"
#include "Objects/Vehicles/NoWheelVehicleBase.h"
#include "Objects/Vehicles/VehicleInfo.h"
#include "Utils/FuncLibs/JSMSystemFuncLib.h"
#include "Statics/JSMClientStatics.h"
#include "Data/DataAssets/ItemData/JSMVehicleDataAsset.h"
#include "Objects/Vehicles/WheelVehicleBase.h"

AWheelVehicleSpawnerActor::AWheelVehicleSpawnerActor()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AWheelVehicleSpawnerActor::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() != NM_Client)
	{
		Execute();
	}
}

void AWheelVehicleSpawnerActor::Execute()
{
	if (!HasAuthority())
	{
		return;
	}

	if (SpawnedActors.Num() > 0)
	{
		return;
	}

	if (bRespawnable)
	{
		SetRespawnTimer();
	}
	else
	{
		SpawnVehicle();
	}
}

void AWheelVehicleSpawnerActor::SpawnVehicle()
{
	if (!HasAuthority())
	{
		return;
	}

	for (FVehicleSpawnData& VehicleSpawnData : SpawnData)
	{
		UJSMItemManagerSubsystem* ItemManager = JSMFL::GetGameInstanceSubsystem<UJSMItemManagerSubsystem>();
		if (IsValid(ItemManager))
		{
			UJSMVehicleDataAsset* VehicleDataAsset = UJSMClientStatics::GetItemDataAsset<UJSMVehicleDataAsset>(VehicleSpawnData.VehicleKeyName);

			if (!IsValid(VehicleDataAsset))
			{
				return;
			}

			if (!VehicleDataAsset->NoWheelVehicleBPClass.IsNull())
			{
				UClass* bpClass = VehicleDataAsset->NoWheelVehicleBPClass.LoadSynchronous();

				FTransform SpawnTransform = GetTransform();
				ANoWheelVehicleBase* VehicleActor = ItemManager->SpawnNoWheelVehicleActor(GetWorld(), bpClass, VehicleSpawnData.VehicleKeyName, SpawnTransform);
				if (IsValid(VehicleActor))
				{
					SpawnedActors.Add(VehicleActor);
					VehicleActor->OnDestroyed.AddDynamic(this, &AWheelVehicleSpawnerActor::OnDestroyActor);
				}
			}
			else if(!VehicleDataAsset->WheelVehicleBPClass.IsNull())
			{
				UClass* bpClass = VehicleDataAsset->WheelVehicleBPClass.LoadSynchronous();

				FTransform SpawnTransform = GetTransform();
				AWheelVehicleBase* VehicleActor = ItemManager->SpawnWheelVehicleActor(GetWorld(), bpClass, VehicleSpawnData.VehicleKeyName, SpawnTransform);
				if (IsValid(VehicleActor))
				{
					SpawnedActors.Add(VehicleActor);
					VehicleActor->OnDestroyed.AddDynamic(this, &AWheelVehicleSpawnerActor::OnDestroyActor);
				}
			}
		}
	}
}

void AWheelVehicleSpawnerActor::SetRespawnTimer()
{
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	TimerManager.SetTimer(SpawnerTimerHandle, this, &AWheelVehicleSpawnerActor::OnSpawnTimer, RespawnTimeInterval, false, RespawnTimeInterval);
}

void AWheelVehicleSpawnerActor::OnSpawnTimer()
{
	SpawnVehicle();
}

void AWheelVehicleSpawnerActor::OnDestroyActor(AActor* DestroyedActor)
{
	SpawnedActors.Remove(DestroyedActor);

	if (SpawnedActors.Num() == 0 && bRespawnable)
	{
		SetRespawnTimer();
	}
}
