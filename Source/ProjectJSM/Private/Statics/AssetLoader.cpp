// Copyright JSM Studio, Inc. All Rights Reserved.

#include "Statics/AssetLoader.h"
#include "Engine/AssetManager.h"

UAssetLoader::UAssetLoader()
{

}

TSharedPtr<FStreamableHandle> UAssetLoader::AssetLoadAsyncStart(const FSoftObjectPath& objPath, FStreamableDelegate finishDelegate)
{
	FStreamableManager& streamableManager = UAssetManager::GetStreamableManager();
	return streamableManager.RequestAsyncLoad(objPath, finishDelegate);
}

TSharedPtr<FStreamableHandle> UAssetLoader::AssetLoadAsyncStart(const FSoftObjectPath& objPath)
{
	FStreamableManager& streamableManager = UAssetManager::GetStreamableManager();
	return streamableManager.RequestAsyncLoad(objPath);
}

TSharedPtr<FStreamableHandle> UAssetLoader::AssetLoadAsyncStart(const TArray<FSoftObjectPath>& objPaths)
{
	if (objPaths.Num() > 0)
	{
		FStreamableManager& streamableManager = UAssetManager::GetStreamableManager();
		return streamableManager.RequestAsyncLoad(objPaths);
	}

	return nullptr;
}

TSharedPtr<FStreamableHandle> UAssetLoader::AssetLoadAsyncStart(const TArray<FSoftObjectPath>& objPaths, FStreamableDelegate finishDelegate)
{
	if (objPaths.Num() > 0)
	{
		FStreamableManager& streamableManager = UAssetManager::GetStreamableManager();
		return streamableManager.RequestAsyncLoad(objPaths, finishDelegate);
	}

	return nullptr;
}

bool UAssetLoader::IsAssetAsyncLoading(const FSoftObjectPath& objPath)
{
	FStreamableManager& streamableManager = UAssetManager::GetStreamableManager();
	return streamableManager.IsAsyncLoadComplete(objPath);
}

void UAssetLoader::FinishAssetAsyncLoading(const FSoftObjectPath& objPath)
{
	FStreamableManager& streamableManager = UAssetManager::GetStreamableManager();
	TArray<TSharedRef<FStreamableHandle>> activeHandles;
	if (streamableManager.GetActiveHandles(objPath, activeHandles))
	{
		for (int i = 0; i < activeHandles.Num(); ++i)
		{
			activeHandles[i].Get().WaitUntilComplete();
		}
	}
}
