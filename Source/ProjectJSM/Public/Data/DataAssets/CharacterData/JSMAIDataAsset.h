// Copyright JSM Studio, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Data/DataAssets/CharacterData/JSMCharacterDataAsset.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "JSMAIDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTJSM_API UJSMAIDataAsset : public UJSMCharacterDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "AI")
	TSoftObjectPtr<class UBlackboardData> BlackBoard;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "AI")
	TSoftObjectPtr<class UBehaviorTree> BehaviorTree;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Sense")
	FFloatRange StimulusStrength;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Sense|Sight")
	bool bHasSightSense;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (EditCondition = "bHasSightSense"), Category = "Sense|Sight")
	float SightMaxAge;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (EditCondition = "bHasSightSense"), Category = "Sense|Sight")
	float SightDistance;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (EditCondition = "bHasSightSense"), Category = "Sense|Sight")
	float LoseSightDistance;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Sense|Team")
	bool AlertTeam;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Sense|Hearing")
	bool bHasHearingSense;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (EditCondition = "bHasHearingSense"),Category = "Sense|Hearing")
	float HearingRange;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (EditCondition = "bHasHearingSense"),Category = "Sense|Hearing")
	float HearingMaxAge;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Sense|Damage")
	float DamageSenseMaxAge;

	virtual TArray<FSoftObjectPath> GetObjectPathsToLoad() override
	{
		TArray<FSoftObjectPath> Paths = Super::GetObjectPathsToLoad();

		if (BlackBoard.IsNull() == false)
		{
			if (BlackBoard.IsPending())
			{
				Paths.Add(BlackBoard.ToSoftObjectPath());
			}
		}

		if (BehaviorTree.IsNull() == false)
		{
			if (BehaviorTree.IsPending())
			{
				Paths.Add(BehaviorTree.ToSoftObjectPath());
			}
		}
		return Paths;
	}

	virtual void LoadSynchronous() override
	{
		if (BlackBoard.IsNull() == false)
		{
			BlackBoard.LoadSynchronous();
		}

		if (BehaviorTree.IsNull() == false)
		{
			BehaviorTree.LoadSynchronous();
		}
	}
};
